unit u_datamodule;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Dialogs, Controls, ZConnection, ZDataset, ZSqlProcessor;

  //Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, Buttons,
  //StdCtrls, ZConnection, RTTICtrls, LR_Class, ZConnection, ZDataset, ZSqlProcessor;


type

  { TDM }

  TDM = class(TDataModule)
    MyBatch: TZSQLProcessor;
    MyData: TZConnection;
    MyQuery: TZQuery;
    procedure DataModuleCreate(Sender: TObject);

    FUNCTION NamaBulan(tglbulan:TDate):String;
    FUNCTION NowX:TDateTime;
    FUNCTION CreateNoByYearYYPrefix(BranchIs : String; DocCode :String; PartCode :String): String;
    FUNCTION BaseQuery(vRequest : String):String;

  private

  public
     UserId , BranchID : String;

  end;

var
  DM: TDM;

implementation

{$R *.lfm}

{ TDM }

procedure TDM.DataModuleCreate(Sender: TObject);
begin
  BranchID:='001';
  MyData.Connected := True;
end;

function TDM.NowX:TDateTime;
var q1:TZQuery;
    cValue : String;
begin
   q1:=TZQuery.Create(Application);
   with q1 do begin
      Connection := DM.MyData;
      Sql.Add('select sysdate() as tanggal');
      Open;
      try
           result:=StrToDateTime(FieldByName('tanggal').AsString);
      except
        cValue := Copy(q1.FieldByName('tanggal').AsString,9,2) + '-'+   // DD
                  Copy(q1.FieldByName('tanggal').AsString,6,2) + '-'+   // MM
                  Copy(q1.FieldByName('tanggal').AsString,1,4) + '-'+   // YYYY
                  ' '+
                  Copy(q1.FieldByName('tanggal').AsString,12,2) + ':'+  // TT
                  Copy(q1.FieldByName('tanggal').AsString,15,2) + ':'+  // NN
                  Copy(q1.FieldByName('tanggal').AsString,18,2);        // SS
        ShowMessage(q1.FieldByName('tanggal').AsString);
        ShowMessage(cValue);
        result:=StrToDateTime(cValue);
      end;
   end;
   q1.free;
end;

function TDM.BaseQuery(vRequest : String):String;
var vQuery : String;
begin
  Case vRequest Of
      'qt_supplier@u_supplier' : Begin
         vQuery:='Select * from t_supplier';
      End;
      'qt_stock@u_stock' : Begin
         vQuery:=' select a.* , b.fc_stockdivision_name , c.fc_brand_name , SUM(d.fn_quantity) as QTY , e.fv_trxdescription as STCK_VEHICLE , f.fv_trxdescription as STCK_VEHICLE_PART from t_stock a '+
                 ' left outer join t_stockdivision b  on a.fc_stockdivision_code = b.fc_stockdivision_code '+
                 ' left outer join t_brand c on a.fc_brandcode  = c.fc_brand_code '+
                 ' left outer join t_invstore d on a.fc_barcode = d.fc_barcode '+
                 ' left outer join t_trxtype e on a.fc_stockspec1 = e.fc_trxcode and e.fc_trxid = '+QuotedStr('STCK_VEHICLE')+
                 ' left outer join t_trxtype f on a.fc_stockspec2 = f.fc_trxcode and f.fc_trxid = '+QuotedStr('STCK_VEHICLE_PART');
      end;


  end;
  Result :=  vQuery;
end;

function TDM.CreateNoByYearYYPrefix(BranchIs : String; DocCode :String; PartCode :String): String;
var
  q                                     : TZQuery;
  Result2Send, vDelimeter, yyIs         : String;

begin
  vDelimeter            := '.';
  yyIs                  := vDelimeter + FormatDateTime('yy',NowX) + vDelimeter;
  q                     := TZQuery.Create(Application);
  With q Do Begin
    Connection                            := DM.MyData;
    SQL.Clear;
    SQL.Text                            :=
        ' select concat(fv_prefix, ' + QuotedStr(yyIs) +  ', ' +
        ' repeat(''0'',fn_count3-( 4 + length(fv_prefix) + length(fn_docno + 1)) ), ' +
        ' fn_docno + 1) as NomorIs ' +
        ' from t_nomor where (fc_branch = '+ QuotedStr(BranchIs)+ ' and fv_document = ' + QuotedStr(DocCode) + ') ' +
        ' and (fv_part = ' + QuotedStr(PartCode)+  ');';
    Open;
  End;
  Result2Send                           := q.FieldByName('NomorIs').AsString;

  With q Do Begin
    SQL.Clear;
    SQL.Add('update t_nomor set fn_docno = fn_docno + 1 ');
    SQL.Add('where (fc_branch = :branchis and fv_document = :docis) and (fv_part = :partis)');
    ParamByName('branchis').Value       := BranchIs;
    ParamByName('docis').Value          := DocCode;
    ParamByName('partis').Value         := PartCode;
    ExecSql;
  End;
  Result                                := Result2Send;
  q.Free;
end;

function TDM.NamaBulan(tglbulan:TDate):String;
begin
     If FormatDateTime('mm',tglbulan) = '01' Then Begin
        Result := FormatDateTime('dd',tglbulan)+' Januari '+FormatDateTime('yyyy',tglbulan);
        Exit;
     End;
     If FormatDateTime('mm',tglbulan) = '02' Then Begin
        Result := FormatDateTime('dd',tglbulan)+' Februari '+FormatDateTime('yyyy',tglbulan);
        Exit;
     End;
     If FormatDateTime('mm',tglbulan) = '03' Then Begin
        Result := FormatDateTime('dd',tglbulan)+' Maret '+FormatDateTime('yyyy',tglbulan);
        Exit;
     End;
     If FormatDateTime('mm',tglbulan) = '04' Then Begin
        Result := FormatDateTime('dd',tglbulan)+' April '+FormatDateTime('yyyy',tglbulan);
        Exit;
     End;
     If FormatDateTime('mm',tglbulan) = '05' Then Begin
        Result := FormatDateTime('dd',tglbulan)+' Mei '+FormatDateTime('yyyy',tglbulan);
        Exit;
     End;
     If FormatDateTime('mm',tglbulan) = '06' Then Begin
        Result := FormatDateTime('dd',tglbulan)+' Juni '+FormatDateTime('yyyy',tglbulan);
        Exit;
     End;
     If FormatDateTime('mm',tglbulan) = '07' Then Begin
        Result := FormatDateTime('dd',tglbulan)+' Juli '+FormatDateTime('yyyy',tglbulan);
        Exit;
     End;
     If FormatDateTime('mm',tglbulan) = '08' Then Begin
        Result := FormatDateTime('dd',tglbulan)+' Agustus '+FormatDateTime('yyyy',tglbulan);
        Exit;
     End;
     If FormatDateTime('mm',tglbulan) = '09' Then Begin
        Result := FormatDateTime('dd',tglbulan)+' September '+FormatDateTime('yyyy',tglbulan);
        Exit;
     End;
     If FormatDateTime('mm',tglbulan) = '10' Then Begin
        Result := FormatDateTime('dd',tglbulan)+' Oktober '+FormatDateTime('yyyy',tglbulan);
        Exit;
     End;
     If FormatDateTime('mm',tglbulan) = '11' Then Begin
        Result := FormatDateTime('dd',tglbulan)+' November '+FormatDateTime('yyyy',tglbulan);
        Exit;
     End;
     If FormatDateTime('mm',tglbulan) = '12' Then Begin
        Result := FormatDateTime('dd',tglbulan)+' Desember '+FormatDateTime('yyyy',tglbulan);
        Exit;
     End;
end;


end.

