unit u_oldmember;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, StdCtrls,
  DBGrids, ComCtrls, Menus, LR_Class, LR_DBSet, ZConnection, ZDataset,
  ZSqlUpdate, db, LR_DSet;
const
  vq_qt_member =
    ' select ' +
    ' a.fn_memberothernumber1,a.fc_membercode,a.fc_membername1, ' +
    ' a.fc_divisioncode ,a.fc_memberposition ,h.fv_trxdescription as hfv_trxdescription, a.fc_membersex ,i.fv_trxdescription  as ifv_trxdescription,'+
    ' a.fd_membercontract1start ,a.fd_membercontract1finish , ' +
    ' a.fd_membercontract2start ,a.fd_membercontract2finish , '  +
    ' a.fc_memberlegalstatus,e.fv_trxdescription as efv_trxdescription ,a.fd_memberlegalstatusdate ,    ' +
    ' a.fv_membernote , ' +
    ' a.fc_memberktpno , ' +
    ' a.fc_memberbirthplace ,a.fd_memberbirthdate , ' +
    ' concat(coalesce ( YEAR(CURRENT_TIMESTAMP) - YEAR(fd_memberbirthdate ) - (RIGHT(date_format( CURRENT_TIMESTAMP,"%Y-%m-%d"), 5) < RIGHT(fd_memberbirthdate , 5)),0 )," tahun") age , '+
    ' date_sub(date_add(fd_memberbirthdate,interval 56 year),interval 1 day) pension_age, '+
    ' a.fc_memberaddress1 , ' +
    ' a.fc_membermarital_status ,d.fv_trxdescription as dfv_trxdescription, ' +
    ' a.fc_membergraduatedid,f.fv_trxdescription as ffv_trxdescription ,a.fc_membergraduatedspec , ' +
    ' a.fc_memberbloodtype , ' +
    ' a.fc_memberreligion ,c.fv_trxdescription as cfv_trxdescription, '+
    ' a.fc_memberphone1 , '+
    ' a.fc_branch ,b.fv_trxdescription as bfv_trxdescription, ' +
    ' a.fd_memberjoindate , ' +
    ' a.fl_memberblacklist,      a.fc_memberblacklistdescription, '+
    ' a.fd_memberblacklistdate, '+
    ' a.fc_memberattendancetype, a.fn_memberleavecount,a.fn_memberleaveadd,' +
    ' a.fn_memberleavecount + a.fn_memberleaveadd as TotLeave ,a.fd_expiredleaveadd'+
    ' from t_oldmember a '+
    ' left outer join t_trxtype b on a.fc_branch = b.fc_trxcode and b.fc_trxid = "CABANG" ' +
    ' left outer join t_trxtype c on a.fc_memberreligion = c.fc_trxcode and c.fc_trxid = "RELIGION" '+
    ' left outer join t_trxtype d on a.fc_membermarital_status = d.fc_trxcode and d.fc_trxid = "MARITAL STATUS" '+
    ' left outer join t_trxtype e on a.fc_memberlegalstatus = e.fc_trxcode and e.fc_trxid = "STAT.KARYAWAN" ' +
    ' left outer join t_trxtype f on a.fc_membergraduatedid = f.fc_trxcode and f.fc_trxid = "GRADUATE" '+
    ' left outer join t_division g on a.fc_divisioncode = g.fc_divisioncode '+
    ' left outer join t_trxtype h on a.fc_memberposition = h.fc_trxcode and h.fc_trxid = "EMP_POSITION" '+
    ' left outer join t_trxtype i on a.fc_membersex = i.fc_trxcode and i.fc_trxid = "GENDER" '
    ;

type

  { Tf_oldmember }

  Tf_oldmember = class(TForm)
    BtnRefresh: TButton;
    CheckBox1: TCheckBox;
    DBGridMember: TDBGrid;
    DBGridMember1: TDBGrid;
    ds_oldmember: TDataSource;
    EdtFixColumn: TEdit;
    EdtSearch: TEdit;
    labelSearch1: TLabel;
    labelSearch2: TLabel;
    Memo1: TMemo;
    labelSearch: TLabel;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    Notebook1: TNotebook;
    Mainpage: TPage;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    PopupMenu1: TPopupMenu;
    qt_division: TZReadOnlyQuery;
    qt_oldmember: TZQuery;
    qt_oldmemberage: TStringField;
    qt_oldmemberfc_branch: TStringField;
    qt_oldmemberfc_divisioncode: TStringField;
    qt_oldmemberfc_memberaddress1: TStringField;
    qt_oldmemberfc_memberattendancetype: TStringField;
    qt_oldmemberfc_memberbirthplace: TStringField;
    qt_oldmemberfc_memberblacklistdescription: TStringField;
    qt_oldmemberfc_memberbloodtype: TStringField;
    qt_oldmemberfc_membercode: TStringField;
    qt_oldmemberfc_membergraduatedid: TStringField;
    qt_oldmemberfc_membergraduatedspec: TStringField;
    qt_oldmemberfc_memberktpno: TStringField;
    qt_oldmemberfc_memberlegalstatus: TStringField;
    qt_oldmemberfc_membermarital_status: TStringField;
    qt_oldmemberfc_membername1: TStringField;
    qt_oldmemberfc_memberphone1: TStringField;
    qt_oldmemberfc_memberposition: TStringField;
    qt_oldmemberfc_memberreligion: TStringField;
    qt_oldmemberfc_membersex: TStringField;
    qt_oldmemberfd_expiredleaveadd: TDateField;
    qt_oldmemberfd_memberbirthdate: TDateField;
    qt_oldmemberfd_memberblacklistdate: TDateField;
    qt_oldmemberfd_membercontract1finish: TDateField;
    qt_oldmemberfd_membercontract1start: TDateField;
    qt_oldmemberfd_membercontract2finish: TDateField;
    qt_oldmemberfd_membercontract2start: TDateField;
    qt_oldmemberfd_memberjoindate: TDateField;
    qt_oldmemberfd_memberlegalstatusdate: TDateField;
    qt_oldmemberfl_memberblacklist: TStringField;
    qt_oldmemberfn_memberleaveadd: TLongintField;
    qt_oldmemberfn_memberleavecount: TLongintField;
    qt_oldmemberfn_memberothernumber1: TLongintField;
    qt_oldmemberfv_membernote: TStringField;
    qt_oldmemberpension_age: TDateField;
    qt_oldmemberTotLeave: TLargeintField;
    qt_trxcabang: TZReadOnlyQuery;
    qt_trxcabangbfv_trxdescription: TStringField;
    qt_trxcabangfc_trxid: TStringField;
    qt_trxcabangfn_trxnumber: TSmallintField;
    qt_trxcabangIDCABANG: TStringField;
    qt_trxgraduated: TZReadOnlyQuery;
    qt_trxgraduatedfc_trxid: TStringField;
    qt_trxgraduatedffv_trxdescription: TStringField;
    qt_trxgraduatedfn_trxnumber: TSmallintField;
    qt_trxgraduatedID_Graduated: TStringField;
    qt_trxmaritalstatus: TZReadOnlyQuery;
    qt_trxmembersex: TZReadOnlyQuery;
    qt_trxmembersexfc_trxid: TStringField;
    qt_trxmembersexfn_trxnumber: TSmallintField;
    qt_trxmembersexID_Gender: TStringField;
    qt_trxmembersexifv_trxdescription: TStringField;
    qt_trxposition: TZReadOnlyQuery;
    qt_trxpositionfc_trxid: TStringField;
    qt_trxpositionfn_trxnumber: TSmallintField;
    qt_trxpositionhfv_trxdescription: TStringField;
    qt_trxpositionID_EmpPosition: TStringField;
    qt_trxreligion: TZReadOnlyQuery;
    qt_trxstatkaryawan: TZReadOnlyQuery;
    qt_trxstatkaryawanefv_trxdescription: TStringField;
    qt_trxstatkaryawanfc_trxid: TStringField;
    qt_trxstatkaryawanfn_trxnumber: TSmallintField;
    qt_trxstatkaryawanID_StatKaryawan: TStringField;
    qut_oldmember: TZUpdateSQL;
    StringField1: TStringField;
    StringField2: TStringField;
    StringField3: TStringField;
    StringField4: TStringField;
    StringField5: TStringField;
    StringField6: TStringField;
    StringField7: TStringField;
    StringField8: TStringField;
    UpDownFIxColumn: TUpDown;
    procedure BtnRefreshClick(Sender: TObject);
    procedure CheckBox1Change(Sender: TObject);
    procedure DBGridMemberColExit(Sender: TObject);
    procedure DBGridMemberTitleClick(Column: TColumn);
    procedure EdtFixColumnChange(Sender: TObject);
    procedure EdtSearchKeyPress(Sender: TObject; var Key: char);
    procedure FormCreate(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure MenuItem2Click(Sender: TObject);
    procedure qt_oldmemberfc_memberblacklistdescriptionChange(Sender: TField);
    procedure qt_oldmemberfl_memberblacklistChange(Sender: TField);
    procedure qt_oldmemberNewRecord(DataSet: TDataSet);
    procedure updateLeaveMember(dateEnd:TDate;memberCode:string);
    function MaxNoSelipMember:LongInt;
  private
    field_search_member:String;
  public

  end;

var
  f_oldmember: Tf_oldmember;

implementation
uses u_datamodule;
{$R *.lfm}

{ Tf_oldmember }

procedure Tf_oldmember.FormCreate(Sender: TObject);
begin
  Memo1.Lines.Add(vq_qt_member);
  qt_trxcabang.Close; qt_trxcabang.Open;
  qt_trxgraduated.Close;qt_trxgraduated.Open;
  qt_trxreligion.Close; qt_trxreligion.Open;
  qt_trxmaritalstatus.Close;qt_trxmaritalstatus.Open;
  qt_trxstatkaryawan.Close;qt_trxstatkaryawan.Open;
  qt_division.Close;qt_division.Open;
  qt_trxposition.Close;qt_trxposition.Open;
  qt_trxmembersex.Close;qt_trxmembersex.Open;
  qt_oldmember.Close;qt_oldmember.Open;
  field_search_member:='fc_membername1';
  labelSearch.Caption:=' By EMPLOYEE NAME';
  DBGridMember.FixedCols:=1;
  //updateLeaveMember(Dm.NowX,'');
end;

procedure Tf_oldmember.MenuItem1Click(Sender: TObject);
var
  vNoSelip: Integer;
  vMembercode :string;
begin
  vMembercode:=qt_oldmemberfc_membercode.AsString;
  vNoSelip:=qt_oldmemberfn_memberothernumber1.AsInteger;
  if MessageDlg('Are you sure want to move to CURRENT EMPLOYEE WITH UPDATE NO SELIP ?', mtConfirmation, mbYesNo, 0) = mrYes then begin
     with DM.MyBatch do begin
       Script.Clear;
       Script.Text:=' update t_member  set fn_memberothernumber1 = fn_memberothernumber1 + 1 where fn_memberothernumber1 >= :noSelip ;'
                      +' insert into t_member select * from t_oldmember where fc_membercode = :member;  '
                      +' delete from t_oldmember  where fc_membercode = :member;'
                   ;
       ParamByName('member').AsString:=vMembercode;
       ParamByName('noSelip').AsInteger:=vNoSelip;
       Execute;
       BtnRefreshClick(Sender);
       ShowMessage('Employee sucessfully moved to current employee ');
    end;
  end;
end;

procedure Tf_oldmember.MenuItem2Click(Sender: TObject);
var
  vMembercode :string;
begin
  vMembercode:=qt_oldmemberfc_membercode.AsString;
  if MessageDlg('Are you sure want to move to CURRENT EMPLOYEE WITHOUT UPDATE NO SELIP ?', mtConfirmation, mbYesNo, 0) = mrYes then begin
     with DM.MyBatch do begin
       Script.Clear;
       Script.Text:=  ' insert into t_member select * from t_oldmember where fc_membercode = :member;  '
                      +' delete from t_oldmember  where fc_membercode = :member;'
                   ;
       ParamByName('member').AsString:=vMembercode;
       Execute;
       BtnRefreshClick(Sender);
       ShowMessage('Employee sucessfully moved to current employee ');
    end;
  end;
end;

procedure Tf_oldmember.qt_oldmemberfc_memberblacklistdescriptionChange(Sender: TField
  );
begin
  qt_oldmemberfd_memberblacklistdate.AsDateTime:=DM.NowX;
end;

procedure Tf_oldmember.qt_oldmemberfl_memberblacklistChange(Sender: TField);
begin
  qt_oldmember.ApplyUpdates;
end;

procedure Tf_oldmember.qt_oldmemberNewRecord(DataSet: TDataSet);
begin
  qt_oldmemberfc_branch.AsString:='001';
  qt_oldmemberfn_memberothernumber1.AsInteger:=MaxNoSelipMember+1;
end;

procedure Tf_oldmember.updateLeaveMember(dateEnd: TDate; memberCode: string);
begin
  qt_oldmember.Close;
  qt_oldmember.Open;
end;

function Tf_oldmember.MaxNoSelipMember: LongInt;
var
  q1:TZQuery;
begin
   q1:=TZQuery.Create(Application);
   with q1 do begin
      Connection := DM.MyData;
      Sql.Add('select max(fn_memberothernumber1 ) maxnoselip from t_member');
      Open;
      result:=q1.FieldByName('maxnoselip').AsInteger;
   end;
   q1.free;
end;


procedure Tf_oldmember.DBGridMemberTitleClick(Column: TColumn);
begin
  if Column.Field.FieldKind = fklookup then begin
     field_search_member:=Column.Field.FieldName;
     Insert('.',field_search_member,2);
     labelSearch.Caption := 'By '+Column.Title.Caption;
     Memo1.Lines.Clear;
     Memo1.Lines.Add(vq_qt_member);
     With qt_oldmember do Begin
          Close;
          SQL.Text:= vq_qt_member + ' order by ' + field_search_member;
          Open;
     End;

  end else if Column.Field.FieldKind = fkdata then begin
     field_search_member:=Column.FieldName;
     labelSearch.Caption := 'By '+Column.Title.Caption;
     Memo1.Lines.Clear;
     Memo1.Lines.Add(vq_qt_member);
     With qt_oldmember do Begin
          Close;
          SQL.Text:= vq_qt_member + ' order by ' + field_search_member;
          Open;
     End;
  end;
end;

procedure Tf_oldmember.DBGridMemberColExit(Sender: TObject);
begin
  qt_oldmember.ApplyUpdates;
end;

procedure Tf_oldmember.BtnRefreshClick(Sender: TObject);
begin
  qt_trxcabang.Close; qt_trxcabang.Open;
  qt_trxgraduated.Close;qt_trxgraduated.Open;
  qt_trxreligion.Close; qt_trxreligion.Open;
  qt_trxmaritalstatus.Close;qt_trxmaritalstatus.Open;
  qt_trxstatkaryawan.Close;qt_trxstatkaryawan.Open;
  qt_division.Close;qt_division.Open;
  qt_trxposition.Close;qt_trxposition.Open;
  qt_trxmembersex.Close;qt_trxmembersex.Open;
  qt_oldmember.Close;qt_oldmember.Open;
end;

procedure Tf_oldmember.CheckBox1Change(Sender: TObject);
begin
  If CheckBox1.Checked Then BEGIN
     dbgridmember1.Visible := True;
     CheckBox1.Caption     := '   ACTIVE / N-A'
  end  Else BEGIN
     dbgridmember1.Visible := False;
     CheckBox1.Caption     := '   SHOW GRID INFO';
  end;
end;

procedure Tf_oldmember.EdtFixColumnChange(Sender: TObject);
begin
  if StrToInt(EdtFixColumn.Text) > DBGridMember.Columns.Count + 1  then begin
     EdtFixColumn.Text := IntToStr(DBGridMember.Columns.Count + 1) ;
  end else if StrToInt(EdtFixColumn.Text) < 1 then begin
     EdtFixColumn.Text := '1';
  end;
  DBGridMember.FixedCols:= StrToInt(EdtFixColumn.Text);
  Memo1.Clear;
  Memo1.Text:= IntToStr(DBGridMember.FixedCols) ;
end;

procedure Tf_oldmember.EdtSearchKeyPress(Sender: TObject; var Key: char);
begin
  if Key = chr(13) then begin
     Memo1.Lines.Clear;
     Memo1.Lines.Add(vq_qt_member + ' where ' + field_search_member + ' like "%' + EdtSearch.Text +'%" order by ' + field_search_member );
     With qt_oldmember do Begin
          Close;
          SQL.Text:= vq_qt_member + ' where ' + field_search_member + ' like "%' + EdtSearch.Text +'%" order by ' + field_search_member ;
          Open;
     End;
  end;
end;

end.

