unit u_member;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, StdCtrls,
  DBGrids, ComCtrls, LR_Class, LR_DBSet, ZConnection, ZDataset, ZSqlUpdate, db, LR_DSet;
const
  vq_qt_member =
    ' select a.fc_branch,a.fc_membercode, a.fc_membername1, a.fc_memberposition,a.fc_memberktpno, '  +
    ' a.fc_memberlegalstatus, a.fd_memberjoindate, a.fc_memberbirthplace, '         +
    ' a.fd_memberbirthdate, a.fc_membermarital_status, a.fc_membergraduatedid, '    +
    ' a.fc_membergraduatedspec, a.fc_memberbloodtype, a.fc_memberreligion, '        +
    ' a.fc_membermarital_status,a.fc_memberlegalstatus,a.fc_membergraduatedid, '    +
    ' a.fc_divisioncode, a.fl_memberblacklist, a.fc_memberblacklistdescription, '   +
    ' a.fd_memberblacklistdate, a.fn_memberothernumber1, '                          +
    ' a.fc_memberattendancetype, a.fn_memberleavecount, a.fn_memberleaveadd,'       +
    ' a.fn_memberleavecount + a.fn_memberleaveadd as TotLeave, '                    +
    ' b.fv_trxdescription  , ' +
    ' c.fv_trxdescription , '  +
    ' d.fv_trxdescription , '  +
    ' e.fv_trxdescription , '  +
    ' f.fv_trxdescription , '  +
    ' g.fc_divisionname, '      +
    ' h.fv_trxdescription as hfv_trxdescription ' +
    ' from t_member a '        +
    ' left outer join t_trxtype b on a.fc_branch = b.fc_trxcode and b.fc_trxid = "CABANG" ' +
    ' left outer join t_trxtype c on a.fc_memberreligion = c.fc_trxcode and c.fc_trxid = "RELIGION" ' +
    ' left outer join t_trxtype d on a.fc_membermarital_status = d.fc_trxcode and d.fc_trxid = "MARITAL STATUS" ' +
    ' left outer join t_trxtype e on a.fc_memberlegalstatus = e.fc_trxcode and e.fc_trxid = "STAT.KARYAWAN" ' +
    ' left outer join t_trxtype f on a.fc_membergraduatedid = f.fc_trxcode and f.fc_trxid = "GRADUATE" ' +
    ' left outer join t_division g on a.fc_divisioncode = g.fc_divisioncode '+
    ' left outer join t_trxtype h on a.fc_memberposition = h.fc_trxcode and h.fc_trxid = "EMP_POSITION" ';

type

  { Tf_member }

  Tf_member = class(TForm)
    BtnReport: TButton;
    BtnRefresh: TButton;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    DBGridMember: TDBGrid;
    DBGridMember1: TDBGrid;
    DBGridMember2: TDBGrid;
    ds_member: TDataSource;
    EdtFixColumn: TEdit;
    EdtSearch: TEdit;
    DataSetMember: TfrDBDataSet;
    labelSearch1: TLabel;
    labelSearch2: TLabel;
    Memo1: TMemo;
    qt_memberfc_memberattendancetype: TStringField;
    qt_memberfc_memberblacklistdescription: TStringField;
    qt_memberfc_memberktpno: TStringField;
    qt_memberfd_memberblacklistdate: TDateField;
    qt_memberfl_memberblacklist: TStringField;
    qt_memberfn_memberleaveadd: TLongintField;
    qt_memberfn_memberleavecount: TLongintField;
    qt_memberfn_memberothernumber1: TLongintField;
    qt_memberTotLeave: TLargeintField;
    qt_trxposition: TZReadOnlyQuery;
    qt_trxpositionfc_trxid: TStringField;
    qt_trxpositionfn_trxnumber: TSmallintField;
    qt_trxpositionhfv_trxdescription: TStringField;
    qt_trxpositionID_EmpPosition: TStringField;
    ReportMember: TfrReport;
    labelSearch: TLabel;
    Notebook1: TNotebook;
    Mainpage: TPage;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    qt_memberfc_branch: TStringField;
    qt_memberfc_divisioncode: TStringField;
    qt_memberfc_memberbirthplace: TStringField;
    qt_memberfc_memberbloodtype: TStringField;
    qt_memberfc_membercode: TStringField;
    qt_memberfc_membergraduatedid: TStringField;
    qt_memberfc_membergraduatedid_1: TStringField;
    qt_memberfc_membergraduatedspec: TStringField;
    qt_memberfc_memberlegalstatus: TStringField;
    qt_memberfc_memberlegalstatus_1: TStringField;
    qt_memberfc_membermarital_status: TStringField;
    qt_memberfc_membermarital_status_1: TStringField;
    qt_memberfc_membername1: TStringField;
    qt_memberfc_memberposition: TStringField;
    qt_memberfc_memberreligion: TStringField;
    qt_memberfd_memberbirthdate: TDateField;
    qt_memberfd_memberjoindate: TDateField;
    qt_trxcabang: TZReadOnlyQuery;
    qt_trxcabangbfv_trxdescription: TStringField;
    qt_trxcabangfc_trxid: TStringField;
    qt_trxcabangfn_trxnumber: TSmallintField;
    qt_trxcabangIDCABANG: TStringField;
    qt_trxgraduated: TZReadOnlyQuery;
    qt_trxgraduatedfc_trxid: TStringField;
    qt_trxgraduatedffv_trxdescription: TStringField;
    qt_trxgraduatedfn_trxnumber: TSmallintField;
    qt_trxgraduatedID_Graduated: TStringField;
    qt_trxreligion: TZReadOnlyQuery;
    qt_trxmaritalstatus: TZReadOnlyQuery;
    qt_trxstatkaryawan: TZReadOnlyQuery;
    StringField1: TStringField;
    StringField2: TStringField;
    StringField3: TStringField;
    StringField4: TStringField;
    StringField5: TStringField;
    StringField6: TStringField;
    StringField7: TStringField;
    UpDownFIxColumn: TUpDown;
    qt_division: TZReadOnlyQuery;
    qt_member: TZQuery;
    qut_member: TZUpdateSQL;
    procedure BtnRefreshClick(Sender: TObject);
    procedure BtnReportClick(Sender: TObject);
    procedure CheckBox1Change(Sender: TObject);
    procedure CheckBox2Change(Sender: TObject);
    procedure DataSetMemberCheckEOF(Sender: TObject; var Eof: Boolean);
    procedure DBGridMemberColExit(Sender: TObject);
    procedure DBGridMemberTitleClick(Column: TColumn);
    procedure EdtFixColumnChange(Sender: TObject);
    procedure EdtSearchKeyPress(Sender: TObject; var Key: char);
    procedure FormCreate(Sender: TObject);
    procedure qt_memberfc_memberblacklistdescriptionChange(Sender: TField);
    procedure qt_memberfd_memberblacklistdateChange(Sender: TField);
    procedure qt_memberfl_memberblacklistChange(Sender: TField);
    procedure qt_memberNewRecord(DataSet: TDataSet);
    procedure updateLeaveMember(dateEnd:TDate;memberCode:string);
    function MaxNoSelipMember:LongInt;
  private
    field_search_member:String;
  public

  end;

var
  f_member: Tf_member;

implementation
uses u_datamodule;
{$R *.lfm}

{ Tf_member }

procedure Tf_member.FormCreate(Sender: TObject);
begin
  Memo1.Lines.Add(vq_qt_member);
  qt_trxcabang.Open;
  qt_trxgraduated.Open;
  qt_trxreligion.Open;
  qt_trxmaritalstatus.Open;
  qt_trxstatkaryawan.Open;
  qt_division.Open;
  qt_trxposition.Open;
  qt_member.Open;
  field_search_member:='fc_membername1';
  labelSearch.Caption:=' By EMPLOYEE NAME';
  DBGridMember.FixedCols:=1;
  updateLeaveMember(Dm.NowX,'');
end;

procedure Tf_member.qt_memberfc_memberblacklistdescriptionChange(Sender: TField
  );
begin
  qt_memberfd_memberblacklistdate.AsDateTime:=DM.NowX;
end;

procedure Tf_member.qt_memberfd_memberblacklistdateChange(Sender: TField);
begin
end;

procedure Tf_member.qt_memberfl_memberblacklistChange(Sender: TField);
var
  noSelip: String;
begin
  if qt_memberfc_memberblacklistdescription.AsString = '' then begin
     ShowMessage('Blacklist description must to be filled');
     qt_member.CancelUpdates;
     Exit;
  end;
  noSelip := qt_memberfn_memberothernumber1.AsString;
  qt_member.ApplyUpdates;
  if qt_memberfl_memberblacklist.AsString = 'T' then begin
    with DM.MyBatch do begin
       Script.Clear;
       Script.Text:=' insert into t_oldmember select * from t_member where fl_memberblacklist = "T"; '
                      +' update t_member  set fn_memberothernumber1 = fn_memberothernumber1 - 1 where fn_memberothernumber1 > '+ noSelip +' ;'
                      +' delete from t_member  where fl_memberblacklist = "T";'
                   ;
       Execute;
       //ParamByName('member').AsString:=qt_memberfc_membercode.AsString;
       //ParamByName('noselip').AsString:=qt_memberfn_memberothernumber1.AsString;
       //ShowMessage(IntToStr(qt_memberfn_memberothernumber1.AsInteger));
       ShowMessage('Employee sucessfully moved to old member');
       qt_member.Close;
       qt_member.Open;
    end;
  end;
end;

procedure Tf_member.qt_memberNewRecord(DataSet: TDataSet);
begin
  qt_memberfc_branch.AsString:='001';
  qt_memberfn_memberothernumber1.AsInteger:=MaxNoSelipMember+1;
end;

procedure Tf_member.updateLeaveMember(dateEnd: TDate; memberCode: string);
begin
  With dm.MyBatch do begin
      Script.Clear;
      Script.Text :=
      //update for check expired leave date
      ' update t_member set fn_memberleaveadd = 0 where fd_expiredleaveadd < now()  ;'
                  //update amount leave this periode
               + ' update '
               + ' t_member a '
               + ' left join '
               + ' ( '
               + ' select '
               + ' a.fc_membercode , '
               + ' a.fc_membername1, '
               + ' sum(b.amount_leave) amount_leave '
               + ' from t_member a '
               + ' left join '
               + ' ( '
               + ' select '
               + ' t1.fc_trxaction1 amount_leave , '
               + ' t1.fv_trxdescription fv_trxdescription1 , '
               + ' t2.fv_trxdescription fv_trxdescription2 '
               + ' from t_trxtype t1 '
               + ' left join '
               + ' ( '
               + ' select fv_trxdescription '
               + ' from t_trxtype where fc_trxid = "RESET_LEAVE_DATE" '
               + ' ) t2 on t1.fv_trxdescription > t2.fv_trxdescription or t1.fv_trxdescription < t2.fv_trxdescription '
               + ' where fc_trxid = "ADD_LEAVE_DATE" '
               + ' ) b on '
               + ' a.fd_memberjoindate <= if( concat(date_format( now(),"%Y" ),"-",b.fv_trxdescription1) > concat(date_format( now(),"%Y" ),"-",b.fv_trxdescription2) '
               + ' , date_sub(concat(date_format( now(),"%Y" ),"-",b.fv_trxdescription1), interval 1 year ) '
               + ' , concat(date_format( now(),"%Y" ),"-",b.fv_trxdescription1)) '
               + ' and '
               + ' if( concat(date_format( now(),"%Y" ),"-",b.fv_trxdescription1) > concat(date_format( now(),"%Y" ),"-",b.fv_trxdescription2) '
               + ' , date_sub(concat(date_format( now(),"%Y" ),"-",b.fv_trxdescription1), interval 1 year ) '
               + ' , concat(date_format( now(),"%Y" ),"-",b.fv_trxdescription1)) <= :now '
               + ' group by fc_membercode '
               + ' ) b on a.fc_membercode = b.fc_membercode '
               + ' set a.fn_memberleavecount = b.amount_leave '
               + ' where a.fc_membercode like :membercode ;'
               //synchronize amount leave with employee leave
               + ' update '
               + ' t_member a '
               + ' left join '
               + ' ( '
	       + ' select '
	       + ' fc_membercode, '
	       + ' count(fd_leavedate) countleave, '
	       + ' fd_leavedate '
	       + ' from t_leave '
	       + ' where '
	       + ' fd_leavedate >= ( '
	       + ' select date_sub( concat(date_format( now(),"%Y" ),"-",fv_trxdescription),interval 1 year ) '
	       + ' from t_trxtype where fc_trxid = "RESET_LEAVE_DATE" '
               + ' ) '
	       + ' and '
	       + ' fd_leavedate <= ( '
	       + ' select concat(date_format( now(),"%Y" ),"-",fv_trxdescription) '
	       + ' from t_trxtype where fc_trxid = "RESET_LEAVE_DATE" '
	       + ' ) '
	       + ' and fc_leavetype in ("L") '
	       + ' group by fc_membercode '
               + ' ) b on a.fc_membercode = b.fc_membercode '
               + ' set '
               + ' a.fn_memberleavecount =  if( b.countleave >= a.fn_memberleaveadd, ( ( a.fn_memberleavecount + a.fn_memberleaveadd  )- b.countleave) , a.fn_memberleavecount ), '
               + ' a.fn_memberleaveadd = if( b.countleave >= a.fn_memberleaveadd, 0 , a.fn_memberleaveadd - b.countleave ) '
               + ' where a.fc_membercode = b.fc_membercode ;' ;

      ParamByName('now').AsDateTime:=dateEnd;
      ParamByName('membercode').AsString:='%'+Membercode+'%';
      Execute;
  end;
  qt_member.Close;
  qt_member.Open;
end;

function Tf_member.MaxNoSelipMember: LongInt;
var
  q1:TZQuery;
begin
   q1:=TZQuery.Create(Application);
   with q1 do begin
      Connection := DM.MyData;
      Sql.Add('select max(fn_memberothernumber1 ) maxnoselip from t_member');
      Open;
      result:=q1.FieldByName('maxnoselip').AsInteger;
   end;
   q1.free;
end;


procedure Tf_member.DBGridMemberTitleClick(Column: TColumn);
begin
  if Column.Field.FieldKind = fklookup then begin
     field_search_member:=Column.Field.FieldName;
     Insert('.',field_search_member,2);
     labelSearch.Caption := 'By '+Column.Title.Caption;
     Memo1.Lines.Clear;
     Memo1.Lines.Add(vq_qt_member);
     With qt_member do Begin
          Close;
          SQL.Text:= vq_qt_member + ' order by ' + field_search_member;
          Open;
     End;

  end else if Column.Field.FieldKind = fkdata then begin
     field_search_member:=Column.FieldName;
     labelSearch.Caption := 'By '+Column.Title.Caption;
     Memo1.Lines.Clear;
     Memo1.Lines.Add(vq_qt_member);
     With qt_member do Begin
          Close;
          SQL.Text:= vq_qt_member + ' order by ' + field_search_member;
          Open;
     End;
  end;
end;

procedure Tf_member.DBGridMemberColExit(Sender: TObject);
begin
  qt_member.ApplyUpdates;
end;

procedure Tf_member.BtnReportClick(Sender: TObject);
begin
  ReportMember.LoadFromFile('ReportMember.lrf');
  ReportMember.ShowReport;
end;

procedure Tf_member.BtnRefreshClick(Sender: TObject);
begin
  qt_member.Close;
  qt_member.Open;
end;

procedure Tf_member.CheckBox1Change(Sender: TObject);
begin
  If CheckBox1.Checked Then BEGIN
     dbgridmember1.Visible := True;
     CheckBox1.Caption     := '   ACTIVE / N-A'
  end  Else BEGIN
     dbgridmember1.Visible := False;
     CheckBox1.Caption     := '   SHOW GRID INFO';
  end;
end;

procedure Tf_member.CheckBox2Change(Sender: TObject);
begin
  If CheckBox2.Checked Then BEGIN
     dbgridmember2.Visible := True;
     CheckBox2.Caption     := '   OVERTIME TYPE AND ANNUAL LEAVE'
  end  Else BEGIN
     dbgridmember2.Visible := False;
     CheckBox2.Caption     := '   SHOW GRID INFO';
  end;
end;

procedure Tf_member.DataSetMemberCheckEOF(Sender: TObject; var Eof: Boolean);
begin

end;

procedure Tf_member.EdtFixColumnChange(Sender: TObject);
begin
  if StrToInt(EdtFixColumn.Text) > DBGridMember.Columns.Count + 1  then begin
     EdtFixColumn.Text := IntToStr(DBGridMember.Columns.Count + 1) ;
  end else if StrToInt(EdtFixColumn.Text) < 1 then begin
     EdtFixColumn.Text := '1';
  end;
  DBGridMember.FixedCols:= StrToInt(EdtFixColumn.Text);
  Memo1.Clear;
  Memo1.Text:= IntToStr(DBGridMember.FixedCols) ;
end;

procedure Tf_member.EdtSearchKeyPress(Sender: TObject; var Key: char);
begin
  if Key = chr(13) then begin
     Memo1.Lines.Clear;
     Memo1.Lines.Add(vq_qt_member + ' where ' + field_search_member + ' like "%' + EdtSearch.Text +'%" order by ' + field_search_member );
     With qt_member do Begin
          Close;
          SQL.Text:= vq_qt_member + ' where ' + field_search_member + ' like "%' + EdtSearch.Text +'%" order by ' + field_search_member ;
          Open;
     End;
  end;
end;

end.

