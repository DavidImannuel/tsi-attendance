unit u_fastingday;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, db, Forms, Controls, Graphics, Dialogs, ExtCtrls, StdCtrls,
  DBGrids, DBCtrls, EditBtn, ZConnection, ZDataset, DBDateTimePicker, LCLType,
  DateTimePicker;

const
  vq_qt_fastingday =
       ' select fc_fastingdayno, min(fd_date) as StartDate, ' +
       '        max(fd_date) as EndDate, '                 +
       '        fv_description '                          +
       ' from t_fastingday ';

type

  { Tf_fastingday }

  Tf_fastingday = class(TForm)
    btn_add: TButton;
    btn_cancel: TButton;
    btn_delete: TButton;
    btn_save: TButton;
    btn_search: TButton;
    btn_reset_periode: TButton;
    cb_filter: TComboBox;
    ComboBox1: TComboBox;
    DateTimePicker1: TDateTimePicker;
    DateTimePicker2: TDateTimePicker;
    ds_fastingday: TDataSource;
    DateTimePickerFinish: TDateTimePicker;
    DateTimePickerStart: TDateTimePicker;
    DBGrid1: TDBGrid;
    Edit1: TEdit;
    EditSearch: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label8: TLabel;
    Notebook1: TNotebook;
    Page1: TPage;
    Page2: TPage;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    p_input: TPanel;
    p_menu: TPanel;
    qt_fastingdayEndDate: TDateField;
    qt_fastingdayfc_fastingdayno: TStringField;
    qt_fastingdayfv_description: TStringField;
    qt_fastingdayStartDate: TDateField;
    Splitter1: TSplitter;
    qt_fastingday: TZQuery;
    procedure btn_addClick(Sender: TObject);
    procedure btn_cancelClick(Sender: TObject);
    procedure btn_deleteClick(Sender: TObject);
    procedure btn_reset_periodeClick(Sender: TObject);
    procedure btn_saveClick(Sender: TObject);
    procedure btn_searchClick(Sender: TObject);
    procedure cb_filterChange(Sender: TObject);
    procedure DateTimePicker1Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure qt_holidayNewRecord(DataSet: TDataSet);
  private

  public

  end;

var
  f_fastingday: Tf_fastingday;

implementation
uses u_datamodule;

{$R *.lfm}

{ Tf_fastingday }

procedure Tf_fastingday.btn_addClick(Sender: TObject);
begin
  {
  qt_fastingday.Append;
  qt_holidayfc_branch.AsString := '001';
  }
  Panel2.Color                 := clWhite;
  Panel2.Enabled               := True;
  btn_cancel.Enabled           := True;
  btn_save.Enabled             := True;
  btn_delete.Enabled           := True;
  DateTimePicker1.SetFocus;
  btn_add.Enabled              := False;
end;

procedure Tf_fastingday.btn_cancelClick(Sender: TObject);
begin
  {
  qt_fastingday.Cancel;
  }
  Panel2.Color                 := clSilver;
  Panel2.Enabled               := False;
  btn_cancel.Enabled           := False;
  btn_save.Enabled             := False;
  btn_delete.Enabled           := True;
  DBGrid1.SetFocus;
  btn_add.Enabled              := True;
end;

procedure Tf_fastingday.btn_deleteClick(Sender: TObject);
var
  Reply, BoxStyle: Integer;
begin
  {
  qt_fastingday.Delete;
  }

  If qt_fastingdayEndDate.AsDateTime <= DM.NowX Then Begin
     //ShowMessage('This data has been using for attendance reference. BE DELETED');
     //Exit;
  end;

  BoxStyle := MB_ICONQUESTION + MB_YESNO;
  Reply    := Application.MessageBox('Are you sure ?', 'Confirmation', BoxStyle);
  If Reply = IDYES Then Begin
     With DM.MyBatch Do Begin
          Script.Text := 'delete from t_fastingday where fc_fastingdayno = :fc_fastingdayno;';
          ParamByName('fc_fastingdayno').AsString    := qt_fastingdayfc_fastingdayno.AsString;
          Execute;
          qt_fastingday.Close;
          qt_fastingday.Open;
     End;

     //masih butuh optimalisasi
     With DM.MyBatch do begin
         Script.Text := ' update t_attendance a '
                      + ' inner join t_fastingday b on a.fd_date = b.fd_date '
                      + ' left join t_dailytimerange c on  c.fc_timerangecode = a.fc_shift1 '
                      + ' left join t_dailytimerange d on  d.fc_timerangecode = a.fc_shift2 '
                      + ' left join t_dailytimerange e on  e.fc_timerangecode = a.fc_shift3 '
                      + ' set '
                      + ' a.fl_fastingshift = if ( a.fd_date = b.fd_date ,if( c.fl_fastingshift = 1 or d.fl_fastingshift = 1 or e.fl_fastingshift = 1,1,0 ),0) ;'
                      //update backup attendance juga
                      +' update t_attendance_backup a '
                      + ' inner join t_fastingday b on a.fd_date = b.fd_date '
                      + ' left join t_dailytimerange c on  c.fc_timerangecode = a.fc_shift1 '
                      + ' left join t_dailytimerange d on  d.fc_timerangecode = a.fc_shift2 '
                      + ' left join t_dailytimerange e on  e.fc_timerangecode = a.fc_shift3 '
                      + ' set '
                      + ' a.fl_fastingshift = if ( a.fd_date = b.fd_date ,if( c.fl_fastingshift = 1 or d.fl_fastingshift = 1 or e.fl_fastingshift = 1,1,0 ),0) ;';
         Execute;
     end;
  End;

end;

procedure Tf_fastingday.btn_reset_periodeClick(Sender: TObject);
begin
  with qt_fastingday do begin
    Close;
    SQL.Text := vq_qt_fastingday +
         ' where (date_format(fd_date,"%Y") = date_format(SYSDATE() ,"%Y") )' +
         ' group by fc_fastingdayno, fv_description '                 +
         ' order by fd_date ';
    Open;
  end;
end;

procedure Tf_fastingday.btn_saveClick(Sender: TObject);
Var
   vCount : Integer;
   vNoFastingday : String;
begin
  vNoFastingday := DM.CreateNoByYearYYPrefix('01','CREATE_FASTINGDAY','');
  For vCount := 0 To StrToInt(Label8.Caption) - 1 Do Begin
      With DM.MyBatch Do Begin
           Script.Clear;
           Script.Text := 'insert into t_fastingday values (:fc_branch, :fc_fastingdayno, :fd_date, :fv_description );';
           ParamByName('fc_branch').AsString       := '001';
           ParamByName('fc_fastingdayno').AsString    := vNoFastingday;
           ParamByName('fd_date').AsDate           := DateTimePicker1.Date + vCount;
           ParamByName('fv_description').AsString  := Edit1.Text;
           Execute;
      End;
  End;
  {butuh optimallisasi}
   With DM.MyBatch Do Begin
         Script.Clear;
         Script.Text := ' update t_attendance a '
                    + ' left join t_fastingday b on a.fd_date = b.fd_date '
                    + ' left join t_dailytimerange c on  c.fc_timerangecode = a.fc_shift1 '
                    + ' left join t_dailytimerange d on  d.fc_timerangecode = a.fc_shift2 '
                    + ' left join t_dailytimerange e on  e.fc_timerangecode = a.fc_shift3 '
                    + ' set '
                    + ' a.fl_fastingshift = if ( a.fd_date = b.fd_date ,if( c.fl_fastingshift = 1 or d.fl_fastingshift = 1 or e.fl_fastingshift = 1,1,0 ),0) ;'
                    //update backup attendance juga
                    +' update t_attendance_backup a '
                    + ' left join t_fastingday b on a.fd_date = b.fd_date '
                    + ' left join t_dailytimerange c on  c.fc_timerangecode = a.fc_shift1 '
                    + ' left join t_dailytimerange d on  d.fc_timerangecode = a.fc_shift2 '
                    + ' left join t_dailytimerange e on  e.fc_timerangecode = a.fc_shift3 '
                    + ' set '
                    + ' a.fl_fastingshift = if ( a.fd_date = b.fd_date ,if( c.fl_fastingshift = 1 or d.fl_fastingshift = 1 or e.fl_fastingshift = 1,1,0 ),0) ;';
         //ParamByName('fd_date').AsDate           := DateTimePicker1.Date + vCount;
         Execute;
    End;
  btn_cancelClick(Self);
  btn_reset_periodeClick(Self);
end;

procedure Tf_fastingday.btn_searchClick(Sender: TObject);
Var
    vqWhere : String;
begin

  vqWhere :=
      ' and   ( fd_date >= :date_start )'                  +
      ' and   ( fd_date <= :date_finish )'                 +
      ' group by fc_fastingdayno, fv_description ' +
      ' order by fd_date;';

  with qt_fastingday do begin
    Close;
    if cb_filter.ItemIndex = -1 then begin
       ShowMessage('Invalid Field');
       Exit;
    end else if cb_filter.ItemIndex = 0 then begin
       SQL.Text := vq_qt_fastingday + //' where fv_description like :search and date_format(fd_date,"%Y-%m-%d") >= date_format(:date_start,"%Y-%m-%d") and date_format(fd_date,"%Y-%m-%d") <= date_format(:date_finish,"%Y-%m-%d") order by fd_date';
       ' where ( fv_description like :search ) ' + vqWhere;
       ParamByName('search').AsString :='%'+EditSearch.Text+'%';
    end;
    ParamByName('date_start').AsDate:=DateTimePickerStart.Date;
    ParamByName('date_finish').AsDate:=DateTimePickerFinish.Date;
    Open;
  end;
end;

procedure Tf_fastingday.cb_filterChange(Sender: TObject);
begin
    Notebook1.PageIndex:=cb_filter.ItemIndex;
end;

procedure Tf_fastingday.DateTimePicker1Change(Sender: TObject);
begin
  Label8.Caption := IntToStr(
                       Trunc(DateTimePicker2.Date) -
                       Trunc(DateTimePicker1.Date) + 1
                            );
end;

procedure Tf_fastingday.FormCreate(Sender: TObject);
begin
  cb_filter.ItemIndex     := 0;
  NoteBook1.PageIndex     := 0;
  ComboBox1.ItemIndex     := 0;

  qt_fastingday.Open;

  Label8.Caption          := '1';
  DateTimePicker1.Date    := DM.NowX;
  DateTimePicker2.Date    := DM.NowX;
end;

procedure Tf_fastingday.qt_holidayNewRecord(DataSet: TDataSet);
begin
  qt_fastingday.FieldByName('fc_branch').AsString:='001';
end;

end.

