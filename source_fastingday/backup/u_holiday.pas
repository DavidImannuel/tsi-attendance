unit u_holiday;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, db, Forms, Controls, Graphics, Dialogs, ExtCtrls, StdCtrls,
  DBGrids, DBCtrls, EditBtn, ZConnection, ZDataset, DBDateTimePicker, LCLType,
  DateTimePicker;

const
  vq_qt_holiday =
       ' select fc_holidayno, min(fd_date) as StartDate, ' +
       '        max(fd_date) as EndDate, '                 +
       '        fv_description, '                          +
       '        case when fc_status = "P" '                +
       '             then "PRIORITY" '                     +
       '             else "REGULAR" '                      +
       '        end as status '                            +
       ' from t_holiday ';

type

  { Tf_holiday }

  Tf_holiday = class(TForm)
    btn_add: TButton;
    btn_cancel: TButton;
    btn_delete: TButton;
    btn_save: TButton;
    btn_search: TButton;
    btn_reset_periode: TButton;
    cb_filter: TComboBox;
    ComboBox1: TComboBox;
    DateTimePicker1: TDateTimePicker;
    DateTimePicker2: TDateTimePicker;
    ds_holiday: TDataSource;
    DateTimePickerFinish: TDateTimePicker;
    DateTimePickerStart: TDateTimePicker;
    DBGrid1: TDBGrid;
    Edit1: TEdit;
    EditSearch: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Notebook1: TNotebook;
    Page1: TPage;
    Page2: TPage;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    p_input: TPanel;
    p_menu: TPanel;
    qt_holidayEndDate: TDateField;
    qt_holidayfc_holidayno: TStringField;
    qt_holidayfv_description: TStringField;
    qt_holidayStartDate: TDateField;
    qt_holidaystatus: TStringField;
    RadioGroup1: TRadioGroup;
    Splitter1: TSplitter;
    qt_holiday: TZQuery;
    procedure btn_addClick(Sender: TObject);
    procedure btn_cancelClick(Sender: TObject);
    procedure btn_deleteClick(Sender: TObject);
    procedure btn_reset_periodeClick(Sender: TObject);
    procedure btn_saveClick(Sender: TObject);
    procedure btn_searchClick(Sender: TObject);
    procedure cb_filterChange(Sender: TObject);
    procedure DateTimePicker1Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure qt_holidayNewRecord(DataSet: TDataSet);
  private

  public

  end;

var
  f_holiday: Tf_holiday;

implementation
uses u_datamodule;

{$R *.lfm}

{ Tf_holiday }

procedure Tf_holiday.btn_addClick(Sender: TObject);
begin
  {
  qt_holiday.Append;
  qt_holidayfc_branch.AsString := '001';
  }
  Panel2.Color                 := clWhite;
  Panel2.Enabled               := True;
  btn_cancel.Enabled           := True;
  btn_save.Enabled             := True;
  btn_delete.Enabled           := True;
  DateTimePicker1.SetFocus;
  btn_add.Enabled              := False;
end;

procedure Tf_holiday.btn_cancelClick(Sender: TObject);
begin
  {
  qt_holiday.Cancel;
  }
  Panel2.Color                 := clSilver;
  Panel2.Enabled               := False;
  btn_cancel.Enabled           := False;
  btn_save.Enabled             := False;
  btn_delete.Enabled           := True;
  DBGrid1.SetFocus;
  btn_add.Enabled              := True;
end;

procedure Tf_holiday.btn_deleteClick(Sender: TObject);
var
  Reply, BoxStyle: Integer;
begin
  {
  qt_holiday.Delete;
  }

  If qt_holidayEndDate.AsDateTime <= DM.NowX Then Begin
     ShowMessage('This data has been using for attendance reference. CAN NOT BE DELETED');
     Exit;
  end;

  BoxStyle := MB_ICONQUESTION + MB_YESNO;
  Reply    := Application.MessageBox('Are you sure to import rotation shift ?', 'Confirmation', BoxStyle);
  If Reply = IDYES Then Begin
     With DM.MyBatch Do Begin
          Script.Text := 'delete from t_holiday where fc_holidayno = :fc_holidayno;';
          ParamByName('fc_holidayno').AsString    := qt_holidayfc_holidayno.AsString;
          Execute;
          qt_holiday.Close;
          qt_holiday.Open;
     End;
  End;

end;

procedure Tf_holiday.btn_reset_periodeClick(Sender: TObject);
begin
  with qt_holiday do begin
    Close;
    SQL.Text := vq_qt_holiday +
         ' where (date_format(fd_date,"%Y") = date_format(SYSDATE() ,"%Y") )' +
         ' group by fc_holidayno, fv_description, fc_status '                 +
         ' order by fd_date ';
    Open;
  end;
end;

procedure Tf_holiday.btn_saveClick(Sender: TObject);
Var
   vCount : Integer;
   vNoHoliday : String;
begin
  vNoHoliday := DM.CreateNoByYearYYPrefix('01','CREATE_HOLIDAY','');
  For vCount := 0 To StrToInt(Label8.Caption) - 1 Do Begin
      With DM.MyBatch Do Begin
           Script.Text := 'insert into t_holiday values (:fc_branch, :fc_holidayno, :fd_date, :fv_description, :fc_status );';
           ParamByName('fc_branch').AsString       := '001';
           ParamByName('fc_holidayno').AsString    := vNoHoliday;
           ParamByName('fd_date').AsDate           := DateTimePicker1.Date + vCount;
           ParamByName('fv_description').AsString  := Edit1.Text;
           If RadioGroup1.ItemIndex = 0 Then
              ParamByName('fc_status').AsString    := 'U'
           Else
              ParamByName('fc_status').AsString    := 'P';
           Execute;
      End;
  End;
  btn_cancelClick(Self);
  btn_reset_periodeClick(Self);
end;

procedure Tf_holiday.btn_searchClick(Sender: TObject);
Var
    vqWhere : String;
begin

  vqWhere :=
      ' and   ( fd_date >= :date_start )'                  +
      ' and   ( fd_date <= :date_finish )'                 +
      ' group by fc_holidayno, fv_description, fc_status ' +
      ' order by fd_date;';

  with qt_holiday do begin
    Close;
    if cb_filter.ItemIndex = -1 then begin
       ShowMessage('Invalid Field');
       Exit;
    end else if cb_filter.ItemIndex = 0 then begin
       SQL.Text := vq_qt_holiday + //' where fv_description like :search and date_format(fd_date,"%Y-%m-%d") >= date_format(:date_start,"%Y-%m-%d") and date_format(fd_date,"%Y-%m-%d") <= date_format(:date_finish,"%Y-%m-%d") order by fd_date';
       ' where ( fv_description like :search ) ' + vqWhere;
       ParamByName('search').AsString :='%'+EditSearch.Text+'%';
    end else if cb_filter.ItemIndex = 1 then begin
        SQL.Text := vq_qt_holiday + //' where fc_status like :search and date_format(fd_date,"%Y-%m-%d") >= date_format(:date_start,"%Y-%m-%d") and date_format(fd_date,"%Y-%m-%d") <= date_format(:date_finish,"%Y-%m-%d") order by fd_date' ;
        ' where ( fc_status like :search ) ' + vqWhere;
        If ComboBox1.ItemIndex = 0 Then
            ParamByName('search').AsString :='U'
        Else
            ParamByName('search').AsString :='P';
    end;
    ParamByName('date_start').AsDate:=DateTimePickerStart.Date;
    ParamByName('date_finish').AsDate:=DateTimePickerFinish.Date;
    Open;
  end;
end;

procedure Tf_holiday.cb_filterChange(Sender: TObject);
begin
    Notebook1.PageIndex:=cb_filter.ItemIndex;
end;

procedure Tf_holiday.DateTimePicker1Change(Sender: TObject);
begin
  Label8.Caption := IntToStr(
                       Trunc(DateTimePicker2.Date) -
                       Trunc(DateTimePicker1.Date) + 1
                            );
end;

procedure Tf_holiday.FormCreate(Sender: TObject);
begin
  cb_filter.ItemIndex     := 0;
  NoteBook1.PageIndex     := 0;
  ComboBox1.ItemIndex     := 0;

  qt_holiday.Open;

  Label8.Caption          := '1';
  DateTimePicker1.Date    := DM.NowX;
  DateTimePicker2.Date    := DM.NowX;
end;

procedure Tf_holiday.qt_holidayNewRecord(DataSet: TDataSet);
begin
  qt_holiday.FieldByName('fc_branch').AsString:='001';
end;

end.

