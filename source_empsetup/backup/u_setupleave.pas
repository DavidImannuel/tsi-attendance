unit u_setupleave;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, db, Forms, Controls, Graphics, Dialogs, ExtCtrls, DBGrids,
  ZDataset;

type

  { Tf_setupleave }

  Tf_setupleave = class(TForm)
    ds_trxresetdateleave: TDataSource;
    DBGrid2: TDBGrid;
    ds_trxadddateleave: TDataSource;
    DBGrid1: TDBGrid;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    qt_trxaddleavedate: TZQuery;
    qt_trxresetleavedate: TZQuery;
    qt_trxaddleavedatefc_trxcode: TStringField;
    qt_trxaddleavedatefc_trxcode1: TStringField;
    qt_trxaddleavedatefc_trxid: TStringField;
    qt_trxaddleavedatefc_trxid1: TStringField;
    qt_trxaddleavedatefv_trxdescription: TStringField;
    qt_trxaddleavedatefv_trxdescription1: TStringField;
    procedure DBGrid1ColExit(Sender: TObject);
  private

  public

  end;

var
  f_setupleave: Tf_setupleave;

implementation

uses u_datamodule;

{$R *.lfm}

{ Tf_setupleave }

procedure Tf_setupleave.DBGrid1ColExit(Sender: TObject);
begin
  qt_trxaddleavedate.ApplyUpdates;
end;

end.

