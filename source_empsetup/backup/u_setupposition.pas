unit u_setupposition;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, db, Forms, Controls, Graphics, Dialogs, ExtCtrls, DBGrids,
  ZDataset;

type

  { Tf_setupposition }

  Tf_setupposition = class(TForm)
    DBGrid4: TDBGrid;
    ds_cabang: TDataSource;
    DBGrid3: TDBGrid;
    ds_marital: TDataSource;
    DBGrid2: TDBGrid;
    ds_trxgender: TDataSource;
    DBGrid1: TDBGrid;
    ds_trxgraduate: TDataSource;
    Panel1: TPanel;
    Panel2: TPanel;
    qt_trxcabangfc_branch: TStringField;
    qt_trxcabangfc_trxcode: TStringField;
    qt_trxcabangfc_trxid: TStringField;
    qt_trxcabangfn_trxnumber: TSmallintField;
    qt_trxcabangfv_trxdescription: TStringField;
    qt_trxgender: TZQuery;
    qt_trxgraduate: TZQuery;
    qt_trxgenderfc_branch: TStringField;
    qt_trxgenderfc_trxcode: TStringField;
    qt_trxgenderfc_trxid: TStringField;
    qt_trxgenderfn_trxnumber: TSmallintField;
    qt_trxgenderfv_trxdescription: TStringField;
    qt_trxmarital: TZQuery;
    qt_trxgraduatefc_branch: TStringField;
    qt_trxgraduatefc_trxcode: TStringField;
    qt_trxgraduatefc_trxid: TStringField;
    qt_trxgraduatefn_trxnumber: TSmallintField;
    qt_trxgraduatefv_trxdescription: TStringField;
    qt_trxmaritalfc_branch: TStringField;
    qt_trxmaritalfc_trxcode: TStringField;
    qt_trxmaritalfc_trxid: TStringField;
    qt_trxmaritalfn_trxnumber: TSmallintField;
    qt_trxmaritalfv_trxdescription: TStringField;
    Splitter1: TSplitter;
    Splitter2: TSplitter;
    Splitter3: TSplitter;
    qt_trxcabang: TZQuery;
    Splitter4: TSplitter;
    procedure DBGrid1ColExit(Sender: TObject);
    procedure DBGrid2ColExit(Sender: TObject);
    procedure DBGrid3ColExit(Sender: TObject);
    procedure DBGrid4ColExit(Sender: TObject);
    procedure qt_trxcabangNewRecord(DataSet: TDataSet);
    procedure qt_trxgenderNewRecord(DataSet: TDataSet);
    procedure qt_trxgraduateNewRecord(DataSet: TDataSet);
    procedure qt_trxmaritalNewRecord(DataSet: TDataSet);
  private

  public

  end;

var
  f_setupposition: Tf_setupposition;

implementation

uses u_datamodule;

{$R *.lfm}

{ Tf_setupposition }

procedure Tf_setupposition.DBGrid1ColExit(Sender: TObject);
begin
  qt_trxgender.ApplyUpdates;
end;

procedure Tf_setupposition.DBGrid2ColExit(Sender: TObject);
begin
  qt_trxgraduate.ApplyUpdates;
end;

procedure Tf_setupposition.DBGrid3ColExit(Sender: TObject);
begin
  qt_trxgraduate.ApplyUpdates;
end;

procedure Tf_setupposition.DBGrid4ColExit(Sender: TObject);
begin
  qt_trxcabang.ApplyUpdates;
end;

procedure Tf_setupposition.qt_trxcabangNewRecord(DataSet: TDataSet);
begin
  qt_trxcabangfc_branch.AsString:='001';
  qt_trxcabangfc_trxid.AsString:='CABANG';
end;

procedure Tf_setupposition.qt_trxgenderNewRecord(DataSet: TDataSet);
begin
  qt_trxgenderfc_branch.AsString:='001';
  qt_trxgenderfc_trxid.AsString:='GENDER';
end;

procedure Tf_setupposition.qt_trxgraduateNewRecord(DataSet: TDataSet);
begin
  qt_trxgraduatefc_branch.AsString:='001';
  qt_trxgraduatefc_trxid.AsString:='GRADUATE';
end;

procedure Tf_setupposition.qt_trxmaritalNewRecord(DataSet: TDataSet);
begin
  qt_trxmaritalfc_branch.AsString:='001';
  qt_trxmaritalfc_trxid.AsString:='MARITAL STATUS';
end;

end.

