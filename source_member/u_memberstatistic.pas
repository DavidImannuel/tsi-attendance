unit u_memberstatistic;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ComCtrls, DBGrids,
  ExtCtrls, StdCtrls, TASources, TAGraph, TADbSource, TASeries, TALegendPanel,
  TAStyles, DateTimePicker, ZDataset, db;

type

  { Tf_memberstatistic }

  Tf_memberstatistic = class(TForm)
    BtnRefresh: TButton;
    Button2: TButton;
    Chart1: TChart;
    Chart2: TChart;
    Chart3: TChart;
    ChartKaryMasukBarSeries: TBarSeries;
    ChartPieSeriesCabang: TPieSeries;
    ChartMarital: TChart;
    ChartMaritalPieSeries: TPieSeries;
    ChartPieSeriesKaryStatus: TPieSeries;
    DateTimePicker1: TDateTimePicker;
    DateTimePicker2: TDateTimePicker;
    DBGrid1: TDBGrid;
    DBGrid2: TDBGrid;
    DBGrid3: TDBGrid;
    DBGrid4: TDBGrid;
    DBGrid5: TDBGrid;
    ds_totalot: TDataSource;
    ds_memberlegal: TDataSource;
    ds_memberjoindate: TDataSource;
    ds_membermarital: TDataSource;
    ds_memberbranch: TDataSource;
    Label1: TLabel;
    Label2: TLabel;
    LblEmpCount: TLabel;
    PageControl1: TPageControl;
    Panel1: TPanel;
    Panel2: TPanel;
    qt_memberbranch: TZReadOnlyQuery;
    qt_memberbranchcabang: TStringField;
    qt_memberbranchjumlah: TLargeintField;
    qt_memberbranchkode: TStringField;
    qt_memberjoindatejumlah: TLargeintField;
    qt_memberjoindateyear: TStringField;
    qt_memberlegalgender: TStringField;
    qt_memberlegaljumlah: TLargeintField;
    qt_memberlegalmemberlegalstatus: TStringField;
    qt_membermaritalfc_membermarital_status: TStringField;
    qt_membermaritaljumlah: TLargeintField;
    qt_membermaritalmembermaritalstatus: TStringField;
    qt_totalot: TZReadOnlyQuery;
    qt_memberlegal: TZReadOnlyQuery;
    qt_memberjoindate: TZReadOnlyQuery;
    qt_membermarital: TZReadOnlyQuery;
    qt_totalotbulan: TLargeintField;
    qt_totalotot1: TFloatField;
    qt_totalotot2: TFloatField;
    qt_totalotot3: TFloatField;
    qt_totalotot4: TFloatField;
    qt_totalotot6: TFloatField;
    qt_totalotot8: TFloatField;
    qt_totalottahun: TStringField;
    qt_totalottotal_ot: TFloatField;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    TabSheet5: TTabSheet;
    procedure BtnRefreshClick(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DrawChartMarital;
    procedure DrawChartCabang;
    procedure DrawChartKaryStatus;
    procedure DrawChartKaryMasuk;

  private

  public

  end;

var
  f_memberstatistic: Tf_memberstatistic;

implementation
uses u_datamodule,u_member;

{$R *.lfm}

{ Tf_memberstatistic }

procedure Tf_memberstatistic.FormCreate(Sender: TObject);
begin
  LblEmpCount.Caption:= IntToStr(f_member.qt_member.RecordCount);
end;

procedure Tf_memberstatistic.DrawChartMarital;
var
  i:integer;
begin
  ChartMaritalPieSeries.Clear;
  qt_membermarital.First;
  for i:=0 to qt_membermarital.RecordCount - 1 do begin
    ChartMaritalPieSeries.AddXY(i,qt_membermaritaljumlah.AsFloat,qt_membermaritalmembermaritalstatus.AsString,RGBToColor(random(255),random(255),random(255)));
    qt_membermarital.Next;
  end;
end;

procedure Tf_memberstatistic.DrawChartCabang;
var
  i:integer;
begin
  ChartPieSeriesCabang.Clear;
  qt_memberbranch.First;
  for i:=0 to qt_memberbranch.RecordCount - 1 do begin
    ChartPieSeriesCabang.AddXY(i,qt_memberbranchjumlah.AsFloat,qt_memberbranchcabang.AsString,RGBToColor(random(255),random(255),random(255)));
    qt_memberbranch.Next;
  end;
end;

procedure Tf_memberstatistic.DrawChartKaryStatus;
var
  i:integer;
begin
  ChartPieSeriesKaryStatus.Clear;
  qt_memberlegal.First;
  for i:=0 to qt_memberlegal.RecordCount - 1 do begin
    ChartPieSeriesKaryStatus.AddXY(i,qt_memberlegaljumlah.AsFloat,qt_memberlegalmemberlegalstatus.AsString +' ('+qt_memberlegalgender.AsString+') ',
    RGBToColor(random(255),random(255),random(255)));
    qt_memberlegal.Next;
  end;
end;

procedure Tf_memberstatistic.DrawChartKaryMasuk;
var
  i:integer;
begin
  ChartKaryMasukBarSeries.Clear;
  qt_memberjoindate.First;
  for i:=0 to qt_memberjoindate.RecordCount - 1 do begin
    ChartKaryMasukBarSeries.AddXY(i,qt_memberjoindatejumlah.AsFloat,qt_memberjoindateyear.AsString,RGBToColor(random(255),random(255),random(255)));
    qt_memberjoindate.Next;
  end;
end;

procedure Tf_memberstatistic.BtnRefreshClick(Sender: TObject);
begin
  qt_memberbranch.Close;qt_memberbranch.Open;
  qt_memberjoindate.Close;qt_memberjoindate.Open;
  qt_memberlegal.Close;qt_memberlegal.Open;
  qt_membermarital.Close;qt_membermarital.Open;
  LblEmpCount.Caption:= IntToStr(f_member.qt_member.RecordCount);
  DrawChartMarital;
  DrawChartCabang;
  DrawChartKaryStatus;
  DrawChartKaryMasuk;
end;

procedure Tf_memberstatistic.Button2Click(Sender: TObject);
begin
  with qt_totalot do begin
    Close;
    ParamByName('datestart').AsDate:=DateTimePicker1.Date;
    ParamByName('datefinish').AsDate:=DateTimePicker2.Date;
    Open;
  end;
end;

end.

