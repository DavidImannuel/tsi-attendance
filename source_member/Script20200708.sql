select 
a.fn_memberothernumber1,a.fc_membercode,a.fc_membername1,
a.fc_membersex ,i.fv_trxdescription  as ifv_trxdescription,
a.fc_divisioncode ,a.fc_memberposition ,h.fv_trxdescription as hfv_trxdescription,
a.fd_membercontract1start ,a.fd_membercontract1finish ,
a.fd_membercontract2start ,a.fd_membercontract2finish ,
a.fc_memberlegalstatus,e.fv_trxdescription as efv_trxdescription ,a.fd_memberlegalstatusdate ,
a.fv_membernote ,
a.fc_memberktpno ,
a.fc_memberbirthplace ,a.fd_memberbirthdate ,a.fn_age ,
a.fc_memberaddress1 ,
a.fc_membermarital_status ,d.fv_trxdescription as dfv_trxdescription,
a.fc_membergraduatedid,f.fv_trxdescription as ffv_trxdescription ,a.fc_membergraduatedspec ,
a.fc_memberbloodtype ,
a.fc_memberreligion ,c.fv_trxdescription as cfv_trxdescription,
a.fc_memberphone1 ,
a.fc_branch ,b.fv_trxdescription as bfv_trxdescription,
a.fd_memberjoindate ,
a.fl_memberblacklist,      a.fc_memberblacklistdescription, 
a.fd_memberblacklistdate, 
a.fc_memberattendancetype, a.fn_memberleavecount,a.fn_memberleaveadd, 
a.fn_memberleavecount + a.fn_memberleaveadd as TotLeave ,
a.fd_expiredleaveadd 
from t_member a
left outer join t_trxtype b on a.fc_branch = b.fc_trxcode and b.fc_trxid = 'CABANG'
left outer join t_trxtype c on a.fc_memberreligion = c.fc_trxcode and c.fc_trxid = 'RELIGION'
left outer join t_trxtype d on a.fc_membermarital_status = d.fc_trxcode and d.fc_trxid = 'MARITAL STATUS'
left outer join t_trxtype e on a.fc_memberlegalstatus = e.fc_trxcode and e.fc_trxid = 'STAT.KARYAWAN'
left outer join t_trxtype f on a.fc_membergraduatedid = f.fc_trxcode and f.fc_trxid = 'GRADUATE'
left outer join t_division g on a.fc_divisioncode = g.fc_divisioncode 
left outer join t_trxtype h on a.fc_memberposition = h.fc_trxcode and h.fc_trxid = 'EMP_POSITION'
left outer join t_trxtype i on a.fc_membersex = i.fc_trxcode and i.fc_trxid = 'GENDER'
order by a.fn_memberothernumber1 


select a.fc_branch,a.fc_membercode, a.fc_membername1,a.fc_memberktpno, 
a.fc_memberposition,       a.fc_memberlegalstatus, 
a.fd_memberjoindate,       a.fc_memberbirthplace, 
a.fd_memberbirthdate,      a.fc_membermarital_status, 
a.fc_membergraduatedid,    a.fc_membergraduatedspec, 
a.fc_memberbloodtype,      a.fc_memberreligion,
a.fc_membermarital_status, a.fc_memberlegalstatus,
a.fc_membergraduatedid,    a.fc_divisioncode, 
a.fl_memberblacklist,      a.fc_memberblacklistdescription, 
a.fd_memberblacklistdate,  a.fn_memberothernumber1,
a.fc_memberattendancetype, a.fn_memberleavecount,
a.fn_memberleaveadd, 
a.fn_memberleavecount + a.fn_memberleaveadd as TotLeave,
b.fv_trxdescription as bfv_trxdescription ,
c.fv_trxdescription as cfv_trxdescription,
d.fv_trxdescription as dfv_trxdescription,
e.fv_trxdescription as efv_trxdescription,
f.fv_trxdescription as ffv_trxdescription,
g.fc_divisionname as gfc_divisionname,
h.fv_trxdescription as hfv_trxdescription
from t_member a
left outer join t_trxtype b on a.fc_branch = b.fc_trxcode and b.fc_trxid = 'CABANG'
left outer join t_trxtype c on a.fc_memberreligion = c.fc_trxcode and c.fc_trxid = 'RELIGION'
left outer join t_trxtype d on a.fc_membermarital_status = d.fc_trxcode and d.fc_trxid = 'MARITAL STATUS'
left outer join t_trxtype e on a.fc_memberlegalstatus = e.fc_trxcode and e.fc_trxid = 'STAT.KARYAWAN'
left outer join t_trxtype f on a.fc_membergraduatedid = f.fc_trxcode and f.fc_trxid = 'GRADUATE'
left outer join t_division g on a.fc_divisioncode = g.fc_divisioncode 
left outer join t_trxtype h on a.fc_memberposition = h.fc_trxcode and h.fc_trxid = 'EMP_POSITION'


update t_member set fn_memberleavecount = 7


select fv_trxdescription from t_trxtype where fc_trxcode = "CHECK_ADD_LEAVE_DATE";
select fv_trxdescription from t_trxtype where fc_trxcode = "CHECK_ADD_LEAVE_DAY";

update t_member 
set 
fn_memberleavecount = 
if( sysdate() >= (select fv_trxdescription from t_trxtype where fc_trxcode = "CHECK_ADD_LEAVE_DATE") 
	and month ( sysdate() ) = month( (select fv_trxdescription from t_trxtype where fc_trxcode = "CHECK_ADD_LEAVE_DATE") ) , 
	fn_memberleavecount 
	, 
	case when day( sysdate() ) >= ( select fv_trxdescription from t_trxtype where fc_trxcode = "CHECK_ADD_LEAVE_DAY" ) 
		and (select fv_trxdescription from t_trxtype where fc_trxcode = "CHECK_ADD_LEAVE_DATE") < sysdate()
		then fn_memberleavecount = fn_memberleavecount + 1
		when fd_expiredleaveadd  < ( select date_add(concat(year(sysdate()),'-01-01') ,interval 1 year) )
		then fn_memberleavecount 
		else fn_memberleavecount end
),
fn_memberleaveadd =
case when fd_expiredleaveadd >= sysdate() then 
	fn_memberleavecount 
when fd_expiredleaveadd < sysdate() then 
	fn_memberleaveadd =  0
when fd_expiredleaveadd < ( select date_add(concat(year(sysdate()),'-01-01') ,interval 1 year) )
	then fn_memberleavecount 
end;

update t_member a
left join t_leave b on a.fc_membercode = b.fc_membercode
set a.fn_memberleaveadd 
= case when(b.fc_leavestatus != 'T') and (b.fd_leavedate <= sysdate() ) and (a.fn_memberleavecount > 0)
then a.fn_memberleaveadd - 1 else  a.fn_memberleaveadd end,
a.fn_memberleavecount 
= case when (b.fc_leavestatus != 'T') and (a.fn_memberleaveadd = 0) and (b.fd_leavedate  <= sysdate() )  and (a.fn_memberleaveadd > 0)
then a.fn_memberleavecount - 1 else a.fn_memberleavecount end;

update t_leave set fc_leavestatus = 'T' where fd_leavedate <= sysdate(); 

update t_trxtype 
set fv_trxdescription = date_format( sysdate(),'%Y-%m-%d' )
where fc_trxcode = "CHECK_ADD_LEAVE_DATE";


select date_add(concat(year(sysdate()),'-01-01') ,interval 1 year)

select sysdate() >= (select fv_trxdescription from t_trxtype where fc_trxcode = "CHECK_ADD_LEAVE_DATE") 
and month (sysdate() ) = month( (select fv_trxdescription from t_trxtype where fc_trxcode = "CHECK_ADD_LEAVE_DATE") )
#######################################################################################################################
-- menghitung leave (cuti)
-- 1. dapatkan cuti tahun kemarin
-- 2. dapatkan cuti tahun ini 
-- 3. dapatkan cuti yang dibuat tahun lalu
-- 4. dapatkan cuti yang dibuat tahun ini 

#hitung umur
SELECT YEAR(CURRENT_TIMESTAMP) - YEAR('2001-05-15') - (RIGHT(date_format( CURRENT_TIMESTAMP,'%Y-%m-%d'), 5) < RIGHT('2001-05-15', 5)) as age 
SELECT DATEDIFF(CURRENT_DATE, STR_TO_DATE('15-05-2001', '%d-%m-%Y'))/365 AS ageInYears 

SELECT YEAR(CURRENT_TIMESTAMP) - YEAR(null ) - (RIGHT(date_format( CURRENT_TIMESTAMP,'%Y-%m-%d'), 5) < RIGHT(null , 5)) as age 


#cabang
select a.fc_branch kode,b.fv_trxdescription cabang,count(*) jumlah from t_member a
inner join t_trxtype b 
on a.fc_branch = b.fc_trxcode and b.fc_trxid = 'CABANG'
group by b.fc_trxcode

#marital status
select a.fc_membermarital_status ,b.fv_trxdescription memebrmaritalstatus,count(*)  jumlah from t_member a
inner join t_trxtype b 
on a.fc_membermarital_status = b.fc_trxcode and b.fc_trxid = 'MARITAL STATUS'
group by b.fc_trxcode

#kontrak tetap
select 
b.fv_trxdescription memberlegalstatus ,c.fv_trxdescription gender,
count(*) jumlah from t_member a
INNER join t_trxtype b 
on a.fc_memberlegalstatus = b.fc_trxcode and b.fc_trxid = 'STAT.KARYAWAN'
inner join t_trxtype c 
on a.fc_membersex = c.fc_trxcode and c.fc_trxid = 'GENDER'
group by b.fc_trxcode,c.fc_trxcode 

#karyawan masuk
select count(*) jumlah,date_format(fd_memberjoindate,"%Y") year
from t_member 
where year(fd_memberjoindate) != "0000"
group by year(fd_memberjoindate)

select 
a.fc_membercode,fc_periode ,a.fd_date ,b.fc_shiftnamealias ,a.fv_description 
from t_attendance a
left join t_dailytimerange b on a.fc_shift1 = b.fc_timerangecode 
where a.fd_date > concat(left(a.fc_periode,4),"-",right(a.fc_periode,2),"-","15")
order by a.fc_periode ,a.fd_date,a.fc_membercode

select 
fc_periode ,a.fd_date ,
coalesce(sum( b.fc_shiftnamealias in ('1') ),0 )jumlahshift1 ,
coalesce(sum( c.fc_shiftnamealias in ('2') ),0)jumlahshift2,
coalesce(sum( d.fc_shiftnamealias in ('3') ),0) jumlahshift3 
from t_attendance a
left join t_dailytimerange b on a.fc_shift1 = b.fc_timerangecode 
left join t_dailytimerange c on a.fc_shift2 = c.fc_timerangecode
left join t_dailytimerange d on a.fc_shift3 = d.fc_timerangecode
where a.fd_date > concat(left(a.fc_periode,4),"-",right(a.fc_periode,2),"-","15")
and a.fd_date >= :datestart and a.fd_date <= :datefinish
group by a.fc_periode ,a.fd_date 

select 
date_format(fd_date,"%Y") tahun,
month(fd_date ) bulan,
sum( fn_ot1 ) ot1,
sum(fn_ot2 ) ot2,
sum( fn_ot3 ) ot3,
sum( fn_ot4 ) ot4,
sum( fn_ot5 ) ot6,
sum( fn_ot6 ) ot8
from t_attendance 
where fd_date > concat(left(fc_periode,4),"-",right(fc_periode,2),"-","15")
and fd_date >= :datestart and fd_date <= :datefinish
group by date_format(fd_date,"%Y")

 update t_attendance  a 
set 
 a.fn_totaltimeshift1 = TIME_TO_SEC( 
 if( a.fc_timeinshift1 > a.fc_timeoutshift1 , 
		     subtime(addtime(a.fc_timeoutshift1,"24:00"), a.fc_timeinshift1 ) , 
		     subtime(a.fc_timeoutshift1, a.fc_timeinshift1 ) 
	             ) )/60/60, 
                     a.fn_totaltimeshift2 = TIME_TO_SEC( 
	             if( a.fc_timeinshift2 > a.fc_timeoutshift2 , 
		     subtime(addtime(a.fc_timeoutshift2,"24:00"), a.fc_timeinshift2 ) , 
		     subtime(a.fc_timeoutshift2, a.fc_timeinshift2 ) 
	             ) )/60/60, 
                     a.fn_totaltimeshift3 = TIME_TO_SEC(
	             if( a.fc_timeinshift3 > a.fc_timeoutshift3 , 
		     subtime(addtime(a.fc_timeoutshift3,"24:00"), a.fc_timeinshift3 ) , 
		     subtime(a.fc_timeoutshift3, a.fc_timeinshift3 ) 
	             ) )/60/60, 
                     a.fn_totalhour = a.fn_totaltimeshift1 + coalesce(a.fn_totaltimeshift2,0 ) + coalesce(a.fn_totaltimeshift3,0), 
                     a.fn_effectivehour = a.fn_totaltimeshift1 + coalesce(a.fn_totaltimeshift2,0) + coalesce(a.fn_totaltimeshift3,0) - 
                     ( coalesce(a.fn_totalbreakshift1,0) + coalesce(a.fn_totalbreakshift2,0) + coalesce(a.fn_totalbreakshift3,0)  )  
                     where a.fc_periode = :periode and a.fd_date = :date and a.fc_membercode = :membercode;        

update t_attendance a
set fc_timeinshift3 = if(a.fc_timeinshift3 = "",null,a.fc_timeinshift3 )
where a.fc_periode = :periode and a.fd_date = :date and a.fc_membercode = :membercode; 


#leave member
select fv_trxdescription from t_trxtype where fc_trxcode = "CHECK_ADD_LEAVE_DATE"
(select concat(date_format(now(),"%Y-%m-" ),(select fv_trxdescription from t_trxtype where fc_trxcode = "CHECK_ADD_LEAVE_DAY") ) )
update t_trxtype set fv_trxdescription = "2020-05-24" where fc_trxcode = "CHECK_ADD_LEAVE_DATE"

update t_member set fn_memberleavecount = 6
update t_member set fn_memberleaveadd = 0
update t_leave set fc_leavestatus = "X" where year(fd_leavedate ) = year(sysdate() )



#update umur 
update t_member set fn_age =
coalesce ( YEAR(CURRENT_TIMESTAMP) - YEAR(fd_memberbirthdate ) - (RIGHT(date_format( CURRENT_TIMESTAMP,'%Y-%m-%d'), 5) < RIGHT(fd_memberbirthdate , 5)),0 )
#cek pergantian tahun

update t_member 
set fn_memberleaveadd = if(year(sysdate() ) > year( ( select fv_trxdescription from t_trxtype where fc_trxcode = "CHECK_ADD_LEAVE_DATE" ) ) ,
fn_memberleavecount,fn_memberleaveadd);

update t_member 
set fn_memberleaveadd = if(sysdate() > fd_expiredleaveadd,0,fn_memberleaveadd );

update t_member 
set fn_memberleavecount =
if( sysdate() >= (select fv_trxdescription from t_trxtype where fc_trxcode = "CHECK_ADD_LEAVE_DATE")   ,
fn_memberleavecount +  if(day(sysdate() ) > ( select fv_trxdescription from t_trxtype where fc_trxcode = "CHECK_ADD_LEAVE_DAY" ) 
, if( year( sysdate() ) = year( (select fv_trxdescription from t_trxtype where fc_trxcode = "CHECK_ADD_LEAVE_DATE") )
	, month( sysdate() ) - month( (select fv_trxdescription from t_trxtype where fc_trxcode = "CHECK_ADD_LEAVE_DATE") )
	, (month( sysdate() ) + 12) - month( (select fv_trxdescription from t_trxtype where fc_trxcode = "CHECK_ADD_LEAVE_DATE") ) )
, if( year( sysdate() ) = year( (select fv_trxdescription from t_trxtype where fc_trxcode = "CHECK_ADD_LEAVE_DATE") )
	, month( sysdate() ) - month( (select fv_trxdescription from t_trxtype where fc_trxcode = "CHECK_ADD_LEAVE_DATE") )
	, (month( sysdate() ) + 12) - month( (select fv_trxdescription from t_trxtype where fc_trxcode = "CHECK_ADD_LEAVE_DATE") ) - 1 )  )
,fn_memberleavecount  );


update t_member a
inner join 
( select fc_membercode,count(*) leavethisyear
from t_leave 
where fc_leavestatus != "T" and fc_leavetype = "AL" and year(fd_leavedate) = year(sysdate() )
group by fc_membercode ) b on a.fc_membercode = b.fc_membercode
set 
a.fn_memberleaveadd = if( b.leavethisyear >= a.fn_memberleaveadd,0,a.fn_memberleaveadd -  b.leavethisyear),
a.fn_memberleavecount = if( a.fd_expiredleaveadd > sysdate(), a.fn_memberleavecount + a.fn_memberleaveadd - b.leavethisyear, a.fn_memberleavecount- b.leavethisyear);

select a.fc_membermarital_status ,b.fv_trxdescription membermaritalstatus,count(*)  jumlah from t_member a
inner join t_trxtype b 
on a.fc_membermarital_status = b.fc_trxcode and b.fc_trxid = 'MARITAL STATUS'
group by b.fc_trxcode




#cek belum import attendance

select a.fc_membercode ,a.fc_membername1,
if(b.fc_periode is not null,"SUDAH IMPORT DATA","BELUM IMPORT DATA") cekimport,
:periode periode
from t_member a
left join
(select fc_membercode,fc_periode from t_attendance where fc_periode = :periode
group by fc_membercode,fc_periode ) b on a.fc_membercode = b.fc_membercode


select a.fc_membercode ,a.fc_membername1,
if(b.fc_periode is not null,"SUDAH IMPORT DATA","BELUM IMPORT DATA") cekimport,
'202001' periode
from t_member a
left join
(select fc_membercode,fc_periode from t_attendance where fc_periode = '202001'
group by fc_membercode,fc_periode ) b on a.fc_membercode = b.fc_membercode
where b.fc_periode is NULL


select 
a.fc_membercode ,a.fc_membername1 ,
b.fv_trxdescription , concat(a.fc_membername1," ( ",coalesce(b.fv_trxdescription,"")," )") memberandposition 
from t_member a
left join t_trxtype b on a.fc_memberposition = b.fc_trxcode and fc_trxid = "EMP_POSITION"
where a.fc_membername1 like :search




update t_member 
set fn_memberleavecount = 6,fn_memberleaveadd = 0

if(day(:date ) >= ( select fv_trxdescription from t_trxtype where fc_trxcode = "CHECK_ADD_LEAVE_DAY" ) 
,if(year(:now) = year((select fv_trxdescription from t_trxtype where fc_trxcode = "CHECK_ADD_LEAVE_DATE"))
	, month(:now) - (select fv_trxdescription from t_trxtype where fc_trxcode = "CHECK_ADD_LEAVE_DATE")
	, 12)	 
,)
 
if(:date  >= ( select fv_trxdescription from t_trxtype where fc_trxcode = "CHECK_ADD_LEAVE_DATE" ) 
,if( year(:now) = year((select fv_trxdescription from t_trxtype where fc_trxcode = "CHECK_ADD_LEAVE_DATE") )
	, month(:now) - (select fv_trxdescription from t_trxtype where fc_trxcode = "CHECK_ADD_LEAVE_DATE")
	, 12)	 
,)
  
 

-- penambahan cuti tahun ini
if( year(:now) = year((select fv_trxdescription from t_trxtype where fc_trxcode = "CHECK_ADD_LEAVE_DATE") )
, if(day(:date ) >= ( select fv_trxdescription from t_trxtype where fc_trxcode = "CHECK_ADD_LEAVE_DAY" )
	, month(:now) - (select fv_trxdescription from t_trxtype where fc_trxcode = "CHECK_ADD_LEAVE_DATE")
	, month(:now) - (select fv_trxdescription from t_trxtype where fc_trxcode = "CHECK_ADD_LEAVE_DATE") - 1) 
, 12)
 

select year(:now) = year( (select fv_trxdescription from t_trxtype where fc_trxcode = "CHECK_ADD_LEAVE_DATE") )
 
 

update 
t_trxtype set fv_trxdescription = "2020-01-23"
where fc_trxid = "CHECK_ADD_LEAVE_DATE"

update t_member 
set fn_memberleavecount = 1,fn_memberleaveadd = 12

update t_member 
set fn_memberleavecount = 
if( year(:date) = year( (select fv_trxdescription from t_trxtype where fc_trxcode = "CHECK_ADD_LEAVE_DATE") ) 
 , if(month(:date ) = month( ( select fv_trxdescription from t_trxtype where fc_trxcode = "CHECK_ADD_LEAVE_DATE" ) ) 
 	, if(day(:date ) >= ( select fv_trxdescription from t_trxtype where fc_trxcode = "CHECK_ADD_LEAVE_DAY" )  
	   , fn_memberleavecount   + 1
 	   , fn_memberleavecount )
 	, if(day(:date ) >= ( select fv_trxdescription from t_trxtype where fc_trxcode = "CHECK_ADD_LEAVE_DAY" ) 
 	   , fn_memberleavecount +  month(:date) - month( (select fv_trxdescription from t_trxtype where fc_trxcode = "CHECK_ADD_LEAVE_DATE") )
 	    + if ( day( (select fv_trxdescription from t_trxtype where fc_trxcode = "CHECK_ADD_LEAVE_DATE") ) < ( select fv_trxdescription from t_trxtype where fc_trxcode = "CHECK_ADD_LEAVE_DAY" ), 1, 0 )
       , fn_memberleavecount + month(:date) - month( (select fv_trxdescription from t_trxtype where fc_trxcode = "CHECK_ADD_LEAVE_DATE") )- 1)) 
, 12) ;


select date_add(concat(date_format(:date,"%Y-%m-"),( select fv_trxdescription from t_trxtype where fc_trxcode = "CHECK_ADD_LEAVE_DAY" ) ), interval 1 month)

select fn_memberleavecount 
from t_member

 update t_trxtype set fv_trxdescription = 
 if(date_format(:date,"%d") >= "23",
 date_add(concat(date_format(:date,"%Y-%m-"),"23" ), interval 1 month)
,concat(date_format(:date,"%Y-%m-"),"23" ) ) 
 where fc_trxcode = "CHECK_ADD_LEAVE_DATE" ;


update t_leave set fc_leavestatus = 'T'

update t_member a  
inner join 
( select fc_membercode,count(*) leavethisyear 
from t_leave 
where fc_leavestatus != "T" and fc_leavetype = "AL" 
group by fc_membercode ) b on a.fc_membercode = b.fc_membercode 
set 
a.fn_memberleaveadd = if( b.leavethisyear >= a.fn_memberleaveadd,0,a.fn_memberleaveadd -  b.leavethisyear), 
a.fn_memberleavecount = if( a.fd_expiredleaveadd > :date, a.fn_memberleavecount + a.fn_memberleaveadd - b.leavethisyear, a.fn_memberleavecount- b.leavethisyear)
where a.fc_membercode = b.fc_membercode; 

select a.fc_membercode,a.fc_membername1 ,fn_memberleavecount,fn_memberleaveadd,b.leavethisyear 
from t_member a  
inner join 
( select fc_membercode,count(*) leavethisyear 
from t_leave 
where fc_leavestatus != "T" and fc_leavetype = "AL" 
group by fc_membercode ) b on a.fc_membercode = b.fc_membercode


update t_member a  
inner join 
( select fc_membercode,count(*) leavethisyear 
from t_leave 
where fc_leavestatus != "T" and fc_leavetype = "AL" 
group by fc_membercode ) b on a.fc_membercode = b.fc_membercode 
set  
a.fn_memberleavecount = if( a.fd_expiredleaveadd > :date and b.leavethisyear >= a.fn_memberleaveadd, a.fn_memberleavecount + a.fn_memberleaveadd - b.leavethisyear, a.fn_memberleavecount- b.leavethisyear),
a.fn_memberleaveadd = if( a.fd_expiredleaveadd > :date and b.leavethisyear >= a.fn_memberleaveadd,0,a.fn_memberleaveadd -  b.leavethisyear)
where a.fc_membercode = b.fc_membercode; 


#hitung shift seluruh perhari
select 
fc_periode ,a.fd_date ,
coalesce(sum( b.fc_shiftnamealias in ('1') or c.fc_shiftnamealias in ('1') or d.fc_shiftnamealias in ('1') ),0 ) jumlahshift1 ,
coalesce(sum( b.fc_shiftnamealias in ('2') or c.fc_shiftnamealias in ('2') or d.fc_shiftnamealias in ('2') ) ,0) jumlahshift2,
coalesce(sum( b.fc_shiftnamealias in ('3') or c.fc_shiftnamealias in ('3') or d.fc_shiftnamealias in ('3') ) ,0) jumlahshift3 
from t_attendance a
left join t_dailytimerange b on a.fc_shift1 = b.fc_timerangecode 
left join t_dailytimerange c on a.fc_shift2 = c.fc_timerangecode
left join t_dailytimerange d on a.fc_shift3 = d.fc_timerangecode
where a.fd_date > concat(left(a.fc_periode,4),"-",right(a.fc_periode,2),"-","15")
and a.fd_date >= :datestart and a.fd_date <= :datefinish
group by a.fc_periode ,a.fd_date 


update 
t_trxtype set fv_trxdescription = "2020-06-25"
where fc_trxid = "CHECK_ADD_LEAVE_DATE"

select fv_trxdescription from t_trxtype where fc_trxcode = "CHECK_ADD_LEAVE_DATE"

update t_member 
set fn_memberleavecount = 6,fn_memberleaveadd = 0

select fc_membercode,fn_memberleavecount ,fn_memberleaveadd 
from t_member where fc_membercode = 'TSI 30007'

update t_member set fn_memberleavecount = 
if( day(:date ) >= ( select fv_trxdescription from t_trxtype where fc_trxcode = "CHECK_ADD_LEAVE_DAY" )
, if(month(:date ) = month( ( select fv_trxdescription from t_trxtype where fc_trxcode = "CHECK_ADD_LEAVE_DATE" ) ) 
	, fn_memberleavecount 
	, fn_memberleavecount + 1)
, fn_memberleavecount) 


update t_member a 
inner join 
( select fc_membercode,count(*) leavethisyear 
from t_leave 
where fc_leavestatus = "T" and fc_leavetype = "AL" 
group by fc_membercode ) b on a.fc_membercode = b.fc_membercode 
set 
a.fn_memberleaveadd = 
	if( a.fd_expiredleaveadd > :date 
	, if( b.leavethisyear >= a.fn_memberleaveadd, 0,  a.fn_memberleaveadd - b.leavethisyear )
	,a.fn_memberleaveadd -  b.leavethisyear),
a.fn_memberleavecount = 
	if( a.fd_expiredleaveadd > :date
	, if( b.leavethisyear >= a.fn_memberleaveadd, a.fn_memberleavecount + a.fn_memberleaveadd - b.leavethisyear , a.fn_memberleavecount)
	, a.fn_memberleavecount - b.leavethisyear ) 
where a.fc_membercode = b.fc_membercode; 



select 
'2001-05-15' birthdate,
date_sub(date_add('2001-05-15',interval 56 year),interval 1 day) pensionday

coalesce ( YEAR(CURRENT_TIMESTAMP) - YEAR(fd_memberbirthdate ) - (RIGHT(date_format( CURRENT_TIMESTAMP,"%Y-%m-%d"), 5) < RIGHT(fd_memberbirthdate , 5)),0 )

select 
a.fn_memberothernumber1,a.fc_membercode,a.fc_membername1,
a.fc_membersex ,i.fv_trxdescription  as ifv_trxdescription,
a.fc_divisioncode ,a.fc_memberposition ,h.fv_trxdescription as hfv_trxdescription,
a.fd_membercontract1start ,a.fd_membercontract1finish ,
a.fd_membercontract2start ,a.fd_membercontract2finish ,
a.fc_memberlegalstatus,e.fv_trxdescription as efv_trxdescription ,a.fd_memberlegalstatusdate ,
a.fv_membernote ,
a.fc_memberktpno ,
a.fc_memberbirthplace ,a.fd_memberbirthdate , 
concat(coalesce ( YEAR(CURRENT_TIMESTAMP) - YEAR(fd_memberbirthdate ) - (RIGHT(date_format( CURRENT_TIMESTAMP,"%Y-%m-%d"), 5) < RIGHT(fd_memberbirthdate , 5)),0 )," tahun") age ,
date_sub(date_add(fd_memberbirthdate,interval 56 year),interval 1 day) pension_age,
a.fc_memberaddress1 ,
a.fc_membermarital_status ,d.fv_trxdescription as dfv_trxdescription,
a.fc_membergraduatedid,f.fv_trxdescription as ffv_trxdescription ,a.fc_membergraduatedspec ,
a.fc_memberbloodtype ,
a.fc_memberreligion ,c.fv_trxdescription as cfv_trxdescription,
a.fc_memberphone1 ,
a.fc_branch ,b.fv_trxdescription as bfv_trxdescription,
a.fd_memberjoindate ,
a.fl_memberblacklist,      a.fc_memberblacklistdescription, 
a.fd_memberblacklistdate, 
a.fc_memberattendancetype, a.fn_memberleavecount,a.fn_memberleaveadd, 
a.fn_memberleavecount + a.fn_memberleaveadd as TotLeave ,
a.fd_expiredleaveadd 
from t_member a
left outer join t_trxtype b on a.fc_branch = b.fc_trxcode and b.fc_trxid = 'CABANG'
left outer join t_trxtype c on a.fc_memberreligion = c.fc_trxcode and c.fc_trxid = 'RELIGION'
left outer join t_trxtype d on a.fc_membermarital_status = d.fc_trxcode and d.fc_trxid = 'MARITAL STATUS'
left outer join t_trxtype e on a.fc_memberlegalstatus = e.fc_trxcode and e.fc_trxid = 'STAT.KARYAWAN'
left outer join t_trxtype f on a.fc_membergraduatedid = f.fc_trxcode and f.fc_trxid = 'GRADUATE'
left outer join t_division g on a.fc_divisioncode = g.fc_divisioncode 
left outer join t_trxtype h on a.fc_memberposition = h.fc_trxcode and h.fc_trxid = 'EMP_POSITION'
left outer join t_trxtype i on a.fc_membersex = i.fc_trxcode and i.fc_trxid = 'GENDER'
order by a.fn_memberothernumber1


