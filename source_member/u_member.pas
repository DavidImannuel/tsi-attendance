unit u_member;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, StdCtrls,
  DBGrids, ComCtrls, Menus, LR_Class, LR_DBSet, ZConnection, ZDataset,
  ZSqlUpdate, db, LR_DSet, DateTimePicker, fpstypes, fpspreadsheet,
  fpsallformats, laz_fpspreadsheet;
const
  OUTPUT_FORMAT = sfExcel5;
  vq_qt_member =
    ' select ' +
    ' a.fn_memberothernumber1,a.fc_membercode,a.fc_membername1, ' +
    ' a.fc_divisioncode ,a.fc_memberposition ,h.fv_trxdescription as hfv_trxdescription, a.fc_membersex ,i.fv_trxdescription  as ifv_trxdescription,'+
    ' a.fd_membercontract1start ,a.fd_membercontract1finish , ' +
    ' a.fd_membercontract2start ,a.fd_membercontract2finish , '  +
    ' a.fc_memberlegalstatus,e.fv_trxdescription as efv_trxdescription ,a.fd_memberlegalstatusdate ,    ' +
    ' a.fv_membernote , ' +
    ' a.fc_memberktpno , ' +
    ' a.fc_memberbirthplace ,a.fd_memberbirthdate , ' +
    ' concat(coalesce ( YEAR(CURRENT_TIMESTAMP) - YEAR(fd_memberbirthdate ) - (RIGHT(date_format( CURRENT_TIMESTAMP,"%Y-%m-%d"), 5) < RIGHT(fd_memberbirthdate , 5)),0 )," tahun") age , '+
    ' date_sub(date_add(fd_memberbirthdate,interval 56 year),interval 1 day) pension_age, '+
    ' a.fc_memberaddress1 , ' +
    ' a.fc_membermarital_status ,d.fv_trxdescription as dfv_trxdescription, ' +
    ' a.fc_membergraduatedid,f.fv_trxdescription as ffv_trxdescription ,a.fc_membergraduatedspec , ' +
    ' a.fc_memberbloodtype , ' +
    ' a.fc_memberreligion ,c.fv_trxdescription as cfv_trxdescription, '+
    ' a.fc_memberphone1 , '+
    ' a.fc_branch ,b.fv_trxdescription as bfv_trxdescription, ' +
    ' a.fd_memberjoindate , ' +
    ' a.fl_memberblacklist,      a.fc_memberblacklistdescription, '+
    ' a.fd_memberblacklistdate, '+
    ' a.fc_memberattendancetype, a.fn_memberleavecount,a.fn_memberleaveadd,' +
    ' a.fn_memberleavecount + a.fn_memberleaveadd as TotLeave ,a.fd_expiredleaveadd'+
    ' from t_member a '+
    ' left outer join t_trxtype b on a.fc_branch = b.fc_trxcode and b.fc_trxid = "CABANG" ' +
    ' left outer join t_trxtype c on a.fc_memberreligion = c.fc_trxcode and c.fc_trxid = "RELIGION" '+
    ' left outer join t_trxtype d on a.fc_membermarital_status = d.fc_trxcode and d.fc_trxid = "MARITAL STATUS" '+
    ' left outer join t_trxtype e on a.fc_memberlegalstatus = e.fc_trxcode and e.fc_trxid = "STAT.KARYAWAN" ' +
    ' left outer join t_trxtype f on a.fc_membergraduatedid = f.fc_trxcode and f.fc_trxid = "GRADUATE" '+
    ' left outer join t_division g on a.fc_divisioncode = g.fc_divisioncode '+
    ' left outer join t_trxtype h on a.fc_memberposition = h.fc_trxcode and h.fc_trxid = "EMP_POSITION" '+
    ' left outer join t_trxtype i on a.fc_membersex = i.fc_trxcode and i.fc_trxid = "GENDER" '
    ;

type

  { Tf_member }

  Tf_member = class(TForm)
    BtnReport: TButton;
    BtnRefresh: TButton;
    BtnExcel: TButton;
    Button1: TButton;
    Button2: TButton;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    DateTimePicker1: TDateTimePicker;
    DBGridMember: TDBGrid;
    DBGridMember1: TDBGrid;
    DBGridMember2: TDBGrid;
    ds_member: TDataSource;
    EdtFixColumn: TEdit;
    EdtSearch: TEdit;
    DataSetMember: TfrDBDataSet;
    Label1: TLabel;
    labelSearch1: TLabel;
    labelSearch2: TLabel;
    Memo1: TMemo;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    PopupMenu1: TPopupMenu;
    qt_memberage: TStringField;
    qt_memberfc_memberaddress1: TStringField;
    qt_memberfc_memberattendancetype: TStringField;
    qt_memberfc_memberblacklistdescription: TStringField;
    qt_memberfc_memberktpno: TStringField;
    qt_memberfc_memberphone1: TStringField;
    qt_memberfc_membersex: TStringField;
    qt_memberfd_expiredleaveadd: TDateField;
    qt_memberfd_memberblacklistdate: TDateField;
    qt_memberfd_membercontract1finish: TDateField;
    qt_memberfd_membercontract1start: TDateField;
    qt_memberfd_membercontract2finish: TDateField;
    qt_memberfd_membercontract2start: TDateField;
    qt_memberfd_memberlegalstatusdate: TDateField;
    qt_memberfl_memberblacklist: TStringField;
    qt_memberfn_memberleaveadd: TLongintField;
    qt_memberfn_memberleavecount: TLongintField;
    qt_memberfn_memberothernumber1: TLongintField;
    qt_memberfv_membernote: TStringField;
    qt_memberpension_age: TDateField;
    qt_memberTotLeave: TLargeintField;
    qt_trxmembersexfc_trxid: TStringField;
    qt_trxmembersexfn_trxnumber: TSmallintField;
    qt_trxmembersexID_Gender: TStringField;
    qt_trxmembersexifv_trxdescription: TStringField;
    qt_trxposition: TZReadOnlyQuery;
    qt_trxpositionfc_trxid: TStringField;
    qt_trxpositionfn_trxnumber: TSmallintField;
    qt_trxpositionhfv_trxdescription: TStringField;
    qt_trxpositionID_EmpPosition: TStringField;
    qt_trxstatkaryawanefv_trxdescription: TStringField;
    qt_trxstatkaryawanfc_trxid: TStringField;
    qt_trxstatkaryawanfn_trxnumber: TSmallintField;
    qt_trxstatkaryawanID_StatKaryawan: TStringField;
    ReportMember: TfrReport;
    labelSearch: TLabel;
    Notebook1: TNotebook;
    Mainpage: TPage;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    qt_memberfc_branch: TStringField;
    qt_memberfc_divisioncode: TStringField;
    qt_memberfc_memberbirthplace: TStringField;
    qt_memberfc_memberbloodtype: TStringField;
    qt_memberfc_membercode: TStringField;
    qt_memberfc_membergraduatedid: TStringField;
    qt_memberfc_membergraduatedspec: TStringField;
    qt_memberfc_memberlegalstatus: TStringField;
    qt_memberfc_membermarital_status: TStringField;
    qt_memberfc_membername1: TStringField;
    qt_memberfc_memberposition: TStringField;
    qt_memberfc_memberreligion: TStringField;
    qt_memberfd_memberbirthdate: TDateField;
    qt_memberfd_memberjoindate: TDateField;
    qt_trxcabang: TZReadOnlyQuery;
    qt_trxcabangbfv_trxdescription: TStringField;
    qt_trxcabangfc_trxid: TStringField;
    qt_trxcabangfn_trxnumber: TSmallintField;
    qt_trxcabangIDCABANG: TStringField;
    qt_trxgraduated: TZReadOnlyQuery;
    qt_trxgraduatedfc_trxid: TStringField;
    qt_trxgraduatedffv_trxdescription: TStringField;
    qt_trxgraduatedfn_trxnumber: TSmallintField;
    qt_trxgraduatedID_Graduated: TStringField;
    qt_trxreligion: TZReadOnlyQuery;
    qt_trxmaritalstatus: TZReadOnlyQuery;
    qt_trxstatkaryawan: TZReadOnlyQuery;
    SaveDialog: TSaveDialog;
    StringField1: TStringField;
    StringField2: TStringField;
    StringField3: TStringField;
    StringField4: TStringField;
    StringField5: TStringField;
    StringField6: TStringField;
    StringField7: TStringField;
    StringField8: TStringField;
    UpDownFIxColumn: TUpDown;
    qt_division: TZReadOnlyQuery;
    qt_member: TZQuery;
    qt_trxmembersex: TZReadOnlyQuery;
    qut_member: TZUpdateSQL;
    procedure BtnExcelClick(Sender: TObject);
    procedure BtnRefreshClick(Sender: TObject);
    procedure BtnReportClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure CheckBox1Change(Sender: TObject);
    procedure CheckBox2Change(Sender: TObject);
    procedure DBGridMemberColExit(Sender: TObject);
    procedure DBGridMemberTitleClick(Column: TColumn);
    procedure EdtFixColumnChange(Sender: TObject);
    procedure EdtSearchKeyPress(Sender: TObject; var Key: char);
    procedure FormCreate(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure MenuItem2Click(Sender: TObject);
    procedure qt_memberfl_memberblacklistChange(Sender: TField);
    procedure qt_memberNewRecord(DataSet: TDataSet);
    procedure updateLeaveMember(date:TDate);
    function MaxNoSelipMember:LongInt;
  private
    field_search_member:String;
  public

  end;

var
  f_member: Tf_member;

implementation
uses u_datamodule,u_memberstatistic;
{$R *.lfm}

{ Tf_member }

procedure Tf_member.MenuItem1Click(Sender: TObject);
var
  vNoSelip: Integer;
  vMembercode :string;
begin
  vMembercode:=qt_memberfc_membercode.AsString;
  vNoSelip:=qt_memberfn_memberothernumber1.AsInteger;
  if MessageDlg('Are you sure want to move to OLD EMPLOYEE WITH UPDATE NO SELIP ?', mtConfirmation, mbYesNo, 0) = mrYes then begin
     with DM.MyBatch do begin
       Script.Clear;
       Script.Text:=' insert into t_oldmember select * from t_member where fc_membercode = :member; '
                      +' update t_member  set fn_memberothernumber1 = fn_memberothernumber1 - 1 where fn_memberothernumber1 > :noSelip ;'
                      +' delete from t_member  where fc_membercode = :member;'
                   ;
       ParamByName('member').AsString:=vMembercode;
       ParamByName('noSelip').AsInteger:=vNoSelip;
       Execute;
       BtnRefreshClick(Sender);
       ShowMessage('Employee sucessfully moved to old employee ');
    end;
  end;
end;

procedure Tf_member.MenuItem2Click(Sender: TObject);
var
  vMembercode :string;
begin
  vMembercode:=qt_memberfc_membercode.AsString;
  if MessageDlg('Are you sure want to move to OLD EMPLOYEE WITHOUT UPDATE NO SELIP ?', mtConfirmation, mbYesNo, 0) = mrYes then begin
     with DM.MyBatch do begin
       Script.Clear;
       Script.Text:=' insert into t_oldmember select * from t_member where fc_membercode = :member; '
                      +' delete from t_member  where fc_membercode = :member;'
                   ;
       ParamByName('member').AsString:=vMembercode;
       Execute;
       BtnRefreshClick(Sender);
       ShowMessage('Employee sucessfully moved to old employee ');
    end;
  end;
end;

procedure Tf_member.qt_memberfl_memberblacklistChange(Sender: TField);
begin
  qt_member.ApplyUpdates;
end;

procedure Tf_member.qt_memberNewRecord(DataSet: TDataSet);
begin
  qt_memberfc_branch.AsString:='001';
  qt_memberfn_memberothernumber1.AsInteger:=MaxNoSelipMember+1;
end;

procedure Tf_member.updateLeaveMember(date:TDate);
begin
  with DM.MyBatch do begin
   Script.Text:=
          ' update t_member a ' +
          ' left join t_trxtype b on b.fc_trxcode = "CHECK_ADD_LEAVE_DATE" '+
          ' set '+
          ' a.fn_memberleaveadd = '+
          ' if( year(:date) > year(b.fv_trxdescription) '+
          ' , a.fn_memberleavecount '+
          ' , if(:date > a.fd_expiredleaveadd,0,a.fn_memberleaveadd ) ),'+
          ' a.fn_memberleavecount = '+
          ' if( year(:date) > year(b.fv_trxdescription), 0, a.fn_memberleavecount ), '+
          ' a.fd_expiredleaveadd = '+
          ' if( year(:date) > year(b.fv_trxdescription), concat(date_format(:date,"%Y"),"-06-30" ) , a.fd_expiredleaveadd ); '+

          'update t_trxtype a '+
          ' left join t_trxtype b on b.fc_trxcode = "CHECK_ADD_LEAVE_DATE" '+
          ' set a.fc_trxaction1 = if( month(:date ) > month(b.fv_trxdescription) and a.fc_trxaction1 = 1 or year(:date) > year(b.fv_trxdescription)  and a.fc_trxaction1 = 1, 0,a.fc_trxaction1 ) '+
          ' where a.fc_trxcode = "CHECK_ADD_LEAVE_DAY"; '+

          ' update '+
          ' t_member a '+
          ' left join t_trxtype b on b.fc_trxcode = "CHECK_ADD_LEAVE_DAY" '+
          ' set '+
          ' a.fn_memberleavecount = if( day(:date) >= 23 and b.fc_trxaction1 = 0, a.fn_memberleavecount + 1,a.fn_memberleavecount ), '+
          ' b.fc_trxaction1 = if( day(:date) >= 23 and b.fc_trxaction1 = 0, 1 , b.fc_trxaction1 ); '+

          ' update t_trxtype set fv_trxdescription = date_format(:date ,"%Y-%m-%d") where fc_trxcode = "CHECK_ADD_LEAVE_DATE";' +

          ' update t_member a '+
          ' inner join '+
          ' ( select fc_membercode,count(*) leavethisyear '+
          ' from t_leave '+
          ' where fc_leavestatus != "T" and fc_leavetype = "AL" and fd_leavedate < :date '+
          ' group by fc_membercode ) b on a.fc_membercode = b.fc_membercode '+
          ' set '+
          ' a.fn_memberleaveadd = if( b.leavethisyear >= a.fn_memberleaveadd, 0,  a.fn_memberleaveadd - b.leavethisyear ) ,'+
          ' a.fn_memberleavecount = if( a.fn_memberleaveadd - b.leavethisyear < 0, a.fn_memberleavecount + a.fn_memberleaveadd - b.leavethisyear , a.fn_memberleavecount) '+
          ' where a.fc_membercode = b.fc_membercode; ' +

          ' update t_leave set fc_leavestatus = "T" where fc_leavetype = "AL" and fd_leavedate < :date ';

          ;
   ParamByName('date').AsDate:=date;
   Execute;
  end;
end;


function Tf_member.MaxNoSelipMember: LongInt;
var
  q1:TZQuery;
begin
   q1:=TZQuery.Create(Application);
   with q1 do begin
      Connection := DM.MyData;
      Sql.Add('select max(fn_memberothernumber1 ) maxnoselip from t_member');
      Open;
      result:=q1.FieldByName('maxnoselip').AsInteger;
   end;
   q1.free;
end;


procedure Tf_member.DBGridMemberTitleClick(Column: TColumn);
begin
  if Column.Field.FieldKind = fklookup then begin
     field_search_member:=Column.Field.FieldName;
     Insert('.',field_search_member,2);
     labelSearch.Caption := 'By '+Column.Title.Caption;
     Memo1.Lines.Clear;
     Memo1.Lines.Add(vq_qt_member);
     With qt_member do Begin
          Close;
          SQL.Text:= vq_qt_member + ' order by ' + field_search_member;
          Open;
     End;

  end else if Column.Field.FieldKind = fkdata then begin
     field_search_member:=Column.FieldName;
     labelSearch.Caption := 'By '+Column.Title.Caption;
     Memo1.Lines.Clear;
     Memo1.Lines.Add(vq_qt_member);
     With qt_member do Begin
          Close;
          SQL.Text:= vq_qt_member + ' order by ' + field_search_member;
          Open;
     End;
  end;
end;

procedure Tf_member.DBGridMemberColExit(Sender: TObject);
begin
  qt_member.ApplyUpdates;
end;

procedure Tf_member.BtnReportClick(Sender: TObject);
begin
  ReportMember.LoadFromFile('ReportMember.lrf');
  ReportMember.ShowReport;
end;

procedure Tf_member.Button1Click(Sender: TObject);
begin
  f_memberstatistic.Show;
  f_memberstatistic.BtnRefreshClick(Sender);
end;

procedure Tf_member.Button2Click(Sender: TObject);
begin
  if MessageDlg('Yakin untuk perpanjangan cuti semua karyawan ?',mtConfirmation,mbYesNo,0) = mrYes then begin
     with DM.MyBatch do begin
      Script.Clear;
      Script.Text:='update t_member set fd_expiredleaveadd = :date ;';
      ParamByName('date').AsDate:=DateTimePicker1.Date;
      Execute;
    end;
    qt_member.Close;
    qt_member.Open;
  end;
end;

procedure Tf_member.BtnRefreshClick(Sender: TObject);
begin
  qt_trxcabang.Close; qt_trxcabang.Open;
  qt_trxgraduated.Close;qt_trxgraduated.Open;
  qt_trxreligion.Close; qt_trxreligion.Open;
  qt_trxmaritalstatus.Close;qt_trxmaritalstatus.Open;
  qt_trxstatkaryawan.Close;qt_trxstatkaryawan.Open;
  qt_division.Close;qt_division.Open;
  qt_trxposition.Close;qt_trxposition.Open;
  qt_trxmembersex.Close;qt_trxmembersex.Open;
  //qt_member.Close;qt_member.Open;
  DM.RefreshDBGgridWithoutLosingCurRow(DBGridMember);
end;

procedure Tf_member.BtnExcelClick(Sender: TObject);
var
  MyWorkbook: TsWorkbook;
  MyWorksheet: TsWorksheet;
  MyFormula: TsRPNFormula;
  MyDir: string;
  i: Longint=1;
begin
  // Initialization
  SaveDialog.FileName:='EMPLOYEE'+FormatDateTime('DD-MM-YYYY',DM.NowX);
  DBGridMember.Enabled:=False;
  if SaveDialog.Execute then  begin
      MyWorkbook := TsWorkbook.Create;
      MyWorksheet := MyWorkbook.AddWorksheet('Employee');
        MyWorksheet.WriteText(0, 0, 'No Selip');
        MyWorksheet.WriteFontStyle(0,0,[fssBold]);
        MyWorksheet.WriteText(0, 1, 'Employee ID');
        MyWorksheet.WriteFontStyle(0,1,[fssBold]);
        MyWorksheet.WriteText(0, 2, 'Employee Name');
        MyWorksheet.WriteFontStyle(0,2,[fssBold]);
        MyWorksheet.WriteText(0, 3, 'Gender');
        MyWorksheet.WriteFontStyle(0,3,[fssBold]);
        MyWorksheet.WriteText(0, 4, 'Division');
        MyWorksheet.WriteFontStyle(0,4,[fssBold]);
        MyWorksheet.WriteText(0, 5, 'Position');
        MyWorksheet.WriteFontStyle(0,5,[fssBold]);
        MyWorksheet.WriteText(0, 6, 'Contract - I Start');
        MyWorksheet.WriteFontStyle(0,6,[fssBold]);
        MyWorksheet.WriteText(0, 7, 'Contract - I FInish');
        MyWorksheet.WriteFontStyle(0,7,[fssBold]);
        MyWorksheet.WriteText(0, 8, 'Contract - II Start');
        MyWorksheet.WriteFontStyle(0,8,[fssBold]);
        MyWorksheet.WriteText(0, 9, 'Contract - II Finish');
        MyWorksheet.WriteFontStyle(0,9,[fssBold]);
        MyWorksheet.WriteText(0, 10, 'Employee Status');
        MyWorksheet.WriteFontStyle(0,10,[fssBold]);
        MyWorksheet.WriteText(0, 11, 'Emp. Status Date');
        MyWorksheet.WriteFontStyle(0,11,[fssBold]);
        MyWorksheet.WriteText(0, 12, 'Note');
        MyWorksheet.WriteFontStyle(0,12,[fssBold]);
        MyWorksheet.WriteText(0, 13, 'KTP / Passport');
        MyWorksheet.WriteFontStyle(0,13,[fssBold]);
        MyWorksheet.WriteText(0, 14, 'Birth Place');
        MyWorksheet.WriteFontStyle(0,14,[fssBold]);
        MyWorksheet.WriteText(0, 15, 'Bird Date');
        MyWorksheet.WriteFontStyle(0,15,[fssBold]);
        MyWorksheet.WriteText(0, 16, 'Age');
        MyWorksheet.WriteFontStyle(0,16,[fssBold]);
        MyWorksheet.WriteText(0, 17, 'Address');
        MyWorksheet.WriteFontStyle(0,17,[fssBold]);
        MyWorksheet.WriteText(0, 18, 'Marital Status');
        MyWorksheet.WriteFontStyle(0,18,[fssBold]);
        MyWorksheet.WriteText(0, 19, 'Graduated');
        MyWorksheet.WriteFontStyle(0,19,[fssBold]);
        MyWorksheet.WriteText(0, 20, 'Graduated Spec');
        MyWorksheet.WriteFontStyle(0,20,[fssBold]);
        MyWorksheet.WriteText(0, 21, 'Blood Type');
        MyWorksheet.WriteFontStyle(0,21,[fssBold]);
        MyWorksheet.WriteText(0, 22, 'Religion');
        MyWorksheet.WriteFontStyle(0,22,[fssBold]);
        MyWorksheet.WriteText(0, 23, 'Phone');
        MyWorksheet.WriteFontStyle(0,23,[fssBold]);
        MyWorksheet.WriteText(0, 24, 'Branch');
        MyWorksheet.WriteFontStyle(0,24,[fssBold]);
        MyWorksheet.WriteText(0, 25, 'Join Date');
        MyWorksheet.WriteFontStyle(0,25,[fssBold]);
        qt_member.First;
      while not qt_member.EOF  do begin
        MyWorksheet.WriteText(i, 0, qt_member.FieldByName('fn_memberothernumber1').AsString);
        MyWorksheet.WriteText(i, 1, qt_member.FieldByName('fc_membercode').AsString);
        MyWorksheet.WriteText(i, 2, qt_member.FieldByName('fc_membername1').AsString);
        MyWorksheet.WriteText(i, 3, qt_member.FieldByName('ifv_trxdescription').AsString);
        MyWorksheet.WriteText(i, 4, qt_member.FieldByName('gfc_divisionname').AsString);
        MyWorksheet.WriteText(i, 5, qt_member.FieldByName('hfv_trxdescription').AsString);
        MyWorksheet.WriteText(i, 6, qt_member.FieldByName('fd_membercontract1start').AsString);
        MyWorksheet.WriteText(i, 7, qt_member.FieldByName('fd_membercontract1finish').AsString);
        MyWorksheet.WriteText(i, 8, qt_member.FieldByName('fd_membercontract2start').AsString);
        MyWorksheet.WriteText(i, 9, qt_member.FieldByName('fd_membercontract2finish').AsString);
        MyWorksheet.WriteText(i, 10, qt_member.FieldByName('efv_trxdescription').AsString);
        MyWorksheet.WriteText(i, 11, qt_member.FieldByName('fd_memberlegalstatusdate').AsString);
        MyWorksheet.WriteText(i, 12, qt_member.FieldByName('fv_membernote').AsString);
        MyWorksheet.WriteText(i, 13, qt_member.FieldByName('fc_memberktpno').AsString);
        MyWorksheet.WriteText(i, 14, qt_member.FieldByName('fc_memberbirthplace').AsString);
        MyWorksheet.WriteText(i, 15, qt_member.FieldByName('fd_memberbirthdate').AsString);
        MyWorksheet.WriteText(i, 16, qt_member.FieldByName('age').AsString);
        MyWorksheet.WriteText(i, 17, qt_member.FieldByName('fc_memberaddress1').AsString);
        MyWorksheet.WriteText(i, 18, qt_member.FieldByName('dfv_trxdescription').AsString);
        MyWorksheet.WriteText(i, 19, qt_member.FieldByName('ffv_trxdescription').AsString);
        MyWorksheet.WriteText(i, 20, qt_member.FieldByName('fc_membergraduatedspec').AsString);
        MyWorksheet.WriteText(i, 21, qt_member.FieldByName('fc_memberbloodtype').AsString);
        MyWorksheet.WriteText(i, 22, qt_member.FieldByName('cfv_trxdescription').AsString);
        MyWorksheet.WriteText(i, 23, qt_member.FieldByName('fc_memberphone1').AsString);
        MyWorksheet.WriteText(i, 24, qt_member.FieldByName('bfv_trxdescription').AsString);
        MyWorksheet.WriteText(i, 25, qt_member.FieldByName('fd_memberjoindate').AsString);
        qt_member.Next;
        inc(i)
      end;
      //if FileExists(SaveDialog.Filename+'.xlsx') then DeleteFile(SaveDialog.Filename+'.xlsx');
      if FileExists(SaveDialog.Filename) then DeleteFile(SaveDialog.Filename);
      MyWorkbook.WriteToFile(SaveDialog.Filename, OUTPUT_FORMAT);
      MyWorkbook.Free;
  end;
  DBGridMember.Enabled:=True;
end;

procedure Tf_member.CheckBox1Change(Sender: TObject);
begin
  If CheckBox1.Checked Then BEGIN
     dbgridmember1.Visible := True;
     CheckBox1.Caption     := '   ACTIVE / N-A'
  end  Else BEGIN
     dbgridmember1.Visible := False;
     CheckBox1.Caption     := '   SHOW GRID INFO';
  end;
end;

procedure Tf_member.CheckBox2Change(Sender: TObject);
begin
  If CheckBox2.Checked Then BEGIN
     dbgridmember2.Visible := True;
     CheckBox2.Caption     := '   OVERTIME TYPE AND ANNUAL LEAVE'
  end  Else BEGIN
     dbgridmember2.Visible := False;
     CheckBox2.Caption     := '   SHOW GRID INFO';
  end;
end;

procedure Tf_member.EdtFixColumnChange(Sender: TObject);
begin
  if StrToInt(EdtFixColumn.Text) > DBGridMember.Columns.Count + 1  then begin
     EdtFixColumn.Text := IntToStr(DBGridMember.Columns.Count + 1) ;
  end else if StrToInt(EdtFixColumn.Text) < 1 then begin
     EdtFixColumn.Text := '1';
  end;
  DBGridMember.FixedCols:= StrToInt(EdtFixColumn.Text);
  Memo1.Clear;
  Memo1.Text:= IntToStr(DBGridMember.FixedCols) ;
end;

procedure Tf_member.EdtSearchKeyPress(Sender: TObject; var Key: char);
begin
  if Key = chr(13) then begin
     Memo1.Lines.Clear;
     Memo1.Lines.Add(vq_qt_member + ' where ' + field_search_member + ' like "%' + EdtSearch.Text +'%" order by ' + field_search_member );
     With qt_member do Begin
          Close;
          SQL.Text:= vq_qt_member + ' where ' + field_search_member + ' like "%' + EdtSearch.Text +'%" order by ' + field_search_member ;
          Open;
     End;
  end;
end;

procedure Tf_member.FormCreate(Sender: TObject);
begin
  Memo1.Lines.Add(vq_qt_member);
  field_search_member:='fc_membername1';
  labelSearch.Caption:=' By EMPLOYEE NAME';
  DBGridMember.FixedCols:=1;
  //updateLeaveMember(Now);
  updateLeaveMember(DM.NowX);
  // karena ada script update maka "refresh" diletakkan setelah update agar SQLUpdate component dapat berjalan dg baik
  BtnRefreshClick(Sender);
end;

end.

