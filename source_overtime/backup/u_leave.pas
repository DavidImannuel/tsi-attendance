unit u_leave;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, db, Forms, Controls, Graphics, Dialogs, LCLType, ExtCtrls,
  DBGrids, StdCtrls, DateTimePicker, ZDataset, ZSqlUpdate, ZConnection, Grids,
  DBCtrls, Buttons;
const
  vq_qt_leave =
    ' select a.fc_branch, a.fc_leaveno, a.fd_leavecreatedate, ' +
    ' a.fc_membercode, b.fc_membername1, a.fc_inputby, ' +
    ' a.fc_leavestatus, a.fc_leaveprogress, a.fc_leavetype, ' +
    ' c.fv_trxdescription, ' +
       ' min(a.fd_leavedate) as DateStart, ' +
       ' max(a.fd_leavedate) as DateFinish, ' +
       ' DATEDIFF(MAX(a.fd_leavedate), MIN(a.fd_leavedate)) + 1 as TotDay, ' +
       ' CASE WHEN (DATE_FORMAT(MIN(a.fd_leavedate),"%Y%m%d") <= DATE_FORMAT(SYSDATE(),"%Y%m%d")) ' +
            ' AND  (DATE_FORMAT(MAX(a.fd_leavedate),"%Y%m%d") >= DATE_FORMAT(SYSDATE(),"%Y%m%d")) ' +
            ' THEN "ON LEAVING" ' +

            ' WHEN (DATE_FORMAT(MIN(a.fd_leavedate),"%Y%m%d") > DATE_FORMAT(SYSDATE(),"%Y%m%d")) ' +
            ' AND  (DATE_FORMAT(MAX(a.fd_leavedate),"%Y%m%d") > DATE_FORMAT(SYSDATE(),"%Y%m%d")) ' +
            ' THEN CONCAT(DATEDIFF(MIN(a.fd_leavedate), SYSDATE()) ," DAYS REMAINING") ' +

            ' WHEN (DATE_FORMAT(MIN(a.fd_leavedate),"%Y%m%d") < DATE_FORMAT(SYSDATE(),"%Y%m%d")) ' +
            ' AND  (DATE_FORMAT(MAX(a.fd_leavedate),"%Y%m%d") < DATE_FORMAT(SYSDATE(),"%Y%m%d")) ' +
            ' THEN "LEAVE IS OVER" ' +
       ' END as LeavePosition, ' +
       ' a.fv_description ' +
' from t_leave a ' +
' left outer join t_member b on a.fc_membercode = b.fc_membercode ' +
' left outer join t_trxtype c on a.fc_leavetype = c.fc_trxcode and c.fc_trxid = "LEAVE_TYPE" ' +
' where ' +
' (select min(fd_leavedate) from t_leave where fc_leaveno = a.fc_leaveno group by fc_branch, fc_leaveno) >= :datestart ' +
' and ' +
' (select max(fd_leavedate) from t_leave where fc_leaveno = a.fc_leaveno group by fc_branch, fc_leaveno) <= :datefinish ' ;
//' group by a.fc_branch, a.fc_leaveno ' +
//' order by a.fc_leaveno ';


const
  mrNoneNI=      20;
  mrOkNI=        mrNoneNI+1;
  mrCancelNI=    mrNoneNI+2;
  mrAbortNI=     mrNoneNI+3;
  mrRetryNI=     mrNoneNI+4;
  mrIgnoreNI=    mrNoneNI+5;
  mrYesNI=       mrNoneNI+6;
  mrNoNI=        mrNoneNI+7;
  mrAllNI=       mrNoneNI+8;
  mrYesToAllNI=  mrNoneNI+10;
  mrCloseNI=     mrNoneNI+11;
  mrLastNI=      mrCloseNI;


type

  { Tf_leave }

  Tf_leave = class(TForm)
    BtnCancel: TBitBtn;
    BtnDELData: TBitBtn;
    BtnEdtData1: TBitBtn;
    BtnEdtData2: TBitBtn;
    BtnNewData: TBitBtn;
    BtnResetPeriode: TButton;
    BtnSave: TBitBtn;
    BtnSearch: TButton;
    Button1: TButton;
    BtnCheckLeave: TButton;
    ComboBoxFilter: TComboBox;
    ds_leavetype: TDataSource;
    DBLookupComboBox3: TDBLookupComboBox;
    ds_member: TDataSource;
    DBLookupComboBox2: TDBLookupComboBox;
    DateTimePickerStart: TDateTimePicker;
    DateTimePickerFinish: TDateTimePicker;
    DateTimePickerInput1: TDateTimePicker;
    DateTimePickerInput2: TDateTimePicker;
    DBGridLeave: TDBGrid;
    ds_leave: TDataSource;
    EdtSHift1: TEdit;
    EdtTimeInShift1: TEdit;
    EdtTimeOutShift1: TEdit;
    EdtShift2: TEdit;
    EdtShift3: TEdit;
    EdtTimeInShift2: TEdit;
    EdtTimeInShift3: TEdit;
    EdtTimeOutShift2: TEdit;
    EdtTimeOutShift3: TEdit;
    EdtSearch: TEdit;
    EdtDaytype: TEdit;
    Label1: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    leave: TPage;
    Memo1: TMemo;
    Memo2: TMemo;
    Notebook1: TNotebook;
    Panel1: TPanel;
    PFooter: TPanel;
    PFormInput: TPanel;
    p_header: TPanel;
    p_menu: TPanel;
    qt_leaveDateFinish: TDateTimeField;
    qt_leaveDateStart: TDateTimeField;
    qt_leavefc_branch: TStringField;
    qt_leavefc_inputby: TStringField;
    qt_leavefc_leaveno: TStringField;
    qt_leavefc_leaveprogress: TStringField;
    qt_leavefc_leavestatus: TStringField;
    qt_leavefc_leavetype: TStringField;
    qt_leavefc_membercode: TStringField;
    qt_leavefc_membername1: TStringField;
    qt_leavefd_leavecreatedate: TDateTimeField;
    qt_leavefv_description: TStringField;
    qt_leavefv_trxdescription: TStringField;
    qt_leaveLeavePosition: TStringField;
    qt_leaveTotDay: TLargeintField;
    qt_trxleave: TZReadOnlyQuery;
    qt_trxleavefc_trxcode: TStringField;
    qt_trxleavefn_trxnumber: TSmallintField;
    qt_trxleaveLeaveReasonLookup: TStringField;
    qut_leave: TZUpdateSQL;
    StringField4: TStringField;
    StringField5: TStringField;
    StringField6: TStringField;
    qt_member: TZReadOnlyQuery;
    qt_memberfc_membercode: TStringField;
    qt_memberfc_membername1: TStringField;
    qt_leave: TZQuery;


    procedure BtnCheckLeaveClick(Sender: TObject);
    procedure BtnEdtData1Click(Sender: TObject);
    procedure BtnEdtData2Click(Sender: TObject);
    procedure BtnNewDataClick(Sender: TObject);
    procedure BtnDELDataClick(Sender: TObject);
    procedure BtnResetPeriodeClick(Sender: TObject);
    procedure BtnCancelClick(Sender: TObject);
    procedure BtnSearchClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure DateTimePickerInput1Change(Sender: TObject);
    procedure DateTimePickerInputChange(Sender: TObject);
    procedure DBComboBox2CloseUp(Sender: TObject);
    procedure DBGridLeaveColExit(Sender: TObject);
    procedure DBGridLeaveDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGridLeaveTitleClick(Column: TColumn);
    procedure DBLookupComboBox1CloseUp(Sender: TObject);
    procedure DBLookupComboBox2Change(Sender: TObject);
    procedure DBLookupComboBox2CloseUp(Sender: TObject);
    procedure DBLookupComboBox2Exit(Sender: TObject);
    procedure DBLookupComboBox2Select(Sender: TObject);
    procedure EdtSearchKeyPress(Sender: TObject; var Key: char);
    procedure EdtSHift1Change(Sender: TObject);
    procedure EdtShift2Change(Sender: TObject);
    procedure EdtShift3Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure qt_leaveNewRecord(DataSet: TDataSet);

    procedure updateAttendance(membercode:string;date:TDate);
  private
    FActiveField: TField;
    vTableNameAttendance:string;
    vLeaveBaseQuery,
    field_search_leave : String;

  public

  end;

var
  f_leave: Tf_leave;

implementation

uses
  u_datamodule,u_extendleave,u_countleave;

{$R *.lfm}

{ Tf_leave }


procedure Tf_leave.FormCreate(Sender: TObject);
begin
  vTableNameAttendance:='t_attendance';
  DateTimePickerStart.Date := DM.NowX;
  DateTimePickerFinish.Date := DM.NowX;

  field_search_leave   := 'fc_leaveno';



  PFormInput.Visible   := False;
  If Not(qt_trxleave.Active) Then qt_trxleave.Open;
  If Not(qt_member.Active)   Then qt_member.Open;
  If Not(qt_leave.Active)    Then qt_leave.Open;

  If DBGridLeave.Enabled = False Then DBGridLeave.Enabled := True;
  DBGridLeave.Columns[0].Title.Color := clYellow;
end;

procedure Tf_leave.BtnNewDataClick(Sender: TObject);
begin
  DateTimePickerInput1.Date  := DM.NowX;
  DateTimePickerInput2.Date  := DM.NowX;
  Label10.Caption            := '1';

  DBGridLeave.Enabled := False;
  PFormInput.Visible  := True;
  PFooter.Visible     := False;

  DBLookupComboBox2.SetFocus;
end;

procedure Tf_leave.BtnEdtData1Click(Sender: TObject);
Var
   vNoLeave                                 : String;
   vCount, Reply, BoxStyle                  : Integer;
begin
   BoxStyle := MB_ICONQUESTION + MB_YESNO;
   Reply := Application.MessageBox('Press YES to save document', 'MessageBoxDemo', BoxStyle);

   If Reply = IDYES Then Begin
      vNoLeave := DM.CreateNoByYearYYPrefix('01','CREATE_LEAVE','');
      For vCount := 0 To StrToInt(Label10.Caption) - 1 Do Begin
          With DM.MyBatch Do Begin
               Script.Clear;
               Script.Text :=
               'insert into t_leave ' +
               'values (:fc_branch,:fc_leaveno, sysdate(), :fd_leavedate, ' +
               '        :fc_leavetype, :fv_description, :fc_membercode, '   +
               '        :fc_inputby, :fc_leavestatus, :fc_leaveprogress, '+
               ':fc_daytype,:fc_shift1, :fc_timeinshift1,:fc_timeoutshift1,'+
               '        :fc_shift2, :fc_timeinshift2,:fc_timeoutshift2,'+
               '        :fc_shift3, :fc_timeinshift3,:fc_timeoutshift3);'
               ;
               ParamByName('fc_branch').AsString        := '001';
               ParamByName('fc_leaveno').AsString       := vNoLeave;
               ParamByName('fd_leavedate').AsDate       := DateTimePickerInput1.Date + vCount;
               ParamByName('fc_leavetype').AsString     := qt_trxleavefc_trxcode.AsString;
               ParamByName('fv_description').AsString   := Memo1.Text;
               ParamByName('fc_membercode').AsString    := qt_memberfc_membercode.AsString;
               ParamByName('fc_inputby').AsString       := 'YUDHA';
               ParamByName('fc_leavestatus').AsString   := 'X';
               ParamByName('fc_leaveprogress').AsString := '';
               ParamByName('fc_daytype').AsString := EdtDaytype.Text;
               ParamByName('fc_shift1').AsString := EdtSHift1.Text;
               ParamByName('fc_timeinshift1').AsString := EdtTimeInShift1.Text;
               ParamByName('fc_timeoutshift1').AsString := EdtTimeOutShift1.Text;
               ParamByName('fc_shift2').AsString := EdtSHift2.Text;
               ParamByName('fc_timeinshift2').AsString := EdtTimeInShift2.Text;
               ParamByName('fc_timeoutshift2').AsString := EdtTimeOutShift2.Text;
               ParamByName('fc_shift3').AsString := EdtSHift3.Text;
               ParamByName('fc_timeinshift3').AsString := EdtTimeInShift3.Text;
               ParamByName('fc_timeoutshift3').AsString := EdtTimeOutShift3.Text;
               Execute;
               updateAttendance(qt_memberfc_membercode.AsString,DateTimePickerInput1.Date + vCount);
          End;
      End;
      Memo1.Clear;
      FormCreate(Self);
      qt_leave.Close; qt_leave.Open;
      DBGridLeave.Enabled := True;
      PFormInput.Visible:=False;
      PFooter.Visible:=True;

   End Else Begin
       // Spare....
   End;


end;

procedure Tf_leave.BtnCheckLeaveClick(Sender: TObject);
begin
  f_countleave.Show;
end;

procedure Tf_leave.BtnEdtData2Click(Sender: TObject);
begin
   DBGridLeave.Enabled := True;
  PFormInput.Visible:=False;
  PFooter.Visible:=True;
end;

procedure Tf_leave.BtnCancelClick(Sender: TObject);
begin
end;


procedure Tf_leave.BtnDELDataClick(Sender: TObject);
Var
Reply, BoxStyle: Integer;
begin
  BoxStyle := MB_ICONQUESTION + MB_YESNO;
  Reply    := Application.MessageBox('Are you sure ?', 'Confirmation', BoxStyle);
  If Reply = IDYES Then Begin
     With DM.MyBatch Do Begin
          Script.Text := 'delete from t_leave where fc_leaveno = :fc_leaveno;';
          ParamByName('fc_leaveno').AsString    := qt_leavefc_leaveno.AsString;
          Execute;
          qt_leave.Close;
          qt_leave.Open;
     End;
  End;
end;

procedure Tf_leave.BtnResetPeriodeClick(Sender: TObject);
begin
 with qt_leave do begin
   Close;
   sql.Clear;
   SQL.Text := vq_qt_leave + 'GROUP BY a.fc_branch, a.fc_leaveno ORDER BY a.fc_leaveno;';
   Open;
 end;
end;

procedure Tf_leave.qt_leaveNewRecord(DataSet: TDataSet);
begin
end;

procedure Tf_leave.updateAttendance(membercode: string; date: TDate);
begin
  With DM.MyBatch do begin
    Script.Clear;
    Script.Text:= // update reference leave
                    ' update '+vTableNameAttendance+' a '
                    +' inner join t_leave b on a.fc_membercode = b.fc_membercode and a.fd_date = b.fd_leavedate '
                    +' inner join t_trxtype c on b.fc_leavetype = c.fc_trxcode and c.fc_trxid = "LEAVE_TYPE" '
                    +' set '
                    +' a.fc_daytype = b.fc_daytype , '
                    +' a.fc_shift1 = b.fc_shift1 , '
                    +' a.fc_timeinshift1 = b.fc_timeinshift1 , '
                    +' a.fc_timeoutshift1 = b.fc_timeoutshift1, '
                    +' a.fc_shift2 = b.fc_shift2 , '
                    +' a.fc_timeinshift2 = b.fc_timeinshift2 , '
                    +' a.fc_timeoutshift2 = b.fc_timeoutshift2, '
                    +' a.fc_shift3 = b.fc_shift3 , '
                    +' a.fc_timeinshift3 = b.fc_timeinshift3 , '
                    +' a.fc_timeoutshift3 = b.fc_timeoutshift3, '
                    +' a.fv_description = c.fv_trxdescription, '
                    +' a.fc_daytype = if(b.fc_leavetype in ("AL","IP","PH"), "",a.fc_daytype ) '
                    +' where a.fd_date = :date and a.fc_membercode = :membercode;'
                  //update shift
                    +' update '+vTableNameAttendance+' a '
                    +' left join t_dailytimerange b on b.fc_timerangecode = a.fc_shift1 '
                    +' left join t_dailytimerange c on c.fc_timerangecode = a.fc_shift2 '
                    +' left join t_dailytimerange d on d.fc_timerangecode = a.fc_shift3  '
                    +' set  '
                    +' a.fc_workinshift1 = COALESCE(b.fc_workin,NULL), '
                    +' a.fc_workoutshift1 = COALESCE(b.fc_workout,NULL) , '
                    +' a.fn_totalbreakshift1 =COALESCE(TIME_TO_SEC( '
                    +' if( b.fc_workbreakin > b.fc_workbreakout , '
                    +' subtime(b.fc_workbreakin,b.fc_workbreakout ) , '
                    +' subtime(b.fc_workbreakout,b.fc_workbreakin ) '
                    +' ) )/60/60,NULL), '
                    +' a.fc_workinshift2 = COALESCE(c.fc_workin,NULL), '
                    +' a.fc_workoutshift2 = COALESCE(c.fc_workout,NULL) , '
                    +' a.fn_totalbreakshift2 = COALESCE(TIME_TO_SEC(  '
                    +' if( b.fc_workbreakin > b.fc_workbreakout , '
                    +' subtime(c.fc_workbreakin,c.fc_workbreakout ) , '
                    +' subtime(c.fc_workbreakout,c.fc_workbreakin ) '
                    +' ) )/60/60,NULL) , '
                    +' a.fc_workinshift3 = COALESCE(d.fc_workin,NULL), '
                    +' a.fc_workoutshift3 = COALESCE(d.fc_workout,NULL) , '
                    +' a.fn_totalbreakshift3 = COALESCE(TIME_TO_SEC(  '
                    +' if( d.fc_workbreakin > d.fc_workbreakout , '
                    +' subtime(d.fc_workbreakin,d.fc_workbreakout ) , '
                    +' subtime(d.fc_workbreakout,d.fc_workbreakin ) '
                    +' ) )/60/60,NULL) '
                    +' where a.fd_date = :date and a.fc_membercode = :membercode;'
                    // update totalhour reference
                    +' update '+vTableNameAttendance+' '
                    +' set '
	            +' fn_totalhourshift1 = TIME_TO_SEC( '
		    +'	if( fc_workinshift1 > fc_workoutshift1 , '
                    +' subtime(addtime(fc_workoutshift1 ,"24:00"),fc_workinshift1 ) , '
		    +' subtime(fc_workoutshift1,fc_workinshift1 ) '
		    +' 	) )/60/60, '
	            +' fn_totalhourshift2 = TIME_TO_SEC( '
		    +' if( fc_workinshift2 > fc_workoutshift2 , '
		    +' subtime(addtime(fc_workoutshift2 ,"24:00"),fc_workinshift2 ) , '
		    +' subtime(fc_workoutshift2,fc_workinshift2 ) '
		    +' ) )/60/60, '
	            +' fn_totalhourshift3 = TIME_TO_SEC( '
		    +' if( fc_workinshift3 > fc_workoutshift3 , '
		    +' subtime(addtime(fc_workoutshift3 ,"24:00"),fc_workinshift3 ) , '
		    +' subtime(fc_workoutshift3,fc_workinshift3 ) '
		    +' ) )/60/60 '
                    +' where fd_date = :date and fc_membercode = :membercode;'
                    //update timein / out shift 1,2,3 date out
                    +' update '+ vTableNameAttendance + ' a '
                    +' set '
                    +' a.fc_timeinshift1 = if( a.fc_timeinshift1 = "" or a.fc_timeinshift1 is NULL,a.fc_workinshift1 ,a.fc_timeinshift1 ), '
                    +' a.fc_timeoutshift1 = if( a.fc_timeoutshift1 = "" or a.fc_timeoutshift1 is NULL,a.fc_workoutshift1 ,a.fc_timeoutshift1 ), '
                    +' a.fc_timeinshift2 = if( a.fc_timeinshift2 = "" or a.fc_timeinshift2 is NULL,a.fc_workinshift2 ,a.fc_timeinshift2 ), '
                    +' a.fc_timeoutshift2 = if( a.fc_timeoutshift2 = "" or a.fc_timeoutshift2 is NULL,a.fc_workoutshift2 ,a.fc_timeoutshift2 ), '
                    +' a.fc_timeinshift3 = if( a.fc_timeinshift3 = "" or a.fc_timeinshift3 is NULL,a.fc_workinshift3 ,a.fc_timeinshift3 ), '
                    +' a.fc_timeoutshift3 = if( a.fc_timeoutshift3 = "" or a.fc_timeoutshift3 is NULL,a.fc_workoutshift3 ,a.fc_timeoutshift3 ) '
                    +' where a.fd_date = :date and a.fc_membercode = :membercode;'
                    //Update Totalhour  & effective Hour time
                    +' update '+vTableNameAttendance +' a '
                    +' set '
                    +' a.fn_totaltimeshift1 = TIME_TO_SEC( '
	            +' if( a.fc_timeinshift1 > a.fc_timeoutshift1 , '
		    +' subtime(addtime(a.fc_timeoutshift1,"24:00"), a.fc_timeinshift1 ) , '
		    +' subtime(a.fc_timeoutshift1, a.fc_timeinshift1 ) '
	            +' ) )/60/60, '
                    +' a.fn_totaltimeshift2 = TIME_TO_SEC( '
	            +' if( a.fc_timeinshift2 > a.fc_timeoutshift2 , '
		    +' subtime(addtime(a.fc_timeoutshift2,"24:00"), a.fc_timeinshift2 ) , '
		    +' subtime(a.fc_timeoutshift2, a.fc_timeinshift2 ) '
	            +' ) )/60/60, '
                    +' a.fn_totaltimeshift3 = TIME_TO_SEC( '
	            +' if( a.fc_timeinshift3 > a.fc_timeoutshift3 , '
		    +' subtime(addtime(a.fc_timeoutshift3,"24:00"), a.fc_timeinshift3 ) , '
		    +' subtime(a.fc_timeoutshift3, a.fc_timeinshift3 ) '
	            +' ) )/60/60, '
                    +' a.fn_totalhour = a.fn_totaltimeshift1 + coalesce(a.fn_totaltimeshift2,0 ) + coalesce(a.fn_totaltimeshift3,0), '
                    +' a.fn_effectivehour = a.fn_totaltimeshift1 + coalesce(a.fn_totaltimeshift2,0) + coalesce(a.fn_totaltimeshift3,0) - '
                    +' ( coalesce(a.fn_totalbreakshift1,0) + coalesce(a.fn_totalbreakshift2,0) + coalesce(a.fn_totalbreakshift3,0)  )  '
                    +' where a.fd_date = :date and a.fc_membercode = :membercode;'
                    //update jumlahshift
                    +' update ' + vTableNameAttendance +' a '
                    +' set '
                    +' fn_jumlahshift1 = if(fn_totaltimeshift1 >= 4,1,1), ' //hanya hitung total jam yang overtime saja
                    +' fn_jumlahshift2 = if(fn_totaltimeshift2 >= 4,1,0), '
                    +' fn_jumlahshift3 = if(fn_totaltimeshift3 >= 4,1,0), '
                    +' fn_jumlahshift = if(fc_timeinshift1 is null,null,fn_jumlahshift1 ) + coalesce(fn_jumlahshift2,0) + coalesce(fn_jumlahshift3,0) '
                    +' where a.fd_date = :date and a.fc_membercode = :membercode;'
                   //update TI1,TI2,TI3
                   +' update ' + vTableNameAttendance
                   +' set '
                   +' fn_effectivehour = '
                   +' case '
	           +' when fc_ti in ("TI1","NR1") then '
		   +' fn_effectivehour + 1 '
	           +' when fc_ti in ("TI2","NR2") then '
		   +' fn_effectivehour + 2 '
	           +' when fc_ti in ("TI3","NR3") then '
		   +' fn_effectivehour + 3 '
	           +' else '
		   +' fn_effectivehour '
                   +' end '
                   +' where fd_date = :date and fc_membercode = :membercode;'
                    //OT Update
                    +' update '+vTableNameAttendance
                   +' set '
                   +' fn_ot1 = '
                   +' case when (fn_totalhourshift1 - fn_totalbreakshift1 ) = 8  then '
	                   +' case when fc_daytype = "N" then '
		              +' case when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) >= 1 then '
			         + ' 1 '
		              +' when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) >= 0 then '
			         +' fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) '
		              +' else 0 end '
	                   +' else 0 end '
                    +' when (fn_totalhourshift1 - fn_totalbreakshift1 ) = 7 then '
	                    +' case when fc_daytype = "N" then '
		               +' case when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) >= 1 then '
			          +' 1 '
		               +' when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) >= 0 then '
			          +' fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) '
		               +' else 0 end '
	                    +' when fc_daytype = "S" then '
		               +' case when fn_effectivehour > 5 then '
			          +' case when fn_effectivehour - 5 >= 1 then '
			             +'1'
			             +' when fn_effectivehour - 5 <= 1 then '
				        +' fn_effectivehour - 5 '
			             +' else 0 end '
		                  +' else 0 end '
	                    +' else 0 end '
                    +' else NULL end, '
                    +' fn_ot2 = '
                    +' case when (fn_totalhourshift1 - fn_totalbreakshift1 ) = 8  then '
	                    +' case when fc_daytype = "N" then '
		                    +' case when fn_ot1 >=1 then '
			                    +' fn_effectivehour - fn_ot1 - (fn_totalhourshift1 - fn_totalbreakshift1 ) '
		                    +' else 0 end '
	                    +' when fc_daytype in ("S","H","O") then '
		                    +' case when fn_effectivehour - 7 >= 0 then '
			                    +' 7 '
		                    +' when fn_effectivehour - 7 <= 0 then '
			                    +' fn_effectivehour '
		                    +' else 0 end '
	                    +' else 0 end '
                    +' when (fn_totalhourshift1 - fn_totalbreakshift1 ) = 7 then '
	                    +' case when fc_daytype = "N" then '
		                    +' case when fn_ot1 >= 1 then '
			               +' fn_effectivehour - fn_ot1 - (fn_totalhourshift1 - fn_totalbreakshift1 ) '
		                    +' else 0 end '
	                    +' when fc_daytype = "S" then '
		                    +' case when fn_ot1 >= 1 then '
			                    +' case when fn_effectivehour - 5 >= 0 then '
				                    +' fn_effectivehour - fn_ot1 - 5 '
			                    +' when fn_effectivehour - 5 <= 0 then '
				                    +' fn_effectivehour '
			                    +' else 0 end '
		                    +' else 0 end '
	                    +' when fc_daytype in ("H","O") then '
		                    +' case when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) >= 0 then '
			                    +' 7 '
		                    +' when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) <= 0 then '
			                    +' fn_effectivehour '
		                    +' else 0 end '
	                    +' else 0 end '
                    +' else NULL end, '
                    +' fn_ot3 = '
                    +' case when (fn_totalhourshift1 - fn_totalbreakshift1 ) = 8  then '
	                    +' case when fc_daytype in ("S","H","O") then '
		                    +' case when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) >= 1 then '
			                    + ' 1 '
		                    +' when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) <= 1 then '
			                    +' if(fn_effectivehour - 7 > 0,fn_effectivehour - 7,0)'
		                    +' else 0 end '
	                    +' else 0 end '
                    +' when (fn_totalhourshift1 - fn_totalbreakshift1 ) = 7 then '
	                    +' case when fc_daytype in ("H","O") then '
		                    +' case when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) >= 1 then '
			                    +' 1 '
		                    +' when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) <= 1 then '
			                    +' if(fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) > 0, fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) ,0 )'
		                    +' else 0 end '
	                    +' else 0 end '
                    +' else NULL end, '
                    +' fn_ot4 = '
                    +' case when (fn_totalhourshift1 - fn_totalbreakshift1 ) = 8  then '
	                    +' case when fc_daytype in ("S","H","O") then '
		                    +' case when fn_ot3 >=1 then '
			                    +' fn_effectivehour - fn_ot3 - 7 '
		                    +' else 0 end '
	                    +' when fc_daytype in ("H+") then '
		                    +' case when fn_effectivehour - 7 >= 0 then '
			                    +' 7 '
		                    +' when fn_effectivehour - 7 <= 0 then '
			                    +' fn_effectivehour '
		                    +' else 0 end '
	                    +' else 0 end '
                    +' when (fn_totalhourshift1 - fn_totalbreakshift1 ) = 7 then '
	                    +' case when fc_daytype in ("H","O") then '
		                    +' case when fn_ot3 >= 1 then '
			                    +' fn_effectivehour - fn_ot3 - (fn_totalhourshift1 - fn_totalbreakshift1 ) '
		                    +' else 0 end '
	                    +' when fc_daytype in ("H+") then '
		                    +' case when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) >= 0 then '
			                    +' 7 '
		                    +' when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) <= 0 then '
			                    +' fn_effectivehour '
		                    +' else 0 end '
	                    +' else 0 end '
                    +' else NULL end, '
                    +' fn_ot5 = '
                    +' case when (fn_totalhourshift1 - fn_totalbreakshift1 ) = 8  then '
	                    +' case when fc_daytype in ("H+") then '
		                    +' case when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) >= 1 then '
			                    +' 1 '
		                    +' when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) <= 1 then '
			                    +' if( fn_effectivehour - 7 > 0 ,fn_effectivehour - 7,0) '
		                    +' else 0 end '
	                    +' else 0 end '
                    +' when (fn_totalhourshift1 - fn_totalbreakshift1 ) = 7 then '
	                    +' case when fc_daytype in ("H+") then '
		                    +' case when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) >= 1 then '
			                    +' 1 '
		                    +' when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) <= 1 then '
			                    +' if( fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) > 0, fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ),0 )'
		                    +' else 0 end '
	                    +' else 0 end '
                    +' else NULL end,'
                    +' fn_ot6 = '
                    +' case when (fn_totalhourshift1 - fn_totalbreakshift1 ) = 8  then '
	                    +' case when fc_daytype in ("H+") then '
		                    +' case when fn_ot5 >=1 then '
			                    +' fn_effectivehour - fn_ot5 - 7 '
		                    +' else 0 end '
	                    +' else 0 end '
                    +' when (fn_totalhourshift1 - fn_totalbreakshift1 ) = 7 then '
	                    +' case when fc_daytype in ("H+") then '
		                    +' case when fn_ot5 >= 1 then '
			                    +' fn_effectivehour - fn_ot5 - (fn_totalhourshift1 - fn_totalbreakshift1 ) '
		                    +' else 0 end '
	                    +' else 0 end '
                    +' else NULL end '
                    +' where fd_date = :date and fc_membercode = :membercode;';
      ParamByName('date').AsDate:=date;
      ParamByName('membercode').AsString:=membercode;
    Execute;
  end;
end;

procedure Tf_leave.DBGridLeaveColExit(Sender: TObject);
begin

end;


procedure Tf_leave.DateTimePickerInputChange(Sender: TObject);
begin

end;

procedure Tf_leave.DBComboBox2CloseUp(Sender: TObject);
begin
end;

procedure Tf_leave.BtnSearchClick(Sender: TObject);
begin
  With qt_leave do Begin
       Close;
       SQL.Clear;
       if ComboBoxFilter.ItemIndex = -1 then begin
       end else if ComboBoxFilter.ItemIndex = 0 then begin
          SQL.Text:= vq_qt_leave + ' and  b.fc_membercode like :search ' + ' group by a.fc_branch, a.fc_leaveno order by a.fc_leaveno ' ;
       end else if ComboBoxFilter.ItemIndex = 1 then begin
          SQL.Text:= vq_qt_leave + ' and  b.fc_membername1 like :search ' + ' group by a.fc_branch, a.fc_leaveno order by a.fc_leaveno ' ;
       end else if ComboBoxFilter.ItemIndex = 2 then begin
          SQL.Text:= vq_qt_leave + ' and  a.fc_leaveno like :search ' + ' group by a.fc_branch, a.fc_leaveno order by a.fc_leaveno ' ;
       end;
       ParamByName('datestart').AsDate:=DateTimePickerStart.Date;
       ParamByName('datefinish').AsDate:=DateTimePickerFinish.Date;
       ParamByName('search').AsString:='%'+EdtSearch.Text+'%';
       Open;
  End;
end;

procedure Tf_leave.Button1Click(Sender: TObject);
begin
  f_extendleave.Show;
end;

procedure Tf_leave.DateTimePickerInput1Change(Sender: TObject);
begin
 Label10.Caption := IntToStr(
                      Trunc(DateTimePickerInput2.Date) -
                      Trunc(DateTimePickerInput1.Date) + 1
                           );
end;

procedure Tf_leave.DBGridLeaveDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
 If qt_leaveLeavePosition.AsString = 'LEAVE IS OVER' Then
    DBGridLeave.Canvas.Font.Color := clRed
 Else If qt_leaveLeavePosition.AsString = 'ON LEAVING' Then
    DBGridLeave.Canvas.Font.Color := clGreen;
end;

procedure Tf_leave.DBGridLeaveTitleClick(Column: TColumn);
Var
   vCountColumn : byte;
begin

end;

procedure Tf_leave.DBLookupComboBox1CloseUp(Sender: TObject);
begin
end;

procedure Tf_leave.DBLookupComboBox2Change(Sender: TObject);
begin
 //ShowMessage('on change  '+qt_memberfc_membername1.asstring + ' ' + qt_memberfc_membercode.AsString);
end;

procedure Tf_leave.DBLookupComboBox2CloseUp(Sender: TObject);
begin
 //ShowMessage('on closeup  '+qt_memberfc_membername1.asstring + ' ' + qt_memberfc_membercode.AsString);
end;

procedure Tf_leave.DBLookupComboBox2Exit(Sender: TObject);
begin
 //ShowMessage('on exit  '+qt_memberfc_membername1.asstring + ' ' + qt_memberfc_membercode.AsString);
end;

procedure Tf_leave.DBLookupComboBox2Select(Sender: TObject);

begin
   //ShowMessage('on select  '+qt_memberfc_membername1.asstring + ' ' + qt_memberfc_membercode.AsString);
end;

procedure Tf_leave.EdtSearchKeyPress(Sender: TObject; var Key: char);
begin
  if Key = chr(13) then begin
    BtnSearchClick(Sender);
  end;
end;

procedure Tf_leave.EdtSHift1Change(Sender: TObject);
var
   q: TZQuery;
begin
 q := TZQuery.Create(Self);
 with q do begin
     Connection:=DM.MyData;
     SQL.Text:='SELECT * FROM t_dailytimerange where fc_timerangecode = :shift';
     ParamByName('shift').AsString:=EdtSHift1.Text;
     Open;
     EdtTimeInShift1.Text:=FieldByName('fc_workin').AsString;
     EdtTimeOutShift1.Text:=FieldByName('fc_workout').AsString;
     Free;
 end;
 if EdtSHift1.Text='' then begin
   EdtSHift2.Enabled:=False;EdtTimeInShift2.Enabled:=False;EdtTimeOutShift2.Enabled:=False;
   EdtSHift2.Text:='';EdtTimeInShift2.Text:='';EdtTimeOutShift2.Text:='';
 end else begin
   EdtSHift2.Enabled:=True;EdtTimeInShift2.Enabled:=True;EdtTimeOutShift2.Enabled:=True;
 end;
end;

procedure Tf_leave.EdtShift2Change(Sender: TObject);
var
   q: TZQuery;
begin
 q := TZQuery.Create(Self);
 with q do begin
     Connection:=DM.MyData;
     SQL.Text:='SELECT * FROM t_dailytimerange where fc_timerangecode = :shift';
     ParamByName('shift').AsString:=EdtSHift2.Text;
     Open;
     EdtTimeInShift2.Text:=FieldByName('fc_workin').AsString;
     EdtTimeOutShift2.Text:=FieldByName('fc_workout').AsString;
     Free;
 end;

 if EdtSHift2.Text='' then begin
   EdtSHift3.Enabled:=False;EdtTimeInShift3.Enabled:=False;EdtTimeOutShift3.Enabled:=False;
   EdtSHift3.Text:='';EdtTimeInShift3.Text:='';EdtTimeOutShift3.Text:='';
 end else begin
   EdtSHift3.Enabled:=True;EdtTimeInShift3.Enabled:=True;EdtTimeOutShift3.Enabled:=True;
 end;

end;

procedure Tf_leave.EdtShift3Change(Sender: TObject);
var
   q: TZQuery;
begin
 q := TZQuery.Create(Self);
 with q do begin
     Connection:=DM.MyData;
     SQL.Text:='SELECT * FROM t_dailytimerange where fc_timerangecode = :shift';
     ParamByName('shift').AsString:=EdtSHift3.Text;
     Open;
     EdtTimeInShift3.Text:=FieldByName('fc_workin').AsString;
     EdtTimeOutShift3.Text:=FieldByName('fc_workout').AsString;
     Free;
 end;

end;

end.

