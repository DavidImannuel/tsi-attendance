unit u_overtime;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, db, Forms, Controls, Graphics, Dialogs, LCLType, ExtCtrls,
  DBGrids, StdCtrls, DateTimePicker, ZDataset, ZSqlUpdate, ZConnection, Grids,
  DBCtrls, Buttons, Menus;
const
  vq_qt_overtime = ' select a.*,concat(a.fc_shift1,a.fc_shift2,a.fc_shift3) shift,b.fc_membername1 from t_overtime a left join t_member b on a.fc_membercode = b.fc_membercode where a.fd_otdate >= :datestart and a.fd_otdate <= :datefinish ';


const
  mrNoneNI=      20;
  mrOkNI=        mrNoneNI+1;
  mrCancelNI=    mrNoneNI+2;
  mrAbortNI=     mrNoneNI+3;
  mrRetryNI=     mrNoneNI+4;
  mrIgnoreNI=    mrNoneNI+5;
  mrYesNI=       mrNoneNI+6;
  mrNoNI=        mrNoneNI+7;
  mrAllNI=       mrNoneNI+8;
  mrYesToAllNI=  mrNoneNI+10;
  mrCloseNI=     mrNoneNI+11;
  mrLastNI=      mrCloseNI;


type

  { Tf_overtime }

  Tf_overtime = class(TForm)
    BtnCancel: TBitBtn;
    BtnDELData: TBitBtn;
    BtnEdtData1: TBitBtn;
    BtnEdtData2: TBitBtn;
    BtnNewData: TBitBtn;
    BtnResetPeriode: TButton;
    BtnSave: TBitBtn;
    BtnSearch: TButton;
    ComboBoxFilter: TComboBox;
    DateTimePicker1: TDateTimePicker;
    DateTimePickerInput1: TDateTimePicker;
    DBLookupListBox1: TDBLookupListBox;
    ds_membersearch: TDataSource;
    DBLookupComboBox1: TDBLookupComboBox;
    ds_member: TDataSource;
    DateTimePickerStart: TDateTimePicker;
    DateTimePickerFinish: TDateTimePicker;
    DBGridLeave: TDBGrid;
    ds_overtime: TDataSource;
    ds_nr: TDataSource;
    EditSeacrhEmp: TEdit;
    EdtNoRest: TEdit;
    EdtSHift1: TEdit;
    EdtTimeInShift1: TEdit;
    EdtTimeOutShift1: TEdit;
    EdtShift2: TEdit;
    EdtShift3: TEdit;
    EdtTimeInShift2: TEdit;
    EdtTimeInShift3: TEdit;
    EdtTimeOutShift2: TEdit;
    EdtTimeOutShift3: TEdit;
    EdtSearch: TEdit;
    EdtDaytype: TEdit;
    Label1: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    leave: TPage;
    Memo1: TMemo;
    Memo2: TMemo;
    MenuItem1: TMenuItem;
    Notebook1: TNotebook;
    Panel1: TPanel;
    PFooter: TPanel;
    PFormInput: TPanel;
    PopupMenu1: TPopupMenu;
    p_header: TPanel;
    p_menu: TPanel;
    qt_membersearchfc_membercode: TStringField;
    qt_membersearchfc_membername1: TStringField;
    qt_membersearchfv_trxdescription: TStringField;
    qt_membersearchmemberandposition: TStringField;
    qt_trxnr: TZReadOnlyQuery;
    qt_overtimefc_branch: TStringField;
    qt_overtimefc_daytype: TStringField;
    qt_overtimefc_inputby: TStringField;
    qt_overtimefc_membercode: TStringField;
    qt_overtimefc_membername1: TStringField;
    qt_overtimefc_otno: TStringField;
    qt_overtimefc_otstatus: TStringField;
    qt_overtimefc_ottype: TStringField;
    qt_overtimefc_shift1: TStringField;
    qt_overtimefc_shift2: TStringField;
    qt_overtimefc_shift3: TStringField;
    qt_overtimefc_ti: TStringField;
    qt_overtimefc_timeinshift1: TStringField;
    qt_overtimefc_timeinshift2: TStringField;
    qt_overtimefc_timeinshift3: TStringField;
    qt_overtimefc_timeoutshift1: TStringField;
    qt_overtimefc_timeoutshift2: TStringField;
    qt_overtimefc_timeoutshift3: TStringField;
    qt_overtimefd_otcreatedate: TDateTimeField;
    qt_overtimefd_otdate: TDateTimeField;
    qt_overtimefv_description: TStringField;
    qt_overtimeshift: TStringField;
    qt_trxnrfc_branch: TStringField;
    qt_trxnrfc_trxcode: TStringField;
    qt_trxnrfc_trxid: TStringField;
    qt_trxnrfn_trxnumber: TSmallintField;
    qt_trxnrfv_trxdescription: TStringField;
    qut_leave: TZUpdateSQL;
    StringField4: TStringField;
    StringField5: TStringField;
    StringField6: TStringField;
    qt_member: TZReadOnlyQuery;
    qt_memberfc_membercode: TStringField;
    qt_memberfc_membername1: TStringField;
    qt_overtime: TZQuery;
    qt_membersearch: TZReadOnlyQuery;

    procedure BtnEdtData1Click(Sender: TObject);
    procedure BtnEdtData2Click(Sender: TObject);
    procedure BtnNewDataClick(Sender: TObject);
    procedure BtnDELDataClick(Sender: TObject);
    procedure BtnResetPeriodeClick(Sender: TObject);
    procedure BtnSearchClick(Sender: TObject);
    procedure DateTimePicker1Change(Sender: TObject);
    procedure DateTimePickerInput1Change(Sender: TObject);
    procedure ds_membersearchDataChange(Sender: TObject; Field: TField);
    procedure EditSeacrhEmpKeyPress(Sender: TObject; var Key: char);
    procedure EdtSearchKeyPress(Sender: TObject; var Key: char);
    procedure EdtSHift1Change(Sender: TObject);
    procedure EdtShift2Change(Sender: TObject);
    procedure EdtShift3Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure updateAttendance(membercode:string;date:TDate);
  private
    FActiveField: TField;
    vTableNameAttendance:string;
    vLeaveBaseQuery,
    field_search_leave : String;

  public

  end;

var
  f_overtime: Tf_overtime;

implementation

uses
  u_datamodule;

{$R *.lfm}

{ Tf_overtime }


procedure Tf_overtime.FormCreate(Sender: TObject);
begin
  vTableNameAttendance:='t_attendance';
  DateTimePickerStart.Date := DM.NowX;
  DateTimePickerFinish.Date := DM.NowX;

  field_search_leave   := 'fc_leaveno';



  PFormInput.Visible   := False;
  If Not(qt_member.Active)   Then qt_member.Open;
  If Not(qt_overtime.Active)    Then qt_overtime.Open;

  If DBGridLeave.Enabled = False Then DBGridLeave.Enabled := True;
  DBGridLeave.Columns[0].Title.Color := clYellow;
end;

procedure Tf_overtime.MenuItem1Click(Sender: TObject);
begin
  updateAttendance(qt_overtimefc_membercode.AsString,qt_overtimefd_otdate.AsDateTime);
  ShowMessage('Overtime Berhasil diterapkan');
end;

procedure Tf_overtime.BtnNewDataClick(Sender: TObject);
begin
  DateTimePickerInput1.Date  := DM.NowX;

  DBGridLeave.Enabled := False;
  PFormInput.Visible  := True;
  PFooter.Visible     := False;

  qt_member.Close;
  qt_member.Open;
  qt_overtime.Append; // akal akalan seakan" create new data
  DateTimePickerInput1Change(Sender);
  //DBLookupComboBox2.SetFocus;
  EditSeacrhEmp.SetFocus;
end;

procedure Tf_overtime.BtnEdtData1Click(Sender: TObject);
Var
   vNoOt                                 : String;
   vCount, Reply, BoxStyle                  : Integer;
begin
   BoxStyle := MB_ICONQUESTION + MB_YESNO;
   Reply := Application.MessageBox('Press YES to save document', 'MessageBoxDemo', BoxStyle);

   If Reply = IDYES Then Begin
     qt_overtime.Cancel;  // cancel akal akalan seakan" create new data
      vNoOt := DM.CreateNoByYearYYPrefix('01','CREATE_OVERTIME','');

          With DM.MyBatch Do Begin
               Script.Clear;
               Script.Text :=
               'insert into t_overtime ' +
               'values (:fc_branch,:fc_otno, sysdate(), :fd_otdate, ' +
               '        :fc_ottype, :fv_description, :fc_membercode, '   +
               '        :fc_inputby, :fc_otstatus, :fc_otprogress, '+
               ':fc_daytype,:fc_shift1, :fc_timeinshift1,:fc_timeoutshift1,'+
               '        :fc_shift2, :fc_timeinshift2,:fc_timeoutshift2,'+
               '        :fc_shift3, :fc_timeinshift3,:fc_timeoutshift3,:fc_ti);'
               ;
               ParamByName('fc_branch').AsString        := '001';
               ParamByName('fc_otno').AsString       := vNoOt;
               ParamByName('fd_otdate').AsDate       := DateTimePickerInput1.Date;
               ParamByName('fc_ottype').AsString     := '';
               ParamByName('fv_description').AsString   := Memo1.Text;
               //ParamByName('fc_membercode').AsString    := qt_memberfc_membercode.AsString;
               ParamByName('fc_membercode').AsString    := qt_membersearchfc_membercode.AsString;
               ParamByName('fc_inputby').AsString       := 'ANDHIKA';
               ParamByName('fc_otstatus').AsString   := 'X';
               ParamByName('fc_otprogress').AsString := '';
               ParamByName('fc_daytype').AsString := EdtDaytype.Text;
               ParamByName('fc_shift1').AsString := EdtSHift1.Text;
               ParamByName('fc_timeinshift1').AsString := EdtTimeInShift1.Text;
               ParamByName('fc_timeoutshift1').AsString := EdtTimeOutShift1.Text;
               ParamByName('fc_shift2').AsString := EdtSHift2.Text;
               ParamByName('fc_timeinshift2').AsString := EdtTimeInShift2.Text;
               ParamByName('fc_timeoutshift2').AsString := EdtTimeOutShift2.Text;
               ParamByName('fc_shift3').AsString := EdtSHift3.Text;
               ParamByName('fc_timeinshift3').AsString := EdtTimeInShift3.Text;
               ParamByName('fc_timeoutshift3').AsString := EdtTimeOutShift3.Text;
               //ParamByName('fc_ti').AsString := qt_trxnrfc_trxcode.AsString;
               ParamByName('fc_ti').AsString := EdtNoRest.Text;
               Execute;
               //updateAttendance(qt_memberfc_membercode.AsString,DateTimePickerInput1.Date);
               updateAttendance(qt_membersearchfc_membercode.AsString,DateTimePickerInput1.Date);
          End;

      Memo1.Clear;
      FormCreate(Self);
      qt_overtime.Close; qt_overtime.Open;
      qt_overtime.Locate('fc_otno',vNoOt,[]);
      EdtNoRest.Text:='';
      DBGridLeave.Enabled := True;
      PFormInput.Visible:=False;
      PFooter.Visible:=True;

   End Else Begin
       // Spare....
   End;


end;

procedure Tf_overtime.BtnEdtData2Click(Sender: TObject);
begin
   qt_overtime.Cancel;  // cancel akal akalan seakan" create new data
   DBGridLeave.Enabled := True;
  PFormInput.Visible:=False;
  PFooter.Visible:=True;
end;

procedure Tf_overtime.BtnDELDataClick(Sender: TObject);
Var
Reply, BoxStyle: Integer;
begin
  BoxStyle := MB_ICONQUESTION + MB_YESNO;
  Reply    := Application.MessageBox('Are you sure ?', 'Confirmation', BoxStyle);
  If Reply = IDYES Then Begin
     With DM.MyBatch Do Begin
          Script.Text := 'delete from t_overtime where fc_otno = :fc_otno;';
          ParamByName('fc_otno').AsString    := qt_overtimefc_otno.AsString;
          Execute;
          Script.Text := 'delete from t_attendance where fd_date = :fd_date and fc_membercode = :fc_membercode;'
                         +' insert into t_attendance select * from t_attendance_backup where  fd_date = :fd_date and fc_membercode = :fc_membercode;' ;
          ParamByName('fd_date').AsDate    := qt_overtimefd_otdate.AsDateTime;
          ParamByName('fc_membercode').AsString    := qt_overtimefc_membercode.AsString;
          Execute;
          qt_overtime.Close;
          qt_overtime.Open;
     End;
  End;
end;

procedure Tf_overtime.BtnResetPeriodeClick(Sender: TObject);
begin
 with qt_overtime do begin
   Close;
   sql.Clear;
   SQL.Text := vq_qt_overtime + 'GROUP BY a.fc_branch, a.fc_leaveno ORDER BY a.fc_leaveno;';
   Open;
 end;
end;

procedure Tf_overtime.updateAttendance(membercode: string; date: TDate);
begin
  With DM.MyBatch do begin
    Script.Clear;
    Script.Text:= // update reference overtime
                    ' update '+vTableNameAttendance+' a '
                    +' inner join t_overtime b on a.fc_membercode = b.fc_membercode and a.fd_date = b.fd_otdate '
                    +' LEFT OUTER JOIN t_overtime c ON (a.fc_membercode = c.fc_membercode and a.fd_date = b.fd_otdate AND '
                    +' (b.fd_otcreatedate < c.fd_otcreatedate OR (b.fd_otcreatedate = c.fd_otcreatedate AND b.fc_otno < c.fc_otno) ) ) '
                    +' set '
                    +' a.fc_shift1 = b.fc_shift1 , '
                    +' a.fc_timeinshift1 = b.fc_timeinshift1 , '
                    +' a.fc_timeoutshift1 = b.fc_timeoutshift1, '
                    +' a.fc_shift2 = b.fc_shift2 , '
                    +' a.fc_timeinshift2 = b.fc_timeinshift2 , '
                    +' a.fc_timeoutshift2 = b.fc_timeoutshift2, '
                    +' a.fc_shift3 = b.fc_shift3 , '
                    +' a.fc_timeinshift3 = b.fc_timeinshift3 , '
                    +' a.fc_timeoutshift3 = b.fc_timeoutshift3, '
                    +' a.fv_description = b.fv_description, '
                    +' a.fc_daytype = b.fc_daytype , '
                    +' a.fc_ti = b.fc_ti '
                    +' where a.fd_date = :date and a.fc_membercode = :membercode AND c.fc_otno IS NULL;'
                     // update fc_daytype
                    +' update '+vTableNameAttendance+' a '
                    +' set '
                    +' a.fc_daytype = if(a.fc_shift1 is null or a.fc_shift1 in(""), "",a.fc_daytype ) '
                    +' where a.fd_date = :date and a.fc_membercode = :membercode;'
                  //update shift
                    +' update '+vTableNameAttendance+' a '
                    +' left join t_dailytimerange b on b.fc_timerangecode = a.fc_shift1 '
                    +' left join t_dailytimerange c on c.fc_timerangecode = a.fc_shift2 '
                    +' left join t_dailytimerange d on d.fc_timerangecode = a.fc_shift3  '
                    +' set  '
                    +' a.fc_workinshift1 = COALESCE(b.fc_workin,NULL), '
                    +' a.fc_workoutshift1 = COALESCE(b.fc_workout,NULL) , '
                    +' a.fn_totalbreakshift1 =COALESCE(TIME_TO_SEC( '
                    +' if( b.fc_workbreakin > b.fc_workbreakout , '
                    +' subtime(b.fc_workbreakin,b.fc_workbreakout ) , '
                    +' subtime(b.fc_workbreakout,b.fc_workbreakin ) '
                    +' ) )/60/60,NULL), '
                    +' a.fc_workinshift2 = COALESCE(c.fc_workin,NULL), '
                    +' a.fc_workoutshift2 = COALESCE(c.fc_workout,NULL) , '
                    +' a.fn_totalbreakshift2 = COALESCE(TIME_TO_SEC(  '
                    +' if( b.fc_workbreakin > b.fc_workbreakout , '
                    +' subtime(c.fc_workbreakin,c.fc_workbreakout ) , '
                    +' subtime(c.fc_workbreakout,c.fc_workbreakin ) '
                    +' ) )/60/60,NULL) , '
                    +' a.fc_workinshift3 = COALESCE(d.fc_workin,NULL), '
                    +' a.fc_workoutshift3 = COALESCE(d.fc_workout,NULL) , '
                    +' a.fn_totalbreakshift3 = COALESCE(TIME_TO_SEC(  '
                    +' if( d.fc_workbreakin > d.fc_workbreakout , '
                    +' subtime(d.fc_workbreakin,d.fc_workbreakout ) , '
                    +' subtime(d.fc_workbreakout,d.fc_workbreakin ) '
                    +' ) )/60/60,NULL) '
                    +' where a.fd_date = :date and a.fc_membercode = :membercode;'
                    // update totalhour reference
                    +' update '+vTableNameAttendance+' '
                    +' set '
	            +' fn_totalhourshift1 = TIME_TO_SEC( '
		    +'	if( fc_workinshift1 > fc_workoutshift1 , '
                    +' subtime(addtime(fc_workoutshift1 ,"24:00"),fc_workinshift1 ) , '
		    +' subtime(fc_workoutshift1,fc_workinshift1 ) '
		    +' 	) )/60/60, '
	            +' fn_totalhourshift2 = TIME_TO_SEC( '
		    +' if( fc_workinshift2 > fc_workoutshift2 , '
		    +' subtime(addtime(fc_workoutshift2 ,"24:00"),fc_workinshift2 ) , '
		    +' subtime(fc_workoutshift2,fc_workinshift2 ) '
		    +' ) )/60/60, '
	            +' fn_totalhourshift3 = TIME_TO_SEC( '
		    +' if( fc_workinshift3 > fc_workoutshift3 , '
		    +' subtime(addtime(fc_workoutshift3 ,"24:00"),fc_workinshift3 ) , '
		    +' subtime(fc_workoutshift3,fc_workinshift3 ) '
		    +' ) )/60/60 '
                    +' where fd_date = :date and fc_membercode = :membercode;'
                    //update timein / out shift 1,2,3 date out
                    +' update '+ vTableNameAttendance + ' a '
                    +' set '
                    +' a.fc_timeinshift1 = if( a.fc_timeinshift1 = "" or a.fc_timeinshift1 is NULL,a.fc_workinshift1 ,a.fc_timeinshift1 ), '
                    +' a.fc_timeoutshift1 = if( a.fc_timeoutshift1 = "" or a.fc_timeoutshift1 is NULL,a.fc_workoutshift1 ,a.fc_timeoutshift1 ), '
                    +' a.fc_timeinshift2 = if( a.fc_timeinshift2 = "" or a.fc_timeinshift2 is NULL,a.fc_workinshift2 ,a.fc_timeinshift2 ), '
                    +' a.fc_timeoutshift2 = if( a.fc_timeoutshift2 = "" or a.fc_timeoutshift2 is NULL,a.fc_workoutshift2 ,a.fc_timeoutshift2 ), '
                    +' a.fc_timeinshift3 = if( a.fc_timeinshift3 = "" or a.fc_timeinshift3 is NULL,a.fc_workinshift3 ,a.fc_timeinshift3 ), '
                    +' a.fc_timeoutshift3 = if( a.fc_timeoutshift3 = "" or a.fc_timeoutshift3 is NULL,a.fc_workoutshift3 ,a.fc_timeoutshift3 ) '
                    +' where a.fd_date = :date and a.fc_membercode = :membercode;'
                    //Update Totalhour  & effective Hour time
                    +' update '+vTableNameAttendance +' a '
                    +' set '
                    +' a.fn_totaltimeshift1 = TIME_TO_SEC( '
	            +' if( a.fc_timeinshift1 > a.fc_timeoutshift1 , '
		    +' subtime(addtime(a.fc_timeoutshift1,"24:00"), a.fc_timeinshift1 ) , '
		    +' subtime(a.fc_timeoutshift1, a.fc_timeinshift1 ) '
	            +' ) )/60/60, '
                    +' a.fn_totaltimeshift2 = TIME_TO_SEC( '
	            +' if( a.fc_timeinshift2 > a.fc_timeoutshift2 , '
		    +' subtime(addtime(a.fc_timeoutshift2,"24:00"), a.fc_timeinshift2 ) , '
		    +' subtime(a.fc_timeoutshift2, a.fc_timeinshift2 ) '
	            +' ) )/60/60, '
                    +' a.fn_totaltimeshift3 = TIME_TO_SEC( '
	            +' if( a.fc_timeinshift3 > a.fc_timeoutshift3 , '
		    +' subtime(addtime(a.fc_timeoutshift3,"24:00"), a.fc_timeinshift3 ) , '
		    +' subtime(a.fc_timeoutshift3, a.fc_timeinshift3 ) '
	            +' ) )/60/60, '
                    +' a.fn_totalhour = a.fn_totaltimeshift1 + coalesce(a.fn_totaltimeshift2,0 ) + coalesce(a.fn_totaltimeshift3,0), '
                    +' a.fn_effectivehour = a.fn_totalhour - '
                       +'case when ( a.fn_totalhour >= ( (2 * if( a.fn_totalhourshift1 < 8 , 8 , a.fn_totalhourshift1 ) ) + 5 ) ) then 3 '
                       +' when ( a.fn_totalhour >= ( if( a.fn_totalhourshift1 < 8 , 8 , a.fn_totalhourshift1 ) + 5 ) ) then 2 '
                       +' when ( a.fn_totalhour >= 5 ) then if(a.fn_totalhourshift1 = 5, if(a.fn_totalhour > 5,1,0) , 1) else 0 end'
                    +' where a.fd_date = :date and a.fc_membercode = :membercode;'
                    +' update ' + vTableNameAttendance +' a '
                    +' set '
                    +' fn_jumlahshift = '                    // untuk mengatasi hari sabtu 8jamkerja dianggap 8+4 baru menambah 1 shift (excel)  (RECHECK lAGI)
                       +' case when ( a.fn_effectivehour >= ( (2 * if( a.fn_totalhourshift1 < 8 , 7 , a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) ) + 4 ) ) then 3 '
                       +' when ( a.fn_effectivehour >= ( if( a.fn_totalhourshift1 < 8 , 7 , a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) + 4 ) ) then 2 '
                       +' when ( a.fn_effectivehour >= 4   ) then 1 when ( a.fn_effectivehour < 4   ) then 0 else null end'
                    +' where a.fd_date = :date and a.fc_membercode = :membercode;'
                   //update TI1,TI2,TI3
                   +' update ' + vTableNameAttendance
                   +' set '
                   +' fn_effectivehour = '
                   +' case '
	           +' when fc_ti in ("TI1","NR1") then '
		   +' fn_effectivehour + 1 '
	           +' when fc_ti in ("TI2","NR2") then '
		   +' fn_effectivehour + 2 '
	           +' when fc_ti in ("TI3","NR3") then '
		   +' fn_effectivehour + 3 '
	           +' else '
		   +' fn_effectivehour '
                   +' end '
                   +' where fd_date = :date and fc_membercode = :membercode;'
                    //OT Update
                   +' update '+vTableNameAttendance
                   +' set '
                   +' fn_ot1 = '
                   +' case when fc_daytype = "N" then '
                                                      //untuk 5 jam kerja effective hour 5 jam karena istirahat setelah pulang   (RECHECK lAGI)
	           +' case when fn_effectivehour - ( fn_totalhourshift1 - if(fn_totalhourshift1 > 5,fn_totalbreakshift1,0) ) >= 1 then '
		   +' 1 '
	           +' when fn_effectivehour - (fn_totalhourshift1 - if(fn_totalhourshift1 > 5,fn_totalbreakshift1,0) ) >= 0 then '
		   +' fn_effectivehour - (fn_totalhourshift1 - if(fn_totalhourshift1 > 5,fn_totalbreakshift1,0) ) '
	           +' else 0 end '
                   +' else if(fc_daytype is null or fc_daytype = "" ,NULL,0) end , '
                   +' fn_ot2 = '
                   +' case when fc_daytype = "N" then '
	           +' case when fn_ot1 >= 1 then '
		   +' fn_effectivehour - fn_ot1 - (fn_totalhourshift1 - if(fn_totalhourshift1 > 5,fn_totalbreakshift1,0) ) '
	           +' else 0 end '
                   +' when fc_daytype in ("H","O") then '
	           +' case when fn_effectivehour - 7 >= 0 then '
		   +' 7 '
	           +' when fn_effectivehour - 7 <= 0 then '
		   +' fn_effectivehour '
	           +' else 0 end '
                   +' else if(fc_daytype is  null or fc_daytype = "" ,NULL,0) end , '
                   +' fn_ot3 = '
                   +' case when fc_daytype in ("H","O") then '
	           +' case when fn_effectivehour - 7 >= 1 then '
		   +' 1 '
	           +' when fn_effectivehour - 7 <= 1 then '
		   +' if(fn_effectivehour - 7 > 0, fn_effectivehour - 7 ,0 )  '
	           +' else 0 end '
                   +' else if(fc_daytype is  null or fc_daytype = "" ,NULL,0) end , '
                   +' fn_ot4 = '
                   +' case when fc_daytype in ("H","O") then '
	           +' case when fn_ot3 >= 1 then '
		   +' fn_effectivehour - fn_ot3 - 7 '
	           +' else 0 end '
                   +' when fc_daytype in ("H+") then '
	           +' case when fn_effectivehour - 7 >= 0 then '
		   +' 7 '
	           +' when fn_effectivehour - 7 <= 0 then '
		   +' fn_effectivehour '
	           +' else 0 end '
                   +' else if(fc_daytype is  null or fc_daytype = "" ,NULL,0) end, '
                   +' fn_ot5 = '
                   +' case when fc_daytype in ("H+") then '
	           +' case when fn_effectivehour - 7 >= 1 then '
		   +' 1 '
	           +' when fn_effectivehour - 7 <= 1 then '
		   +' if( fn_effectivehour - 7 > 0 ,fn_effectivehour - 7,0) '
	           +' else 0 end '
                   +' else if(fc_daytype is  null or fc_daytype = "" ,NULL,0) end , '
                   +' fn_ot6 = '
                   +' case when fc_daytype in ("H+") then '
	           +' case when fn_ot5 >=1 then '
		   +' fn_effectivehour - fn_ot5 - 7 '
	           +' else 0 end '
                   +' else if(fc_daytype is  null or fc_daytype = "" ,NULL,0) end '
                   +' where fd_date = :date and fc_membercode = :membercode;';
      ParamByName('date').AsDate:=date;
      ParamByName('membercode').AsString:=membercode;
    Execute;
  end;
end;

procedure Tf_overtime.BtnSearchClick(Sender: TObject);
begin
  With qt_overtime do Begin
       Close;
       SQL.Clear;
       if ComboBoxFilter.ItemIndex = -1 then begin
       end else if ComboBoxFilter.ItemIndex = 0 then begin
          SQL.Text:= vq_qt_overtime + ' and  b.fc_membername1 like :search '  ;
       end else if ComboBoxFilter.ItemIndex = 1 then begin
          SQL.Text:= vq_qt_overtime + ' and  a.fc_membercode like :search ';
       end else if ComboBoxFilter.ItemIndex = 2 then begin
          SQL.Text:= vq_qt_overtime + ' and  a.fc_otno like :search ' ;
       end;
       ParamByName('datestart').AsDate:=DateTimePickerStart.Date;
       ParamByName('datefinish').AsDate:=DateTimePickerFinish.Date;
       ParamByName('search').AsString:='%'+EdtSearch.Text+'%';
       Open;
  End;
end;

procedure Tf_overtime.DateTimePicker1Change(Sender: TObject);
begin
  if FormatDateTime('MM',DateTimePicker1.Date) = '01' then begin
     DateTimePickerStart.Date:=StrToDate('16'+DateSeparator+'12'+DateSeparator+IntToStr( StrToInt( FormatDateTime('YYYY',DateTimePicker1.Date) ) - 1 ) );
     DateTimePickerFinish.Date:=StrToDate('15'+DateSeparator+'01'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
  end else if FormatDateTime('MM',DateTimePicker1.Date) = '02' then begin
     DateTimePickerStart.Date:=StrToDate('16'+DateSeparator+'01'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
     DateTimePickerFinish.Date:=StrToDate('15'+DateSeparator+'02'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
  end else if FormatDateTime('MM',DateTimePicker1.Date) = '03' then begin
     DateTimePickerStart.Date:=StrToDate('16'+DateSeparator+'02'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
     DateTimePickerFinish.Date:=StrToDate('15'+DateSeparator+'03'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
  end else if FormatDateTime('MM',DateTimePicker1.Date) = '04' then begin
     DateTimePickerStart.Date:=StrToDate('16'+DateSeparator+'03'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
     DateTimePickerFinish.Date:=StrToDate('15'+DateSeparator+'04'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
  end else if FormatDateTime('MM',DateTimePicker1.Date) = '05' then begin
     DateTimePickerStart.Date:=StrToDate('16'+DateSeparator+'04'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
     DateTimePickerFinish.Date:=StrToDate('15'+DateSeparator+'05'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
  end else if FormatDateTime('MM',DateTimePicker1.Date) = '06' then begin
     DateTimePickerStart.Date:=StrToDate('16'+DateSeparator+'05'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
     DateTimePickerFinish.Date:=StrToDate('15'+DateSeparator+'06'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
  end else if FormatDateTime('MM',DateTimePicker1.Date) = '07' then begin
     DateTimePickerStart.Date:=StrToDate('16'+DateSeparator+'06'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
     DateTimePickerFinish.Date:=StrToDate('15'+DateSeparator+'07'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
  end else if FormatDateTime('MM',DateTimePicker1.Date) = '08' then begin
     DateTimePickerStart.Date:=StrToDate('16'+DateSeparator+'07'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
     DateTimePickerFinish.Date:=StrToDate('15'+DateSeparator+'08'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
  end else if FormatDateTime('MM',DateTimePicker1.Date) = '09' then begin
     DateTimePickerStart.Date:=StrToDate('16'+DateSeparator+'08'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
     DateTimePickerFinish.Date:=StrToDate('15'+DateSeparator+'09'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
  end else if FormatDateTime('MM',DateTimePicker1.Date) = '10' then begin
     DateTimePickerStart.Date:=StrToDate('16'+DateSeparator+'09'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
     DateTimePickerFinish.Date:=StrToDate('15'+DateSeparator+'10'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
  end else if FormatDateTime('MM',DateTimePicker1.Date) = '11' then begin
     DateTimePickerStart.Date:=StrToDate('16'+DateSeparator+'10'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
     DateTimePickerFinish.Date:=StrToDate('15'+DateSeparator+'11'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
  end else if FormatDateTime('MM',DateTimePicker1.Date) = '12' then begin
     DateTimePickerStart.Date:=StrToDate('16'+DateSeparator+'11'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
     DateTimePickerFinish.Date:=StrToDate('15'+DateSeparator+'12'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
  end;
end;

procedure Tf_overtime.DateTimePickerInput1Change(Sender: TObject);
var
   q:TZQuery;
begin
  q := TZQuery.Create(Self);
 with q do begin
     Connection:=DM.MyData;
     SQL.Text:='SELECT * FROM t_attendance where fc_membercode = :fc_membercode and fd_date = :fd_date';
     ParamByName('fc_membercode').AsString:=qt_membersearchfc_membercode.AsString;
     ParamByName('fd_date').AsDate:=DateTimePickerInput1.Date;
     Open;
     Memo1.Text:=FieldByName('fv_description').AsString;
     EdtSHift1.Text:=Trim(FieldByName('fc_shift1').AsString);
     EdtTimeInShift1.Text:=Trim(FieldByName('fc_timeinshift1').AsString);
     EdtTimeOutShift1.Text:=Trim(FieldByName('fc_timeoutshift1').AsString);
     EdtSHift2.Text:=Trim(FieldByName('fc_shift2').AsString);
     EdtTimeInShift2.Text:=Trim(FieldByName('fc_timeinshift2').AsString);
     EdtTimeOutShift2.Text:=Trim(FieldByName('fc_timeoutshift2').AsString);
     EdtSHift3.Text:=Trim(FieldByName('fc_shift3').AsString);
     EdtTimeInShift3.Text:=Trim(FieldByName('fc_timeinshift3').AsString);
     EdtTimeOutShift3.Text:=Trim(FieldByName('fc_timeoutshift3').AsString);
     EdtDaytype.Text:=Trim(FieldByName('fc_daytype').AsString);
     EdtNoRest.Text:=Trim(FieldByName('fc_ti').AsString);
     Free;
 end;
end;

procedure Tf_overtime.ds_membersearchDataChange(Sender: TObject; Field: TField);
begin
  DateTimePickerInput1Change(sender);
end;

procedure Tf_overtime.EditSeacrhEmpKeyPress(Sender: TObject; var Key: char);
begin
  if Key = #13 then begin
     with qt_membersearch do begin
       Close;
       ParamByName('search').AsString:='%'+EditSeacrhEmp.Text+'%';
       Open;
     end;
  end;
end;

procedure Tf_overtime.EdtSearchKeyPress(Sender: TObject; var Key: char);
begin
  if Key = chr(13) then begin
    BtnSearchClick(Sender);
  end;
end;

procedure Tf_overtime.EdtSHift1Change(Sender: TObject);
var
   q: TZQuery;
begin
 q := TZQuery.Create(Self);
 with q do begin
     Connection:=DM.MyData;
     SQL.Text:='SELECT * FROM t_dailytimerange where fc_timerangecode = :shift';
     ParamByName('shift').AsString:=EdtSHift1.Text;
     Open;
     EdtTimeInShift1.Text:=FieldByName('fc_workin').AsString;
     EdtTimeOutShift1.Text:=FieldByName('fc_workout').AsString;
     Free;
 end;
 if EdtSHift1.Text='' then begin
   EdtSHift2.Enabled:=False;EdtTimeInShift2.Enabled:=False;EdtTimeOutShift2.Enabled:=False;
   EdtSHift2.Text:='';EdtTimeInShift2.Text:='';EdtTimeOutShift2.Text:='';
 end else begin
   EdtSHift2.Enabled:=True;EdtTimeInShift2.Enabled:=True;EdtTimeOutShift2.Enabled:=True;
 end;
end;

procedure Tf_overtime.EdtShift2Change(Sender: TObject);
var
   q: TZQuery;
begin
 q := TZQuery.Create(Self);
 with q do begin
     Connection:=DM.MyData;
     SQL.Text:='SELECT * FROM t_dailytimerange where fc_timerangecode = :shift';
     ParamByName('shift').AsString:=EdtSHift2.Text;
     Open;
     EdtTimeInShift2.Text:=FieldByName('fc_workin').AsString;
     EdtTimeOutShift2.Text:=FieldByName('fc_workout').AsString;
     Free;
 end;

 if EdtSHift2.Text='' then begin
   EdtSHift3.Enabled:=False;EdtTimeInShift3.Enabled:=False;EdtTimeOutShift3.Enabled:=False;
   EdtSHift3.Text:='';EdtTimeInShift3.Text:='';EdtTimeOutShift3.Text:='';
 end else begin
   EdtSHift3.Enabled:=True;EdtTimeInShift3.Enabled:=True;EdtTimeOutShift3.Enabled:=True;
 end;

end;

procedure Tf_overtime.EdtShift3Change(Sender: TObject);
var
   q: TZQuery;
begin
 q := TZQuery.Create(Self);
 with q do begin
     Connection:=DM.MyData;
     SQL.Text:='SELECT * FROM t_dailytimerange where fc_timerangecode = :shift';
     ParamByName('shift').AsString:=EdtSHift3.Text;
     Open;
     EdtTimeInShift3.Text:=FieldByName('fc_workin').AsString;
     EdtTimeOutShift3.Text:=FieldByName('fc_workout').AsString;
     Free;
 end;

end;

end.

