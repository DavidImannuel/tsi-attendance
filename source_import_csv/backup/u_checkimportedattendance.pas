unit u_checkimportedattendance;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, DBGrids,
  StdCtrls, DateTimePicker, ZDataset, db, Grids;

type

  { Tf_checkimportedattendance }

  Tf_checkimportedattendance = class(TForm)
    Button1: TButton;
    ComboBox1: TComboBox;
    DateTimePicker1: TDateTimePicker;
    ds_cekimportdata: TDataSource;
    DBGrid1: TDBGrid;
    Panel1: TPanel;
    Panel2: TPanel;
    qt_cekimportdata: TZQuery;
    qt_cekimportdatacekimport: TStringField;
    qt_cekimportdatafc_membercode: TStringField;
    qt_cekimportdatafc_membername1: TStringField;
    qt_cekimportdataperiode: TStringField;
    procedure Button1Click(Sender: TObject);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
  private

  public

  end;

var
  f_checkimportedattendance: Tf_checkimportedattendance;

implementation

{$R *.lfm}

{ Tf_checkimportedattendance }

procedure Tf_checkimportedattendance.Button1Click(Sender: TObject);
begin
  //ShowMessage(FormatDateTime('YYYYMM',DateTimePicker1.Date));
  with qt_cekimportdata do begin
    Close;
    SQL.Text:=' select a.fc_membercode ,a.fc_membername1, '
             +' if(b.fc_periode is not null,"SUDAH IMPORT DATA","BELUM IMPORT DATA") cekimport, '
             +' :periode periode from t_member a '
             +' left join '
             +' (select fc_membercode,fc_periode from t_attendance where fc_periode = :periode '
             +' group by fc_membercode,fc_periode ) b on a.fc_membercode = b.fc_membercode ';
    if ComboBox1.ItemIndex = 0 then begin
      SQL.Add(' ');
    end else if ComboBox1.ItemIndex = 1 then begin
      SQL.Add(' where b.fc_periode is null');
    end else if ComboBox1.ItemIndex = 2 then begin
       SQL.Add(' where b.fc_periode is not null');
    end;
    ParamByName('periode').AsString:=FormatDateTime('YYYYMM',DateTimePicker1.Date);
    //ParamByName('periode').AsString:='202001';
    Open;
  end;
end;

procedure Tf_checkimportedattendance.DBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Field:TField;
begin
  Field := Column.Field;
  if Assigned(Field) and SameText(Field.FieldName, 'cekimport') then
  begin
    if Field.AsString = 'BELUM IMPORT DATA' then begin
      DBGrid1.Canvas.Font.Color := clRed;
    end else begin
      DBGrid1.Canvas.Font.Color := clGreen;
    end;
  end;
  DBGrid1.DefaultDrawColumnCell(Rect, DataCol, Column, State);
end;

end.

