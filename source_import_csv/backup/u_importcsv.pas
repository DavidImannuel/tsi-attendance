unit u_importcsv;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, StdCtrls,
  Buttons, ComCtrls, DBGrids, DateTimePicker, DBDateTimePicker, LR_DBSet,
  LR_Class, ZConnection, ZSqlProcessor, ZDataset, ZSqlUpdate, dateutils, db,
  LCLType, Menus, DBCtrls;

type

  { Tf_importcsv }

  Tf_importcsv = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtnReplaceRSCode: TBitBtn;
    BitBtnClearMemoReplaceRSCode: TBitBtn;
    BitBtnClearMemo: TBitBtn;
    BitBtnCreateGrid: TBitBtn;
    BitBtnCreateGrid1: TBitBtn;
    BitBtnMergeShiftAttendance: TBitBtn;
    BitBtnMergeShiftAttendance1: TBitBtn;
    BitBtnOpenfile: TBitBtn;
    BitBtnOpenfile1: TBitBtn;
    BitBtnPasteMemo: TBitBtn;
    BitBtnRefreshGridTempRotateShift: TBitBtn;
    BitBtnRefreshGrid1: TBitBtn;
    BtnDelimiterMemo: TBitBtn;
    btnSaveFile: TBitBtn;
    Button1: TButton;
    CheckBox1: TCheckBox;
    ComboBox1: TComboBox;
    ComboBoxOptReplace: TComboBox;
    DateTimePickerPeriode: TDateTimePicker;
    ds_temp_attendance: TDataSource;
    DateStartImportShiftRotate: TDateTimePicker;
    DateFinishImportShiftRotate: TDateTimePicker;
    DBGrid1: TDBGrid;
    DBGridTempRotateShift: TDBGrid;
    ds_temprotateshift: TDataSource;
    EditReplaceRotateShift: TEdit;
    EditOpenFile: TEdit;
    EditOpenFile1: TEdit;
    EditSaveFile: TEdit;
    ImageList: TImageList;
    Label1: TLabel;
    Label10: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    LabelDays: TLabel;
    MemoReplaceRotateShiftCode: TMemo;
    MemoDelimeter: TMemo;
    Notebook1: TNotebook;
    OpenDialog: TOpenDialog;
    Page1: TPage;
    Panel1: TPanel;
    Panel10: TPanel;
    Panel11: TPanel;
    Panel12: TPanel;
    Panel13: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    Panel6: TPanel;
    Panel7: TPanel;
    Panel8: TPanel;
    PopupMenuDBGridRotateShift: TPopupMenu;
    ProgessBarDelimeter: TProgressBar;
    ProgressBar1: TProgressBar;
    ProgressBar2: TProgressBar;
    qt_attendancefc_daytype: TStringField;
    qt_attendancefc_membercode: TStringField;
    qt_attendancefc_membername1: TStringField;
    qt_attendancefc_memberposition: TStringField;
    qt_attendancefc_periode: TStringField;
    qt_attendancefc_status: TStringField;
    qt_attendancefc_ti: TStringField;
    qt_attendancefd_date: TDateField;
    qt_attendancefd_dategenerate: TDateTimeField;
    qt_attendancefd_realdatein: TDateTimeField;
    qt_attendancefd_realdateout: TDateTimeField;
    qt_attendancefn_effectivehour: TFloatField;
    qt_attendancefn_ot1: TFloatField;
    qt_attendancefn_ot2: TFloatField;
    qt_attendancefn_ot3: TFloatField;
    qt_attendancefn_ot4: TFloatField;
    qt_attendancefn_ot5: TFloatField;
    qt_attendancefn_ot6: TFloatField;
    qt_attendancefn_totalhour: TFloatField;
    qt_attendancefv_description: TStringField;
    qt_attendanceshift: TStringField;
    qt_check_reference: TZReadOnlyQuery;
    REPLACE: TMenuItem;
    SaveDialog: TSaveDialog;
    Splitter1: TSplitter;
    MyProcessor: TZSQLProcessor;
    qt_temprotateshift: TZQuery;
    Splitter2: TSplitter;
    qt_temp_attendance: TZQuery;
    Splitter3: TSplitter;
    TAKE1DLEFT: TMenuItem;
    TAKE2DLEFT: TMenuItem;
    TAKELEFTRIGHTCHAR: TMenuItem;
    TAKEMIDCHAR: TMenuItem;
    TRIM1D: TMenuItem;
    TRIM2D: TMenuItem;
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtnClearMemoReplaceRSCodeClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtnClearMemoClick(Sender: TObject);
    procedure BitBtnCreateGrid1Click(Sender: TObject);
    procedure BitBtnCreateGridClick(Sender: TObject);
    procedure BitBtnMergeShiftAttendance1Click(Sender: TObject);
    procedure BitBtnMergeShiftAttendanceClick(Sender: TObject);
    procedure BitBtnOpenfile1Click(Sender: TObject);
    procedure BitBtnOpenfileClick(Sender: TObject);
    procedure BitBtnPasteMemoClick(Sender: TObject);
    procedure BitBtnRefreshGridTempRotateShiftClick(Sender: TObject);
    procedure BitBtnReplaceRSCodeClick(Sender: TObject);
    procedure BtnDelimiterMemoClick(Sender: TObject);
    procedure btnSaveFileClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure CheckBox1Change(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure ComboBoxOptReplaceChange(Sender: TObject);
    procedure DateTimePickerPeriodeChange(Sender: TObject);
    procedure DateTimePicker1Exit(Sender: TObject);
    procedure DateFinishImportShiftRotateExit(Sender: TObject);
    procedure DBGridTempRotateShiftCellClick(Column: TColumn);
    procedure DBGridTempRotateShiftDblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PaintGrid;
    procedure ImportShiftRotateAttendance(TableName:String);
    FUNCTION Func_checkReference(vKeyFind, vFieldReplace : String) : String;
    procedure REPLACEClick(Sender: TObject);
    procedure TAKE1DLEFTClick(Sender: TObject);
    procedure TAKE2DLEFTClick(Sender: TObject);
    procedure TAKELEFTRIGHTCHARClick(Sender: TObject);
    procedure TAKEMIDCHARClick(Sender: TObject);
    procedure TRIM1DClick(Sender: TObject);
    procedure TRIM2DClick(Sender: TObject);
    procedure SetUpGridPickList(FieldName,SQL: String;DBGrid : TDBGrid);
    procedure ClearCharTempRotateShift;
    Procedure RunImportShiftThread;
  private
    vTableNameTempRotateShift      : String;      // Variable Nama Table. Ini nanti iku USERID
    vTableNameTempAttendance      : String;      // Variable Nama Table. Ini nanti iku USERID
    vCountFieldDate : Integer;     // Total "hari" yang di block di Excel
    vTableNameAttendance : String;
    vUPD_FieldName, vUPD_OldValue, vUPD_EmployeeName, vUPD_Blank : String;
    curRecNo : LongInt;
    curField : TField;

  public

  end;

  THackDBGrid = class(TDBGrid);

  { ThreadImportShift }

  ThreadImportShift = class(TThread)
    protected
      procedure Execute; override;
  end;

var
  f_importcsv: Tf_importcsv;

implementation

uses u_datamodule,u_checkimportedattendance;

{$R *.lfm}

{ ThreadImportShift }

procedure ThreadImportShift.Execute;
begin
  with f_importcsv do begin
   Label2.Caption:='IMPORTING SHIFT ROTATE ( LOADING... )';
   Label2.Font.Color:=clBlue;
   DBGridTempRotateShift.Enabled:=False;
   ImportShiftRotateAttendance(vTableNameAttendance);
   ImportShiftRotateAttendance('t_attendance_backup');
   DBGridTempRotateShift.Enabled:=True;
   Label2.Caption:='IMPORTING SHIFT ROTATE ( SUCCESS. )';
   Label2.Font.Color:=clGreen;
  end;
end;

{ Tf_importcsv }
function Tf_importcsv.Func_checkReference(vKeyFind, vFieldReplace: String
  ): String;
Begin
     With qt_check_reference Do Begin
          Close;
          SQL.Clear;
          SQL.Text  :=
                'SELECT ' + vFieldReplace + ' AS fc_refis '
              + ' FROM '  + vTableNameTempRotateShift
              + ' WHERE (fc_nameis = :fc_nameis) '
              + ' AND   (fc_blank  = :fc_blankis); ';
              ParamByName('fc_nameis').AsString   := vKeyFind;
              ParamByName('fc_blankis').AsString  := 'O';
          Open;

          If qt_check_reference.RecordCount = 1 Then
             Result := qt_check_reference.FieldByName('fc_refis').AsString
          Else
             Result := '*';
     end;
end;

procedure Tf_importcsv.REPLACEClick(Sender: TObject);
Var
   xOldValue : String;
begin
  // REPLACE A / B (LABOR ONLY)
  xOldValue := Func_checkReference(vUPD_OldValue, vUPD_FieldName);

     With MyProcessor Do Begin
       Script.Clear;
       Script.Text   :=
                'UPDATE ' + vTableNameTempRotateShift + ' SET '
              + vUPD_FieldName + ' = CASE WHEN length('+vUPD_FieldName+') = 1 then '
              +' CASE WHEN  '+vUPD_FieldName + ' =  ' + QuotedStr('B') + ' then 2 '
              + ' WHEN '+vUPD_FieldName + ' =  ' + QuotedStr('A') + ' then 1 end '
              + 'else '+ vUPD_FieldName +' end '
              + ' WHERE (fc_nameis  = '+QuotedStr(vUPD_EmployeeName)+') '
              + ' AND   (fc_blank  = '+QuotedStr(vUPD_Blank)+');';
       Execute;

    End;
    BitBtnRefreshGridTempRotateShiftClick(Sender);
end;

procedure Tf_importcsv.TAKE1DLEFTClick(Sender: TObject);
Var
   xOldValue : String;
begin
  // MENGAMBIL 1 DIGIT SEBELAH KIRI
  xOldValue := Func_checkReference(vUPD_OldValue, vUPD_FieldName);
  With MyProcessor Do Begin
       Script.Clear;
       Script.Text   :=
                'UPDATE ' + vTableNameTempRotateShift + ' SET '
              + vUPD_FieldName + '= LEFT('+ vUPD_FieldName +',1)'
              + ' WHERE (fc_nameis  = '+QuotedStr(vUPD_EmployeeName)+') '
              + ' AND   (fc_blank  = '+QuotedStr(vUPD_Blank)+');';


       Execute;

  End;
  BitBtnRefreshGridTempRotateShiftClick(Sender);
end;

procedure Tf_importcsv.TAKE2DLEFTClick(Sender: TObject);
Var
   xOldValue : String;
begin
  // MENGAMBIL 1 DIGIT SEBELAH KIRI
  xOldValue := Func_checkReference(vUPD_OldValue, vUPD_FieldName);
  With MyProcessor Do Begin
       Script.Clear;
       Script.Text   :=
                'UPDATE ' + vTableNameTempRotateShift + ' SET '
              + vUPD_FieldName + '= LEFT('+ vUPD_FieldName +',2)'
              + ' WHERE (fc_nameis  = '+QuotedStr(vUPD_EmployeeName)+') '
              + ' AND   (fc_blank  = '+QuotedStr(vUPD_Blank)+');';



       Execute;

  End;
  BitBtnRefreshGridTempRotateShiftClick(Sender);
end;

procedure Tf_importcsv.TAKELEFTRIGHTCHARClick(Sender: TObject);
Var
   xOldValue : String;
begin
  // TAKE LEFT / RIGTH SIDE CHAR
  xOldValue := Func_checkReference(vUPD_OldValue, vUPD_FieldName);

     With MyProcessor Do Begin
       Script.Clear;
       Script.Text   :=
                'UPDATE ' + vTableNameTempRotateShift + ' SET '
              + vUPD_FieldName + ' = CASE WHEN length('+vUPD_FieldName+') = 4 then '
              + ' CONCAT(LEFT('+vUPD_FieldName+',1),RIGHT('+vUPD_FieldName+',1)) ELSE '+ vUPD_FieldName +' END '
              + ' WHERE (fc_nameis  = '+QuotedStr(vUPD_EmployeeName)+') '
              + ' AND   (fc_blank  = '+QuotedStr(vUPD_Blank)+');';
       Execute;

    End;
     BitBtnRefreshGridTempRotateShiftClick(Sender);
end;

procedure Tf_importcsv.TAKEMIDCHARClick(Sender: TObject);
Var
   xOldValue : String;
begin
  // MENGAMBIL 1 / 2 DIGIT TENGAH KARAKTER
  xOldValue := Func_checkReference(vUPD_OldValue, vUPD_FieldName);

     With MyProcessor Do Begin
       Script.Clear;
       Script.Text   :=
                'UPDATE ' + vTableNameTempRotateShift + ' SET '
              + vUPD_FieldName + '= SUBSTRING('+ vUPD_FieldName +',2,CASE WHEN length('+vUPD_FieldName+') < 4 then 1 else 2 end )'
              + ' WHERE (fc_nameis  = '+QuotedStr(vUPD_EmployeeName)+') '
              + ' AND   (fc_blank  = '+QuotedStr(vUPD_Blank)+');';
       Execute;

    End;
     BitBtnRefreshGridTempRotateShiftClick(Sender);
end;

procedure Tf_importcsv.TRIM1DClick(Sender: TObject);
Var
   xOldValue : String;
begin
  // MENGHAPUS 1 DIGIT SEBELAH KIRI
  xOldValue := Func_checkReference(vUPD_OldValue, vUPD_FieldName);
  With MyProcessor Do Begin
       Script.Clear;
       Script.Text   :=
                'UPDATE ' + vTableNameTempRotateShift + ' SET '
              + vUPD_FieldName + '= SUBSTRING('+ vUPD_FieldName +',2,100)'
              + ' WHERE (fc_nameis  = '+QuotedStr(vUPD_EmployeeName)+') '
              + ' AND   (fc_blank  = '+QuotedStr(vUPD_Blank)+');';
       Execute;

  End;
  BitBtnRefreshGridTempRotateShiftClick(Sender);
end;

procedure Tf_importcsv.TRIM2DClick(Sender: TObject);
Var
   xOldValue : String;
begin
  // MENGHAPUS 2 DIGIT SEBELAH KIRI
  xOldValue := Func_checkReference(vUPD_OldValue, vUPD_FieldName);
  With MyProcessor Do Begin
       Script.Clear;
       Script.Text   :=
                'UPDATE ' + vTableNameTempRotateShift + ' SET '
              + vUPD_FieldName + '= SUBSTRING('+ vUPD_FieldName +',3,100)'
              + ' WHERE (fc_nameis  = '+QuotedStr(vUPD_EmployeeName)+') '
              + ' AND   (fc_blank  = '+QuotedStr(vUPD_Blank)+');';
       Execute;

  End;
  BitBtnRefreshGridTempRotateShiftClick(Sender);
end;

procedure Tf_importcsv.SetUpGridPickList(FieldName, SQL: String;DBGrid :TDBGrid);
var
   SLPickList: TStringList;
   Query: TZQuery;
   i: Integer;
begin
  SLPickList:= TStringList.Create;
  Query:=TZQuery.Create(Self);
  try
    Query.Connection := DM.MyData;
    Query.SQL.Text:=SQL;
    Query.Open;
    while not Query.EOF do begin
      SLPickList.Add(Query.Fields[0].AsString);
      Query.Next;
    end;
    for i:= 0 to DBGrid.Columns.Count -1 do begin
      if DBGrid.Columns[i].FieldName = FieldName then begin
         DBGrid.Columns[i].PickList := SLPickList;
         Break;
      end;
    end;
  finally
    SLPickList.Free;
    Query.Free;
  end;
end;

procedure Tf_importcsv.ClearCharTempRotateShift;
var
   iField:Integer;
begin
  //remove /trim unprintable char
  for iField:= 1 to qt_temprotateshift.FieldCount - 3 do begin

     with MyProcessor do begin
        Script.Clear;
        Script.Text:= ' update '
                    + vTableNameTempRotateShift
                    + ' set fc_date'+IntToStr(iField)+' = trim(REPLACE(fc_date'+IntToStr(iField)+', CHAR(13), "") )';
        Execute;
     end;
   end;
end;

procedure Tf_importcsv.RunImportShiftThread;
begin
  With ThreadImportShift.Create(True) do begin
    Start;FreeOnTerminate:=True;
  end;
end;

procedure Tf_importcsv.ImportShiftRotateAttendance(TableName: String);
Var
  iField:Integer;
  vPB:TProgressBar;
begin

  if TableName = 't_attendance' then vPB:=ProgressBar2 else vPB:=ProgressBar1;

  //ShowMessage('Insert starting, Please wait....');
  vPb.Max:=qt_temprotateshift.FieldCount - 3;
  for iField:= 1 to qt_temprotateshift.FieldCount - 3 do begin

     //insert
     with MyProcessor do begin
        Script.Clear;
        Script.Text:= ' insert into '+TableName
                    +'(fc_branch ,fd_dategenerate,fc_periode ,fd_date, fc_membercode,fc_memberattendancetype ,fc_shift1 ,fc_shift2 ,fc_shift3,fc_daytype,'
                    + 'fc_workinshift1,fc_workoutshift1,fn_totalbreakshift1,fc_workinshift2,fc_workoutshift2,fn_totalbreakshift2,fc_workinshift3,fc_workoutshift3,fn_totalbreakshift3,'
                    +' fc_timeinshift1,fc_timeoutshift1,fc_timeinshift2,fc_timeoutshift2,fc_timeinshift3,fc_timeoutshift3,'
                    +' fn_totalhourshift1,fn_totalhourshift2,fn_totalhourshift3,fn_totaltimeshift1,fn_totaltimeshift2,fn_totaltimeshift3,'
                    +' fn_totalhour,fn_effectivehour,fc_ti,fn_ot1,fn_ot2,fn_ot3,fn_ot4,fn_ot5,fn_ot6,fn_jumlahshift1,fn_jumlahshift2,fn_jumlahshift3,fn_jumlahshift) '
                    + ' select '
                    + ' "001" fc_branch, '
                    + ' SYSDATE() fd_dategenerate, '
                    + ' date_format(:periode,"%Y%m") fc_periode,'
                    + ' date_add(:datestart, INTERVAL :i day) fd_date, '
                    + ' b.fc_membercode , '
                    + ' b.fc_memberattendancetype, '
                    + ' left(a.fc_date'+IntToStr(iField)+',1) fc_shift1 , '
                    + ' case '
	            + ' when length(a.fc_date'+IntToStr(iField)+') > 1  then mid(a.fc_date'+IntToStr(iField)+',2,1) '
	            + ' else "" '
                    + ' end fc_shift2, '
                    + ' case '
	            + ' when length(a.fc_date'+IntToStr(iField)+') > 2  then right(a.fc_date'+IntToStr(iField)+',1) '
	            + ' else "" '
                    + ' end fc_shift3,@fc_daytype := case '
                    + ' when a.fc_date'+IntToStr(iField)+' = "" then "" '
                    + ' when c.fc_status = "P" then "H+" '
                    + ' when c.fc_status = "U" then "H" '
                    + ' else "N" end fc_daytype , '
                    + ' d.fc_workin fc_workinshift1,d.fc_workout fc_workoutshift1, '
                    + ' @fn_totalbreakshift1 := coalesce(TIME_TO_SEC( if( d.fc_workbreakin > d.fc_workbreakout , subtime(d.fc_workbreakin,d.fc_workbreakout ) , '
                    + ' subtime(d.fc_workbreakout,d.fc_workbreakin ) ) )/60/60,NULL) fn_totalbreakshift1, '
                    + ' e.fc_workin fc_workinshift2,e.fc_workout fc_workoutshift2, '
                    + ' @fn_totalbreakshift2 := coalesce(TIME_TO_SEC( if( e.fc_workbreakin > e.fc_workbreakout , subtime(e.fc_workbreakin,e.fc_workbreakout ) , '
                    + ' subtime(e.fc_workbreakout,e.fc_workbreakin ) ) )/60/60,NULL) fn_totalbreakshift2 , '
                    + ' f.fc_workin fc_workinshift3,f.fc_workout fc_workoutshift3, '
                    + ' @fn_totalbreakshift3 := coalesce(TIME_TO_SEC( if( f.fc_workbreakin > f.fc_workbreakout , subtime(f.fc_workbreakin,f.fc_workbreakout ) , '
                    + ' subtime(f.fc_workbreakout,f.fc_workbreakin ) ) )/60/60,NULL) fn_totalbreakshift3, '
                    + ' d.fc_workin fc_timeinshift1,d.fc_workout fc_timeoutshift1, '
                    + ' e.fc_workin fc_timeinshift2,e.fc_workout fc_timeoutshift2, '
                    + ' f.fc_workin fc_timeinshift3,f.fc_workout fc_timeoutshift3, '
                    + ' @fn_totalhourshift1 := coalesce(TIME_TO_SEC( if( d.fc_workin > d.fc_workout , subtime(addtime(d.fc_workout,"24:00"),d.fc_workin ) , '
                    + ' subtime(d.fc_workout,d.fc_workin ) ) )/60/60,NULL) fn_totalhourshift1, '
                    + ' @fn_totalhourshift2 := coalesce(TIME_TO_SEC( if( e.fc_workin > e.fc_workout , subtime(addtime(e.fc_workout ,"24:00"),e.fc_workin ) , '
                    + ' subtime(e.fc_workout,e.fc_workin ) ) )/60/60,NULL) fn_totalhourshift2, '
                    + ' @fn_totalhourshift3 := coalesce(TIME_TO_SEC( if( f.fc_workin > f.fc_workout , subtime(addtime(f.fc_workout ,"24:00"),f.fc_workin ), '
                    + ' subtime(f.fc_workout,f.fc_workin ) ) )/60/60,NULL) fn_totalhourshift3, '
                    + ' @fn_totalhourshift1 fn_totaltimeshift1, @fn_totalhourshift2 fn_totaltimeshift2, @fn_totalhourshift3 fn_totaltimeshift3, '
                    + ' @fn_totalhour := @fn_totalhourshift1  + coalesce(@fn_totalhourshift2,0 ) + coalesce(@fn_totalhourshift3,0) fn_totalhour, '

                    +' @fn_effectivehour := @fn_totalhour - ' //untuk 5 jam kerja sabtu yg 7 jam kerja th = 8
                    +'case when ( @fn_totalhour >= ( (2 * if( @fn_totalhourshift1 < 8 , 8 , @fn_totalhourshift1 ) ) + 5 ) ) then 3 '
                    +' when ( @fn_totalhour >= ( if( @fn_totalhourshift1 < 8 , 8 , @fn_totalhourshift1 ) + 5 ) ) then 2 '
                    +' when ( @fn_totalhour >= 5 ) then if(@fn_totalhour = 5, if(@fn_totalhour > 5,1,0) , 1) else 0 end fn_effectivehour,'

                    + ' "" fc_ti,'

                    +' @fn_ot1 := '
                    +' case when @fc_daytype = "N" then '
                                                      //untuk 5 jam kerja effective hour 5 jam karena istirahat setelah pulang   (RECHECK lAGI)
	            +' case when @fn_effectivehour - ( @fn_totalhourshift1 - if( @fn_totalhourshift1 > 5, @fn_totalbreakshift1,0) ) >= 1 then 1 '
	            +' when @fn_effectivehour - ( @fn_totalhourshift1 - if( @fn_totalhourshift1 > 5, @fn_totalbreakshift1,0) ) >= 0 then '
		    +' @fn_effectivehour - ( @fn_totalhourshift1 - if( @fn_totalhourshift1 > 5, @fn_totalbreakshift1,0) ) '
	            +' else 0 end '
                    +' else if(@fc_daytype is null or @fc_daytype = "" ,NULL,0) end fn_ot1, '

                    + ' @fn_ot2 := case when @fc_daytype = "N" then '
                    + ' case when @fn_ot1 >= 1 then @fn_effectivehour - @fn_ot1 - (@fn_totalhourshift1 - @fn_totalbreakshift1 ) '
                    + ' else 0 end when @fc_daytype in ("H","O") then case when @fn_effectivehour - 7 >= 0 then 7 '
                    + ' when @fn_effectivehour - 7 <= 0 then @fn_effectivehour else 0 end '
                    + ' else if(@fc_daytype is  null or @fc_daytype = "" ,NULL,0) end fn_ot2, '
                    + ' @fn_ot3 :=  case when @fc_daytype in ("H","O") then case when @fn_effectivehour - 7 >= 1 then 1 '
                    + ' when @fn_effectivehour - 7 <= 1 then if(@fn_effectivehour - 7 > 0, @fn_effectivehour - 7 ,0 ) '
                    + ' else 0 end else if(@fc_daytype is  null or @fc_daytype = "" ,NULL,0) end fn_ot3 , '
                    + ' @fn_ot4 := case when @fc_daytype in ("H","O") then case when @fn_ot3 >= 1 then @fn_effectivehour - @fn_ot3 - 7 else 0 end '
                    + ' when @fc_daytype in ("H+") then case when @fn_effectivehour - 7 >= 0 then 7 when @fn_effectivehour - 7 <= 0 then @fn_effectivehour else 0 end '
                    + ' else if(@fc_daytype is  null or @fc_daytype = "" ,NULL,0) end fn_ot4 , '
                    + ' @fn_ot5 := case when @fc_daytype in ("H+") then case when @fn_effectivehour - 7 >= 1 then 1 '
                    + ' when @fn_effectivehour - 7 <= 1 then if( @fn_effectivehour - 7 > 0 ,@fn_effectivehour - 7,0) else 0 end '
                    + ' else if(@fc_daytype is  null or @fc_daytype = "" ,NULL,0) end fn_ot5 , '
                    + ' @fn_ot6 :=case when @fc_daytype in ("H+") then case when @fn_ot5 >=1 then '
                    + ' @fn_effectivehour - @fn_ot5 - 7 else 0 end else if(@fc_daytype is  null or @fc_daytype = "" ,NULL,0) end fn_ot6 ,'
                    + ' @fn_jumlahshift1 :=  if(@fn_totalhourshift1 >= 4,1,0) fn_jumlahshift1,'
                    + ' @fn_jumlahshift2 :=  if(@fn_totalhourshift2 >= 4,1,0) fn_jumlahshift2,'
                    + ' @fn_jumlahshift3 :=  if(@fn_totalhourshift3 >= 4,1,0) fn_jumlahshift3,'

                    + ' @fn_jumlahshift := '                    // untuk mengatasi hari sabtu 7 jamkerja dianggap 8+4 baru menambah 1 shift (excel)  (RECHECK lAGI)
                    + ' case when ( @fn_effectivehour >= ( (2 * if( @fn_totalhourshift1 < 8 , 7 , @fn_totalhourshift1 - @fn_totalbreakshift1 ) ) + 4 ) ) then 3 '
                    + ' when ( @fn_effectivehour >= ( if( @fn_totalhourshift1 < 8 , 7 , @fn_totalhourshift1 - @fn_totalbreakshift1 ) + 4 ) ) then 2 '
                    + ' when ( @fn_effectivehour >= 4   ) then 1 when ( @fn_effectivehour < 4   ) then 0 else '' end fn_jumlahshift'

                    + ' from '+ vTableNameTempRotateShift +' a '
                    + ' left join t_member b on trim( lower(a.fc_nameis) ) = trim( lower(b.fc_membername1) ) '
                    + ' left join t_holiday c on date_add(:datestart, INTERVAL :i day) = c.fd_date '
                    + ' left join t_dailytimerange d on d.fc_timerangecode = left(a.fc_date'+IntToStr(iField)+',1) '
                    + ' left join t_dailytimerange e on e.fc_timerangecode = case  when length(a.fc_date'+IntToStr(iField)+') > 1  then mid(a.fc_date'+IntToStr(iField)+',2,1)  else ""  end '
                    + ' left join t_dailytimerange f on f.fc_timerangecode = case when length(a.fc_date'+IntToStr(iField)+') > 2  then right(a.fc_date'+IntToStr(iField)+',1) else "" end '
                    + ' where a.fc_blank <> "O" '
                    //insert or update
                    + ' on duplicate key update '
                    + ' fc_shift1 = left(fc_date'+IntToStr(iField)+',1), '
                    + ' fc_shift2 = '
                    + ' case '
	            + ' when length(fc_date'+IntToStr(iField)+') > 1  then mid(fc_date'+IntToStr(iField)+',2,1) '
	            + ' else "" '
                    + ' end, '
                    + ' fc_shift3 = '
                    + ' case '
	            + ' when length(fc_date'+IntToStr(iField)+') > 2  then right(fc_date'+IntToStr(iField)+',1) '
	            + ' else "" '
                    + ' end , '
                    + ' fc_daytype =  @fc_daytype ,'
                    + ' fc_workinshift1 = d.fc_workin ,fc_workoutshift1 = d.fc_workout , '
                    + ' fn_totalbreakshift1 = @fn_totalbreakshift1 ,'
                    + ' fc_workinshift2 = e.fc_workin ,fc_workoutshift2 = e.fc_workout ,  '
                    + ' fn_totalbreakshift2 = @fn_totalbreakshift2  , '
                    + ' fc_timeinshift3 = f.fc_workin ,fc_timeoutshift3 = f.fc_workout, '
                    + ' fn_totalbreakshift3 = @fn_totalbreakshift3 , '
                    + ' fc_timeinshift1 = d.fc_workin ,fc_timeoutshift1 = d.fc_workout , '
                    + ' fc_timeinshift2 = e.fc_workin ,fc_timeoutshift2 = e.fc_workout , '
                    + ' fc_timeinshift3 = f.fc_workin ,fc_timeoutshift3 = f.fc_workout , '
                    + ' fn_totalhourshift1 =  @fn_totalhourshift1, '
                    + ' fn_totalhourshift2 = @fn_totalhourshift2 , '
                    + ' fn_totalhourshift3 =  @fn_totalhourshift3 , '
                    + ' fn_totaltimeshift1 =  @fn_totalhourshift1, '
                    + ' fn_totaltimeshift2 =  @fn_totalhourshift2, '
                    + ' fn_totaltimeshift3 = @fn_totalhourshift3,'
                    + ' fn_totalhour =  @fn_totalhour, '
                    + ' fn_effectivehour = @fn_effectivehour, '
                    + ' fc_ti = "", '
                    + ' fn_ot1 = @fn_ot1, '
                    + ' fn_ot2 = @fn_ot2, '
                    + ' fn_ot3 = @fn_ot3, '
                    + ' fn_ot4 = @fn_ot4, '
                    + ' fn_ot5 = @fn_ot5, '
                    + ' fn_ot6 = @fn_ot6,'
                    + ' fn_jumlahshift1 =  @fn_jumlahshift1 ,'
                    + ' fn_jumlahshift2 =  @fn_jumlahshift2 ,'
                    + ' fn_jumlahshift3 =  @fn_jumlahshift3 ,'
                    + ' fn_jumlahshift =  @fn_jumlahshift'
                    + ' ;'
                    ;
        ParamByName('periode').AsDate:=DateTimePickerPeriode.Date;
        ParamByName('datestart').AsDate:=DateStartImportShiftRotate.Date;
        ParamByName('i').AsInteger:= iField - 1;
        Execute;

        vPB.Position:=iField;
     end;
   end;
    //ShowMessage('Import shift rotate to rapid attendance SUCCESS');
end;

procedure Tf_importcsv.BitBtnClearMemoClick(Sender: TObject);
begin
  MemoDelimeter.SelectAll;
  MemoDelimeter.Clear;
end;

procedure Tf_importcsv.BitBtn1Click(Sender: TObject);
begin
  Panel2.Width:=0;
end;


procedure Tf_importcsv.BitBtnClearMemoReplaceRSCodeClick(Sender: TObject);
begin
  MemoReplaceRotateShiftCode.Clear;
end;

procedure Tf_importcsv.BitBtn2Click(Sender: TObject);
begin
  Panel11.Width:=0;
end;

procedure Tf_importcsv.BitBtnCreateGrid1Click(Sender: TObject);
var
   vScriptTempAttendance: String;
begin
  vTableNameTempAttendance:='t_tempandhikafinger';
  vScriptTempAttendance  :=
  'DROP TABLE IF EXISTS ' + vTableNameTempAttendance + ';'
           +' CREATE TABLE `'+vTableNameTempAttendance+'` ( '
           +' `fc_pin` varchar(30) DEFAULT NULL, '
           +' `fc_nip` varchar(30) DEFAULT NULL, '
           +' `fc_nama` varchar(50) DEFAULT NULL, '
           +' `fc_jabatan` varchar(50) DEFAULT NULL, '
           +' `fc_departemen` varchar(50) DEFAULT NULL, '
           +' `fc_kantor` varchar(50) DEFAULT NULL, '
           +' `fc_tanggal` varchar(50) DEFAULT NULL, '
           +' `fc_scan1` varchar(50) DEFAULT NULL, '
           +' `fc_scan2` varchar(50) DEFAULT NULL, '
           +' `fc_scan3` varchar(50) DEFAULT NULL, '
           +' `fc_scan4` varchar(50) DEFAULT NULL '
           +');'
        + ' LOAD DATA LOCAL INFILE '
        + QuotedStr(
             StringReplace(OpenDialog.FileName,
                           '\','\\',
                           [rfReplaceAll, rfIgnoreCase]
                          )
                   )
        + ' INTO TABLE '
        + vTableNameTempAttendance
        + ' '
        + 'FIELDS TERMINATED BY ' + QuotedStr(';')
        + ' LINES TERMINATED BY '+ QuotedStr(chr(13)) +' IGNORE 2 ROWS ;' //ignore 2 headers
        ;
  //ShowMessage(OpenDialog.FileName);
  With MyProcessor Do Begin
       Script.Clear;
       Script.Text := vScriptTempAttendance;
       Execute;
  End;
  ShowMessage('Import CSV FINGER Success !!');
  With qt_temp_attendance do begin
     Close;
     SQL.Text:='select * from '+vTableNameTempAttendance;
     Open;
  end;
  DBGrid1.AutoFillColumns:=True; //for adjustment columns
  DBGrid1.AutoFillColumns:=False;
end;

procedure Tf_importcsv.BitBtnCreateGridClick(Sender: TObject);
Var
  vPrefixDate           : Integer;     // Urut-urutan hari-per-hari
  vSCriptLoadDataInfile : String;      // Script untuk LOAD DATA INFILE. Karena dipakai 2x
  vPoolStringList       : TStringList; // untuk nampung Field Tanggal
begin
  vTableNameTempRotateShift           := 't_tempdavidrotateshift';

  vPoolStringList      := TStringList.Create;
  With vPoolStringList Do Begin

     // Jika sudah ada maka akan dihapus.
     Add('DROP TABLE IF EXISTS ' + vTableNameTempRotateShift + ';');
     Add('CREATE TABLE ' + vTableNameTempRotateShift + ' (');

     {
     2 field dibawah ini MUTLAK ADA. Jadi ditambahkan manual
       fc_nameis : nama karyawan dari Excelnya
       fc_blank  : karena hasil PASTE selalu ada kolom/field
                   kosong ndek depan`e
     }
     Add('fc_nameis varchar(100) not null default '+ QuotedStr('') +',');
     Add('fc_blank  varchar(2)   not null default '+ QuotedStr('') +',');

     For vPrefixDate := 1 to vCountFieldDate + 1 Do Begin
         If vPrefixDate < vCountFieldDate + 1 Then
            Add(
                'fc_date' + IntToStr(vPrefixDate)
                          + ' char(40) not null default '
                          + QuotedSTr('')
                          + ','
               )
         Else
           Add(
               'fc_date' + IntToStr(vPrefixDate)
                         + ' char(40) not null default '
                         + QuotedSTr('')
              );
     End;
     Add(');');
  End;

  vSCriptLoadDataInfile  :=
        'LOAD DATA LOCAL INFILE '
        + QuotedStr(
             StringReplace(OpenDialog.FileName,
                           '\','\\',
                           [rfReplaceAll, rfIgnoreCase]
                          )
                   )
        + ' INTO TABLE '
        + vTableNameTempRotateShift
        + ' '
        + 'FIELDS TERMINATED BY ' + QuotedStr(';')  ;
        //+ ' ENCLOSED BY '+QuotedStr('"')
        //+' LINES TERMINATED BY "\n"';
  With MyProcessor Do Begin
       Script.Clear;
       Script.Text :=
              // INSERT 1st "Insert Group"
              vPoolStringList.Text
              + vSCriptLoadDataInfile
              + ';'
              // Marking / UPDATE the 1st "Insert Group" with (O)
              + ' UPDATE ' + vTableNameTempRotateShift
              + ' SET fc_blank = ' + QuotedStr('O')
              + ';'
              +
              // INSRET 2nd "Insert Group" without marking (O)
              // And user could modify record without marking.
              vSCriptLoadDataInfile
              + ';'
              + ' UPDATE ' + vTableNameTempRotateShift
              + ' SET fc_nameis = LTRIM(fc_nameis);';
       Execute;
  End;
  vPoolStringList.Free;
  PaintGrid;
  ClearCharTempRotateShift;
end;


procedure Tf_importcsv.BitBtnMergeShiftAttendance1Click(Sender: TObject);
begin
  with MyProcessor do begin
     Script.Clear;
     Script.Text:= ' update ' +vTableNameAttendance+ ' a '
                   + ' inner join ' + vTableNameTempAttendance+' b '
                   + ' on a.fd_date = str_to_date(b.fc_tanggal,"%d/%m/%Y") and a.fc_membercode = concat("TSI ",b.fc_nip) and a.fc_attendancetype = "ATTENDANCE" '
                   + ' set a.fd_realdatein = concat(str_to_date(b.fc_tanggal,"%d/%m/%Y")," ",b.fc_scan1), '
                   + ' a.fd_realdateout = concat(str_to_date(b.fc_tanggal,"%d/%m/%Y")," ", '
                   + ' coalesce( nullif( coalesce(fc_scan4,""),"" ), nullif( coalesce(fc_scan3,""),"" ),nullif( coalesce(fc_scan2,""),"" ) )) ';
     Execute;
  end;
  ShowMessage('Update attendance Success');
end;

procedure Tf_importcsv.BitBtnMergeShiftAttendanceClick(Sender: TObject);
var
  Reply, BoxStyle: Integer;
begin
  BoxStyle := MB_ICONQUESTION + MB_YESNO;
  Reply := Application.MessageBox('Are you sure to import rotation shift ?', 'Confirmation', BoxStyle);
  if Reply = IDYES then begin
       //RunImportShiftThread;
       Label2.Caption:='IMPORTING SHIFT ROTATE ( LOADING... )';
       Label2.Font.Color:=clBlue;
       DBGridTempRotateShift.Enabled:=False;
       ImportShiftRotateAttendance(vTableNameAttendance);
       ImportShiftRotateAttendance('t_attendance_backup');
       DBGridTempRotateShift.Enabled:=True;
       Label2.Caption:='IMPORTING SHIFT ROTATE ( SUCCESS. )';
       Label2.Font.Color:=clGreen;
    end
    else Application.MessageBox('Import Canceled', 'Import canceled', MB_ICONHAND);
end;

procedure Tf_importcsv.BitBtnOpenfile1Click(Sender: TObject);
begin
   if OpenDialog.Execute then begin
      EditOpenFile1.Text:= OpenDialog.FileName;
   end;
end;

procedure Tf_importcsv.BitBtnOpenfileClick(Sender: TObject);
begin
   if OpenDialog.Execute then begin
      EditOpenFile.Text:= OpenDialog.FileName;
   end;
end;

procedure Tf_importcsv.BitBtnPasteMemoClick(Sender: TObject);
begin
  MemoDelimeter.SelectAll;
  MemoDelimeter.PasteFromClipboard;
end;

procedure Tf_importcsv.BitBtnRefreshGridTempRotateShiftClick(Sender: TObject);
var
   rowDelta: Integer;
   row: integer;
   ds : TDataSet;
begin

  qt_temprotateshift.Close;
  qt_temprotateshift.Open;
  PaintGrid;
  DBGridTempRotateShift.DataSource.DataSet.RecNo:=curRecNo;
  DBGridTempRotateShift.SelectedField:=curField;

  {ds := THackDBGrid(DBGridTempRotateShift).DataSource.DataSet;

   rowDelta := -1 + THackDBGrid(DBGridTempRotateShift).Row;
   row := ds.RecNo; //urutan record sebenarnya

   ds.Close;
   ds.Open;

   with ds do
   begin
     DisableControls;
     RecNo := row;
     MoveBy(-rowDelta) ;  //dbgrid move keatas dulu
     MoveBy(rowDelta) ;   // baru ke bawah sehingga "seperti" scroll position tidak berubah
     EnableControls;
   end;       }
end;

procedure Tf_importcsv.BitBtnReplaceRSCodeClick(Sender: TObject);
var
  iField : Integer;
  Shifcode,OptReplace : String;
begin

  if ComboBoxOptReplace.ItemIndex = -1 then begin
     ShowMessage('Invalid Option');
     Exit;
  end;

  Shifcode:= Copy(MemoReplaceRotateShiftCode.Text,0,(length(MemoReplaceRotateShiftCode.Text)-1));

  //remove /trim unprintable char
  ClearCharTempRotateShift;


  for iField:= 1 to qt_temprotateshift.FieldCount - 3 do begin

     if ComboBoxOptReplace.ItemIndex = 0 then begin
       OptReplace:= '"'+EditReplaceRotateShift.Text+'"' ;
     end
     else if ComboBoxOptReplace.ItemIndex = 1 then begin
       OptReplace:= 'SUBSTRING( fc_date'+IntToStr(iField)+',2,100)' ;
     end
     else if ComboBoxOptReplace.ItemIndex = 2 then begin
       OptReplace:= 'SUBSTRING( fc_date'+IntToStr(iField)+',3,100)' ;
     end
     else if ComboBoxOptReplace.ItemIndex = 3 then begin
       OptReplace:= 'LEFT( fc_date'+IntToStr(iField)+',1)' ;
     end
     else if ComboBoxOptReplace.ItemIndex = 4 then begin
       OptReplace:= 'LEFT( fc_date'+IntToStr(iField)+',2)' ;
     end
     else if ComboBoxOptReplace.ItemIndex = 5 then begin
       OptReplace:= 'SUBSTRING(fc_date'+IntToStr(iField)+',2,CASE WHEN length(fc_date'+IntToStr(iField)+') < 4 then 1 else 2 end )' ;
     end
     else if ComboBoxOptReplace.ItemIndex = 6 then begin
       OptReplace:= 'case when fc_date'+IntToStr(iField)+' = "A" then 1 when fc_date'+IntToStr(iField)+' = "B" then 2 end ' ;
     end
     else if ComboBoxOptReplace.ItemIndex = 7 then begin
       OptReplace:= 'concat( left(fc_date'+IntToStr(iField)+',1) , right(fc_date'+IntToStr(iField)+',1)  ) ';
     end
     else if ComboBoxOptReplace.ItemIndex = 8 then begin
       OptReplace:= 'concat( substring(fc_date'+IntToStr(iField)+',1,1) , substring(fc_date'+IntToStr(iField)+',3,1)  ) ';
     end;

     with MyProcessor do begin
        Script.Clear;
        //Script.Text:= ' update '
        //            + vTableNameTempRotateShift
        //            + ' set '
        //            + 'fc_date'+IntToStr(iField)+' = '
        //            + OptReplace
        //            + ' where fc_date'+IntToStr(iField)
        //            +' in ( '+Shifcode+' ) and fc_blank <> "O" ';
        Script.Text:= ' update '
                    + vTableNameTempRotateShift
                    + ' set '
                    + 'fc_date'+IntToStr(iField)+' = '
                    + OptReplace
                    + ' where trim(REPLACE(fc_date'+IntToStr(iField)+', CHAR(13), "") ) '
                    +' in ( '+Shifcode+' ) and fc_blank <> "O" ';
        Execute;
     end;
   end;
  BitBtnRefreshGridTempRotateShiftClick(Sender);
  ShowMessage('Replace Done');
  MemoReplaceRotateShiftCode.Clear;
  //MemoReplaceRotateShiftCode.Text:=MyProcessor.Script.Text;
end;

procedure Tf_importcsv.BtnDelimiterMemoClick(Sender: TObject);
Var
   vCounterLineMemo: integer;
   vOneLineRecord: string;
   vPositionCharSearching: integer;
begin
  ProgessBarDelimeter.Max         := MemoDelimeter.Lines.Count - 1;

  For vCounterLineMemo     := 0 to MemoDelimeter.Lines.Count - 1 Do Begin
     vOneLineRecord        := MemoDelimeter.Lines[vCounterLineMemo];
     ProgessBarDelimeter.StepIt;
     ProgessBarDelimeter.Position := vCounterLineMemo;
     ProgessBarDelimeter.Update;
     Repeat
       vPositionCharSearching := Pos(chr(9), vOneLineRecord);
       If vPositionCharSearching > 0 Then Begin
          Delete(vOneLineRecord, vPositionCharSearching, Length(chr(9)));
          Insert(';', vOneLineRecord, vPositionCharSearching);
          MemoDelimeter.Lines[vCounterLineMemo] := vOneLineRecord;
       End;
     Until
       vPositionCharSearching = 0;
     MemoDelimeter.Refresh;
     MemoDelimeter.Repaint;
  End;

  EditSaveFile.Text:='CSV-IMPORTSHIFT-' + FormatDateTime('DDMMYYYY',Now);
  EditSaveFile.SetFocus;
end;

procedure Tf_importcsv.btnSaveFileClick(Sender: TObject);
begin
  SaveDialog.FileName  := EditSaveFile.Text;
  If SaveDialog.Execute Then MemoDelimeter.Lines.SaveToFile( SaveDialog.Filename );
  MemoDelimeter.Lines.SaveToFile( SaveDialog.Filename );
end;

procedure Tf_importcsv.Button1Click(Sender: TObject);
begin
  f_checkimportedattendance.Show;
end;

procedure Tf_importcsv.CheckBox1Change(Sender: TObject);
begin
  if CheckBox1.Checked then begin
     DateStartImportShiftRotate.ReadOnly:=True;
     DateFinishImportShiftRotate.ReadOnly:=true;
  end else begin
     DateStartImportShiftRotate.ReadOnly:=False;
     DateFinishImportShiftRotate.ReadOnly:=False;
  end;
end;

procedure Tf_importcsv.ComboBox1Change(Sender: TObject);
begin
  //if ComboBox1.ItemIndex = 0 then begin
  //   Page1.Show;
  //end else if ComboBox1.ItemIndex = 1 then begin
  //   Page2.Show;
  //end else ShowMessage('Invalid menu');
end;

procedure Tf_importcsv.ComboBoxOptReplaceChange(Sender: TObject);
begin
  if ComboBoxOptReplace.ItemIndex = 0 then begin
     EditReplaceRotateShift.Enabled:=True;
  end else begin
     EditReplaceRotateShift.Text:='';
     EditReplaceRotateShift.Enabled:=False;
  end;
end;

procedure Tf_importcsv.DateTimePickerPeriodeChange(Sender: TObject);
begin

  if FormatDateTime('MM',DateTimePickerPeriode.Date) = '01' then begin
     DateStartImportShiftRotate.Date:=StrToDate('16'+DateSeparator+'12'+DateSeparator+IntToStr( StrToInt( FormatDateTime('YYYY',DateTimePickerPeriode.Date) ) - 1 ) );
     DateFinishImportShiftRotate.Date:=StrToDate('15'+DateSeparator+'01'+DateSeparator+FormatDateTime('YYYY',DateTimePickerPeriode.Date) );
  end else if FormatDateTime('MM',DateTimePickerPeriode.Date) = '02' then begin
     DateStartImportShiftRotate.Date:=StrToDate('16'+DateSeparator+'01'+DateSeparator+FormatDateTime('YYYY',DateTimePickerPeriode.Date) );
     DateFinishImportShiftRotate.Date:=StrToDate('15'+DateSeparator+'02'+DateSeparator+FormatDateTime('YYYY',DateTimePickerPeriode.Date) );
  end else if FormatDateTime('MM',DateTimePickerPeriode.Date) = '03' then begin
     DateStartImportShiftRotate.Date:=StrToDate('16'+DateSeparator+'02'+DateSeparator+FormatDateTime('YYYY',DateTimePickerPeriode.Date) );
     DateFinishImportShiftRotate.Date:=StrToDate('15'+DateSeparator+'03'+DateSeparator+FormatDateTime('YYYY',DateTimePickerPeriode.Date) );
  end else if FormatDateTime('MM',DateTimePickerPeriode.Date) = '04' then begin
     DateStartImportShiftRotate.Date:=StrToDate('16'+DateSeparator+'03'+DateSeparator+FormatDateTime('YYYY',DateTimePickerPeriode.Date) );
     DateFinishImportShiftRotate.Date:=StrToDate('15'+DateSeparator+'04'+DateSeparator+FormatDateTime('YYYY',DateTimePickerPeriode.Date) );
  end else if FormatDateTime('MM',DateTimePickerPeriode.Date) = '05' then begin
     DateStartImportShiftRotate.Date:=StrToDate('16'+DateSeparator+'04'+DateSeparator+FormatDateTime('YYYY',DateTimePickerPeriode.Date) );
     DateFinishImportShiftRotate.Date:=StrToDate('15'+DateSeparator+'05'+DateSeparator+FormatDateTime('YYYY',DateTimePickerPeriode.Date) );
  end else if FormatDateTime('MM',DateTimePickerPeriode.Date) = '06' then begin
     DateStartImportShiftRotate.Date:=StrToDate('16'+DateSeparator+'05'+DateSeparator+FormatDateTime('YYYY',DateTimePickerPeriode.Date) );
     DateFinishImportShiftRotate.Date:=StrToDate('15'+DateSeparator+'06'+DateSeparator+FormatDateTime('YYYY',DateTimePickerPeriode.Date) );
  end else if FormatDateTime('MM',DateTimePickerPeriode.Date) = '07' then begin
     DateStartImportShiftRotate.Date:=StrToDate('16'+DateSeparator+'06'+DateSeparator+FormatDateTime('YYYY',DateTimePickerPeriode.Date) );
     DateFinishImportShiftRotate.Date:=StrToDate('15'+DateSeparator+'07'+DateSeparator+FormatDateTime('YYYY',DateTimePickerPeriode.Date) );
  end else if FormatDateTime('MM',DateTimePickerPeriode.Date) = '08' then begin
     DateStartImportShiftRotate.Date:=StrToDate('16'+DateSeparator+'07'+DateSeparator+FormatDateTime('YYYY',DateTimePickerPeriode.Date) );
     DateFinishImportShiftRotate.Date:=StrToDate('15'+DateSeparator+'08'+DateSeparator+FormatDateTime('YYYY',DateTimePickerPeriode.Date) );
  end else if FormatDateTime('MM',DateTimePickerPeriode.Date) = '09' then begin
     DateStartImportShiftRotate.Date:=StrToDate('16'+DateSeparator+'08'+DateSeparator+FormatDateTime('YYYY',DateTimePickerPeriode.Date) );
     DateFinishImportShiftRotate.Date:=StrToDate('15'+DateSeparator+'09'+DateSeparator+FormatDateTime('YYYY',DateTimePickerPeriode.Date) );
  end else if FormatDateTime('MM',DateTimePickerPeriode.Date) = '10' then begin
     DateStartImportShiftRotate.Date:=StrToDate('16'+DateSeparator+'09'+DateSeparator+FormatDateTime('YYYY',DateTimePickerPeriode.Date) );
     DateFinishImportShiftRotate.Date:=StrToDate('15'+DateSeparator+'10'+DateSeparator+FormatDateTime('YYYY',DateTimePickerPeriode.Date) );
  end else if FormatDateTime('MM',DateTimePickerPeriode.Date) = '11' then begin
     DateStartImportShiftRotate.Date:=StrToDate('16'+DateSeparator+'10'+DateSeparator+FormatDateTime('YYYY',DateTimePickerPeriode.Date) );
     DateFinishImportShiftRotate.Date:=StrToDate('15'+DateSeparator+'11'+DateSeparator+FormatDateTime('YYYY',DateTimePickerPeriode.Date) );
  end else if FormatDateTime('MM',DateTimePickerPeriode.Date) = '12' then begin
     DateStartImportShiftRotate.Date:=StrToDate('16'+DateSeparator+'11'+DateSeparator+FormatDateTime('YYYY',DateTimePickerPeriode.Date) );
     DateFinishImportShiftRotate.Date:=StrToDate('15'+DateSeparator+'12'+DateSeparator+FormatDateTime('YYYY',DateTimePickerPeriode.Date) );
  end;

  DateFinishImportShiftRotate.OnExit(Sender);

end;

procedure Tf_importcsv.DateFinishImportShiftRotateExit(Sender: TObject);
begin
  vCountFieldDate := trunc(DateFinishImportShiftRotate.Date)-trunc(DateStartImportShiftRotate.Date);
  LabelDays.Caption  := IntToStr(vCountFieldDate + 1) + ' Days'
end;

procedure Tf_importcsv.DBGridTempRotateShiftCellClick(Column: TColumn);
begin
  curRecNo:= DBGridTempRotateShift.DataSource.DataSet.RecNo;
  curField := DBGridTempRotateShift.SelectedField;

  vUPD_FieldName      := Column.FieldName;
  vUPD_OldValue       := Func_checkReference(
                           qt_temprotateshift.FieldByName('fc_nameis').AsString,
                           Column.FieldName
                                       );
  vUPD_EmployeeName   := qt_temprotateshift.FieldByName('fc_nameis').AsString;
  vUPD_Blank          := qt_temprotateshift.FieldByName('fc_blank').AsString;
end;

procedure Tf_importcsv.DBGridTempRotateShiftDblClick(Sender: TObject);
Var
   Memo : TMemo;
   MemoOldValue,ShiftCode : String;
begin
   Memo := MemoReplaceRotateShiftCode;
   MemoOldValue:=Memo.Text;
   ShiftCode := '"'+Trim(DBGridTempRotateShift.SelectedField.AsString) + '",';
   Memo.Text:= Copy( MemoOldValue,1,Length(MemoOldValue) ) + ShiftCode ;
end;

procedure Tf_importcsv.FormCreate(Sender: TObject);
begin
  vTableNameAttendance := 't_attendance';
  DateTimePickerPeriode.Date:=Now;
  if CheckBox1.Checked then begin
     DateStartImportShiftRotate.ReadOnly:=True;
     DateFinishImportShiftRotate.ReadOnly:=true;
  end else begin
     DateStartImportShiftRotate.ReadOnly:=False;
     DateFinishImportShiftRotate.ReadOnly:=False;
  end;
  DateTimePickerPeriodeChange(Sender);
end;

procedure Tf_importcsv.DateTimePicker1Exit(Sender: TObject);
begin
  vCountFieldDate := trunc(DateFinishImportShiftRotate.Date)-trunc(DateStartImportShiftRotate.Date);
  LabelDays.Caption  := IntToStr(vCountFieldDate) + ' Days'
end;

procedure Tf_importcsv.PaintGrid;
Var
   vColumnPaint : Integer;
Begin
  With qt_temprotateshift Do Begin
       Close;
       SQL.Clear;
       SQL.Text :=
                   'select case when coalesce(fc_blank, '+ QuotedStr('X') + ') = ' + QuotedStr('O')
                 + '       then fc_nameis '
                 + '       else '+ QuotedStr('') +' end as Name, '
                 + '       '+vTableNameTempRotateShift+'.* '
                 + 'from '+vTableNameTempRotateShift+' order by fc_nameis ASC, fc_blank DESC;';
       Open;
       //ShowMessage('jumlah elemen : ' + IntToStr(vPoolStringList.Count) + ' vCountFieldDate : ' + IntToStr(vCountFieldDate));
       //With vPoolStringList Do Begin
       For vColumnPaint := 0 To vCountFieldDate + 3 Do Begin
           With DBGridTempRotateShift Do Begin
                // fixed cols
                FixedCols:=4;
                // Mulai Column / field ke 3 isi`e wes tanggal. Jadi judulnya adalah TANGGAL
                If vColumnPaint > 2 Then Begin
                   Columns[vColumnPaint].Width          := 40;
                   Columns[vColumnPaint].Title.Caption  :=
                   FormatDateTime('DD',IncDay(DateStartImportShiftRotate.Date, vColumnPaint - 3));
                End Else Begin
                   // Column 1 dan 2 Judul`e nggawe dewe.
                   Case vColumnPaint Of
                        0 : Begin
                                Columns[vColumnPaint].ReadOnly       := True;
                                Columns[vColumnPaint].Width          := 150;
                                Columns[vColumnPaint].Title.Caption  := 'Employee Name';
                            End;
                        1 : Begin
                                Columns[vColumnPaint].Visible        := False;
                            End;
                        2 : Begin
                                Columns[vColumnPaint].Title.Caption  := '[*]';
                                Columns[vColumnPaint].Width          := 20;
                                Columns[vColumnPaint].ReadOnly       := True;
                            End;
                   End;
                End;
           End;
       End;
  end;

End;

end.

