unit u_attendance;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, StdCtrls,
  Buttons, ComCtrls, DBGrids, DateTimePicker, DBDateTimePicker, LR_DBSet,
  LR_Class, ZConnection, ZSqlProcessor, ZDataset, ZSqlUpdate, dateutils, db,
  LCLType, Menus, DBCtrls;
const
  vq_qt_attendance = ' select '
                   + ' a.fc_branch,a.fc_periode ,a.fd_dategenerate ,'
                   + ' a.fc_membercode,b.fc_membername1, '
                   + ' b.fc_memberattendancetype ,b.fn_memberothernumber1 , '
                   + ' b.fc_memberposition, '
                   + ' a.fd_date ,a.fc_daytype , '
                   + ' concat(fc_shift1,fc_shift2,fc_shift3) shift , '
                   + ' fc_shift1 ,fc_shift2,fc_shift3, '
                   + ' fc_timeinshift1,fc_timeoutshift1 , '
                   + ' fc_timeinshift2,fc_timeoutshift2 , '
                   + ' fc_timeinshift3,fc_timeoutshift3 , '
                   + ' fc_workinshift1 timein1, '
                   + ' if(fc_workoutshift2 in ("","00:00",NULL), fc_workoutshift1 ,fc_workoutshift2 ) timeout1, '
                   + ' fc_workinshift3 timein2, '
                   + ' fc_workoutshift3 timeout2, '
                   + ' fd_realdatein ,fd_realdateout, '
                   + ' fn_totalhour ,fn_effectivehour, '
                   + ' fn_ot1 ,fn_ot2 ,fn_ot3 ,fn_ot4 ,fn_ot5 ,fn_ot6, '
                   + ' length(concat(fc_shift1,fc_shift2,fc_shift3)) jmlh_shift, '
                   + ' fv_description, '
                   + ' fc_ti , '
                   + ' ( fn_ot1 + fn_ot2 + fn_ot3 + fn_ot4 + fn_ot5 + fn_ot6 )  total_ot, '
                   + ' ( select count(fd_date) from t_attendance where '
                   + ' "1" in (fc_shift1,fc_shift2,fc_shift3 ) and fc_membercode = a.fc_membercode  and fd_date  >= :datestart and fd_date <= :datefinish '
                   + ' or '
                   + ' "1" in (fc_shift1,fc_shift2,fc_shift3 ) and fc_membercode = a.fc_membercode  and fc_periode = DATE_FORMAT(:datestart,"%Y%m" ) and  fd_date  <= :datestart '
                   + ' ) totalshift1, '
                   + ' ( select count(fd_date) from t_attendance where '
                   + ' "2" in (fc_shift1,fc_shift2,fc_shift3 ) and fc_membercode = a.fc_membercode  and fd_date  >= :datestart and fd_date <= :datefinish '
                   + ' or '
                   + ' "3" in (fc_shift1,fc_shift2,fc_shift3 ) and fc_membercode = a.fc_membercode  and fc_periode = DATE_FORMAT(:datestart,"%Y%m" ) and  fd_date  <= :datestart '
                   + ' ) totalshift2, '
                   + ' ( select count(fd_date) from t_attendance where '
                   + ' "3" in (fc_shift1,fc_shift2,fc_shift3 ) and fc_membercode = a.fc_membercode  and fd_date  >= :datestart and fd_date <= :datefinish '
                   + ' or '
                   + ' "3" in (fc_shift1,fc_shift2,fc_shift3 ) and fc_membercode = a.fc_membercode  and fc_periode = DATE_FORMAT(:datestart,"%Y%m" ) and  fd_date  <= :datestart '
                   + ' ) totalshift3, '
                   + ' ( fn_ot1 + fn_ot2 + fn_ot3 + fn_ot4 + fn_ot5 + fn_ot6 )  total_ot, '
                   + ' ( select count(fd_date) from t_attendance where '
                   + ' fc_daytype not in ("") and fc_membercode = a.fc_membercode  and fd_date  >= :datestart and fd_date <= :datefinish '
                   + ' or '
                   + ' fc_daytype not in ("") and fc_membercode = a.fc_membercode  and fc_periode = DATE_FORMAT(:datestart,"%Y%m" ) and  fd_date  <= :datestart '
                   + ' ) totalworkday '
                   + ' from t_attendance a '
                   + ' left join t_member b on a.fc_membercode = b.fc_membercode '
                   + ' left join t_trxtype c on b.fc_memberposition = c.fc_trxcode and c.fc_trxcode = "EMP_POSITION" '
                   + ' where '
                   + ' a.fc_membercode like :member and b.fl_memberblacklist = :active and a.fd_date  >= :datestart and a.fd_date <= :datefinish '
                   + ' or '
                   + ' a.fc_membercode like :member and b.fl_memberblacklist = :active and a.fc_periode = DATE_FORMAT(:datestart,"%Y%m" ) and  a.fd_date  <= :datestart '
                   + ' or '
                   + ' b.fc_membername1 like :member and b.fl_memberblacklist = :active and a.fd_date  >= :datestart and a.fd_date <= :datefinish '
                   + ' or '
                   + ' b.fc_membername1 like :member and b.fl_memberblacklist = :active and a.fc_periode = DATE_FORMAT(:datestart,"%Y%m" ) and  a.fd_date  <= :datestart '
                   + ' order by b.fc_membercode,a.fd_date '
                   ;
type

  { Tf_attendance }

  Tf_attendance = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtnApplyPeriode: TBitBtn;
    BitBtnReplaceRSCode: TBitBtn;
    BitBtnClearMemoReplaceRSCode: TBitBtn;
    BitBtnClearMemo: TBitBtn;
    BitBtnCreateGrid: TBitBtn;
    BitBtnCreateGrid1: TBitBtn;
    BitBtnMergeShiftAttendance: TBitBtn;
    BitBtnMergeShiftAttendance1: TBitBtn;
    BitBtnOpenfile: TBitBtn;
    BitBtnOpenfile1: TBitBtn;
    BitBtnPasteMemo: TBitBtn;
    BitBtnRefreshGridTempRotateShift: TBitBtn;
    BitBtnRefreshGrid1: TBitBtn;
    BtnDelimiterMemo: TBitBtn;
    btnSaveFile: TBitBtn;
    ComboBox1: TComboBox;
    ComboBoxMonthPeriode: TComboBox;
    ComboBoxOptReplace: TComboBox;
    ds_temp_attendance: TDataSource;
    DateStartImportShiftRotate: TDateTimePicker;
    DateFinishImportShiftRotate: TDateTimePicker;
    DBGrid1: TDBGrid;
    DBGridTempRotateShift: TDBGrid;
    ds_temprotateshift: TDataSource;
    EditYearPeriode: TEdit;
    EditReplaceRotateShift: TEdit;
    EditOpenFile: TEdit;
    EditOpenFile1: TEdit;
    EditSaveFile: TEdit;
    ImageList: TImageList;
    Label1: TLabel;
    Label10: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    LabelDays: TLabel;
    MemoReplaceRotateShiftCode: TMemo;
    MemoDelimeter: TMemo;
    Notebook1: TNotebook;
    OpenDialog: TOpenDialog;
    Page1: TPage;
    Panel1: TPanel;
    Panel10: TPanel;
    Panel11: TPanel;
    Panel12: TPanel;
    Panel13: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    Panel6: TPanel;
    Panel7: TPanel;
    Panel8: TPanel;
    PopupMenuDBGridRotateShift: TPopupMenu;
    ProgessBarDelimeter: TProgressBar;
    qt_attendancefc_daytype: TStringField;
    qt_attendancefc_membercode: TStringField;
    qt_attendancefc_membername1: TStringField;
    qt_attendancefc_memberposition: TStringField;
    qt_attendancefc_periode: TStringField;
    qt_attendancefc_status: TStringField;
    qt_attendancefc_ti: TStringField;
    qt_attendancefd_date: TDateField;
    qt_attendancefd_dategenerate: TDateTimeField;
    qt_attendancefd_realdatein: TDateTimeField;
    qt_attendancefd_realdateout: TDateTimeField;
    qt_attendancefn_effectivehour: TFloatField;
    qt_attendancefn_ot1: TFloatField;
    qt_attendancefn_ot2: TFloatField;
    qt_attendancefn_ot3: TFloatField;
    qt_attendancefn_ot4: TFloatField;
    qt_attendancefn_ot5: TFloatField;
    qt_attendancefn_ot6: TFloatField;
    qt_attendancefn_totalhour: TFloatField;
    qt_attendancefv_description: TStringField;
    qt_attendanceshift: TStringField;
    qt_check_reference: TZReadOnlyQuery;
    REPLACE: TMenuItem;
    SaveDialog: TSaveDialog;
    Splitter1: TSplitter;
    MyProcessor: TZSQLProcessor;
    qt_temprotateshift: TZQuery;
    Splitter2: TSplitter;
    qt_temp_attendance: TZQuery;
    Splitter3: TSplitter;
    TAKE1DLEFT: TMenuItem;
    TAKE2DLEFT: TMenuItem;
    TAKELEFTRIGHTCHAR: TMenuItem;
    TAKEMIDCHAR: TMenuItem;
    TRIM1D: TMenuItem;
    TRIM2D: TMenuItem;
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtnApplyPeriodeClick(Sender: TObject);
    procedure BitBtnClearMemoReplaceRSCodeClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtnClearMemoClick(Sender: TObject);
    procedure BitBtnCreateGrid1Click(Sender: TObject);
    procedure BitBtnCreateGridClick(Sender: TObject);
    procedure BitBtnMergeShiftAttendance1Click(Sender: TObject);
    procedure BitBtnMergeShiftAttendanceClick(Sender: TObject);
    procedure BitBtnOpenfile1Click(Sender: TObject);
    procedure BitBtnOpenfileClick(Sender: TObject);
    procedure BitBtnPasteMemoClick(Sender: TObject);
    procedure BitBtnRefreshGridTempRotateShiftClick(Sender: TObject);
    procedure BitBtnReplaceRSCodeClick(Sender: TObject);
    procedure BtnDelimiterMemoClick(Sender: TObject);
    procedure btnSaveFileClick(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure ComboBoxMonthPeriodeChange(Sender: TObject);
    procedure ComboBoxOptReplaceChange(Sender: TObject);
    procedure DateTimePicker1Exit(Sender: TObject);
    procedure DateFinishImportShiftRotateExit(Sender: TObject);
    procedure DBGridAttendanceEnter(Sender: TObject);
    procedure DBGridTempRotateShiftCellClick(Column: TColumn);
    procedure DBGridTempRotateShiftDblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PaintGrid;
    procedure ImportShiftRotate;
    FUNCTION Func_checkReference(vKeyFind, vFieldReplace : String) : String;
    procedure REPLACEClick(Sender: TObject);
    procedure TAKE1DLEFTClick(Sender: TObject);
    procedure TAKE2DLEFTClick(Sender: TObject);
    procedure TAKELEFTRIGHTCHARClick(Sender: TObject);
    procedure TAKEMIDCHARClick(Sender: TObject);
    procedure TRIM1DClick(Sender: TObject);
    procedure TRIM2DClick(Sender: TObject);
    procedure SetUpGridPickList(FieldName,SQL: String;DBGrid : TDBGrid);
  private
    vTableNameTempRotateShift      : String;      // Variable Nama Table. Ini nanti iku USERID
    vTableNameTempAttendance      : String;      // Variable Nama Table. Ini nanti iku USERID
    vCountFieldDate : Integer;     // Total "hari" yang di block di Excel
    vTableNameAttendance : String;
    vUPD_FieldName, vUPD_OldValue, vUPD_EmployeeName, vUPD_Blank : String;
    curRecNo : LongInt;
    curField : TField;

  public

  end;

var
  f_attendance: Tf_attendance;

implementation

uses u_datamodule;

{$R *.lfm}

{ Tf_attendance }
function Tf_attendance.Func_checkReference(vKeyFind, vFieldReplace: String
  ): String;
Begin
     With qt_check_reference Do Begin
          Close;
          SQL.Clear;
          SQL.Text  :=
                'SELECT ' + vFieldReplace + ' AS fc_refis '
              + ' FROM '  + vTableNameTempRotateShift
              + ' WHERE (fc_nameis = :fc_nameis) '
              + ' AND   (fc_blank  = :fc_blankis); ';
              ParamByName('fc_nameis').AsString   := vKeyFind;
              ParamByName('fc_blankis').AsString  := 'O';
          Open;

          If qt_check_reference.RecordCount = 1 Then
             Result := qt_check_reference.FieldByName('fc_refis').AsString
          Else
             Result := '*';
     end;
end;

procedure Tf_attendance.REPLACEClick(Sender: TObject);
Var
   xOldValue : String;
begin
  // REPLACE A / B (LABOR ONLY)
  xOldValue := Func_checkReference(vUPD_OldValue, vUPD_FieldName);

     With MyProcessor Do Begin
       Script.Clear;
       Script.Text   :=
                'UPDATE ' + vTableNameTempRotateShift + ' SET '
              + vUPD_FieldName + ' = CASE WHEN length('+vUPD_FieldName+') = 1 then '
              +' CASE WHEN  '+vUPD_FieldName + ' =  ' + QuotedStr('B') + ' then 2 '
              + ' WHEN '+vUPD_FieldName + ' =  ' + QuotedStr('A') + ' then 1 end '
              + 'else '+ vUPD_FieldName +' end '
              + ' WHERE (fc_nameis  = '+QuotedStr(vUPD_EmployeeName)+') '
              + ' AND   (fc_blank  = '+QuotedStr(vUPD_Blank)+');';
       Execute;

    End;
    BitBtnRefreshGridTempRotateShiftClick(Sender);
end;

procedure Tf_attendance.TAKE1DLEFTClick(Sender: TObject);
Var
   xOldValue : String;
begin
  // MENGAMBIL 1 DIGIT SEBELAH KIRI
  xOldValue := Func_checkReference(vUPD_OldValue, vUPD_FieldName);
  With MyProcessor Do Begin
       Script.Clear;
       Script.Text   :=
                'UPDATE ' + vTableNameTempRotateShift + ' SET '
              + vUPD_FieldName + '= LEFT('+ vUPD_FieldName +',1)'
              + ' WHERE (fc_nameis  = '+QuotedStr(vUPD_EmployeeName)+') '
              + ' AND   (fc_blank  = '+QuotedStr(vUPD_Blank)+');';


       Execute;

  End;
  BitBtnRefreshGridTempRotateShiftClick(Sender);
end;

procedure Tf_attendance.TAKE2DLEFTClick(Sender: TObject);
Var
   xOldValue : String;
begin
  // MENGAMBIL 1 DIGIT SEBELAH KIRI
  xOldValue := Func_checkReference(vUPD_OldValue, vUPD_FieldName);
  With MyProcessor Do Begin
       Script.Clear;
       Script.Text   :=
                'UPDATE ' + vTableNameTempRotateShift + ' SET '
              + vUPD_FieldName + '= LEFT('+ vUPD_FieldName +',2)'
              + ' WHERE (fc_nameis  = '+QuotedStr(vUPD_EmployeeName)+') '
              + ' AND   (fc_blank  = '+QuotedStr(vUPD_Blank)+');';



       Execute;

  End;
  BitBtnRefreshGridTempRotateShiftClick(Sender);
end;

procedure Tf_attendance.TAKELEFTRIGHTCHARClick(Sender: TObject);
Var
   xOldValue : String;
begin
  // TAKE LEFT / RIGTH SIDE CHAR
  xOldValue := Func_checkReference(vUPD_OldValue, vUPD_FieldName);

     With MyProcessor Do Begin
       Script.Clear;
       Script.Text   :=
                'UPDATE ' + vTableNameTempRotateShift + ' SET '
              + vUPD_FieldName + ' = CASE WHEN length('+vUPD_FieldName+') = 4 then '
              + ' CONCAT(LEFT('+vUPD_FieldName+',1),RIGHT('+vUPD_FieldName+',1)) ELSE '+ vUPD_FieldName +' END '
              + ' WHERE (fc_nameis  = '+QuotedStr(vUPD_EmployeeName)+') '
              + ' AND   (fc_blank  = '+QuotedStr(vUPD_Blank)+');';
       Execute;

    End;
     BitBtnRefreshGridTempRotateShiftClick(Sender);
end;

procedure Tf_attendance.TAKEMIDCHARClick(Sender: TObject);
Var
   xOldValue : String;
begin
  // MENGAMBIL 1 / 2 DIGIT TENGAH KARAKTER
  xOldValue := Func_checkReference(vUPD_OldValue, vUPD_FieldName);

     With MyProcessor Do Begin
       Script.Clear;
       Script.Text   :=
                'UPDATE ' + vTableNameTempRotateShift + ' SET '
              + vUPD_FieldName + '= SUBSTRING('+ vUPD_FieldName +',2,CASE WHEN length('+vUPD_FieldName+') < 4 then 1 else 2 end )'
              + ' WHERE (fc_nameis  = '+QuotedStr(vUPD_EmployeeName)+') '
              + ' AND   (fc_blank  = '+QuotedStr(vUPD_Blank)+');';
       Execute;

    End;
     BitBtnRefreshGridTempRotateShiftClick(Sender);
end;

procedure Tf_attendance.TRIM1DClick(Sender: TObject);
Var
   xOldValue : String;
begin
  // MENGHAPUS 1 DIGIT SEBELAH KIRI
  xOldValue := Func_checkReference(vUPD_OldValue, vUPD_FieldName);
  With MyProcessor Do Begin
       Script.Clear;
       Script.Text   :=
                'UPDATE ' + vTableNameTempRotateShift + ' SET '
              + vUPD_FieldName + '= SUBSTRING('+ vUPD_FieldName +',2,100)'
              + ' WHERE (fc_nameis  = '+QuotedStr(vUPD_EmployeeName)+') '
              + ' AND   (fc_blank  = '+QuotedStr(vUPD_Blank)+');';
       Execute;

  End;
  BitBtnRefreshGridTempRotateShiftClick(Sender);
end;

procedure Tf_attendance.TRIM2DClick(Sender: TObject);
Var
   xOldValue : String;
begin
  // MENGHAPUS 2 DIGIT SEBELAH KIRI
  xOldValue := Func_checkReference(vUPD_OldValue, vUPD_FieldName);
  With MyProcessor Do Begin
       Script.Clear;
       Script.Text   :=
                'UPDATE ' + vTableNameTempRotateShift + ' SET '
              + vUPD_FieldName + '= SUBSTRING('+ vUPD_FieldName +',3,100)'
              + ' WHERE (fc_nameis  = '+QuotedStr(vUPD_EmployeeName)+') '
              + ' AND   (fc_blank  = '+QuotedStr(vUPD_Blank)+');';
       Execute;

  End;
  BitBtnRefreshGridTempRotateShiftClick(Sender);
end;

procedure Tf_attendance.SetUpGridPickList(FieldName, SQL: String;DBGrid :TDBGrid);
var
   SLPickList: TStringList;
   Query: TZQuery;
   i: Integer;
begin
  SLPickList:= TStringList.Create;
  Query:=TZQuery.Create(Self);
  try
    Query.Connection := DM.MyData;
    Query.SQL.Text:=SQL;
    Query.Open;
    while not Query.EOF do begin
      SLPickList.Add(Query.Fields[0].AsString);
      Query.Next;
    end;
    for i:= 0 to DBGrid.Columns.Count -1 do begin
      if DBGrid.Columns[i].FieldName = FieldName then begin
         DBGrid.Columns[i].PickList := SLPickList;
         Break;
      end;
    end;
  finally
    SLPickList.Free;
    Query.Free;
  end;
end;

procedure Tf_attendance.ImportShiftRotate;
Var
  iField:Integer;
begin

    //With MyProcessor do begin
    // Script.Clear;
    // Script.Text:='truncate x_attendance;';
    // Execute;
    //end;
  
  ShowMessage('Insert starting, Please wait....');
  for iField:= 1 to qt_temprotateshift.FieldCount - 3 do begin
      //Memo5.Lines.Add('insert into t_attendace select from fc_date' + IntToStr(i));
     //date Int

     //insert
     with MyProcessor do begin
        Script.Clear;
        Script.Text:= ' insert into '+vTableNameAttendance+'(fc_branch ,fd_dategenerate,fc_periode ,fd_date, fc_membercode,fc_memberattendancetype ,fc_shift1 ,fc_shift2 ,fc_shift3,fc_daytype ) '
                    + ' select '
                    + ' "001" fc_branch, '
                    + ' SYSDATE() fd_dategenerate, '
                    + ' date_format(:datestart,"%Y%m") fc_periode,'
                    + ' date_add(:datestart, INTERVAL :i day) fd_date, '
                    + ' b.fc_membercode , '
                    + ' b.fc_memberattendancetype, '
                    + ' left(a.fc_date'+IntToStr(iField)+',1) fc_shift1 , '
                    + ' case '
	            + ' when length(a.fc_date'+IntToStr(iField)+') > 1  then mid(a.fc_date'+IntToStr(iField)+',2,1) '
	            + ' else "" '
                    + ' end fc_shift2, '
                    + ' case '
	            + ' when length(a.fc_date'+IntToStr(iField)+') > 2  then right(a.fc_date'+IntToStr(iField)+',1) '
	            + ' else "" '
                    + ' end fc_shift3,case '
                    + ' when a.fc_date'+IntToStr(iField)+' = "" then "" '
                    + ' when c.fc_status = "P" then "H+" '
                    + ' when c.fc_status = "U" then "H" '
                    + ' when dayofweek( date_add(:datestart, INTERVAL :i day) ) = 7 then "S" '
                    + 'else "N" end fc_daytype '
                    + ' from '+ vTableNameTempRotateShift +' a '
                    + ' left join t_member b on a.fc_nameis = b.fc_membername1 '
                    + ' left join t_holiday c on date_add(:datestart, INTERVAL :i day) = c.fd_date '
                    + ' where a.fc_blank <> "O" '
                    + ' on duplicate key update '
                    + ' fc_shift1 = left(fc_date'+IntToStr(iField)+',1), '
                    +' fc_shift2 = '
                    +' case '
	            +' when length(fc_date'+IntToStr(iField)+') > 1  then mid(fc_date'+IntToStr(iField)+',2,1) '
	            +' else "" '
                    +' end, '
                    +' fc_shift3 = '
                    +' case '
	            +' when length(fc_date'+IntToStr(iField)+') > 2  then right(fc_date'+IntToStr(iField)+',1) '
	            +' else "" '
                    +' end ; '
                    ;
        ParamByName('datestart').AsDate:=DateStartImportShiftRotate.Date;
        ParamByName('i').AsInteger:= iField - 1;
        Execute;
     end;
   end;

    with MyProcessor do begin
       Script.Clear;
                    //update shift
       Script.Text:=' update '+vTableNameAttendance+' a '
                    +' left join t_dailytimerange b on b.fc_timerangecode = a.fc_shift1 '
                    +' left join t_dailytimerange c on c.fc_timerangecode = a.fc_shift2 '
                    +' left join t_dailytimerange d on d.fc_timerangecode = a.fc_shift3  '
                    +' set  '
                    +' a.fc_workinshift1 = COALESCE(b.fc_workin,NULL), '
                    +' a.fc_workoutshift1 = COALESCE(b.fc_workout,NULL) , '
                    +' a.fn_totalbreakshift1 =coalesce(TIME_TO_SEC( '
                    +' if( b.fc_workbreakin > b.fc_workbreakout , '
                    +' subtime(b.fc_workbreakin,b.fc_workbreakout ) , '
                    +' subtime(b.fc_workbreakout,b.fc_workbreakin ) '
                    +' ) )/60/60,NULL), '
                    +' a.fc_workinshift2 = COALESCE(c.fc_workin,NULL), '
                    +' a.fc_workoutshift2 = COALESCE(c.fc_workout,NULL) , '
                    +' a.fn_totalbreakshift2 = coalesce(TIME_TO_SEC(  '
                    +' if( b.fc_workbreakin > b.fc_workbreakout , '
                    +' subtime(c.fc_workbreakin,c.fc_workbreakout ) , '
                    +' subtime(c.fc_workbreakout,c.fc_workbreakin ) '
                    +' ) )/60/60,NULL) , '
                    +' a.fc_workinshift3 = COALESCE(d.fc_workin,NULL), '
                    +' a.fc_workoutshift3 = COALESCE(d.fc_workout,NULL) , '
                    +' a.fn_totalbreakshift3 = coalesce(TIME_TO_SEC(  '
                    +' if( d.fc_workbreakin > d.fc_workbreakout , '
                    +' subtime(d.fc_workbreakin,d.fc_workbreakout ) , '
                    +' subtime(d.fc_workbreakout,d.fc_workbreakin ) '
                    +' ) )/60/60,NULL) '
                    +' where a.fc_periode = date_format(:datestart,"%Y%m"); '
                    // update totalhour reference
                    +' update '+vTableNameAttendance+' '
                    +' set '
	            +' fn_totalhourshift1 = coalesce(TIME_TO_SEC( '
		    +'	if( fc_workinshift1 > fc_workoutshift1 , '
                    +' subtime(addtime(fc_workoutshift1 ,"24:00"),fc_workinshift1 ) , '
		    +' subtime(fc_workoutshift1,fc_workinshift1 ) '
		    +' 	) )/60/60,NULL), '
	            +' fn_totalhourshift2 = coalesce(TIME_TO_SEC( '
		    +' if( fc_workinshift2 > fc_workoutshift2 , '
		    +' subtime(addtime(fc_workoutshift2 ,"24:00"),fc_workinshift2 ) , '
		    +' subtime(fc_workoutshift2,fc_workinshift2 ) '
		    +' ) )/60/60,NULL), '
	            +' fn_totalhourshift3 = coalesce(TIME_TO_SEC( '
		    +' if( fc_workinshift3 > fc_workoutshift3 , '
		    +' subtime(addtime(fc_workoutshift3 ,"24:00"),fc_workinshift3 ) , '
		    +' subtime(fc_workoutshift3,fc_workinshift3 ) '
		    +' ) )/60/60,NULL) '
                    +' where fc_periode = date_format(:datestart,"%Y%m"); '
                    //update timein / out shift 1,2,3 date out
                    +' update '+ vTableNameAttendance  +' a '
                    +' set '
                    +' a.fc_timeinshift1 = if( a.fc_timeinshift1 = "" or a.fc_timeinshift1 is NULL,a.fc_workinshift1 ,a.fc_timeinshift1 ), '
                    +' a.fc_timeoutshift1 = if( a.fc_timeoutshift1 = "" or a.fc_timeoutshift1 is NULL,a.fc_workoutshift1 ,a.fc_timeoutshift1 ), '
                    +' a.fc_timeinshift2 = if( a.fc_timeinshift2 = "" or a.fc_timeinshift2 is NULL,a.fc_workinshift2 ,a.fc_timeinshift2 ), '
                    +' a.fc_timeoutshift2 = if( a.fc_timeoutshift2 = "" or a.fc_timeoutshift2 is NULL,a.fc_workoutshift2 ,a.fc_timeoutshift2 ), '
                    +' a.fc_timeinshift3 = if( a.fc_timeinshift3 = "" or a.fc_timeinshift3 is NULL,a.fc_workinshift3 ,a.fc_timeinshift3 ), '
                    +' a.fc_timeoutshift3 = if( a.fc_timeoutshift3 = "" or a.fc_timeoutshift3 is NULL,a.fc_workoutshift3 ,a.fc_timeoutshift3 ) '
                    +' where fc_periode = date_format(:datestart,"%Y%m"); '
                    //Update Totalhour  & effective Hour
                    +' update '+vTableNameAttendance +' a '
                    +' set '
                    +' a.fn_totaltimeshift1 = TIME_TO_SEC( '
	            +' if( a.fc_timeinshift1 > a.fc_timeoutshift1 , '
		    +' subtime(a.fc_timeinshift1 ,a.fc_timeoutshift1 ) , '
		    +' subtime(a.fc_timeoutshift1, a.fc_timeinshift1 ) '
	            +' ) )/60/60, '
                    +' a.fn_totaltimeshift2 = TIME_TO_SEC( '
	            +' if( a.fc_timeinshift2 > a.fc_timeoutshift2 , '
		    +' subtime(a.fc_timeinshift2 ,a.fc_timeoutshift2 ) , '
		    +' subtime(a.fc_timeoutshift2, a.fc_timeinshift2 ) '
	            +' ) )/60/60, '
                    +' a.fn_totaltimeshift3 = TIME_TO_SEC( '
	            +' if( a.fc_timeinshift3 > a.fc_timeoutshift3 , '
		    +' subtime(a.fc_timeinshift3 ,a.fc_timeoutshift3 ) , '
		    +' subtime(a.fc_timeoutshift3, a.fc_timeinshift3 ) '
	            +' ) )/60/60, '
                    +' a.fn_totalhour = a.fn_totaltimeshift1 + coalesce(a.fn_totaltimeshift2,0 ) + coalesce(a.fn_totaltimeshift3,0), '
                    +' a.fn_effectivehour = a.fn_totaltimeshift1 + coalesce(a.fn_totaltimeshift2,0) + coalesce(a.fn_totaltimeshift3,0) - '
                    +' ( coalesce(a.fn_totalbreakshift1,0) + coalesce(a.fn_totalbreakshift2,0) + coalesce(a.fn_totalbreakshift3,0)  )  '
                    +' where fc_periode = date_format(:datestart,"%Y%m"); '
                   //update TI1,TI2,TI3
     //              +' update ' + vTableNameAttendance
     //              +' set '
     //              +' fn_effectivehour = '
     //              +' case '
	    //       +' when fc_ti in ("TI1","NR1") then '
		   //+' fn_effectivehour + 1 '
	    //       +' when fc_ti in ("TI2","NR2") then '
		   //+' fn_effectivehour + 2 '
	    //       +' when fc_ti in ("TI3","NR3") then '
		   //+' fn_effectivehour + 3 '
	    //       +' else '
		   //+' fn_effectivehour '
     //              +' end '
     //              +' where fc_periode = date_format(:datestart,"%Y%m"); '
                    //OT Update
                    +' update '+vTableNameAttendance
                   +' set '
                   +' fn_ot1 = '
                   +' case when (fn_totalhourshift1 - fn_totalbreakshift1 ) = 8  then '
	                   +' case when fc_daytype = "N" then '
		              +' case when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) >= 1 then '
			         + ' 1 '
		              +' when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) >= 0 then '
			         +' fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) '
		              +' else 0 end '
	                   +' else 0 end '
                    +' when (fn_totalhourshift1 - fn_totalbreakshift1 ) = 7 then '
	                    +' case when fc_daytype = "N" then '
		               +' case when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) >= 1 then '
			          +' 1 '
		               +' when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) >= 0 then '
			          +' fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) '
		               +' else 0 end '
	                    +' when fc_daytype = "S" then '
		               +' case when fn_effectivehour > 5 then '
			          +' case when fn_effectivehour - 5 >= 1 then '
			             +'1'
			             +' when fn_effectivehour - 5 <= 1 then '
				        +' fn_effectivehour - 5 '
			             +' else 0 end '
		                  +' else 0 end '
	                    +' else 0 end '
                    +' else NULL end, '
                    +' fn_ot2 = '
                    +' case when (fn_totalhourshift1 - fn_totalbreakshift1 ) = 8  then '
	                    +' case when fc_daytype = "N" then '
		                    +' case when fn_ot1 >=1 then '
			                    +' fn_effectivehour - fn_ot1 - (fn_totalhourshift1 - fn_totalbreakshift1 ) '
		                    +' else 0 end '
	                    +' when fc_daytype in ("S","H","O") then '
		                    +' case when fn_effectivehour - 7 >= 0 then '
			                    +' 7 '
		                    +' when fn_effectivehour - 7 <= 0 then '
			                    +' fn_effectivehour '
		                    +' else 0 end '
	                    +' else 0 end '
                    +' when (fn_totalhourshift1 - fn_totalbreakshift1 ) = 7 then '
	                    +' case when fc_daytype = "N" then '
		                    +' case when fn_ot1 >= 1 then '
			               +' fn_effectivehour - fn_ot1 - (fn_totalhourshift1 - fn_totalbreakshift1 ) '
		                    +' else 0 end '
	                    +' when fc_daytype = "S" then '
		                    +' case when fn_ot1 >= 1 then '
			                    +' case when fn_effectivehour - 5 >= 0 then '
				                    +' fn_effectivehour - fn_ot1 - 5 '
			                    +' when fn_effectivehour - 5 <= 0 then '
				                    +' fn_effectivehour '
			                    +' else 0 end '
		                    +' else 0 end '
	                    +' when fc_daytype in ("H","O") then '
		                    +' case when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) >= 0 then '
			                    +' 7 '
		                    +' when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) <= 0 then '
			                    +' fn_effectivehour '
		                    +' else 0 end '
	                    +' else 0 end '
                    +' else NULL end, '
                    +' fn_ot3 = '
                    +' case when (fn_totalhourshift1 - fn_totalbreakshift1 ) = 8  then '
	                    +' case when fc_daytype in ("S","H","O") then '
		                    +' case when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) >= 1 then '
			                    + ' 1 '
		                    +' when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) <= 1 then '
			                    +' if(fn_effectivehour - 7 > 0,fn_effectivehour - 7,0)'
		                    +' else 0 end '
	                    +' else 0 end '
                    +' when (fn_totalhourshift1 - fn_totalbreakshift1 ) = 7 then '
	                    +' case when fc_daytype in ("H","O") then '
		                    +' case when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) >= 1 then '
			                    +' 1 '
		                    +' when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) <= 1 then '
			                    +' if(fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) > 0, fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) ,0 )'
		                    +' else 0 end '
	                    +' else 0 end '
                    +' else NULL end, '
                    +' fn_ot4 = '
                    +' case when (fn_totalhourshift1 - fn_totalbreakshift1 ) = 8  then '
	                    +' case when fc_daytype in ("S","H","O") then '
		                    +' case when fn_ot3 >=1 then '
			                    +' fn_effectivehour - fn_ot3 - 7 '
		                    +' else 0 end '
	                    +' when fc_daytype in ("H+") then '
		                    +' case when fn_effectivehour - 7 >= 0 then '
			                    +' 7 '
		                    +' when fn_effectivehour - 7 <= 0 then '
			                    +' fn_effectivehour '
		                    +' else 0 end '
	                    +' else 0 end '
                    +' when (fn_totalhourshift1 - fn_totalbreakshift1 ) = 7 then '
	                    +' case when fc_daytype in ("H","O") then '
		                    +' case when fn_ot3 >= 1 then '
			                    +' fn_effectivehour - fn_ot3 - (fn_totalhourshift1 - fn_totalbreakshift1 ) '
		                    +' else 0 end '
	                    +' when fc_daytype in ("H+") then '
		                    +' case when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) >= 0 then '
			                    +' 7 '
		                    +' when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) <= 0 then '
			                    +' fn_effectivehour '
		                    +' else 0 end '
	                    +' else 0 end '
                    +' else NULL end, '
                    +' fn_ot5 = '
                    +' case when (fn_totalhourshift1 - fn_totalbreakshift1 ) = 8  then '
	                    +' case when fc_daytype in ("H+") then '
		                    +' case when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) >= 1 then '
			                    +' 1 '
		                    +' when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) <= 1 then '
			                    +' if( fn_effectivehour - 7 > 0 ,fn_effectivehour - 7,0) '
		                    +' else 0 end '
	                    +' else 0 end '
                    +' when (fn_totalhourshift1 - fn_totalbreakshift1 ) = 7 then '
	                    +' case when fc_daytype in ("H+") then '
		                    +' case when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) >= 1 then '
			                    +' 1 '
		                    +' when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) <= 1 then '
			                    +' if( fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) > 0, fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ),0 )'
		                    +' else 0 end '
	                    +' else 0 end '
                    +' else NULL end,'
                    +' fn_ot6 = '
                    +' case when (fn_totalhourshift1 - fn_totalbreakshift1 ) = 8  then '
	                    +' case when fc_daytype in ("H+") then '
		                    +' case when fn_ot5 >=1 then '
			                    +' fn_effectivehour - fn_ot5 - 7 '
		                    +' else 0 end '
	                    +' else 0 end '
                    +' when (fn_totalhourshift1 - fn_totalbreakshift1 ) = 7 then '
	                    +' case when fc_daytype in ("H+") then '
		                    +' case when fn_ot5 >= 1 then '
			                    +' fn_effectivehour - fn_ot5 - (fn_totalhourshift1 - fn_totalbreakshift1 ) '
		                    +' else 0 end '
	                    +' else 0 end '
                    +' else NULL end '
                    +' where fc_periode = date_format(:datestart,"%Y%m"); '
                      ;
       ParamByName('datestart').AsDate:=DateStartImportShiftRotate.Date;
       Execute;
    end;
  ShowMessage('Import shift rotate Success');
end;

procedure Tf_attendance.BitBtnClearMemoClick(Sender: TObject);
begin
  MemoDelimeter.SelectAll;
  MemoDelimeter.Clear;
end;

procedure Tf_attendance.BitBtn1Click(Sender: TObject);
begin
  Panel2.Width:=0;
end;


procedure Tf_attendance.BitBtnApplyPeriodeClick(Sender: TObject);
begin
  if ComboBoxMonthPeriode.ItemIndex = 0 then begin
     DateStartImportShiftRotate.Date:=StrToDate('01'+DateSeparator+'16'+DateSeparator+EditYearPeriode.Text);
     DateFinishImportShiftRotate.Date:=StrToDate('02'+DateSeparator+'15'+DateSeparator+EditYearPeriode.Text);
  end else if ComboBoxMonthPeriode.ItemIndex = 1 then begin
     DateStartImportShiftRotate.Date:=StrToDate('02'+DateSeparator+'16'+DateSeparator+EditYearPeriode.Text);
     DateFinishImportShiftRotate.Date:=StrToDate('03'+DateSeparator+'15'+DateSeparator+EditYearPeriode.Text);
  end else if ComboBoxMonthPeriode.ItemIndex = 2 then begin
     DateStartImportShiftRotate.Date:=StrToDate('03'+DateSeparator+'16'+DateSeparator+EditYearPeriode.Text);
     DateFinishImportShiftRotate.Date:=StrToDate('04'+DateSeparator+'15'+DateSeparator+EditYearPeriode.Text);
  end else if ComboBoxMonthPeriode.ItemIndex = 3 then begin
     DateStartImportShiftRotate.Date:=StrToDate('04'+DateSeparator+'16'+DateSeparator+EditYearPeriode.Text);
     DateFinishImportShiftRotate.Date:=StrToDate('05'+DateSeparator+'15'+DateSeparator+EditYearPeriode.Text);
  end else if ComboBoxMonthPeriode.ItemIndex = 4 then begin
     DateStartImportShiftRotate.Date:=StrToDate('05'+DateSeparator+'16'+DateSeparator+EditYearPeriode.Text);
     DateFinishImportShiftRotate.Date:=StrToDate('06'+DateSeparator+'15'+DateSeparator+EditYearPeriode.Text);
  end else if ComboBoxMonthPeriode.ItemIndex = 5 then begin
     DateStartImportShiftRotate.Date:=StrToDate('06'+DateSeparator+'16'+DateSeparator+EditYearPeriode.Text);
     DateFinishImportShiftRotate.Date:=StrToDate('07'+DateSeparator+'15'+DateSeparator+EditYearPeriode.Text);
  end else if ComboBoxMonthPeriode.ItemIndex = 6 then begin
     DateStartImportShiftRotate.Date:=StrToDate('07'+DateSeparator+'16'+DateSeparator+EditYearPeriode.Text);
     DateFinishImportShiftRotate.Date:=StrToDate('08'+DateSeparator+'15'+DateSeparator+EditYearPeriode.Text);
  end else if ComboBoxMonthPeriode.ItemIndex = 7 then begin
     DateStartImportShiftRotate.Date:=StrToDate('08'+DateSeparator+'16'+DateSeparator+EditYearPeriode.Text);
     DateFinishImportShiftRotate.Date:=StrToDate('09'+DateSeparator+'15'+DateSeparator+EditYearPeriode.Text);
  end else if ComboBoxMonthPeriode.ItemIndex = 8 then begin
     DateStartImportShiftRotate.Date:=StrToDate('09'+DateSeparator+'16'+DateSeparator+EditYearPeriode.Text);
     DateFinishImportShiftRotate.Date:=StrToDate('10'+DateSeparator+'15'+DateSeparator+EditYearPeriode.Text);
  end else if ComboBoxMonthPeriode.ItemIndex = 9 then begin
     DateStartImportShiftRotate.Date:=StrToDate('10'+DateSeparator+'16'+DateSeparator+EditYearPeriode.Text);
     DateFinishImportShiftRotate.Date:=StrToDate('11'+DateSeparator+'15'+DateSeparator+EditYearPeriode.Text);
  end else if ComboBoxMonthPeriode.ItemIndex = 10 then begin
     DateStartImportShiftRotate.Date:=StrToDate('11'+DateSeparator+'16'+DateSeparator+EditYearPeriode.Text);
     DateFinishImportShiftRotate.Date:=StrToDate('12'+DateSeparator+'15'+DateSeparator+EditYearPeriode.Text);
  end else if ComboBoxMonthPeriode.ItemIndex = 11 then begin
     DateStartImportShiftRotate.Date:=StrToDate('12'+DateSeparator+'16'+DateSeparator+EditYearPeriode.Text);
     DateFinishImportShiftRotate.Date:=StrToDate('01'+DateSeparator+'15'+DateSeparator+IntToStr(StrToInt(EditYearPeriode.Text) + 1));
  end;

  DateFinishImportShiftRotate.OnExit(Sender);
end;

procedure Tf_attendance.BitBtnClearMemoReplaceRSCodeClick(Sender: TObject);
begin
  MemoReplaceRotateShiftCode.Clear;
end;

procedure Tf_attendance.BitBtn2Click(Sender: TObject);
begin
  Panel11.Width:=0;
end;

procedure Tf_attendance.BitBtnCreateGrid1Click(Sender: TObject);
var
   vScriptTempAttendance: String;
begin
  vTableNameTempAttendance:='t_tempodavidattendancedate';
  vScriptTempAttendance  :=
  'DROP TABLE IF EXISTS ' + vTableNameTempAttendance + ';'
           +' CREATE TABLE `'+vTableNameTempAttendance+'` ( '
           +' `fc_no` varchar(30) DEFAULT NULL, '
           +' `fc_membercode` varchar(30) DEFAULT NULL, '
           +' `fc_nofinger` varchar(30) DEFAULT NULL, '
           +' `fc_name` varchar(50) DEFAULT NULL, '
           +' `fc_positionmember` varchar(50) DEFAULT NULL, '
           +' `fc_datein` varchar(50) DEFAULT NULL, '
           +' `fc_timein` varchar(50) DEFAULT NULL, '
           +' `fc_description` varchar(50) DEFAULT NULL, '
           +' `fc_dateout` varchar(50) DEFAULT NULL, '
           +' `fc_timeout` varchar(50) DEFAULT NULL '
           +');'
        + ' LOAD DATA LOCAL INFILE '
        + QuotedStr(
             StringReplace(OpenDialog.FileName,
                           '\','\\',
                           [rfReplaceAll, rfIgnoreCase]
                          )
                   )
        + ' INTO TABLE '
        + vTableNameTempAttendance
        + ' '
        + 'FIELDS TERMINATED BY ' + QuotedStr(';') +';'
        ;
  //ShowMessage(OpenDialog.FileName);
  With MyProcessor Do Begin
       Script.Clear;
       Script.Text := vScriptTempAttendance;
       Execute;
  End;
  ShowMessage('Import CSV Attendance Success !!');
  With qt_temp_attendance do begin
     Close;
     SQL.Text:='select * from '+vTableNameTempAttendance;
     Open;
  end;
end;

procedure Tf_attendance.BitBtnCreateGridClick(Sender: TObject);
Var
  vPrefixDate           : Integer;     // Urut-urutan hari-per-hari
  vSCriptLoadDataInfile : String;      // Script untuk LOAD DATA INFILE. Karena dipakai 2x
  vPoolStringList       : TStringList; // untuk nampung Field Tanggal
begin
  vTableNameTempRotateShift           := 't_tempdavidrotateshift';

  vPoolStringList      := TStringList.Create;
  With vPoolStringList Do Begin

     // Jika sudah ada maka akan dihapus.
     Add('DROP TABLE IF EXISTS ' + vTableNameTempRotateShift + ';');
     Add('CREATE TABLE ' + vTableNameTempRotateShift + ' (');

     {
     2 field dibawah ini MUTLAK ADA. Jadi ditambahkan manual
       fc_nameis : nama karyawan dari Excelnya
       fc_blank  : karena hasil PASTE selalu ada kolom/field
                   kosong ndek depan`e
     }
     Add('fc_nameis varchar(100) not null default '+ QuotedStr('') +',');
     Add('fc_blank  varchar(2)   not null default '+ QuotedStr('') +',');

     For vPrefixDate := 1 to vCountFieldDate + 1 Do Begin
         If vPrefixDate < vCountFieldDate + 1 Then
            Add(
                'fc_date' + IntToStr(vPrefixDate)
                          + ' char(40) not null default '
                          + QuotedSTr('')
                          + ','
               )
         Else
           Add(
               'fc_date' + IntToStr(vPrefixDate)
                         + ' char(40) not null default '
                         + QuotedSTr('')
              );
     End;
     Add(');');
  End;

  vSCriptLoadDataInfile  :=
        'LOAD DATA LOCAL INFILE '
        + QuotedStr(
             StringReplace(OpenDialog.FileName,
                           '\','\\',
                           [rfReplaceAll, rfIgnoreCase]
                          )
                   )
        + ' INTO TABLE '
        + vTableNameTempRotateShift
        + ' '
        + 'FIELDS TERMINATED BY ' + QuotedStr(';');
  With MyProcessor Do Begin
       Script.Clear;
       Script.Text :=
              // INSERT 1st "Insert Group"
              vPoolStringList.Text
              + vSCriptLoadDataInfile
              + ';'
              // Marking / UPDATE the 1st "Insert Group" with (O)
              + ' UPDATE ' + vTableNameTempRotateShift
              + ' SET fc_blank = ' + QuotedStr('O')
              + ';'
              +
              // INSRET 2nd "Insert Group" without marking (O)
              // And user could modify record without marking.
              vSCriptLoadDataInfile
              + ';'
              + ' UPDATE ' + vTableNameTempRotateShift
              + ' SET fc_nameis = LTRIM(fc_nameis);';
       Execute;
  End;
  vPoolStringList.Free;
  PaintGrid;
end;


procedure Tf_attendance.BitBtnMergeShiftAttendance1Click(Sender: TObject);
begin
  with MyProcessor do begin
     Script.Clear;
     Script.Text:= // update real date in /out
                  ' update ' +vTableNameAttendance+' a '
                   +' left join '+ vTableNameTempAttendance +' b '
                   +' on a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d") '
                   +' and a.fc_membercode = concat("TSI ",b.fc_membercode) '
                   +' left join t_dailytimerange c on left(concat(fc_shift1 ,fc_shift2,fc_shift3),1) = c.fc_timerangecode '
                   +' left join t_dailytimerange d on right(concat(fc_shift1 ,fc_shift2,fc_shift3),1) = d.fc_timerangecode '
                   +' set '
                   +' a.fd_realdatein = STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"), '
                   +' a.fd_realdateout = STR_TO_DATE(b.fc_dateout ,"%m/%d/%Y %H:%i:%s") '
                   +' where a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d") '
                   +' and a.fc_membercode = concat("TSI ",b.fc_membercode) ; '
                   //UPDATE  fc_date in/out
            //       +' update ' + vTableNameAttendance + ' a '
            //       +' left join '+ vTableNameTempAttendance +' b '
            //       +' on a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d") '
            //       +' and a.fc_membercode = concat("TSI ",b.fc_membercode) '
            //       +' left join t_dailytimerange c on left(concat(a.fc_shift1 ,a.fc_shift2,a.fc_shift3),1) = c.fc_timerangecode '
            //       +' left join t_dailytimerange d on right(concat(a.fc_shift1 ,a.fc_shift2,a.fc_shift3),1) = d.fc_timerangecode '
            //       +' set '
            //       +' a.fc_datein = '
            //       +' case '
            //       +' when '
	           //+' a.fd_realdatein < concat(DATE_FORMAT(a.fd_realdatein ,"%Y-%m-%d") ," ",coalesce(c.fc_workin,"00:00"))'
	           //+' then concat(DATE_FORMAT(a.fd_realdatein ,"%Y-%m-%d") ," ",coalesce(c.fc_workin,"00:00")) '
            //       +' else a.fd_realdatein '
            //       +' end, '
            //       +' a.fc_dateout = '
            //       +' case '
            //       +' when time_to_sec(timediff(a.fd_realdateout, concat(DATE_FORMAT(a.fd_realdateout ,"%Y-%m-%d") ," ",coalesce(d.fc_workout ,"00:00") ) ) )/60/60 > 1.5 '
	           //+' then a.fd_realdateout '
            //       +' when a.fd_realdateout > concat(DATE_FORMAT(a.fd_realdateout ,"%Y-%m-%d") ," ",coalesce(d.fc_workout ,"00:00")) '
            //       +' then  concat(DATE_FORMAT(a.fd_realdateout ,"%Y-%m-%d") ," ",coalesce(d.fc_workout ,"00:00")) '
            //       +' when a.fd_realdateout < concat(DATE_FORMAT(a.fd_realdateout ,"%Y-%m-%d") ," ",coalesce(d.fc_workout ,"00:00")) '
            //       +' then a.fd_realdateout '
            //       +' else a.fd_realdateout '
            //       +' end '
            //       +' where a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d") '
            //       +' and a.fc_membercode = concat("TSI ",b.fc_membercode) ; '
                    //UPDATE  total hour
                   //+' update ' + vTableNameAttendance + ' a '
                   //+' left join '+ vTableNameTempAttendance +' b '
                   //+' on a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d") '
                   //+' and a.fc_membercode = concat("TSI ",b.fc_membercode) '
                   //+' set '
                   //+' a.fn_totalhour = time_to_sec(timediff(a.fc_dateout ,a.fc_datein ) ) / 60 /60 '
                   //+' where a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d") '
                   //+' and a.fc_membercode = concat("TSI ",b.fc_membercode) ; '
                   //UPDATE  effective hour
                   //+' update ' + vTableNameAttendance + ' a '
                   //+' left join '+ vTableNameTempAttendance +' b '
                   //+' on a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d") '
                   //+' and a.fc_membercode = concat("TSI ",b.fc_membercode) '
                   //+' set '
                   //+' a.fn_effectivehour = a.fn_totalhour - (a.fn_totalbreakshift1 + a.fn_totalbreakshift2 + a.fn_totalbreakshift3 ) '
                   //+' where a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d") '
                   //+' and a.fc_membercode = concat("TSI ",b.fc_membercode) ; '
                   //UPDATE  ot1
     //              +' update ' + vTableNameAttendance + ' a '
     //              +' left join '+ vTableNameTempAttendance +' b '
     //              +' on a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d") '
     //              +' and a.fc_membercode = concat("TSI ",b.fc_membercode) '
     //              +' set '
     //              +' a.fn_ot1 = '
     //              +' case when (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) = 8  then '
	    //       +' case when a.fc_daytype = "N" then '
		   //+' case when a.fn_effectivehour - ( a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) >= 1 then '
		   //+'	1 '
		   //+' when a.fn_effectivehour - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) >= 0 then '
     //              +' a.fn_effectivehour - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) '
		   //+' else 0 end '
	    //       +' else 0 end '
     //              +' when (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) = 7 then '
	    //       +' case when a.fc_daytype = "N" then '
		   //+' case when a.fn_effectivehour - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) >= 1 then '
		   //+' 1 '
		   //+' when a.fn_effectivehour - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) >= 0 then '
     //              +' a.fn_effectivehour - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) '
		   //+' else 0 end '
	    //       +' when a.fc_daytype = "S" then '
		   //+' case when a.fn_effectivehour > 5 then '
		   //+' case when a.fn_effectivehour - 5 >= 1 then '
		   //+' 1 '
		   //+' when a.fn_effectivehour - 5 <= 1 then '
		   //+' a.fn_effectivehour - 5 '
		   //+' else 0 end '
		   //+' else 0 end '
	    //       +' else 0 end '
     //              +' else 0 end '
     //              +' where a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d") '
     //              +' and a.fc_membercode = concat("TSI ",b.fc_membercode) ; '
                   //UPDATE  ot2
     //              +' update ' + vTableNameAttendance + ' a '
     //              +' left join '+ vTableNameTempAttendance +' b '
     //              +' on a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d") '
     //              +' and a.fc_membercode = concat("TSI ",b.fc_membercode) '
     //              +' set '
     //              +' a.fn_ot2 = '
     //              +' case when (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) = 8  then '
	    //       +' case when a.fc_daytype = "N" then '
		   //+' case when a.fn_ot1 >=1 then '
		   //+' a.fn_effectivehour - a.fn_ot1 - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) '
		   //+' else 0 end '
	    //       +' when a.fc_daytype in ("S","H","O") then '
		   //+' case when a.fn_effectivehour - 7 >= 0 then '
		   //+' 7 '
		   //+' when a.fn_effectivehour - 7 <= 0 then '
		   //+' a.fn_effectivehour '
		   //+' else 0 end '
	    //       +' else 0 end '
     //              +' when (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) = 7 then '
	    //       +' case when a.fc_daytype = "N" then '
		   //+' case when a.fn_ot1 >= 1 then '
		   //+' a.fn_effectivehour - a.fn_ot1 - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) '
		   //+' else 0 end '
	    //       +' when a.fc_daytype = "S" then '
		   //+' case when a.fn_ot1 >= 1 then '
		   //+' case when a.fn_effectivehour - 5 >= 0 then '
		   //+' a.fn_effectivehour - a.fn_ot1 - 5 '
		   //+' when a.fn_effectivehour - 5 <= 0 then '
		   //+' a.fn_effectivehour '
		   //+' else 0 end '
		   //+' else 0 end '
	    //       +' when a.fc_daytype in ("H","O") then '
		   //+' case when a.fn_effectivehour - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) >= 0 then '
		   //+' 7 '
		   //+' when a.fn_effectivehour - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) <= 0 then '
		   //+' a.fn_effectivehour '
		   //+' else 0 end '
	    //       +' else 0 end '
     //              +' else 0 end '
     //              +' where a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d") '
     //              +' and a.fc_membercode = concat("TSI ",b.fc_membercode) ; '
                   //UPDATE  ot3
     //              +' update ' + vTableNameAttendance + ' a '
     //              +' left join '+ vTableNameTempAttendance +' b '
     //              +' on a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d") '
     //              +' and a.fc_membercode = concat("TSI ",b.fc_membercode) '
     //              +' set '
     //              +' a.fn_ot3 = '
     //              +' case when (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) = 8  then '
	    //       +' case when a.fc_daytype in ("S","H","O") then '
		   //+' case when a.fn_effectivehour - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) >= 1 then '
		   //+' 1 '
		   //+' when a.fn_effectivehour - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) <= 1 then '
		   //+' if(a.fn_effectivehour - 7 > 0,a.fn_effectivehour - 7,0) '
		   //+' else 0 end '
	    //       +' else 0 end '
     //              +' when (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) = 7 then '
	    //       +' case when a.fc_daytype in ("H","O") then '
		   //+' case when a.fn_effectivehour - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) >= 1 then '
		   //+' 1 '
		   //+' when a.fn_effectivehour - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) <= 1 then '
		   //+' if(a.fn_effectivehour - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) > 0,a.fn_effectivehour - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ),0) '
		   //+' else 0 end '
	    //       +' else 0 end '
     //              +' else 0 end '
     //              +' where a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d") '
     //              +' and a.fc_membercode = concat("TSI ",b.fc_membercode) ; '
                   //UPDATE  ot4
     //              +' update ' + vTableNameAttendance + ' a '
     //              +' left join '+ vTableNameTempAttendance +' b '
     //              +' on a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d") '
     //              +' and a.fc_membercode = concat("TSI ",b.fc_membercode) '
     //              +' set '
     //              +' fn_ot4 = '
     //              +' case when (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) = 8  then '
	    //       +' case when a.fc_daytype in ("S","H","O") then '
		   //+' case when a.fn_ot3 >=1 then '
		   //+' a.fn_effectivehour - fn_ot3 - 7 '
		   //+' else 0 end '
	    //       +' when a.fc_daytype in ("H+") then '
		   //+' case when a.fn_effectivehour - 7 >= 0 then '
		   //+' 7 '
		   //+' when a.fn_effectivehour - 7 <= 0 then '
		   //+' a.fn_effectivehour '
		   //+' else 0 end '
	    //       +' else 0 end '
     //              +' when (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) = 7 then '
	    //       +' case when a.fc_daytype in ("H","O") then '
		   //+' case when a.fn_ot3 >= 1 then '
		   //+' a.fn_effectivehour - a.fn_ot3 - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) '
		   //+' else 0 end '
	    //       +' when a.fc_daytype in ("H+") then '
		   //+' case when a.fn_effectivehour - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) >= 0 then '
		   //+' 7 '
		   //+' when a.fn_effectivehour - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) <= 0 then '
		   //+' a.fn_effectivehour '
		   //+' else 0 end '
	    //       +' else 0 end '
     //              +' else 0 end '
     //              +' where a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d") '
     //              +' and a.fc_membercode = concat("TSI ",b.fc_membercode) ; '
                    //UPDATE  ot5
     //              +' update ' + vTableNameAttendance + ' a '
     //              +' left join '+ vTableNameTempAttendance +' b '
     //              +' on a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d") '
     //              +' and a.fc_membercode = concat("TSI ",b.fc_membercode) '
     //              +' set '
     //              +' a.fn_ot5 = '
     //              +' case when (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) = 8  then '
	    //       +' case when a.fc_daytype in ("H+") then '
		   //+' case when a.fn_effectivehour - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) >= 1 then '
		   //+' 1 '
		   //+' when a.fn_effectivehour - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) <= 1 then '
		   //+' if(a.fn_effectivehour - 7 > 0,a.fn_effectivehour - 7,0 )'
		   //+' else 0 end '
	    //       +' else 0 end '
     //              +' when (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) = 7 then '
	    //       +' case when a.fc_daytype in ("H+") then '
		   //+' case when a.fn_effectivehour - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) >= 1 then '
		   //+' 1 '
		   //+' when a.fn_effectivehour - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) <= 1 then '
		   //+' if(a.fn_effectivehour - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) > 0,a.fn_effectivehour - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ),0 )'
		   //+' else 0 end '
	    //       +' else 0 end '
     //              +' else 0 end '
     //              +' where a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d") '
     //              +' and a.fc_membercode = concat("TSI ",b.fc_membercode) ; '
                   //UPDATE  ot6
     //              +' update ' + vTableNameAttendance + ' a '
     //              +' left join '+ vTableNameTempAttendance +' b '
     //              +' on a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d") '
     //              +' and a.fc_membercode = concat("TSI ",b.fc_membercode) '
     //              +' set '
     //              +' a.fn_ot6 = '
     //              +' case when (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) = 8  then '
	    //       +' case when a.fc_daytype in ("H+") then '
		   //+' case when a.fn_ot5 >=1 then '
		   //+' a.fn_effectivehour - a.fn_ot5 - 7 '
		   //+' else 0 end '
	    //       +' else 0 end '
     //              +' when (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) = 7 then '
	    //       +' case when a.fc_daytype in ("H+") then '
		   //+' case when a.fn_ot5 >= 1 then '
		   //+' a.fn_effectivehour - a.fn_ot5 - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) '
		   //+' else 0 end '
	    //       +' else 0 end '
     //              +' else 0 end '
     //              +' where a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d") '
     //              +' and a.fc_membercode = concat("TSI ",b.fc_membercode) ; '
                   ;
     Execute;
  end;
  ShowMessage('Update attendance Success');
end;

procedure Tf_attendance.BitBtnMergeShiftAttendanceClick(Sender: TObject);
var
  Reply, BoxStyle: Integer;
begin
  BoxStyle := MB_ICONQUESTION + MB_YESNO;
  Reply := Application.MessageBox('Are you sure to import rotation shift ?', 'Confirmation', BoxStyle);
  if Reply = IDYES then
         ImportShiftRotate
    else Application.MessageBox('No         ', 'Merge canceled', MB_ICONHAND);
end;

procedure Tf_attendance.BitBtnOpenfile1Click(Sender: TObject);
begin
   if OpenDialog.Execute then begin
      EditOpenFile1.Text:= OpenDialog.FileName;
   end;
end;

procedure Tf_attendance.BitBtnOpenfileClick(Sender: TObject);
begin
   if OpenDialog.Execute then begin
      EditOpenFile.Text:= OpenDialog.FileName;
   end;
end;

procedure Tf_attendance.BitBtnPasteMemoClick(Sender: TObject);
begin
  MemoDelimeter.SelectAll;
  MemoDelimeter.PasteFromClipboard;
end;

procedure Tf_attendance.BitBtnRefreshGridTempRotateShiftClick(Sender: TObject);
begin
  qt_temprotateshift.Close;
  qt_temprotateshift.Open;
  PaintGrid;
  DBGridTempRotateShift.DataSource.DataSet.RecNo:=curRecNo;
  DBGridTempRotateShift.SelectedField:=curField;
end;

procedure Tf_attendance.BitBtnReplaceRSCodeClick(Sender: TObject);
var
  iField : Integer;
  Shifcode,OptReplace : String;
begin

  if ComboBoxOptReplace.ItemIndex = -1 then begin
     ShowMessage('Invalid Option');
     Exit;
  end;

  Shifcode:= Copy(MemoReplaceRotateShiftCode.Text,0,(length(MemoReplaceRotateShiftCode.Text)-1));

  for iField:= 1 to qt_temprotateshift.FieldCount - 3 do begin

     if ComboBoxOptReplace.ItemIndex = 0 then begin
       OptReplace:= '"'+EditReplaceRotateShift.Text+'"' ;
     end
     else if ComboBoxOptReplace.ItemIndex = 1 then begin
       OptReplace:= 'SUBSTRING( fc_date'+IntToStr(iField)+',2,100)' ;
     end
     else if ComboBoxOptReplace.ItemIndex = 2 then begin
       OptReplace:= 'SUBSTRING( fc_date'+IntToStr(iField)+',3,100)' ;
     end
     else if ComboBoxOptReplace.ItemIndex = 3 then begin
       OptReplace:= 'LEFT( fc_date'+IntToStr(iField)+',1)' ;
     end
     else if ComboBoxOptReplace.ItemIndex = 4 then begin
       OptReplace:= 'LEFT( fc_date'+IntToStr(iField)+',2)' ;
     end
     else if ComboBoxOptReplace.ItemIndex = 5 then begin
       OptReplace:= 'SUBSTRING(fc_date'+IntToStr(iField)+',2,CASE WHEN length(fc_date'+IntToStr(iField)+') < 4 then 1 else 2 end )' ;
     end
     else if ComboBoxOptReplace.ItemIndex = 6 then begin
       OptReplace:= 'case when fc_date'+IntToStr(iField)+' = "A" then 1 when fc_date'+IntToStr(iField)+' = "B" then 2 end ' ;
     end
     else if ComboBoxOptReplace.ItemIndex = 7 then begin
       OptReplace:= 'concat( left(fc_date'+IntToStr(iField)+',1) , right(fc_date'+IntToStr(iField)+',1)  ) ';
     end
     else if ComboBoxOptReplace.ItemIndex = 8 then begin
       OptReplace:= 'concat( substring(fc_date'+IntToStr(iField)+',1,1) , substring(fc_date'+IntToStr(iField)+',3,1)  ) ';
     end;

     with MyProcessor do begin
        Script.Clear;
        Script.Text:= ' update '
                    + vTableNameTempRotateShift
                    + ' set '
                    + 'fc_date'+IntToStr(iField)+' = '
                    + OptReplace
                    + ' where fc_date'+IntToStr(iField)
                    +' in ( '+Shifcode+' ) and fc_blank <> "O" ';
        Execute;
     end;
   end;
  BitBtnRefreshGridTempRotateShiftClick(Sender);
  ShowMessage('Replace Done');
  MemoReplaceRotateShiftCode.Clear;
end;

procedure Tf_attendance.BtnDelimiterMemoClick(Sender: TObject);
Var
   vCounterLineMemo: integer;
   vOneLineRecord: string;
   vPositionCharSearching: integer;
begin
  ProgessBarDelimeter.Max         := MemoDelimeter.Lines.Count - 1;

  For vCounterLineMemo     := 0 to MemoDelimeter.Lines.Count - 1 Do Begin
     vOneLineRecord        := MemoDelimeter.Lines[vCounterLineMemo];
     ProgessBarDelimeter.StepIt;
     ProgessBarDelimeter.Position := vCounterLineMemo;
     ProgessBarDelimeter.Update;
     Repeat
       vPositionCharSearching := Pos(chr(9), vOneLineRecord);
       If vPositionCharSearching > 0 Then Begin
          Delete(vOneLineRecord, vPositionCharSearching, Length(chr(9)));
          Insert(';', vOneLineRecord, vPositionCharSearching);
          MemoDelimeter.Lines[vCounterLineMemo] := vOneLineRecord;
       End;
     Until
       vPositionCharSearching = 0;
     MemoDelimeter.Refresh;
     MemoDelimeter.Repaint;
  End;

  EditSaveFile.Text:='FILE' + FormatDateTime('DDMMYYYY',Now) + '.csv';
  EditSaveFile.SetFocus;
end;

procedure Tf_attendance.btnSaveFileClick(Sender: TObject);
begin
  SaveDialog.FileName  := EditSaveFile.Text;
  If SaveDialog.Execute Then MemoDelimeter.Lines.SaveToFile( SaveDialog.Filename );
  MemoDelimeter.Lines.SaveToFile( SaveDialog.Filename );
end;

procedure Tf_attendance.ComboBox1Change(Sender: TObject);
begin
  //if ComboBox1.ItemIndex = 0 then begin
  //   Page1.Show;
  //end else if ComboBox1.ItemIndex = 1 then begin
  //   Page2.Show;
  //end else ShowMessage('Invalid menu');
end;

procedure Tf_attendance.ComboBoxMonthPeriodeChange(Sender: TObject);
begin

end;

procedure Tf_attendance.ComboBoxOptReplaceChange(Sender: TObject);
begin
  if ComboBoxOptReplace.ItemIndex = 0 then begin
     EditReplaceRotateShift.Enabled:=True;
  end else begin
     EditReplaceRotateShift.Text:='';
     EditReplaceRotateShift.Enabled:=False;
  end;
end;

procedure Tf_attendance.DateFinishImportShiftRotateExit(Sender: TObject);
begin
  vCountFieldDate := trunc(DateFinishImportShiftRotate.Date)-trunc(DateStartImportShiftRotate.Date);
  LabelDays.Caption  := IntToStr(vCountFieldDate) + ' Days'
end;

procedure Tf_attendance.DBGridAttendanceEnter(Sender: TObject);
begin
end;

procedure Tf_attendance.DBGridTempRotateShiftCellClick(Column: TColumn);
begin
  curRecNo:= DBGridTempRotateShift.DataSource.DataSet.RecNo;
  curField := DBGridTempRotateShift.SelectedField;

  vUPD_FieldName      := Column.FieldName;
  vUPD_OldValue       := Func_checkReference(
                           qt_temprotateshift.FieldByName('fc_nameis').AsString,
                           Column.FieldName
                                       );
  vUPD_EmployeeName   := qt_temprotateshift.FieldByName('fc_nameis').AsString;
  vUPD_Blank          := qt_temprotateshift.FieldByName('fc_blank').AsString;
end;

procedure Tf_attendance.DBGridTempRotateShiftDblClick(Sender: TObject);
Var
   Memo : TMemo;
   MemoOldValue,ShiftCode : String;
begin
   Memo := MemoReplaceRotateShiftCode;
   MemoOldValue:=Memo.Text;
   ShiftCode := '"'+DBGridTempRotateShift.SelectedField.AsString + '",';
   Memo.Text:= Copy( MemoOldValue,1,Length(MemoOldValue) ) + ShiftCode ;
end;

procedure Tf_attendance.FormCreate(Sender: TObject);
begin
  vTableNameAttendance := 't_attendance';
  EditYearPeriode.Text:=FormatDateTime('YYYY',Now);
end;

procedure Tf_attendance.DateTimePicker1Exit(Sender: TObject);
begin
  vCountFieldDate := trunc(DateFinishImportShiftRotate.Date)-trunc(DateStartImportShiftRotate.Date);
  LabelDays.Caption  := IntToStr(vCountFieldDate) + ' Days'
end;

procedure Tf_attendance.PaintGrid;
Var
   vColumnPaint : Integer;
Begin
  With qt_temprotateshift Do Begin
       Close;
       SQL.Clear;
       SQL.Text :=
                   'select case when coalesce(fc_blank, '+ QuotedStr('X') + ') = ' + QuotedStr('O')
                 + '       then fc_nameis '
                 + '       else '+ QuotedStr('') +' end as Name, '
                 + '       '+vTableNameTempRotateShift+'.* '
                 + 'from '+vTableNameTempRotateShift+' order by fc_nameis ASC, fc_blank DESC;';
       Open;

       //ShowMessage('jumlah elemen : ' + IntToStr(vPoolStringList.Count) + ' vCountFieldDate : ' + IntToStr(vCountFieldDate));
       //With vPoolStringList Do Begin
       For vColumnPaint := 0 To vCountFieldDate + 3 Do Begin
           With DBGridTempRotateShift Do Begin
                // fixed cols
                FixedCols:=4;
                // Mulai Column / field ke 3 isi`e wes tanggal. Jadi judulnya adalah TANGGAL
                If vColumnPaint > 2 Then Begin
                   Columns[vColumnPaint].Width          := 40;
                   Columns[vColumnPaint].Title.Caption  :=
                   FormatDateTime('DD',IncDay(DateStartImportShiftRotate.Date, vColumnPaint - 3));
                End Else Begin
                   // Column 1 dan 2 Judul`e nggawe dewe.
                   Case vColumnPaint Of
                        0 : Begin
                                Columns[vColumnPaint].ReadOnly       := True;
                                Columns[vColumnPaint].Width          := 150;
                                Columns[vColumnPaint].Title.Caption  := 'Employee Name';
                            End;
                        1 : Begin
                                Columns[vColumnPaint].Visible        := False;
                            End;
                        2 : Begin
                                Columns[vColumnPaint].Title.Caption  := '[*]';
                                Columns[vColumnPaint].Width          := 20;
                                Columns[vColumnPaint].ReadOnly       := True;
                            End;
                   End;
                End;
           End;
       End;
  end;

End;

end.

