unit u_presetattendance;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, StdCtrls,
  DBGrids, ZConnection, ZDataset, db;

type

  { Tf_presetup_attendance }

  Tf_presetup_attendance = class(TForm)
    ComboBox1: TComboBox;
    ds_leaversn: TDataSource;
    DBGrid1: TDBGrid;
    DBGrid3: TDBGrid;
    ds_dailytimerange: TDataSource;
    Label1: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label9: TLabel;
    MainPage: TPage;
    Notebook1: TNotebook;
    Page1: TPage;
    Panel1: TPanel;
    Panel11: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel6: TPanel;
    Panel9: TPanel;
    qt_dailytimerange: TZQuery;
    qt_dailytimerangefc_branch: TStringField;
    qt_dailytimerangefc_shiftnamealias: TStringField;
    qt_dailytimerangefc_timerangecode: TStringField;
    qt_dailytimerangefc_timerangedescription: TStringField;
    qt_dailytimerangefc_workallowin: TStringField;
    qt_dailytimerangefc_workallowout: TStringField;
    qt_dailytimerangefc_workbreakin: TStringField;
    qt_dailytimerangefc_workbreakout: TStringField;
    qt_dailytimerangefc_workin: TStringField;
    qt_dailytimerangefc_workout: TStringField;
    qt_dailytimerangefd_datestart: TDateTimeField;
    qt_dailytimerangefl_fastingshift: TSmallintField;
    qt_trxleavefc_branch: TStringField;
    qt_trxleavefc_trxcode: TStringField;
    qt_trxleavefc_trxid: TStringField;
    qt_trxleavefn_trxnumber: TSmallintField;
    qt_trxleavefv_trxdescription: TStringField;
    qt_trxleave: TZQuery;
    procedure ComboBox1Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure qt_dailytimerangefl_fastingshiftChange(Sender: TField);
    procedure qt_dailytimerangeNewRecord(DataSet: TDataSet);
    procedure qt_trxleaveNewRecord(DataSet: TDataSet);
  private

  public

  end;

var
  f_presetup_attendance: Tf_presetup_attendance;

implementation
uses u_datamodule;

{$R *.lfm}

{ Tf_presetup_attendance }

procedure Tf_presetup_attendance.ComboBox1Change(Sender: TObject);
begin
  if ComboBox1.ItemIndex = 0 then begin
     MainPage.Show;
  end else if ComboBox1.ItemIndex =1 then begin
     Page1.Show;
  end;
end;

procedure Tf_presetup_attendance.FormCreate(Sender: TObject);
begin
  qt_dailytimerange.Open;
  qt_trxleave.Open;
end;

procedure Tf_presetup_attendance.qt_dailytimerangefl_fastingshiftChange(
  Sender: TField);
begin
  qt_dailytimerange.ApplyUpdates;
end;

procedure Tf_presetup_attendance.qt_dailytimerangeNewRecord(DataSet: TDataSet);
begin
  qt_dailytimerangefc_branch.AsString        := '001';
  qt_dailytimerangefc_timerangecode.AsString := char(qt_dailytimerange.RecordCount + 65);
end;

procedure Tf_presetup_attendance.qt_trxleaveNewRecord(DataSet: TDataSet);
begin
  qt_trxleave.FieldByName('fc_branch').AsString:='001';
  qt_trxleavefc_trxid.AsString:='LEAVE_TYPE';
end;

end.

