unit u_report_attendance;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, db, Forms, Controls, Graphics, Dialogs, ExtCtrls, StdCtrls,
  Buttons, DBGrids, DBCtrls, ExtDlgs,
  ZDataset, DateTimePicker, LR_Class, LR_DBSet, lr_e_pdf,
  fpstypes, fpspreadsheet, fpsallformats, laz_fpspreadsheet;

const
  OUTPUT_FORMAT = sfExcel5;

type

  { Tf_report_attendance }

  Tf_report_attendance = class(TForm)
    BitBtnFilter: TBitBtn;
    BitBtnPrint: TBitBtn;
    BitBtnExcel: TBitBtn;
    BitBtnRefreshAttendance: TBitBtn;
    CheckBox1: TCheckBox;
    CheckBoxOt: TCheckBox;
    CheckBoxAttendanceFilter: TCheckBox;
    CheckBoxFastingday: TCheckBox;
    CheckBoxOt1: TCheckBox;
    DateTimePicker1: TDateTimePicker;
    DateStartFastingday: TDateTimePicker;
    DateFinishFastingday: TDateTimePicker;
    ds_attendance: TDataSource;
    DBGrid1: TDBGrid;
    EditSearchAttendance: TEdit;
    EditSearchPosition: TEdit;
    EditSearchDivision: TEdit;
    frDBDataSet: TfrDBDataSet;
    frReport: TfrReport;
    frTNPDFExport1: TfrTNPDFExport;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label6: TLabel;
    Memo1: TMemo;
    Memo2: TMemo;
    Panel1: TPanel;
    Panel14: TPanel;
    qt_attendance: TZQuery;
    qt_attendancefastingday: TLargeintField;
    qt_attendancefc_daytype: TStringField;
    qt_attendancefc_memberattendancetype: TStringField;
    qt_attendancefc_membercode: TStringField;
    qt_attendancefc_membername1: TStringField;
    qt_attendancefc_periode: TStringField;
    qt_attendancefc_ti: TStringField;
    qt_attendancefd_date: TDateField;
    qt_attendancefinish_periode: TStringField;
    qt_attendancefn_effectivehour: TFloatField;
    qt_attendancefn_jumlahshift: TLongintField;
    qt_attendancefn_memberothernumber1: TLongintField;
    qt_attendancefn_ot1: TFloatField;
    qt_attendancefn_ot2: TFloatField;
    qt_attendancefn_ot3: TFloatField;
    qt_attendancefn_ot4: TFloatField;
    qt_attendancefn_ot5: TFloatField;
    qt_attendancefn_ot6: TFloatField;
    qt_attendancefn_totalhour: TFloatField;
    qt_attendancefv_description: TStringField;
    qt_attendancefv_trxdescription: TStringField;
    qt_attendanceshift: TStringField;
    qt_attendancestart_periode: TStringField;
    qt_attendancetimein1: TStringField;
    qt_attendancetimein2: TStringField;
    qt_attendancetimein3: TStringField;
    qt_attendancetimeout1: TStringField;
    qt_attendancetimeout2: TStringField;
    qt_attendancetimeout3: TStringField;
    qt_attendancetotalsemua_ot: TFloatField;
    qt_attendancetotalsemua_ot1: TFloatField;
    qt_attendancetotalsemua_ot2: TFloatField;
    qt_attendancetotal_shift12: TLargeintField;
    qt_attendancetotal_shift3: TLargeintField;
    qt_attendancetotal_workday: TLargeintField;
    SaveDialog: TSaveDialog;
    backup_query: TZQuery;
    backup_query_20210119: TZQuery;
    procedure BitBtnExcelClick(Sender: TObject);
    procedure BitBtnFilterClick(Sender: TObject);
    procedure BitBtnPrintClick(Sender: TObject);
    procedure BitBtnRefreshAttendanceClick(Sender: TObject);
    procedure CheckBoxFastingdayChange(Sender: TObject);
    procedure EditSearchAttendanceKeyPress(Sender: TObject; var Key: char);
    procedure EditSearchDivisionKeyPress(Sender: TObject; var Key: char);
    procedure EditSearchPositionKeyPress(Sender: TObject; var Key: char);
    procedure FormCreate(Sender: TObject);
    procedure frReportEnterRect(Memo: TStringList; View: TfrView);
  private

  public

  end;

var
  f_report_attendance: Tf_report_attendance;

implementation

uses u_datamodule;

{$R *.lfm}

{ Tf_report_attendance }

procedure Tf_report_attendance.BitBtnPrintClick(Sender: TObject);
begin

  BitBtnFilterClick(Sender);

  if CheckBoxOt1.Checked then begin
    frReport.LoadFromFile('AttendanceReport.lrf');
  end else frReport.LoadFromFile('AttendanceReport_WO_OT6_OT8.lrf');

  frReport.FindObject('Memo65').Memo.Text:= FormatDateTime('DD/MM/YYYY',DateStartFastingday.Date)
  + ' - '+ FormatDateTime('DD/MM/YYYY',DateFinishFastingday.Date) ;

  if CheckBoxOt.Checked = False then begin
    frReport.FindObject('Memo55').Visible:=false;
    frReport.FindObject('Memo56').Visible:=false;
    frReport.FindObject('Memo57').Visible:=false;
    frReport.FindObject('Memo58').Visible:=false;
  end;

  if CheckBoxFastingday.Checked = False then begin
    frReport.FindObject('Memo64').Visible:=false;
    frReport.FindObject('Memo65').Visible:=false;
    frReport.FindObject('Memo9').Visible:=false;
  end;

  frReport.ShowReport;
end;

procedure Tf_report_attendance.BitBtnRefreshAttendanceClick(Sender: TObject);
begin
  qt_attendance.Close;
  qt_attendance.Open;
end;

procedure Tf_report_attendance.CheckBoxFastingdayChange(Sender: TObject);
begin
  if CheckBoxFastingday.Checked then begin
    DateStartFastingday.Enabled:=True;
    DateFinishFastingday.Enabled:=True;
  end else begin
    DateStartFastingday.Enabled:=False;
    DateFinishFastingday.Enabled:=False;
  end;
end;

procedure Tf_report_attendance.EditSearchAttendanceKeyPress(Sender: TObject;
  var Key: char);
begin
  if Key= chr(13) then BitBtnFilterClick(Sender);
end;

procedure Tf_report_attendance.EditSearchDivisionKeyPress(Sender: TObject;
  var Key: char);
begin
  if Key= chr(13) then BitBtnFilterClick(Sender);
end;

procedure Tf_report_attendance.EditSearchPositionKeyPress(Sender: TObject;
  var Key: char);
begin
  if Key= chr(13) then BitBtnFilterClick(Sender);
end;

procedure Tf_report_attendance.FormCreate(Sender: TObject);
begin
  DateTimePicker1.Date:=DM.NowX;
  CheckBoxFastingdayChange(Sender);
end;

procedure Tf_report_attendance.frReportEnterRect(Memo: TStringList;
  View: TfrView);
begin
  //if frReport.FindObject('Memo25').Memo = 'H' then begin
  //  View.Memo.;
  //end;
end;

procedure Tf_report_attendance.BitBtnExcelClick(Sender: TObject);
var
  MyWorkbook: TsWorkbook;
  MyWorksheet: TsWorksheet;
  MyFormula: TsRPNFormula;
  MyDir: string;
  i: Longint=1;
begin
  // Initialization
    if SaveDialog.Execute then begin
        // Create the spreadsheet
        MyWorkbook := TsWorkbook.Create;
        MyWorksheet := MyWorkbook.AddWorksheet('Attendance');
        MyWorksheet.WriteText(0, 0, 'PERIODE');
        MyWorksheet.WriteFontStyle(0,0,[fssBold]);
        MyWorksheet.WriteText(0, 1, 'DATE');
        MyWorksheet.WriteFontStyle(0,1,[fssBold]);
        MyWorksheet.WriteText(0, 2, 'EMPLOYEE ID');
        MyWorksheet.WriteFontStyle(0,2,[fssBold]);
        MyWorksheet.WriteText(0, 3, 'EMPLOYEE NAME');
        MyWorksheet.WriteFontStyle(0,3,[fssBold]);
        MyWorksheet.WriteText(0, 4, 'EMPLOYEE POSITION');
        MyWorksheet.WriteFontStyle(0,4,[fssBold]);
        MyWorksheet.WriteText(0, 5, 'DAYTYPE');
        MyWorksheet.WriteFontStyle(0,5,[fssBold]);
        MyWorksheet.WriteText(0, 6, 'SHIFT');
        MyWorksheet.WriteFontStyle(0,6,[fssBold]);
        MyWorksheet.WriteText(0, 7, 'TIME IN 1');
        MyWorksheet.WriteFontStyle(0,7,[fssBold]);
        MyWorksheet.WriteText(0, 8, 'TIME OUT 1');
        MyWorksheet.WriteFontStyle(0,8,[fssBold]);
        MyWorksheet.WriteText(0, 9, 'TIME IN 2');
        MyWorksheet.WriteFontStyle(0,9,[fssBold]);
        MyWorksheet.WriteText(0, 10, 'TIME OUT 2');
        MyWorksheet.WriteFontStyle(0,10,[fssBold]);
        MyWorksheet.WriteText(0, 11, 'Total Hour');
        MyWorksheet.WriteFontStyle(0,11,[fssBold]);
        MyWorksheet.WriteText(0, 12, 'Effective Hour');
        MyWorksheet.WriteFontStyle(0,12,[fssBold]);
        MyWorksheet.WriteText(0, 13, '1');
        MyWorksheet.WriteFontStyle(0,13,[fssBold]);
        MyWorksheet.WriteText(0, 14, '2');
        MyWorksheet.WriteFontStyle(0,14,[fssBold]);
        MyWorksheet.WriteText(0, 15, '3');
        MyWorksheet.WriteFontStyle(0,15,[fssBold]);
        MyWorksheet.WriteText(0, 16, '4');
        MyWorksheet.WriteFontStyle(0,16,[fssBold]);
        MyWorksheet.WriteText(0, 17, '6');
        MyWorksheet.WriteFontStyle(0,17,[fssBold]);
        MyWorksheet.WriteText(0, 18, '8');
        MyWorksheet.WriteFontStyle(0,18,[fssBold]);
        MyWorksheet.WriteText(0, 19, 'Jumlah Shift');
        MyWorksheet.WriteFontStyle(0,19,[fssBold]);
        MyWorksheet.WriteText(0, 20, 'No Rest');
        MyWorksheet.WriteFontStyle(0,20,[fssBold]);
        MyWorksheet.WriteText(0, 21, 'DESCRIPTION');
        MyWorksheet.WriteFontStyle(0,21,[fssBold]);
        qt_attendance.First;
        while not qt_attendance.EOF  do begin
          MyWorksheet.WriteText(i, 0, qt_attendance.FieldByName('fc_periode').AsString);
          MyWorksheet.WriteText(i, 1, qt_attendance.FieldByName('fd_date').AsString);
          MyWorksheet.WriteText(i, 2, qt_attendance.FieldByName('fc_membercode').AsString);
          MyWorksheet.WriteText(i, 3, qt_attendance.FieldByName('fc_membername1').AsString);
          MyWorksheet.WriteText(i, 4, qt_attendance.FieldByName('fv_trxdescription').AsString);
          MyWorksheet.WriteText(i, 5, qt_attendance.FieldByName('fc_daytype').AsString);
          MyWorksheet.WriteText(i, 6, qt_attendance.FieldByName('shift').AsString);
          MyWorksheet.WriteText(i, 7, qt_attendance.FieldByName('timein1').AsString);
          MyWorksheet.WriteText(i, 8, qt_attendance.FieldByName('timeout1').AsString);
          MyWorksheet.WriteText(i, 9, qt_attendance.FieldByName('timein2').AsString);
          MyWorksheet.WriteText(i, 10, qt_attendance.FieldByName('timeout2').AsString);
          MyWorksheet.WriteText(i, 11, qt_attendance.FieldByName('fn_totalhour').AsString);
          MyWorksheet.WriteText(i, 12, qt_attendance.FieldByName('fn_effectivehour').AsString);
          MyWorksheet.WriteText(i, 13, qt_attendance.FieldByName('fn_ot1').AsString);
          MyWorksheet.WriteText(i, 14, qt_attendance.FieldByName('fn_ot2').AsString);
          MyWorksheet.WriteText(i, 15, qt_attendance.FieldByName('fn_ot3').AsString);
          MyWorksheet.WriteText(i, 16, qt_attendance.FieldByName('fn_ot4').AsString);
          MyWorksheet.WriteText(i, 17, qt_attendance.FieldByName('fn_ot5').AsString);
          MyWorksheet.WriteText(i, 18, qt_attendance.FieldByName('fn_ot6').AsString);
          MyWorksheet.WriteText(i, 19, qt_attendance.FieldByName('fn_jumlahshift').AsString);
          MyWorksheet.WriteText(i, 20, qt_attendance.FieldByName('fc_ti').AsString);
          MyWorksheet.WriteText(i, 21, qt_attendance.FieldByName('fv_description').AsString);
          qt_attendance.Next;
          inc(i);
        end;
      if FileExists(SaveDialog.Filename+STR_EXCEL_EXTENSION) then DeleteFile(SaveDialog.Filename+STR_EXCEL_EXTENSION);
      MyWorkbook.WriteToFile(SaveDialog.Filename + STR_EXCEL_EXTENSION, OUTPUT_FORMAT);
      MyWorkbook.Free;
    end;
end;

procedure Tf_report_attendance.BitBtnFilterClick(Sender: TObject);
begin
  with qt_attendance do begin
     SQL.Clear;
     if CheckBox1.Checked then SQL.Text:=Memo2.Text
      else SQL.Text:=Memo1.Text;
     Close;
     ParamByName('periode').AsString:=FormatDateTime('YYYYMM',DateTimePicker1.Date);
     ParamByName('startfastingday').AsDate:=DateStartFastingday.Date;
     ParamByName('finishfastingday').AsDate:=DateFinishFastingday.Date;
     ParamByName('member').AsString:='%'+EditSearchAttendance.Text+'%';
     ParamByName('position').AsString:='%'+EditSearchPosition.Text+'%';
     ParamByName('division').AsString:='%'+EditSearchDivision.Text+'%';
     Open;
  end;
end;

end.

