unit u_division;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, db, Forms, Controls, Graphics, Dialogs, DBGrids, ExtCtrls,
  StdCtrls, ComCtrls, ZConnection, ZDataset, ZSqlUpdate;

const
  vq_qt_division = 'select d.fc_branch,d.fc_divisioncode,d.fc_divisionname ,'+
                    'd.fc_divisionmanagerid ,d.fc_divisionsupervisorid ,d.fc_divisionstocktype2 ,' +
                    'm1.fc_membername1 manager,' +
                    'm2.fc_membername1 supervisor ' +
                    'from t_division d ' +
                    'left join t_member m1 on d.fc_divisionmanagerid = m1.fc_membercode '  +
                    'left join t_member m2 on d.fc_divisionsupervisorid = m2.fc_membercode ';

type

  { Tf_division }

  Tf_division = class(TForm)
    ds_vision: TDataSource;
    DBGridDivision: TDBGrid;
    EdtFixColumn: TEdit;
    EdtSearch: TEdit;
    labelSearch: TLabel;
    Mainpage: TPage;
    Memo1: TMemo;
    Notebook1: TNotebook;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    qt_division: TZQuery;
    qt_divisionfc_branch: TStringField;
    qt_divisionfc_divisioncode: TStringField;
    qt_divisionfc_divisionmanagerid: TStringField;
    qt_divisionfc_divisionname: TStringField;
    qt_divisionfc_divisionstocktype2: TStringField;
    qt_divisionfc_divisionsupervisorid: TStringField;
    qt_divisionmanager: TStringField;
    qt_divisionsupervisor: TStringField;
    qt_member_manager: TZReadOnlyQuery;
    qt_member_managerIDMANAGER: TStringField;
    qt_member_managerm1fc_membername1: TStringField;
    qt_member_supervisor: TZReadOnlyQuery;
    qt_member_supervisorIDSUPERVISOR: TStringField;
    qt_member_supervisorm2fc_membername1: TStringField;
    StringField1: TStringField;
    StringField2: TStringField;
    UpDownFIxColumn: TUpDown;
    qut_division: TZUpdateSQL;
    procedure DBGridDivisionColExit(Sender: TObject);
    procedure DBGridDivisionTitleClick(Column: TColumn);
    procedure EdtFixColumnChange(Sender: TObject);
    procedure EdtSearchKeyPress(Sender: TObject; var Key: char);
    procedure FormCreate(Sender: TObject);
    procedure qt_divisionNewRecord(DataSet: TDataSet);
  private
    field_search_division:String;

  public

  end;

var
  f_division: Tf_division;

implementation

uses u_datamodule;
{$R *.lfm}

{ Tf_division }

procedure Tf_division.FormCreate(Sender: TObject);
begin
  qt_member_manager.Open;
  qt_member_supervisor.Open;
  qt_division.Open;
  field_search_division:='fc_divisionname';
  labelSearch.Caption:='Search by DIVISION NAME';
end;

procedure Tf_division.qt_divisionNewRecord(DataSet: TDataSet);
begin
  qt_divisionfc_branch.AsString:='001';
end;

procedure Tf_division.DBGridDivisionTitleClick(Column: TColumn);
begin
  if Column.Field.FieldKind = fkData then begin
    field_search_division:=Column.FieldName;
    Insert('d.',field_search_division,0);
  end else if Column.Field.FieldKind = fkLookup then begin
    field_search_division:= Column.Field.FieldName;
    Insert('.',field_search_division,3);
  end;

  labelSearch.Caption:='Search by '+Column.Title.Caption;
    With qt_division do begin
      Close;
      SQL.Text:= vq_qt_division + ' order by ' + field_search_division;
      Open;
    end;
end;

procedure Tf_division.DBGridDivisionColExit(Sender: TObject);
begin
  qt_division.ApplyUpdates;
end;

procedure Tf_division.EdtFixColumnChange(Sender: TObject);
begin
  if StrToInt(EdtFixColumn.Text) > DBGridDivision.Columns.Count + 1  then begin
     EdtFixColumn.Text := IntToStr(DBGridDivision.Columns.Count + 1) ;
  end else if StrToInt(EdtFixColumn.Text) < 1 then begin
     EdtFixColumn.Text := '1';
  end;
  DBGridDivision.FixedCols:= StrToInt(EdtFixColumn.Text);
end;

procedure Tf_division.EdtSearchKeyPress(Sender: TObject; var Key: char);
begin
  if Key = chr(13) then begin
    With qt_division do Begin
      Close;
      SQL.Text:= vq_qt_division + ' where ' + field_search_division + ' like "%' + EdtSearch.Text +'%" order by ' + field_search_division ;
      Open;
      Memo1.Clear;
      Memo1.Text:= vq_qt_division + ' where ' + field_search_division + ' like "%' + EdtSearch.Text +'%" order by ' + field_search_division;
    End;
  end;
end;

end.

