unit u_performance_appraisal;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, StdCtrls,
  DBGrids, ZDataset, DB, DateTimePicker,
  fpstypes, fpspreadsheet, fpsallformats, laz_fpspreadsheet, fpsexport, fpDBExport;

const
  OUTPUT_FORMAT = sfExcel5;

type

  { Tf_performanceappraisal }

  Tf_performanceappraisal = class(TForm)
    Button1: TButton;
    ds_performanceappraisal: TDataSource;
    DateTimePicker1: TDateTimePicker;
    DBGrid1: TDBGrid;
    Edit1: TEdit;
    FPSExport1: TFPSExport;
    Panel1: TPanel;
    qt_performanceappraisal: TZQuery;
    qt_performanceappraisalalpha: TLargeintField;
    qt_performanceappraisalempposition: TStringField;
    qt_performanceappraisalfc_membercode: TStringField;
    qt_performanceappraisalfc_membername1: TStringField;
    qt_performanceappraisallate: TLargeintField;
    qt_performanceappraisallwp: TLargeintField;
    qt_performanceappraisalpermit: TLargeintField;
    qt_performanceappraisalsick: TLargeintField;
    qt_performanceappraisalyear: TStringField;
    SaveDialog: TSaveDialog;
    procedure Button1Click(Sender: TObject);
    procedure DateTimePicker1Change(Sender: TObject);
    procedure Edit1KeyPress(Sender: TObject; var Key: char);
    procedure FormCreate(Sender: TObject);
  private

  public

  end;

var
  f_performanceappraisal: Tf_performanceappraisal;

implementation

uses
  u_datamodule ;

{$R *.lfm}

{ Tf_performanceappraisal }

procedure Tf_performanceappraisal.Button1Click(Sender: TObject);
var
  MyWorkbook: TsWorkbook;
  MyWorksheet: TsWorksheet;
  MyFormula: TsRPNFormula;
  MyDir: string;
  i: Longint;
begin
  // Initialization
    if SaveDialog.Execute then begin
      if FileExists(SaveDialog.Filename) then DeleteFile(SaveDialog.Filename);
      FPSExport1.FileName:=SaveDialog.FileName;
      FPSExport1.Execute;
    end;
end;

procedure Tf_performanceappraisal.DateTimePicker1Change(Sender: TObject);
begin
  with qt_performanceappraisal do begin
    Close;
    ParamByName('date').AsDate:=DateTimePicker1.Date;
    ParamByName('member').AsString:='%'+Edit1.Text+'%';
    Open;
  end;
end;

procedure Tf_performanceappraisal.Edit1KeyPress(Sender: TObject; var Key: char);
begin
  if Key = Char(13) then DateTimePicker1Change(Sender);
end;

procedure Tf_performanceappraisal.FormCreate(Sender: TObject);
begin
  DateTimePicker1.Date:=DM.NowX;
  DateTimePicker1Change(Sender);
end;

end.

