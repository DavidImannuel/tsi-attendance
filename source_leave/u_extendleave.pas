unit u_extendleave;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, db, Forms, Controls, Graphics, Dialogs, ExtCtrls, StdCtrls,
  Buttons, DBCtrls, ZDataset, DateTimePicker;

type

  { Tf_extendleave }

  Tf_extendleave = class(TForm)
    BtnEdtData1: TBitBtn;
    DateTimePickerInput1: TDateTimePicker;
    DBLookupComboBox2: TDBLookupComboBox;
    EditAmountLeave: TEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    procedure BtnEdtData1Click(Sender: TObject);
  private

  public

  end;

var
  f_extendleave: Tf_extendleave;

implementation
uses u_datamodule,u_leave;

{$R *.lfm}

{ Tf_extendleave }

procedure Tf_extendleave.BtnEdtData1Click(Sender: TObject);
begin
  with DM.MyBatch do begin
    Script.Clear;
    Script.Text:=' update t_member set  fn_memberleaveadd = :amountleave,  fd_expiredleaveadd = :date where fc_membercode = :member ';
    ParamByName('amountleave').AsInteger:=StrToInt(EditAmountLeave.Text);
    ParamByName('date').AsDate:=DateTimePickerInput1.Date;
    ParamByName('member').AsString:=f_leave.qt_memberfc_membercode.AsString;
    Execute;
    ShowMessage('Extend leave success');
  end;
end;

end.

