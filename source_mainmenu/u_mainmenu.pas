unit u_mainmenu;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, Buttons,
  StdCtrls, ComCtrls, ZConnection, RTTICtrls, LR_Class,u_datamodule;

type

  { Tf_mainmenu }

  Tf_mainmenu = class(TForm)
    ComboBox1: TComboBox;
    Edit1: TEdit;
    Edit2: TEdit;
    frReport1: TfrReport;
    Image1: TImage;
    Image2: TImage;
    ImageList1: TImageList;
    Label1: TLabel;
    Label2: TLabel;
    Panel1: TPanel;
    SpeedButton1: TSpeedButton;
    SpeedButtonEmpSetup: TSpeedButton;
    SpeedButtonOldMember: TSpeedButton;
    SpeedButtonPreSetupPos: TSpeedButton;
    SpeedButtonPreSetupDiv: TSpeedButton;
    SpeedButtonReportAttendance: TSpeedButton;
    SpeedButtonFastingday: TSpeedButton;
    SpeedButtonMember: TSpeedButton;
    SpeedButtonOvertime: TSpeedButton;
    SpeedButtonLeave: TSpeedButton;
    SpeedButtonAttendance: TSpeedButton;
    SpeedButtonPreSetup: TSpeedButton;
    SpeedButtonHoliday: TSpeedButton;
    SpeedButtonImportcsv: TSpeedButton;
    StatusBar1: TStatusBar;
    ToggleBox1: TToggleBox;
    procedure ComboBox1Enter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButtonEmpSetupClick(Sender: TObject);
    procedure SpeedButtonFastingdayClick(Sender: TObject);
    procedure SpeedButtonImportcsvClick(Sender: TObject);
    procedure SpeedButtonMemberClick(Sender: TObject);
    procedure SpeedButtonMemberMouseEnter(Sender: TObject);
    procedure SpeedButtonMemberMouseLeave(Sender: TObject);
    procedure SpeedButtonOldMemberClick(Sender: TObject);
    procedure SpeedButtonOvertimeClick(Sender: TObject);
    procedure SpeedButtonLeaveClick(Sender: TObject);
    procedure SpeedButtonAttendanceClick(Sender: TObject);
    procedure SpeedButtonPreSetupPosClick(Sender: TObject);
    procedure SpeedButtonPreSetupDivClick(Sender: TObject);
    procedure SpeedButtonPreSetupClick(Sender: TObject);
    procedure SpeedButtonHolidayClick(Sender: TObject);
    procedure SpeedButtonReportAttendanceClick(Sender: TObject);
    procedure ToggleBox1Click(Sender: TObject);
  private

  public

  end;

var
  f_mainmenu: Tf_mainmenu;

implementation
uses u_member, u_overtime, u_leave,
  u_presetattendance,u_holiday,u_attendance,u_importcsv,u_fastingday,
  u_report_attendance,u_division,u_setupposition,u_empsetup,u_oldmember,
  u_databasesetting;

{$R *.lfm}

{ Tf_mainmenu }


procedure Tf_mainmenu.SpeedButtonMemberMouseEnter(Sender: TObject);
begin
  //SpeedButtonMember.Font.Size := SpeedButtonMember.Font.Size + 5;
  (Sender as TSpeedButton).Font.Size := (Sender as TSpeedButton).Font.Size + 5;
end;

procedure Tf_mainmenu.SpeedButtonMemberMouseLeave(Sender: TObject);
begin
  //SpeedButtonMember.Font.Size := SpeedButtonMember.Font.Size - 5;
  (Sender as TSpeedButton).Font.Size := (Sender as TSpeedButton).Font.Size - 5;
end;

procedure Tf_mainmenu.SpeedButtonOldMemberClick(Sender: TObject);
begin
  f_oldmember.Show;
end;

procedure Tf_mainmenu.SpeedButtonOvertimeClick(Sender: TObject);
begin
   f_overtime.Visible := True;
   f_overtime.Show;
end;

procedure Tf_mainmenu.SpeedButtonLeaveClick(Sender: TObject);
begin
  f_leave.Show;
end;

procedure Tf_mainmenu.SpeedButtonAttendanceClick(Sender: TObject);
begin
 f_attendance.Show;
end;

procedure Tf_mainmenu.SpeedButtonPreSetupPosClick(Sender: TObject);
begin
  f_setupposition.Show;
end;

procedure Tf_mainmenu.SpeedButtonPreSetupDivClick(Sender: TObject);
begin
  f_division.Show;
end;

procedure Tf_mainmenu.SpeedButtonPreSetupClick(Sender: TObject);
begin
  f_presetup_attendance.Show;
end;

procedure Tf_mainmenu.SpeedButtonHolidayClick(Sender: TObject);
begin
  f_holiday.Show;
end;

procedure Tf_mainmenu.SpeedButtonReportAttendanceClick(Sender: TObject);
begin
  f_report_attendance.Show;
end;

procedure Tf_mainmenu.ToggleBox1Click(Sender: TObject);
begin
  if ComboBox1.Items.Strings[ComboBox1.ItemIndex] = 'PRODUCTION-PUBLIC' then begin
    DM.ReadJSONConfig('mysql-production-public','config.json');
  end else if ComboBox1.Items.Strings[ComboBox1.ItemIndex] = 'PRODUCTION-LOCAL' then begin
    DM.ReadJSONConfig('mysql-production-local','config.json');
  end else if ComboBox1.Items.Strings[ComboBox1.ItemIndex] = 'DEVELOPMENT-PUBLIC' then begin
    DM.ReadJSONConfig('mysql-development-public','config.json');
  end else if ComboBox1.Items.Strings[ComboBox1.ItemIndex] = 'DEVELOPMENT-LOCAL' then begin
    DM.ReadJSONConfig('mysql-development-local','config.json');
  end else begin
    MessageDlg('HARAP PILIH SERVER DENGAN BENAR !!',mtError,[mbOK],0);
    Exit;
  end;
  Image2.Visible         :=False;
  SpeedButtonMember.Enabled   := True;
  SpeedButtonOvertime.Enabled   := True;
  SpeedButtonLeave.Enabled   := True;
  SpeedButtonAttendance.Enabled   := True;
  SpeedButtonPreSetup.Enabled   := True;
  SpeedButtonHoliday.Enabled   := True;
  SpeedButtonImportcsv.Enabled:= True;
  SpeedButtonFastingday.Enabled:=True;
  SpeedButtonReportAttendance.Enabled:=True;
  SpeedButtonPreSetupPos.Enabled:=True;
  SpeedButtonPreSetupDiv.Enabled:=True;
  SpeedButtonEmpSetup.Enabled:=True;
  SpeedButtonOldMember.Enabled:=True;
  SpeedButton1.Enabled:=True;
  StatusBar1.Panels[0].Text:='Terhubung dengan mysql server '+DM.MyData.HostName+' PORT '+IntToStr(DM.MyData.Port)+' DB '+DM.MyData.Database;
end;

procedure Tf_mainmenu.SpeedButtonMemberClick(Sender: TObject);
begin
    f_member.Show;
end;

procedure Tf_mainmenu.SpeedButtonImportcsvClick(Sender: TObject);
begin
   f_importcsv.Show;
end;

procedure Tf_mainmenu.SpeedButtonFastingdayClick(Sender: TObject);
begin
  f_fastingday.Show;
end;

procedure Tf_mainmenu.SpeedButtonEmpSetupClick(Sender: TObject);
begin
  f_empsetup.Show;
end;

procedure Tf_mainmenu.SpeedButton1Click(Sender: TObject);
begin
  f_databasesetting.Show;
end;

procedure Tf_mainmenu.ComboBox1Enter(Sender: TObject);
begin
  ComboBox1.ItemIndex:=0;
end;

procedure Tf_mainmenu.FormShow(Sender: TObject);
begin
  StatusBar1.Panels[0].Text:='Terhubung dengan mysql server '+DM.MyData.HostName+' PORT '+IntToStr(DM.MyData.Port)+' DB '+DM.MyData.Database;
end;

end.

