program p_attendance;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, zcomponent, runtimetypeinfocontrols, datetimectrls, tachartlazaruspkg,
  lazreportpdfexport, u_mainmenu, u_datamodule, u_member, u_overtime, u_leave,
  u_presetattendance, u_holiday, u_attendance, u_importcsv, u_fastingday,
  u_report_attendance, u_setupposition, u_division, u_empsetup, u_oldmember,
  u_countleave, u_countshiftperday, u_memberstatistic,
  u_checkimportedattendance, u_checkattendancesave, u_performance_appraisal;

{$R *.res}

begin
  RequireDerivedFormResource:=True;
  Application.Scaled:=True;
  Application.Initialize;
  Application.CreateForm(TDM, DM);
  Application.CreateForm(Tf_mainmenu, f_mainmenu);
  Application.CreateForm(Tf_overtime, f_overtime);
  Application.CreateForm(Tf_oldmember, f_oldmember);
  Application.CreateForm(Tf_overtime, f_overtime);
  Application.CreateForm(Tf_presetup_attendance, f_presetup_attendance);
  Application.CreateForm(Tf_fastingday, f_fastingday);
  Application.CreateForm(Tf_importcsv, f_importcsv);
  Application.CreateForm(Tf_attendance, f_attendance);
  Application.CreateForm(Tf_holiday, f_holiday);
  Application.CreateForm(Tf_report_attendance, f_report_attendance);
  Application.CreateForm(Tf_empsetup, f_empsetup);
  Application.CreateForm(Tf_division, f_division);
  Application.CreateForm(Tf_member, f_member);
  Application.CreateForm(Tf_setupposition, f_setupposition);
  Application.CreateForm(Tf_countleave, f_countleave);
  Application.CreateForm(Tf_leave, f_leave);
  Application.CreateForm(Tf_counshiftperday, f_counshiftperday);
  Application.CreateForm(Tf_memberstatistic, f_memberstatistic);
  Application.CreateForm(Tf_checkimportedattendance, f_checkimportedattendance);
  Application.CreateForm(Tf_checkattendancesave, f_checkattendancesave);
  Application.CreateForm(Tf_performanceappraisal, f_performanceappraisal);
  Application.Run;
end.

