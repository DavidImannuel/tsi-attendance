unit u_attendance;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, Buttons,
  StdCtrls, ZConnection, RTTICtrls, LR_Class;

type

  { Tf_mainmenu }

  Tf_mainmenu = class(TForm)
    Edit1: TEdit;
    Edit2: TEdit;
    frReport1: TfrReport;
    Image1: TImage;
    Image2: TImage;
    ImageList1: TImageList;
    Label1: TLabel;
    Label2: TLabel;
    Panel1: TPanel;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    SpeedButton4: TSpeedButton;
    SpeedButton5: TSpeedButton;
    SpeedButton6: TSpeedButton;
    ToggleBox1: TToggleBox;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton1MouseEnter(Sender: TObject);
    procedure SpeedButton1MouseLeave(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure ToggleBox1Change(Sender: TObject);
  private

  public

  end;

var
  f_mainmenu: Tf_mainmenu;

implementation
uses u_member, u_overtime, u_leave, u_presetattendance,u_holiday;

{$R *.lfm}

{ Tf_mainmenu }


procedure Tf_mainmenu.ToggleBox1Change(Sender: TObject);
begin
  Image2.Visible         :=False;
  SpeedButton1.Enabled   := True;
  SpeedButton2.Enabled   := True;
  SpeedButton3.Enabled   := True;
  SpeedButton4.Enabled   := True;
  SpeedButton5.Enabled   := True;
  SpeedButton6.Enabled   := True;
end;

procedure Tf_mainmenu.SpeedButton1MouseEnter(Sender: TObject);
begin
  //SpeedButton1.Font.Size := SpeedButton1.Font.Size + 5;
  (Sender as TSpeedButton).Font.Size := (Sender as TSpeedButton).Font.Size + 5;
end;

procedure Tf_mainmenu.SpeedButton1MouseLeave(Sender: TObject);
begin
  //SpeedButton1.Font.Size := SpeedButton1.Font.Size - 5;
  (Sender as TSpeedButton).Font.Size := (Sender as TSpeedButton).Font.Size - 5;
end;

procedure Tf_mainmenu.SpeedButton2Click(Sender: TObject);
begin
   f_overtime.Visible := True;
   f_overtime.Show;
end;

procedure Tf_mainmenu.SpeedButton3Click(Sender: TObject);
begin
  f_leave.Visible := True;
  f_leave.Show;
end;

procedure Tf_mainmenu.SpeedButton4Click(Sender: TObject);
begin

end;

procedure Tf_mainmenu.SpeedButton5Click(Sender: TObject);
begin
  f_presetup_attendance.Show;
end;

procedure Tf_mainmenu.SpeedButton1Click(Sender: TObject);
begin
    f_holiday.Show;
end;




end.

