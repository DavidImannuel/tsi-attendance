unit u_databasesetting;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, StdCtrls,
  Buttons,process,ShellApi;

const
  EXPORTDB_BAT_FILE = 'export_db.bat';
  IMPORTDB_BAT_FILE = 'import_db.bat';

type

  { Tf_databasesetting }

  Tf_databasesetting = class(TForm)
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    SaveDialog1: TSaveDialog;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
  private

  public

  end;

var
  f_databasesetting: Tf_databasesetting;

implementation
uses
  u_datamodule;

{$R *.lfm}

{ Tf_databasesetting }

procedure Tf_databasesetting.SpeedButton1Click(Sender: TObject);
var
  BatFile : TextFile;
  BatFilename : String;
  Proc : TProcess;
begin
  SaveDialog1.FileName:='';
  SaveDialog1.FileName:='backup_db_tatsumi_attendance_'+FormatDateTime('yyyymmdd',dm.NowX);
  if SaveDialog1.Execute then begin
    if MessageDlg('CONFIRMATION','Yakin untuk Mengexport Database ? ',mtConfirmation,mbYesNo,0) = mrYes then begin
      BatFilename:=ParamStr(0)+PathDelim+EXPORTDB_BAT_FILE;
      if FileExists(BatFilename) then DeleteFile(BatFilename);

      AssignFile(BatFile,EXPORTDB_BAT_FILE);
      Rewrite(BatFile);
      WriteLn(BatFile,'ECHO starting export database');
      WriteLn(BatFile,'mysqldump -h'+DM.MyData.HostName+' -u'+DM.MyData.User+' -p'+DM.MyData.Password+' -P'+IntToStr(DM.MyData.Port)+' '+DM.MyData.Database +
              ' --routines'+' > '+ SaveDialog1.FileName);
      WriteLn(BatFile,'ECHO success export database');
      WriteLn(BatFile,'pause');
      CloseFile(BatFile);

      Proc:=TProcess.Create(Nil);
      Proc.Executable:=EXPORTDB_BAT_FILE;
      Proc.Execute;
      Proc.Free;
    end;
  end;
end;

procedure Tf_databasesetting.SpeedButton2Click(Sender: TObject);
var
  BatFile : TextFile;
  BatFilename : String;
  Proc : TProcess;
begin
  SaveDialog1.FileName:='';
  SaveDialog1.FileName:='backup_db_tatsumi_attendance_'+FormatDateTime('yyyymmdd',dm.NowX);
  if SaveDialog1.Execute then begin
    if MessageDlg('CONFIRMATION','Yakin untuk Mengimport Database ? ',mtConfirmation,mbYesNo,0) = mrYes then begin
      BatFilename:=ParamStr(0)+PathDelim+IMPORTDB_BAT_FILE;
      if FileExists(BatFilename) then DeleteFile(BatFilename);
      AssignFile(BatFile,IMPORTDB_BAT_FILE);
      Rewrite(BatFile);
      WriteLn(BatFile,'ECHO starting export database');
      WriteLn(BatFile,'mysql -h'+DM.MyData.HostName+' -u'+DM.MyData.User+' -p'+DM.MyData.Password+' -P'+IntToStr(DM.MyData.Port)+' '+DM.MyData.Database+' < '+ SaveDialog1.FileName);
      WriteLn(BatFile,'ECHO success export database');
      WriteLn(BatFile,'pause');
      CloseFile(BatFile);

      Proc:=TProcess.Create(Nil);
      Proc.Executable:=IMPORTDB_BAT_FILE;
      Proc.Execute;
      Proc.Free;
    end;
  end;
end;

end.

