unit u_datamodule;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, db, Forms, Dialogs, Controls, ZConnection, ZDataset,
  ZSqlProcessor, DBGrids, ExtCtrls, strutils,JsonTools;

type

  { TDM }

  TDM = class(TDataModule)
    MyBatch: TZSQLProcessor;
    MyData: TZConnection;
    MyQuery: TZQuery;
    Timer1: TTimer;
    procedure DataModuleCreate(Sender: TObject);
    function ReadJSONConfig(connType,filename : String):Boolean;
    procedure ExportRecordToCsv(desc,query,filename,delim:string);
    procedure ImportCsv(table, filename, delim: string;IgnoreFirstRow:Boolean);
    procedure RefreshDBGgridWithoutLosingCurRow(vDBGrid:TDBGrid);
    FUNCTION NamaBulan(tglbulan:TDate):String;
    FUNCTION NowX:TDateTime;
    FUNCTION CreateNoByYearYYPrefix(BranchIs : String; DocCode :String; PartCode :String): String;
    procedure Timer1Timer(Sender: TObject);

  private

  public
     UserId , BranchID : String;

  end;

  THackDBGrid = class(TDBGrid); //for RefreshDBGgridWithoutLosingCurRow procedure
var
  DM: TDM;

implementation

{$R *.lfm}

{ TDM }

procedure TDM.DataModuleCreate(Sender: TObject);
begin
 {
   // local wifi
   Host:=10.11.10.183
   Database:=db_tatsumi_attendance
   user:=root
   Password:=surabaya
   Port:=3309
   // internet
   Host:=202.6.227.246
   Database:=db_tatsumi_attendance
   user:=root
   Password:=surabaya
   Port:=8089
 }
  BranchID:='001';
  //MyData.Connected := True;
  DM.ReadJSONConfig('mysql-production-public','config.json');
  //MyData.LibraryLocation:=GetCurrentDir+PathDelim+'mysqldll'+PathDelim+'libmysql.dll';
end;

function TDM.ReadJSONConfig(connType, filename: String): Boolean;
begin

  Result:=False;

  if not FileExists(filename) then begin
     ShowMessage('File Config Tidak ada Hubungi Developer');
  end;

  With TJsonNode.Create do begin
    LoadFromFile(filename);
    MyData.Connected:=False;

    case connType of
      'mysql-production-public' : begin
        MyData.HostName:=Find('mysql-production-public/hostname').AsString;
        MyData.Port:=StrToInt(Find('mysql-production-public/port').AsString);
        MyData.User:=Find('mysql-production-public/user').AsString;
        MyData.Password:=Find('mysql-production-public/password').AsString;
        MyData.LibraryLocation:=Find('mysql-production-public/libraryLocation').AsString;
        MyData.Database:=Find('mysql-production-public/database').AsString;
      end;
      'mysql-production-local' : begin
        MyData.HostName:=Find('mysql-production-local/hostname').AsString;
        MyData.Port:=StrToInt(Find('mysql-production-local/port').AsString);
        MyData.User:=Find('mysql-production-local/user').AsString;
        MyData.Password:=Find('mysql-production-local/password').AsString;
        MyData.LibraryLocation:=Find('mysql-production-local/libraryLocation').AsString;
        MyData.Database:=Find('mysql-production-local/database').AsString;
      end;
      'mysql-development-public' : begin
        MyData.HostName:=Find('mysql-development-public/hostname').AsString;
        MyData.Port:=StrToInt(Find('mysql-development-public/port').AsString);
        MyData.User:=Find('mysql-development-public/user').AsString;
        MyData.Password:=Find('mysql-development-public/password').AsString;
        MyData.LibraryLocation:=Find('mysql-development-public/libraryLocation').AsString;
        MyData.Database:=Find('mysql-development-public/database').AsString;
      end;
      'mysql-development-local' : begin
        MyData.HostName:=Find('mysql-development-local/hostname').AsString;
        MyData.Port:=StrToInt(Find('mysql-development-local/port').AsString);
        MyData.User:=Find('mysql-development-local/user').AsString;
        MyData.Password:=Find('mysql-development-local/password').AsString;
        MyData.LibraryLocation:=Find('mysql-development-local/libraryLocation').AsString;
        MyData.Database:=Find('mysql-development-local/database').AsString;
      end;
    end;

    try
      MyData.Connected:=True;
      Result:=True;
    except
       on E:exception do begin
         ShowMessage('MYSQL Tidak Bisa Connect Hubungi Developer. ERROR : '+E.Message);
         //Application.Terminate;
       end;
    end;

  end;

end;

procedure TDM.ExportRecordToCsv(desc, query, filename, delim: string);
begin
  with MyBatch do begin
    try

      if FileExists(filename) then DeleteFile(filename);
      Script.Text:=query+' into outfile '+QuotedStr(filename)
                  +' FIELDS TERMINATED BY '+ QuotedStr(delim);
      Execute;

    except
       on E:Exception do begin
         ShowMessage(E.Message);
       end;
    end;
  end;
end;

procedure TDM.ImportCsv(table, filename, delim: string;
  IgnoreFirstRow: Boolean);
begin
  with MyBatch do begin
    try
      if IgnoreFirstRow then begin
        Script.Text:=' load data local infile '+QuotedStr(filename)
                  +' REPLACE ' //delete jika sudah ada lalu insert
                  +' into table ' + table
                  +' FIELDS TERMINATED BY '+ QuotedStr(delim)
                  +' ENCLOSED BY '+QuotedStr('"')
                  //+' lines terminated by ' +QuotedStr('\r\n')
                  +' lines terminated by '+ QuotedStr(chr(13)) //chr13 = enter jika menggunakan \r atau \n kurang work
                  +' IGNORE 1 LINES '
                  +' ;';
      end else begin
        Script.Text:=' load data local infile '+QuotedStr(filename)
                  +' into table ' + table
                  +' FIELDS TERMINATED BY '+ QuotedStr(delim)
                  +' ENCLOSED BY '+QuotedStr('"')
                  +' lines terminated by ' +QuotedStr(chr(13))
                  +' ;';
      end;
      Execute;
    except
       on E:Exception do begin
         ShowMessage(E.Message);
       end;
    end;
  end;
end;

function TDM.NowX:TDateTime;
var q1:TZQuery;
    cValue : String;
begin
   q1:=TZQuery.Create(Application);
   with q1 do begin
      Connection := DM.MyData;
      Sql.Add('select sysdate() as tanggal');
      Open;
      try
           result:=StrToDateTime(FieldByName('tanggal').AsString);
      except
        cValue := Copy(q1.FieldByName('tanggal').AsString,9,2) + '-'+   // DD
                  Copy(q1.FieldByName('tanggal').AsString,6,2) + '-'+   // MM
                  Copy(q1.FieldByName('tanggal').AsString,1,4) + '-'+   // YYYY
                  ' '+
                  Copy(q1.FieldByName('tanggal').AsString,12,2) + ':'+  // TT
                  Copy(q1.FieldByName('tanggal').AsString,15,2) + ':'+  // NN
                  Copy(q1.FieldByName('tanggal').AsString,18,2);        // SS
        ShowMessage(q1.FieldByName('tanggal').AsString);
        ShowMessage(cValue);
        result:=StrToDateTime(cValue);
      end;
   end;
   q1.free;
end;


function TDM.CreateNoByYearYYPrefix(BranchIs : String; DocCode :String; PartCode :String): String;
var
  q                                     : TZQuery;
  Result2Send, vDelimeter, yyIs         : String;

begin
  vDelimeter            := '.';
  yyIs                  := vDelimeter + FormatDateTime('yy',NowX) + vDelimeter;
  q                     := TZQuery.Create(Application);
  With q Do Begin
    Connection                            := DM.MyData;
    SQL.Clear;
    SQL.Text                            :=
        ' select concat(fv_prefix, ' + QuotedStr(yyIs) +  ', ' +
        ' repeat(''0'',fn_count3-( 4 + length(fv_prefix) + length(fn_docno + 1)) ), ' +
        ' fn_docno + 1) as NomorIs ' +
        ' from t_nomor where (fc_branch = '+ QuotedStr(BranchIs)+ ' and fv_document = ' + QuotedStr(DocCode) + ') ' +
        ' and (fv_part = ' + QuotedStr(PartCode)+  ');';
    Open;
  End;
  Result2Send                           := q.FieldByName('NomorIs').AsString;

  With q Do Begin
    SQL.Clear;
    SQL.Add('update t_nomor set fn_docno = fn_docno + 1 ');
    SQL.Add('where (fc_branch = :branchis and fv_document = :docis) and (fv_part = :partis)');
    ParamByName('branchis').Value       := BranchIs;
    ParamByName('docis').Value          := DocCode;
    ParamByName('partis').Value         := PartCode;
    ExecSql;
  End;
  Result                                := Result2Send;
  q.Free;
end;

procedure TDM.Timer1Timer(Sender: TObject);
begin
  MyData.Ping;
end;

procedure TDM.RefreshDBGgridWithoutLosingCurRow(vDBGrid:TDBGrid);
var
   rowDelta: Integer;
   row: integer;
   ds : TDataSet;
begin
   ds := THackDBGrid(vDBGrid).DataSource.DataSet;

   rowDelta := -1 + THackDBGrid(vDBGrid).Row;
   {
    THackDBGrid(DBGrid1).Row urutan record dari grid yang terlihat 1 ( record paling atas )

    jika position pointer record di 8
    melakukan dataset.MoveBY(-8) maka benar dbgrid pointer akan keatas 8 terhitung 1 dari posisi record
    maka posisi record dikurangi 8
    lalu jika di moveby(8)
    dataset.MoveBY(8) maka dbgrid pointer akan turun 8 terhitung setelah "next record" position / position record setelah moveby(-8) dianggap 0
    mengakibatkan record sebelum nya yang di 16
   }
   row := ds.RecNo; //urutan record sebenarnya

   ds.Close;
   ds.Open;

   with ds do
   begin
     DisableControls;
     RecNo := row;
     MoveBy(-rowDelta) ;  //dbgrid move keatas dulu
     MoveBy(rowDelta) ;   // baru ke bawah sehingga "seperti" scroll position tidak berubah
     EnableControls;
   end;
end;

function TDM.NamaBulan(tglbulan:TDate):String;
begin
     If FormatDateTime('mm',tglbulan) = '01' Then Begin
        Result := FormatDateTime('dd',tglbulan)+' Januari '+FormatDateTime('yyyy',tglbulan);
        Exit;
     End;
     If FormatDateTime('mm',tglbulan) = '02' Then Begin
        Result := FormatDateTime('dd',tglbulan)+' Februari '+FormatDateTime('yyyy',tglbulan);
        Exit;
     End;
     If FormatDateTime('mm',tglbulan) = '03' Then Begin
        Result := FormatDateTime('dd',tglbulan)+' Maret '+FormatDateTime('yyyy',tglbulan);
        Exit;
     End;
     If FormatDateTime('mm',tglbulan) = '04' Then Begin
        Result := FormatDateTime('dd',tglbulan)+' April '+FormatDateTime('yyyy',tglbulan);
        Exit;
     End;
     If FormatDateTime('mm',tglbulan) = '05' Then Begin
        Result := FormatDateTime('dd',tglbulan)+' Mei '+FormatDateTime('yyyy',tglbulan);
        Exit;
     End;
     If FormatDateTime('mm',tglbulan) = '06' Then Begin
        Result := FormatDateTime('dd',tglbulan)+' Juni '+FormatDateTime('yyyy',tglbulan);
        Exit;
     End;
     If FormatDateTime('mm',tglbulan) = '07' Then Begin
        Result := FormatDateTime('dd',tglbulan)+' Juli '+FormatDateTime('yyyy',tglbulan);
        Exit;
     End;
     If FormatDateTime('mm',tglbulan) = '08' Then Begin
        Result := FormatDateTime('dd',tglbulan)+' Agustus '+FormatDateTime('yyyy',tglbulan);
        Exit;
     End;
     If FormatDateTime('mm',tglbulan) = '09' Then Begin
        Result := FormatDateTime('dd',tglbulan)+' September '+FormatDateTime('yyyy',tglbulan);
        Exit;
     End;
     If FormatDateTime('mm',tglbulan) = '10' Then Begin
        Result := FormatDateTime('dd',tglbulan)+' Oktober '+FormatDateTime('yyyy',tglbulan);
        Exit;
     End;
     If FormatDateTime('mm',tglbulan) = '11' Then Begin
        Result := FormatDateTime('dd',tglbulan)+' November '+FormatDateTime('yyyy',tglbulan);
        Exit;
     End;
     If FormatDateTime('mm',tglbulan) = '12' Then Begin
        Result := FormatDateTime('dd',tglbulan)+' Desember '+FormatDateTime('yyyy',tglbulan);
        Exit;
     End;
end;


end.

