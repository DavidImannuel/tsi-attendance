unit u_countleave;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, db, Forms, Controls, Graphics, Dialogs, ExtCtrls, DBGrids,
  StdCtrls, DBCtrls, ZDataset, DBDateTimePicker, DateTimePicker;

type

  { Tf_countleave }

  Tf_countleave = class(TForm)
    Button1: TButton;
    Button2: TButton;
    ds_leave: TDataSource;
    DateTimePicker1: TDateTimePicker;
    DateTimePicker2: TDateTimePicker;
    DBGrid1: TDBGrid;
    Edit1: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Panel1: TPanel;
    qt_leaveempposition: TStringField;
    qt_leavefc_membercode: TStringField;
    qt_leavefc_membername1: TStringField;
    qt_leavejumlah: TLargeintField;
    qt_leaveleavetype: TStringField;
    qt_leave: TZQuery;
    procedure Button1Click(Sender: TObject);
  private

  public

  end;

var
  f_countleave: Tf_countleave;

implementation

uses u_datamodule;

{$R *.lfm}

{ Tf_countleave }

procedure Tf_countleave.Button1Click(Sender: TObject);
begin
  with qt_leave do begin
    CLose;
    ParamByName('member').AsString:='%'+Edit1.Text+'%';
    ParamByName('datestart').AsDate:=DateTimePicker1.Date;
    ParamByName('datefinish').AsDate:=DateTimePicker2.Date;
    Open;
  end;
end;

end.

