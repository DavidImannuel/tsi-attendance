ALTER TABLE `db_tatsumi`.`t_attendance` ADD COLUMN `fc_adjustmenttype` CHAR(1) NOT NULL DEFAULT '+' AFTER `fc_attendancetype`;
ALTER TABLE `db_tatsumi`.`t_attendance_backup` ADD COLUMN `fc_adjustmenttype` CHAR(1) NOT NULL DEFAULT '+' AFTER `fc_attendancetype`;
update t_attendance set 
fn_totaltimeshift1 = (fn_totaltimeshift1 * -1),
fn_totaltimeshift2 = (fn_totaltimeshift2 * -1),
fn_totaltimeshift3 = (fn_totaltimeshift3 * -1),
fn_jumlahshift1 = (fn_jumlahshift1 * -1),
fn_jumlahshift2 = (fn_jumlahshift2 * -1),
fn_jumlahshift3 = (fn_jumlahshift3 * -1)

select * from t_attendance where fc_attendancetype = 'ADJUSTMENT'

-- script report attendance
select a.fc_periode,date_format(d.start_periode,'%d %M %Y') start_periode,date_format(d.finish_periode,'%d %M %Y') finish_periode,
b.fc_membername1,b.fc_membercode,b.fc_memberattendancetype,b.fn_memberothernumber1,c.fv_trxdescription,d.fastingday,a.fd_date,
if( coalesce(a.fc_daytype,'') != '' and a.fc_daytype not in ('H','H+') ,
 if( time_to_sec( if( a.fc_workinshift1 > a.fc_workoutshift1 , 
  subtime(addtime(a.fc_workoutshift1 ,"24:00"),a.fc_workinshift1 ) , 
  subtime(a.fc_workoutshift1,a.fc_workinshift1 ) 
  ) )/60/60 = 5 and dayofweek(a.fd_date) = 7,
 "S",
 a.fc_daytype)
, a.fc_daytype) fc_daytype,
concat(coalesce(f.fc_shiftnamealias,''),coalesce(g.fc_shiftnamealias,''),coalesce(h.fc_shiftnamealias,'')) shift,
a.fc_timeinshift1 timein1,a.fc_timeoutshift1 timeout1,a.fc_timeinshift2 timein2,a.fc_timeoutshift2 timeout2,a.fc_timeinshift3 timein3,a.fc_timeoutshift3 timeout3,
a.fn_totalhour ,a.fn_effectivehour, a.fn_ot1 ,a.fn_ot2 ,a.fn_ot3 ,a.fn_ot4 ,a.fn_ot5 ,a.fn_ot6,a.fn_jumlahshift ,a.fc_ti ,a.fv_description, 
d.totalsemua_ot1,d.totalsemua_ot - d.totalsemua_ot1 totalsemua_ot2,
d.total_workday,d.totalsemua_ot,total_shift1 + total_shift2 total_shift12,total_shift3
from t_attendance a
inner join t_member b on a.fc_membercode = b.fc_membercode and b.fl_memberblacklist = 'F'
left join t_trxtype c on b.fc_memberposition = c.fc_trxcode and c.fc_trxid = 'EMP_POSITION'
left join ( 
	select a.fc_membercode,a.fc_periode,date_sub(concat(a.fc_periode,'16') ,INTERVAL 1 MONTH) start_periode,max(a.fd_date) finish_periode ,
sum(a.fn_ot1 ) total_ot1, sum(a.fn_ot2 ) total_ot2, sum(a.fn_ot3 ) total_ot3 , sum(a.fn_ot4 ) total_ot4, sum(a.fn_ot5 ) total_ot5, sum(a.fn_ot6 ) total_ot6,
(sum(a.fn_ot1 ) * 1.5 ) + (sum(a.fn_ot2 ) * 2 ) + (sum(a.fn_ot3 ) * 3 ) + (sum(a.fn_ot4 ) * 4 ) + (sum(a.fn_ot5 ) * 6 ) + (sum(a.fn_ot6 ) * 8 ) totalsemua_ot,	
(sum( if( a.fd_date <= last_day(date_sub(concat(a.fc_periode,'15') ,INTERVAL 1 MONTH)), a.fn_ot1,0 )   ) * 1.5 ) + 
(sum( if( a.fd_date <= last_day(date_sub(concat(a.fc_periode,'15') ,INTERVAL 1 MONTH)), a.fn_ot2,0 )   ) * 2 ) +
(sum( if( a.fd_date <= last_day(date_sub(concat(a.fc_periode,'15') ,INTERVAL 1 MONTH)), a.fn_ot3,0 )   ) * 3 ) +
(sum( if( a.fd_date <= last_day(date_sub(concat(a.fc_periode,'15') ,INTERVAL 1 MONTH)), a.fn_ot4,0 )   ) * 4 ) +
(sum( if( a.fd_date <= last_day(date_sub(concat(a.fc_periode,'15') ,INTERVAL 1 MONTH)), a.fn_ot5,0 )   ) * 6 ) +
(sum( if( a.fd_date <= last_day(date_sub(concat(a.fc_periode,'15') ,INTERVAL 1 MONTH)), a.fn_ot6,0 )   ) * 8 ) totalsemua_ot1,
sum( length(coalesce(a.fc_daytype)) > 0 and fc_adjustmenttype = '+' ) - sum( length(coalesce(a.fc_daytype)) > 0 and fc_adjustmenttype = '-' ) total_workday,
sum( '1' in (coalesce(b.fc_shiftnamealias,'') ,coalesce(c.fc_shiftnamealias,'')  ,coalesce(d.fc_shiftnamealias,'') ) and fc_adjustmenttype = '+' ) -
sum( '1' in (coalesce(b.fc_shiftnamealias,'')  ,coalesce(c.fc_shiftnamealias,'') ,coalesce(d.fc_shiftnamealias,'') ) and fc_adjustmenttype = '-' ) 
total_shift1,
sum( '2' in (coalesce(b.fc_shiftnamealias,'') ,coalesce(c.fc_shiftnamealias,'')  ,coalesce(d.fc_shiftnamealias,'') ) and fc_adjustmenttype = '+' ) -
sum( '2' in (coalesce(b.fc_shiftnamealias,'') ,coalesce(c.fc_shiftnamealias,'')  ,coalesce(d.fc_shiftnamealias,'') ) and fc_adjustmenttype = '-' ) total_shift2,
sum( '3' in (coalesce(b.fc_shiftnamealias,'') ,coalesce(c.fc_shiftnamealias,'')  ,coalesce(d.fc_shiftnamealias,'') ) and fc_adjustmenttype = '+' ) -
sum( '3' in (coalesce(b.fc_shiftnamealias,'') ,coalesce(c.fc_shiftnamealias,'')  ,coalesce(d.fc_shiftnamealias,'') ) and fc_adjustmenttype = '-' ) total_shift3,
sum(a.fn_jumlahshift ) total_shift,
( sum( if( b.fl_fastingshift = 1 and a.fd_date >= :startfastingday and a.fd_date <= :finishfastingday ,b.fl_fastingshift,0) ) ) +
( sum( if( c.fl_fastingshift = 1 and a.fd_date >= :startfastingday and a.fd_date <= :finishfastingday ,c.fl_fastingshift,0) ) ) +
( sum( if( d.fl_fastingshift = 1 and a.fd_date >= :startfastingday and a.fd_date <= :finishfastingday ,d.fl_fastingshift,0) ) ) fastingday
from t_attendance a
left join t_dailytimerange b on a.fc_shift1 = b.fc_timerangecode 
left join t_dailytimerange c on a.fc_shift2 = c.fc_timerangecode 
left join t_dailytimerange d on a.fc_shift3 = d.fc_timerangecode 
group by fc_periode,fc_membercode 
 ) d on a.fc_periode = d.fc_periode and a.fc_membercode = d.fc_membercode
left join t_division e on e.fc_divisioncode = b.fc_divisioncode
left join t_dailytimerange f on a.fc_shift1 = f.fc_timerangecode 
left join t_dailytimerange g on a.fc_shift2 = g.fc_timerangecode 
left join t_dailytimerange h on a.fc_shift3 = h.fc_timerangecode
where 
b.fc_membercode  like :member and c.fv_trxdescription like :position and e.fc_divisioncode like :division and a.fc_periode = :periode
or
b.fc_membername1  like :member and c.fv_trxdescription like :position and e.fc_divisioncode like :division and a.fc_periode = :periode
order by b.fn_memberothernumber1,b.fc_membercode,a.fc_periode,a.fd_date;


-- script finger

select fc_membercode,
fd_date,fc_workinshift1 timein,
coalesce( nullif( coalesce(fc_timeoutshift3,''),'' ), nullif( coalesce(fc_timeoutshift2,''),'' ), nullif( coalesce(fc_timeoutshift1,''),'' ) ) timeout,
 concat(fc_timeinshift1,' - ',fc_timeoutshift1) shift1, concat(fc_timeinshift2,' - ',fc_timeoutshift2) shift2,  concat(fc_timeinshift3,' - ',fc_timeoutshift3) shift3,
 concat(fd_date,' ',cast(fc_workinshift1 as time)) absent_in,
 concat( if( coalesce( nullif( coalesce(fc_timeoutshift3,''),'' ), nullif( coalesce(fc_timeoutshift2,''),'' ),nullif( coalesce(fc_timeoutshift1,''),'' ) ) >= fc_workinshift1, 
 date_add(fd_date,interval 1 day) ,fd_date )
 ,' ',cast(coalesce( nullif( coalesce(fc_timeoutshift3,''),'' ),
nullif( coalesce(fc_timeoutshift2,''),'' ),
nullif( coalesce(fc_timeoutshift1,''),'' ) ) as time) ) absent_out
from t_attendance 

select fc_nama,str_to_date(fc_tanggal,"%d-%m-%Y")  from t_tempandhikafinger

show create table t_attendance

select 
a.fd_date,concat(a.fc_shift1,a.fc_shift2,a.fc_shift3) ,a.fc_membercode,
b.fc_tanggal,b.fc_scan1,b.fc_scan2,b.fc_scan3,b.fc_scan4
from t_attendance a
inner join t_tempandhikafinger b 
on a.fd_date = str_to_date(b.fc_tanggal,"%d-%m-%Y") and a.fc_membercode = concat("TSI ",b.fc_nip) and a.fc_attendancetype = "ATTENDANCE"

select 
a.fd_date,concat(a.fc_shift1,a.fc_shift2,a.fc_shift3) ,a.fc_membercode,
b.fc_tanggal,b.fc_scan1,b.fc_scan2,b.fc_scan3,b.fc_scan4
from t_attendance a
inner join t_tempandhikafinger b 
on date_add(a.fd_date,interval 1 day )= str_to_date(b.fc_tanggal,"%d-%m-%Y") and a.fc_membercode = concat("TSI ",b.fc_nip) 
and a.fc_attendancetype = "ATTENDANCE" and coalesce( nullif( coalesce(fc_timeoutshift3,''),'' ), nullif( coalesce(fc_timeoutshift2,''),'' ),nullif( coalesce(fc_timeoutshift1,''),'' ) ) >= fc_workinshift1


update t_attendance a
inner join t_tempandhikafinger b 
on a.fd_date = str_to_date(b.fc_tanggal,"%d-%m-%Y") and a.fc_membercode = concat("TSI ",b.fc_nip) and a.fc_attendancetype = "ATTENDANCE"
set a.fd_realdatein = concat(str_to_date(b.fc_tanggal,"%d-%m-%Y")," ",b.fc_scan1),
a.fd_realdateout = concat(str_to_date(b.fc_tanggal,"%d-%m-%Y")," ",
	coalesce( nullif( coalesce(fc_scan4,''),'' ), nullif( coalesce(fc_scan3,''),'' ),nullif( coalesce(fc_scan2,''),'' ) )) 
    
    
select 
a.fd_date,concat(a.fc_shift1,a.fc_shift2,a.fc_shift3) ,a.fc_membercode,
b.fc_tanggal,b.fc_scan1,b.fc_scan2,b.fc_scan3,b.fc_scan4
from t_attendance a
inner join t_tempandhikafinger b 
on a.fd_date = str_to_date(b.fc_tanggal,"%d-%m-%Y") and a.fc_membercode = concat("TSI ",b.fc_nip) and a.fc_attendancetype = "ATTENDANCE"
where a.fd_date >= '2020-08-18' and a.fd_date <='2020-08-25' and fc_shift3 = '3'

select * from t_tempandhikafinger where fc_nip = '77720'


update t_member set fc_membercode = 'TSI 71818' where fc_membercode = 'TSI 66316';
update t_overtime set fc_membercode = 'TSI 71818' where fc_membercode = 'TSI 66316';
update t_leave set fc_membercode = 'TSI 71818' where fc_membercode = 'TSI 66316';
update t_attendance set fc_membercode = 'TSI 71818' where fc_membercode = 'TSI 66316';
update t_attendance_backup set fc_membercode = 'TSI 71818' where fc_membercode = 'TSI 66316';













