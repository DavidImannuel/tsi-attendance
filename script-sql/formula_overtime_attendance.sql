-- update shift
update t_attendance a 
left join t_dailytimerange b on b.fc_timerangecode = a.fc_shift1 
left join t_dailytimerange c on c.fc_timerangecode = a.fc_shift2 
left join t_dailytimerange d on d.fc_timerangecode = a.fc_shift3  
set  
a.fc_workinshift1 = COALESCE(b.fc_workin,NULL), 
a.fc_workoutshift1 = COALESCE(b.fc_workout,NULL) , 
a.fn_totalbreakshift1 =COALESCE(TIME_TO_SEC( 
if( b.fc_workbreakin > b.fc_workbreakout , 
subtime(b.fc_workbreakin,b.fc_workbreakout ) , 
subtime(b.fc_workbreakout,b.fc_workbreakin ) 
) )/60/60,NULL), 
a.fc_workinshift2 = COALESCE(c.fc_workin,NULL), 
a.fc_workoutshift2 = COALESCE(c.fc_workout,NULL) , 
a.fn_totalbreakshift2 = COALESCE(TIME_TO_SEC(  
if( b.fc_workbreakin > b.fc_workbreakout , 
subtime(c.fc_workbreakin,c.fc_workbreakout ) , 
subtime(c.fc_workbreakout,c.fc_workbreakin ) 
) )/60/60,NULL) , 
a.fc_workinshift3 = COALESCE(d.fc_workin,NULL), 
a.fc_workoutshift3 = COALESCE(d.fc_workout,NULL) , 
a.fn_totalbreakshift3 = COALESCE(TIME_TO_SEC(  
if( d.fc_workbreakin > d.fc_workbreakout , 
subtime(d.fc_workbreakin,d.fc_workbreakout ) , 
subtime(d.fc_workbreakout,d.fc_workbreakin ) 
) )/60/60,NULL) 
where a.fc_periode = :periode and a.fd_date = :date and a.fc_membercode = :membercode;
-- update totalhour reference
update t_attendance
set 
fn_totalhourshift1 = TIME_TO_SEC( 
if( fc_workinshift1 > fc_workoutshift1 , 
	subtime(addtime(fc_workoutshift1 ,"24:00"),fc_workinshift1 ) , 
	subtime(fc_workoutshift1,fc_workinshift1 ) 
	) )/60/60, 
fn_totalhourshift2 = TIME_TO_SEC( 
	if( fc_workinshift2 > fc_workoutshift2 , 
	subtime(addtime(fc_workoutshift2 ,"24:00"),fc_workinshift2 ) , 
	subtime(fc_workoutshift2,fc_workinshift2 ) 
	) )/60/60, 
	fn_totalhourshift3 = TIME_TO_SEC( 
	if( fc_workinshift3 > fc_workoutshift3 , 
	subtime(addtime(fc_workoutshift3 ,"24:00"),fc_workinshift3 ) , 
	subtime(fc_workoutshift3,fc_workinshift3 ) 
	) )/60/60 
where fc_periode = :periode and fd_date = :date and fc_membercode = :membercode;
-- update timein / out shift 1,2,3 date out
update t_attendance a 
set 
a.fc_timeinshift1 = a.fc_workinshift1 , 
a.fc_timeoutshift1 = a.fc_workoutshift1 , 
a.fc_timeinshift2 = a.fc_workinshift2 , 
a.fc_timeoutshift2 = a.fc_workoutshift2 , 
a.fc_timeinshift3 = a.fc_workinshift3 , 
a.fc_timeoutshift3 = a.fc_workoutshift3  
where a.fc_periode = :periode and a.fd_date = :date and a.fc_membercode = :membercode;
-- Update Totalhour  & effective Hour time
update t_attendance a 
set 
a.fn_totaltimeshift1 = TIME_TO_SEC( 
if( a.fc_timeinshift1 > a.fc_timeoutshift1 , 
subtime(addtime(a.fc_timeoutshift1,"24:00"), a.fc_timeinshift1 ) , 
subtime(a.fc_timeoutshift1, a.fc_timeinshift1 ) 
) )/60/60, 
a.fn_totaltimeshift2 = TIME_TO_SEC( 
if( a.fc_timeinshift2 > a.fc_timeoutshift2 , 
subtime(addtime(a.fc_timeoutshift2,"24:00"), a.fc_timeinshift2 ) , 
subtime(a.fc_timeoutshift2, a.fc_timeinshift2 ) 
) )/60/60, 
a.fn_totaltimeshift3 = TIME_TO_SEC( 
if( a.fc_timeinshift3 > a.fc_timeoutshift3 , 
subtime(addtime(a.fc_timeoutshift3,"24:00"), a.fc_timeinshift3 ) , 
subtime(a.fc_timeoutshift3, a.fc_timeinshift3 ) 
) )/60/60, 
a.fn_totalbreakshift1 = if(fn_totaltimeshift1 >= 5,1,0), -- break1 update ( TOO RISK )
a.fn_totalbreakshift2 = if(fn_totaltimeshift2 >= 5,1,0), -- break2 update ( TOO RISK )
a.fn_totalbreakshift3 = if(fn_totaltimeshift3 >= 5,1,0), -- break3 update ( TOO RISK )
a.fn_totalhour = a.fn_totaltimeshift1 + coalesce(a.fn_totaltimeshift2,0 ) + coalesce(a.fn_totaltimeshift3,0), 
a.fn_effectivehour = a.fn_totaltimeshift1 + coalesce(a.fn_totaltimeshift2,0) + coalesce(a.fn_totaltimeshift3,0) - 
( coalesce(a.fn_totalbreakshift1,0) + coalesce(a.fn_totalbreakshift2,0) + coalesce(a.fn_totalbreakshift3,0)  )  
where a.fc_periode = :periode and a.fd_date = :date and a.fc_membercode = :membercode;
-- update jumlahshift
update t_attendance a 
set 
fn_jumlahshift1 = if(fn_totaltimeshift1 >= 4,1,1), -- hanya hitung total jam yang overtime saja
fn_jumlahshift2 = if(fn_totaltimeshift2 >= 4,1,0), 
fn_jumlahshift3 = if(fn_totaltimeshift3 >= 4,1,0), 
fn_jumlahshift = if(fc_timeinshift1 is null,null,fn_jumlahshift1 ) + coalesce(fn_jumlahshift2,0) + coalesce(fn_jumlahshift3,0) 
where a.fc_periode = :periode and a.fd_date = :date and a.fc_membercode = :membercode;
-- update TI1,TI2,TI3
update t_attendance
set 
fn_effectivehour = 
	case 
		when fc_ti in ("TI1","NR1") then 
		fn_effectivehour + 1 
		when fc_ti in ("TI2","NR2") then 
		fn_effectivehour + 2 
		when fc_ti in ("TI3","NR3") then 
		fn_effectivehour + 3 
		else 
		fn_effectivehour 
		end 
where fc_periode = :periode and fd_date = :date and fc_membercode = :membercode;
-- OT Update
update t_attendance
set 
fn_ot1 = 
	case when fc_daytype = "N" then 
		case when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) >= 1 then 
			1 
		when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) >= 0 then 
			fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) 
		else 0 end 
	else if(fc_daytype is null or fc_daytype = "" ,NULL,0) end , 
fn_ot2 = 
	case when fc_daytype = "N" then 
			case when fn_ot1 >= 1 then 
				fn_effectivehour - fn_ot1 - (fn_totalhourshift1 - fn_totalbreakshift1 ) 
			else 0 end 
		when fc_daytype in ("H","O") then 
	case when fn_effectivehour - 7 >= 0 then 
			7 
		when fn_effectivehour - 7 <= 0 then 
			fn_effectivehour 
		else 0 end 
	else if(fc_daytype is  null or fc_daytype = "" ,NULL,0) end , 
fn_ot3 = 
	case when fc_daytype in ("H","O") then 
		case when fn_effectivehour - 7 >= 1 then 
			1 
		when fn_effectivehour - 7 <= 1 then 
			if(fn_effectivehour - 7 > 0, fn_effectivehour - 7 ,0 )  
		else 0 end 
	else if(fc_daytype is  null or fc_daytype = "" ,NULL,0) end , 
fn_ot4 = 
	case when fc_daytype in ("H","O") then 
		case when fn_ot3 >= 1 then 
			fn_effectivehour - fn_ot3 - 7 
			else 0 end 
	when fc_daytype in ("H+") then 
		case when fn_effectivehour - 7 >= 0 then 
			7 
		when fn_effectivehour - 7 <= 0 then 
		fn_effectivehour 
		else 0 end 
	else if(fc_daytype is  null or fc_daytype = "" ,NULL,0) end, 
fn_ot5 = 
	case when fc_daytype in ("H+") then 
		case when fn_effectivehour - 7 >= 1 then 
			1 
		when fn_effectivehour - 7 <= 1 then 
			if( fn_effectivehour - 7 > 0 ,fn_effectivehour - 7,0) 
		else 0 end 
	else if(fc_daytype is  null or fc_daytype = "" ,NULL,0) end , 
fn_ot6 = 
	case when fc_daytype in ("H+") then 
		case when fn_ot5 >=1 then 
			fn_effectivehour - fn_ot5 - 7 
		else 0 end 
	else if(fc_daytype is  null or fc_daytype = "" ,NULL,0) end 
where fc_periode = :periode and fd_date = :date and fc_membercode = :membercode;