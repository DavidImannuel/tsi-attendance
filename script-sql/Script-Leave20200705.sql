update 
t_member a 
left join 
( 
select 
a.fc_membercode , 
a.fc_membername1, 
sum(b.amount_leave) amount_leave 
from t_member a 
left join 
( 
select 
t1.fc_trxaction1 amount_leave , 
t1.fv_trxdescription fv_trxdescription1 , 
t2.fv_trxdescription fv_trxdescription2 
 from t_trxtype t1 
 left join 
 ( 
select fv_trxdescription 
 from t_trxtype where fc_trxid = "RESET_LEAVE_DATE" 
 ) t2 on t1.fv_trxdescription > t2.fv_trxdescription or t1.fv_trxdescription < t2.fv_trxdescription 
 where fc_trxid = "ADD_LEAVE_DATE" 
 ) b on 
 a.fd_memberjoindate <= if( concat(date_format( now(),"%Y" ),"-",b.fv_trxdescription1) > concat(date_format( now(),"%Y" ),"-",b.fv_trxdescription2) 
 , date_sub(concat(date_format( now(),"%Y" ),"-",b.fv_trxdescription1), interval 1 year ) 
 , concat(date_format( now(),"%Y" ),"-",b.fv_trxdescription1)) 
 and 
 if( concat(date_format( now(),"%Y" ),"-",b.fv_trxdescription1) > concat(date_format( now(),"%Y" ),"-",b.fv_trxdescription2) 
 , date_sub(concat(date_format( now(),"%Y" ),"-",b.fv_trxdescription1), interval 1 year ) 
 , concat(date_format( now(),"%Y" ),"-",b.fv_trxdescription1)) <= :now 
 group by fc_membercode 
 ) b on a.fc_membercode = b.fc_membercode 
 set a.fn_memberleavecount = b.amount_leave 
 where a.fc_membercode like :membercode ;


select 
a.fc_membercode , 
a.fc_membername1, 
sum(b.amount_leave) amount_leave 
from t_member a 
left join 
( 
select 
t1.fc_trxaction1 amount_leave , 
t1.fv_trxdescription fv_trxdescription1 , 
t2.fv_trxdescription fv_trxdescription2 
 from t_trxtype t1 
 left join 
 ( 
select fv_trxdescription 
 from t_trxtype where fc_trxid = "RESET_LEAVE_DATE" 
 ) t2 on t1.fv_trxdescription > t2.fv_trxdescription or t1.fv_trxdescription < t2.fv_trxdescription 
 where fc_trxid = "ADD_LEAVE_DATE" 
 ) b on 
 a.fd_memberjoindate <= if( concat(date_format( now(),"%Y" ),"-",b.fv_trxdescription1) > concat(date_format( now(),"%Y" ),"-",b.fv_trxdescription2) 
 , date_sub(concat(date_format( now(),"%Y" ),"-",b.fv_trxdescription1), interval 1 year ) 
 , concat(date_format( now(),"%Y" ),"-",b.fv_trxdescription1)) 
 and 
 if( concat(date_format( now(),"%Y" ),"-",b.fv_trxdescription1) > concat(date_format( now(),"%Y" ),"-",b.fv_trxdescription2) 
 , date_sub(concat(date_format( now(),"%Y" ),"-",b.fv_trxdescription1), interval 1 year ) 
 , concat(date_format( now(),"%Y" ),"-",b.fv_trxdescription1)) <= :now 
 group by fc_membercode 

 
select 
fv_trxdescription ,fc_trxaction1 ,concat(date_format(now(),"%Y"),"-",fv_trxdescription ) leave_add_date
from t_trxtype 
where fc_trxid = "ADD_LEAVE_DATE"

select 
fv_trxdescription ,fc_trxaction1 ,concat(date_format(date_sub(now(),interval 1 YEAR),"%Y"),"-",fv_trxdescription ) leave_add_date
from t_trxtype 
where fc_trxid = "ADD_LEAVE_DATE"




select 
a.fc_membercode , a.fc_membername1,
a.fd_memberjoindate,
coalesce(sum(b.fc_trxaction1),0) sum_leave_pastyear ,
count(c.fc_leaveno) count_amountleave_pastyear 
from t_member a
left join 
( select 
fv_trxdescription ,fc_trxaction1 ,concat(date_format(date_sub(now(),interval 1 YEAR),"%Y"),"-",fv_trxdescription ) leave_add_date
from t_trxtype 
where fc_trxid = "ADD_LEAVE_DATE" 
) b on b.leave_add_date >= a.fd_memberjoindate
left join t_leave c on a.fc_membercode = c.fc_membercode 
and c.fd_leavedate >= concat(date_format(date_sub(now(),interval 1 YEAR),"%Y"),"-",'01-23')
and c.fd_leavedate <= concat(date_format(date_sub(now(),interval 1 YEAR),"%Y"),"-",'12-23')
group by a.fc_membercode

select 
fv_trxdescription ,fc_trxaction1 ,concat(date_format(date_sub(now(),interval 1 YEAR),"%Y"),"-",fv_trxdescription ) leave_add_date
from t_trxtype 
where fc_trxid = "ADD_LEAVE_DATE"



update t_member 
set fd_expiredleaveadd = '2020-06-30'

select 
a.fc_membercode , a.fc_membername1,
a.fd_memberjoindate,a.fd_expiredleaveadd ,
coalesce(sum(b.fc_trxaction1),0) sum_leave_pastyear  
from t_member a
left join 
( select 
fv_trxdescription ,fc_trxaction1 ,concat(date_format(date_sub(now(),interval 1 YEAR),"%Y"),"-",fv_trxdescription ) leave_add_date
from t_trxtype 
where fc_trxid = "ADD_LEAVE_DATE" 
) b on b.leave_add_date >= a.fd_memberjoindate
group by a.fc_membercode

update t_member a
left join 
(select 
a.fc_membercode , a.fc_membername1,
a.fd_memberjoindate,a.fd_expiredleaveadd ,
coalesce(sum(b.fc_trxaction1),0) sum_leave_pastyear  
from t_member a
left join 
	( select fv_trxdescription ,fc_trxaction1 ,concat(date_format(date_sub(now(),interval 1 YEAR),"%Y"),"-",fv_trxdescription ) leave_add_date
		from t_trxtype where fc_trxid = "ADD_LEAVE_DATE" 
	) b on b.leave_add_date >= a.fd_memberjoindate
group by a.fc_membercode
) b on a.fc_membercode = b.fc_membercode 
set a.fn_memberleaveadd = b.sum_leave_pastyear 


 