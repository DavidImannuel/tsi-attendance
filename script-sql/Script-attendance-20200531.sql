select 
a.fc_periode ,a.fd_date ,
a.fc_membercode,b.fc_membername1,
-- b.fc_memberattendancetype ,b.fn_memberothernumber1 ,
-- b.fc_memberposition, 
-- a.fc_daytype ,
-- concat(a.fc_shift1,a.fc_shift2,a.fc_shift3) shift ,
-- a.fc_timeinshift1 timein1,
-- if(a.fc_timeinshift2 in ("","00:00") or a.fc_timeinshift2 is null , a.fc_timeoutshift1 ,a.fc_timeoutshift2 ) timeout1,
-- a.fc_timeinshift3 timein2,
-- a.fc_timeoutshift3 timeout2,
-- a.fn_totalhour ,a.fn_effectivehour, 
-- a.fn_ot1 ,a.fn_ot2 ,a.fn_ot3 ,a.fn_ot4 ,a.fn_ot5 ,a.fn_ot6,
-- a.fn_jumlahshift ,
-- a.fv_description, 
-- a.fc_ti ,
-- a.fl_fastingshift ,
e.mindate,
if( d.fc_fastingdayno is not null,concat("Uang makan Puasa (",d.StartDate," - ",d.EndDate,") ","gatahu"," hari" ),"" ) uang_makanpuasa
from t_attendance a
left join t_member b on a.fc_membercode = b.fc_membercode
left join t_trxtype c on b.fc_memberposition = c.fc_trxcode and c.fc_trxcode = "EMP_POSITION"
left join (
select fc_fastingdayno, min(fd_date) StartDate, 
       max(fd_date) EndDate, 
       fv_description,
       date_format(fd_date,"%Y%m") fastingPeriode
from t_fastingday 
group by fc_fastingdayno, fv_description,date_format(fd_date,"%Y%m")
order by fd_date
) d on d.fastingPeriode = a.fc_periode 
left join (
select 
fd_date ,fc_periode,fc_membercode,
if(fd_date < concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" ) , 
concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" )
,fd_date ) mindate
from t_attendance
group by fc_periode ,fc_membercode,fd_date
order by fc_membercode,fc_periode,fd_date 
) e on  e.fc_membercode = a.fc_membercode and a.fc_periode = e.fc_periode and a.fd_date = e.fd_date
where 
a.fc_membercode like :member and b.fl_memberblacklist = :active 
and e.mindate >= :datestart and e.mindate <= :datefinish
order by a.fc_membercode,a.fc_periode ,a.fd_date

select 
t1.fc_periode ,
t1.fd_date ,
month(t2.mindate) monthperiode,
t1.fc_membercode,
sum(t1.fn_ot1 ) "1,5",
sum(t1.fn_ot2 ) "2",
sum(t1.fn_ot3 ) "3" ,
sum(t1.fn_ot4 ) "4",
sum(t1.fn_ot5 ) "5",
sum(t1.fn_ot6 ) "6",
(sum(t1.fn_ot1 ) * 1.5 ) + (sum(t1.fn_ot2 ) * 2 ) + (sum(t1.fn_ot3 ) * 3 ) + (sum(t1.fn_ot4 ) * 4 ) + (sum(t1.fn_ot5 ) * 6 ) + (sum(t1.fn_ot6 ) * 8 ) total_ot,
sum( length(substring( t1.fc_daytype,1,1)) ) total_workday,
sum( '1' in (t1.fc_shift1,t1.fc_shift2 ,t1.fc_shift3) ) total_shift1,
sum( '2' in (t1.fc_shift1,t1.fc_shift2 ,t1.fc_shift3) )  total_shift2,
sum( '3' in (t1.fc_shift1,t1.fc_shift2 ,t1.fc_shift3) ) total_shift3,
sum(t1.fl_fastingshift ) "Uang makan puasa (hari)"
from t_attendance t1
left join (
select 
fd_date ,fc_periode,fc_membercode,
if(fd_date < concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" ) , 
concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" )
,fd_date ) mindate
from t_attendance
group by fc_periode ,fc_membercode,fd_date
order by fc_membercode,fc_periode,fd_date 
) t2 on  t2.fc_membercode = t1.fc_membercode and t2.fc_periode = t1.fc_periode and t1.fd_date = t2.fd_date 
where t1.fc_periode = :periode
and t1.fc_membercode like "%TSI 52713%"
and t2.mindate >= :datestart
and t2.mindate <= :datefinish
group by t1.fc_periode,t1.fc_membercode,month(t2.mindate)
order by t1.fc_membercode,t1.fc_periode,month(t2.mindate)




############################################################################################################3

select 
a.fc_periode ,a.fd_date ,
a.fc_membercode,b.fc_membername1,
e.mindate,
if( d.fc_fastingdayno is not null,concat("Uang makan Puasa (",d.StartDate," - ",d.EndDate,") ",f.fastingshift," hari" ),"" ) uang_makanpuasa,
f.total_ot1 fmot1,
f.total_ot2 fmot2,
f.total_ot3 fmot3,
f.total_ot4 fmot4,
f.total_ot5 fmot5,
f.total_ot6 fmot6,
f.total_ot fmot,
g.total_ot1 lmot1,
g.total_ot2 lmot2,
g.total_ot3 lmot3,
g.total_ot4 lmot4,
g.total_ot5 lmot5,
g.total_ot6 lmot6,
g.total_ot lmot
from t_attendance a
left join t_member b on a.fc_membercode = b.fc_membercode
left join t_trxtype c on b.fc_memberposition = c.fc_trxcode and c.fc_trxcode = "EMP_POSITION"
left join (
select fc_fastingdayno, min(fd_date) StartDate, 
       max(fd_date) EndDate, 
       fv_description,
       date_format(fd_date,"%Y%m") fastingPeriode
from t_fastingday 
group by fc_fastingdayno, fv_description,date_format(fd_date,"%Y%m")
) d on  a.fc_periode = d.fastingPeriode
left join (
select 
fd_date ,fc_periode,fc_membercode,
if(fd_date < concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" ) , 
concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" )
,fd_date ) mindate
from t_attendance
group by fc_periode ,fc_membercode,fd_date
order by fc_membercode,fc_periode,fd_date 
) e on  e.fc_membercode = a.fc_membercode and a.fc_periode = e.fc_periode and a.fd_date = e.fd_date
left join (
	select 
	t1.fc_periode ,
	t1.fd_date ,
	month(t2.mindate) monthperiode,
	last_day( min(t2.mindate) ) mindateperiode,
	t1.fc_membercode,
	sum(t1.fn_ot1 ) total_ot1,
	sum(t1.fn_ot2 ) total_ot2,
	sum(t1.fn_ot3 ) total_ot3 ,
	sum(t1.fn_ot4 ) total_ot4,
	sum(t1.fn_ot5 ) total_ot5,
	sum(t1.fn_ot6 ) total_ot6,
	(sum(t1.fn_ot1 ) * 1.5 ) + (sum(t1.fn_ot2 ) * 2 ) + (sum(t1.fn_ot3 ) * 3 ) + (sum(t1.fn_ot4 ) * 4 ) + (sum(t1.fn_ot5 ) * 6 ) + (sum(t1.fn_ot6 ) * 8 ) total_ot,	
	sum( length(substring( t1.fc_daytype,1,1)) ) total_workday,
	sum( '1' in (t1.fc_shift1,t1.fc_shift2 ,t1.fc_shift3) ) total_shift1,
	sum( '2' in (t1.fc_shift1,t1.fc_shift2 ,t1.fc_shift3) )  total_shift2,
	sum( '3' in (t1.fc_shift1,t1.fc_shift2 ,t1.fc_shift3) ) total_shift3,
	sum(t1.fl_fastingshift ) fastingshift
	from t_attendance t1
	left join (
		select 
		fd_date ,fc_periode,fc_membercode,
		if(fd_date < concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" ) , 
		concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" )
		,fd_date ) mindate,
		min( '2020-04-16' ) mindateperiode
		from t_attendance
		group by fc_periode ,fc_membercode,fd_date
		order by fc_membercode,fc_periode,fd_date 
		) t2 on  t2.fc_membercode = t1.fc_membercode and t2.fc_periode = t1.fc_periode and t1.fd_date = t2.fd_date 
	where 
-- 	t2.mindate >= :datestart
-- 	and t2.mindate <= last_day( :datestart )	
	t2.mindate >= last_day( :datestart )
	and t2.mindate <= :datefinish
	group by t1.fc_periode,t1.fc_membercode
	order by t1.fc_membercode,t1.fc_periode
) f on  f.fc_membercode = a.fc_membercode and a.fc_periode = f.fc_periode 
left join (
	select 
	t1.fc_periode ,
	t1.fd_date ,
	month(t2.mindate) monthperiode,
	last_day( min(t2.mindate) ) mindateperiode,
	t1.fc_membercode,
	sum(t1.fn_ot1 ) total_ot1,
	sum(t1.fn_ot2 ) total_ot2,
	sum(t1.fn_ot3 ) total_ot3 ,
	sum(t1.fn_ot4 ) total_ot4,
	sum(t1.fn_ot5 ) total_ot5,
	sum(t1.fn_ot6 ) total_ot6,
	(sum(t1.fn_ot1 ) * 1.5 ) + (sum(t1.fn_ot2 ) * 2 ) + (sum(t1.fn_ot3 ) * 3 ) + (sum(t1.fn_ot4 ) * 4 ) + (sum(t1.fn_ot5 ) * 6 ) + (sum(t1.fn_ot6 ) * 8 ) total_ot,	
	sum( length(substring( t1.fc_daytype,1,1)) ) total_workday,
	sum( '1' in (t1.fc_shift1,t1.fc_shift2 ,t1.fc_shift3) ) total_shift1,
	sum( '2' in (t1.fc_shift1,t1.fc_shift2 ,t1.fc_shift3) )  total_shift2,
	sum( '3' in (t1.fc_shift1,t1.fc_shift2 ,t1.fc_shift3) ) total_shift3,
	sum(t1.fl_fastingshift ) fastingshift
	from t_attendance t1
	left join (
		select 
		fd_date ,fc_periode,fc_membercode,
		if(fd_date < concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" ) , 
		concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" )
		,fd_date ) mindate
		from t_attendance
		group by fc_periode ,fc_membercode,fd_date
		order by fc_membercode,fc_periode,fd_date 
		) t2 on  t2.fc_membercode = t1.fc_membercode and t2.fc_periode = t1.fc_periode and t1.fd_date = t2.fd_date 
	where 
	t2.mindate >= :datestart
	and t2.mindate <= :datefinish
	group by t1.fc_periode,t1.fc_membercode
	order by t1.fc_membercode,t1.fc_periode
) g on  g.fc_membercode = a.fc_membercode and a.fc_periode = g.fc_periode 
where 
a.fc_membercode like :member and b.fl_memberblacklist = :active 
and e.mindate >= :datestart and e.mindate <= :datefinish
order by a.fc_membercode,a.fc_periode ,a.fd_date





select 
a.fc_periode,a.fd_date ,
a.fc_membercode,b.fc_membername1 , 
-- c.fv_trxdescription ,d.fc_divisionname 
e.periodedate
from t_attendance a
left join t_member b on a.fc_membercode = b.fc_membercode
left join t_trxtype c on b.fc_memberposition = c.fc_trxcode and c.fc_trxid = "EMP_POSITION"
left join t_division d on b.fc_divisioncode = d.fc_divisioncode 
left join ( 
	select fc_membercode,fd_date,fc_periode ,if(fd_date < concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" ) , 
	concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" ) ,fd_date ) 
	periodedate from t_attendance
) e on e.fc_membercode = a.fc_membercode and a.fd_date = e.fd_date and a.fc_periode = e.fc_periode
where 
a.fc_membercode like :member 
and e.periodedate >= :datestart and e.periodedate <= :datefinish
order by e.periodedate



select 
a.fc_periode,a.fd_date ,
a.fc_membercode,b.fc_membername1 , 
c.fv_trxdescription ,d.fc_divisionname, 
month(e.periodedate) monthperiode,
sum(a.fn_ot1 ) total_ot1,
sum(a.fn_ot2 ) total_ot2,
sum(a.fn_ot3 ) total_ot3 ,
sum(a.fn_ot4 ) total_ot4,
sum(a.fn_ot5 ) total_ot5,
sum(a.fn_ot6 ) total_ot6,
(sum(a.fn_ot1 ) * 1.5 ) + (sum(a.fn_ot2 ) * 2 ) + (sum(a.fn_ot3 ) * 3 ) + (sum(a.fn_ot4 ) * 4 ) + (sum(a.fn_ot5 ) * 6 ) + (sum(a.fn_ot6 ) * 8 ) total_ot,	
sum( length(substring( a.fc_daytype,1,1)) ) total_workday,
sum( '1' in (a.fc_shift1,a.fc_shift2 ,a.fc_shift3) ) total_shift1,
sum( '2' in (a.fc_shift1,a.fc_shift2 ,a.fc_shift3) )  total_shift2,
sum( '3' in (a.fc_shift1,a.fc_shift2 ,a.fc_shift3) ) total_shift3,
-- sum(a.fl_fastingshift ) fastingshift,
concat( "uang makan puasa tanggal ",f.mindate, " s/d ", f.maxdate," : ",sum(a.fl_fastingshift ) ) uangmakanpuasa
from t_attendance a
left join t_member b on a.fc_membercode = b.fc_membercode
left join t_trxtype c on b.fc_memberposition = c.fc_trxcode and c.fc_trxid = "EMP_POSITION"
left join t_division d on b.fc_divisioncode = d.fc_divisioncode 
left join ( 
	select fc_membercode,fd_date,fc_periode ,if(fd_date < concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" ) , 
	concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" ) ,fd_date ) 
	periodedate from t_attendance
) e on e.fc_membercode = a.fc_membercode and a.fd_date = e.fd_date and a.fc_periode = e.fc_periode
left join (
	select min(fd_date) mindate,max(fd_date) maxdate,
	month(fd_date) monthdate,fv_description 
	from t_fastingday group by fc_fastingdayno ,month(fd_date)
) f on month(e.periodedate) =  f.monthdate
where 
a.fc_membercode like :member 
and e.periodedate >= :datestart and e.periodedate <= :datefinish
group by a.fc_membercode ,a.fc_periode  ,month(e.periodedate)
order by b.fn_memberothernumber1,a.fc_membercode,e.periodedate







select 
a.fc_periode,a.fc_membercode,
b.fc_membername1,b.fv_trxdescription,b.fc_divisionname,
b.fn_memberothernumber1,cast(b.uangmakanpuasa as char) uangmakanpuasa,
b.fc_memberattendancetype ,

a.fd_date,b.periodedate,a.fc_daytype,
concat(a.fc_shift1,a.fc_shift2,a.fc_shift3) shift ,
a.fc_timeinshift1 timein1,
if(a.fc_timeinshift2 in ("","00:00") or a.fc_timeinshift2 is null , a.fc_timeoutshift1 ,a.fc_timeoutshift2 ) timeout1,
a.fc_timeinshift3 timein2,
a.fc_timeoutshift3 timeout2,
a.fn_totalhour ,a.fn_effectivehour, 
a.fn_ot1 ,a.fn_ot2 ,a.fn_ot3 ,a.fn_ot4 ,a.fn_ot5 ,a.fn_ot6,
a.fn_jumlahshift ,
a.fc_ti ,
a.fv_description, 

b.total_ot1,
b.total_ot2,
b.total_ot3,
b.total_ot4,
b.total_ot5,
b.total_ot6,
b.totalshift,

b.total_workday,
b.total_shift1,b.total_shift2,b.total_shift3,
b.total_ot alltotalot,
c.total_ot fptotalot,
b.total_ot - c.total_ot ltotalot
from t_attendance a
left join
(
select 
a.fc_periode,a.fd_date ,
a.fc_membercode,b.fc_membername1 , b.fn_memberothernumber1 ,b.fc_memberattendancetype ,
c.fv_trxdescription,b.fc_memberposition ,d.fc_divisionname,b.fc_divisioncode,
e.periodedate,
month(e.periodedate) monthperiode,
sum(a.fn_ot1 ) total_ot1,
sum(a.fn_ot2 ) total_ot2,
sum(a.fn_ot3 ) total_ot3 ,
sum(a.fn_ot4 ) total_ot4,
sum(a.fn_ot5 ) total_ot5,
sum(a.fn_ot6 ) total_ot6,
(sum(a.fn_ot1 ) * 1.5 ) + (sum(a.fn_ot2 ) * 2 ) + (sum(a.fn_ot3 ) * 3 ) + (sum(a.fn_ot4 ) * 4 ) + (sum(a.fn_ot5 ) * 6 ) + (sum(a.fn_ot6 ) * 8 ) total_ot,	
sum( length(substring( a.fc_daytype,1,1)) ) total_workday,
sum( '1' in (a.fc_shift1,a.fc_shift2 ,a.fc_shift3) ) total_shift1,
sum( '2' in (a.fc_shift1,a.fc_shift2 ,a.fc_shift3) )  total_shift2,
sum( '3' in (a.fc_shift1,a.fc_shift2 ,a.fc_shift3) ) total_shift3,
sum(a.fn_jumlahshift ) totalshift,
concat( "uang makan puasa tanggal ",f.mindate, " s/d ", f.maxdate," : ",sum(a.fl_fastingshift )," hari" ) uangmakanpuasa
from t_attendance a
left join t_member b on a.fc_membercode = b.fc_membercode
left join t_trxtype c on b.fc_memberposition = c.fc_trxcode and c.fc_trxid = "EMP_POSITION"
left join t_division d on b.fc_divisioncode = d.fc_divisioncode 
left join ( 
	select fc_membercode,fd_date,fc_periode ,if(fd_date < concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" ) , 
	concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" ) ,fd_date ) 
	periodedate from t_attendance
) e on e.fc_membercode = a.fc_membercode and a.fd_date = e.fd_date and a.fc_periode = e.fc_periode
left join (
	select min(fd_date) mindate,max(fd_date) maxdate,
	fv_description 
	from t_fastingday group by fc_fastingdayno 
) f on month(e.periodedate) = month(f.mindate) and year(e.periodedate) = year(f.mindate) 
	or month(e.periodedate) = month(f.maxdate) and year(e.periodedate) = year(f.maxdate)
	where e.periodedate >= :datestart and e.periodedate <= :datefinish
	group by a.fc_membercode ,a.fc_periode 
)b on a.fc_membercode = b.fc_membercode and a.fc_periode = b.fc_periode
left join
(
select 
a.fc_periode,a.fd_date ,
a.fc_membercode,b.fc_membername1,
(sum(a.fn_ot1 ) * 1.5 ) + (sum(a.fn_ot2 ) * 2 ) + (sum(a.fn_ot3 ) * 3 ) + (sum(a.fn_ot4 ) * 4 ) + (sum(a.fn_ot5 ) * 6 ) + (sum(a.fn_ot6 ) * 8 ) total_ot
from t_attendance a
left join t_member b on a.fc_membercode = b.fc_membercode
left join t_trxtype c on b.fc_memberposition = c.fc_trxcode and c.fc_trxid = "EMP_POSITION"
left join t_division d on b.fc_divisioncode = d.fc_divisioncode 
left join ( 
	select fc_membercode,fd_date,fc_periode ,if(fd_date < concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" ) , 
	concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" ) ,fd_date ) 
	periodedate from t_attendance
) e on e.fc_membercode = a.fc_membercode and a.fd_date = e.fd_date and a.fc_periode = e.fc_periode
left join (
	select min(fd_date) mindate,max(fd_date) maxdate,
	fv_description 
	from t_fastingday group by fc_fastingdayno 
) f on month(e.periodedate) = month(f.mindate) and year(e.periodedate) = year(f.mindate) 
	or month(e.periodedate) = month(f.maxdate) and year(e.periodedate) = year(f.maxdate)
	where e.periodedate >= :datestart and e.periodedate <= last_day(:datestart)
group by a.fc_membercode ,a.fc_periode 
)c on a.fc_membercode = c.fc_membercode and a.fc_periode = c.fc_periode
where 
a.fc_membercode  like :member and b.fv_trxdescription like :position and b.fc_divisioncode like :division and b.periodedate >= :datestart and b.periodedate <= :datefinish
or 
b.fc_membername1 like :member and b.fv_trxdescription like :position and b.fc_divisioncode like :division and b.periodedate >= :datestart and b.periodedate <= :datefinish
order by b.fn_memberothernumber1,a.fc_membercode,a.fd_date,b.periodedate

select 
a.fc_branch,a.fc_periode ,a.fd_dategenerate , 
a.fc_membercode,b.fc_membername1, 
c.fv_trxdescription fc_memberposition, 
a.fd_date ,a.fc_daytype , 
a.fc_shift1 ,a.fc_shift2,a.fc_shift3,
a.fc_timeinshift1,a.fc_timeoutshift1 , 
a.fc_timeinshift2,a.fc_timeoutshift2 , 
a.fc_timeinshift3,a.fc_timeoutshift3 , 
a.fd_realdatein ,a.fd_realdateout, 
a.fn_totalhour ,a.fn_effectivehour, 
a.fn_ot1 ,a.fn_ot2 ,a.fn_ot3 ,a.fn_ot4 ,a.fn_ot5 ,a.fn_ot6, 
a.fc_ti, 
a.fv_description, 
a.fl_ischecked1 , 
a.fl_ischeked2 
from t_attendance a 
left join t_member b on a.fc_membercode = b.fc_membercode  
left join t_trxtype c on b.fc_memberposition = c.fc_trxcode and c.fc_trxid = "EMP_POSITION" 
left join ( 
select fc_periode,fc_membercode,fd_date from t_attendance         
where fd_date >= :datestart
and fd_date <= :datefinish
group by
fc_membercode,
fc_periode ) d on
a.fc_membercode = d.fc_membercode
and a.fc_periode = d.fc_periode
where
a.fc_membercode like :member
and b.fl_memberblacklist = :active
and a.fd_date >= :datestart
and a.fd_date <= :datefinish
or a.fc_membercode like :member
and b.fl_memberblacklist = :active
and a.fc_periode = d.fc_periode
and a.fd_date <= CONCAT(substring(d.fc_periode , 1, 4), "-", substring(d.fc_periode , 5, 6), "-", "15")
or b.fc_membername1 like :member and c.fv_trxdescription like :position
and a.fd_date >= :datestart
and a.fd_date <= :datefinish
or b.fc_membername1 like :member and c.fv_trxdescription like :position
and a.fc_periode = d.fc_periode
and a.fd_date <= CONCAT(substring(d.fc_periode , 1, 4), "-", substring(d.fc_periode , 5, 6), "-", "15")
order by
a.fc_membercode ,
a.fc_periode ,
a.fd_date


select 
a.fc_membercode,b.fc_membername1 ,
d.fv_trxdescription empposition,c.fv_trxdescription leavetype,
count(fd_leavedate ) jumlah
from t_leave a
left join t_member b on a.fc_membercode = b.fc_membercode
left join t_trxtype c on c.fc_trxcode = a.fc_leavetype and c.fc_trxid = "LEAVE_TYPE"
left join t_trxtype d on d.fc_trxcode = b.fc_memberposition and d.fc_trxid = "EMP_POSITION"
where a.fc_membercode like :member and a.fd_leavedate >= :datestart and a.fd_leavedate <= :datefinish and a.fc_leavetype = :leavetype
or b.fc_membername1 like :member and a.fd_leavedate >= :datestart and a.fd_leavedate <= :datefinish and a.fc_leavetype = :leavetype
group by a.fc_leavetype ,a.fc_membercode 

select * from t_trxtype where fc_trxid = "LEAVE_TYPE" order by fn_trxnumber 


update t_nomor 
set fn_docno = 0



select fc_trxcode ,fv_trxdescription 
from t_trxtype where fc_trxid = 'EMP_POSITION'

truncate t_backupmember 
insert into t_backupmember select * from t_member 

#############################################################################################################

select 
a.fc_periode,a.fc_membercode,
b.fc_membername1,b.fv_trxdescription,b.fc_divisionname,
b.fn_memberothernumber1,cast(b.uangmakanpuasa as char) uangmakanpuasa,
b.fc_memberattendancetype ,

a.fd_date,b.periodedate,a.fc_daytype,
concat(a.fc_shift1,a.fc_shift2,a.fc_shift3) shift ,
a.fc_timeinshift1 timein1,
if(a.fc_timeinshift2 in ("","00:00") or a.fc_timeinshift2 is null , a.fc_timeoutshift1 ,a.fc_timeoutshift2 ) timeout1,
a.fc_timeinshift3 timein2,
a.fc_timeoutshift3 timeout2,
a.fn_totalhour ,a.fn_effectivehour, 
a.fn_ot1 ,a.fn_ot2 ,a.fn_ot3 ,a.fn_ot4 ,a.fn_ot5 ,a.fn_ot6,
a.fn_jumlahshift ,
a.fc_ti ,
a.fv_description, 

b.total_ot1,
b.total_ot2,
b.total_ot3,
b.total_ot4,
b.total_ot5,
b.total_ot6,
b.totalshift,

b.total_workday,
b.total_shift1,b.total_shift2,b.total_shift3,
b.total_ot alltotalot,
c.total_ot fptotalot,
b.total_ot - c.total_ot ltotalot
from t_attendance a
left join
(
select 
a.fc_periode,a.fd_date ,
a.fc_membercode,b.fc_membername1 , b.fn_memberothernumber1 ,b.fc_memberattendancetype ,
c.fv_trxdescription,b.fc_memberposition ,d.fc_divisionname,b.fc_divisioncode,
e.periodedate,
month(e.periodedate) monthperiode,
sum(a.fn_ot1 ) total_ot1,
sum(a.fn_ot2 ) total_ot2,
sum(a.fn_ot3 ) total_ot3 ,
sum(a.fn_ot4 ) total_ot4,
sum(a.fn_ot5 ) total_ot5,
sum(a.fn_ot6 ) total_ot6,
(sum(a.fn_ot1 ) * 1.5 ) + (sum(a.fn_ot2 ) * 2 ) + (sum(a.fn_ot3 ) * 3 ) + (sum(a.fn_ot4 ) * 4 ) + (sum(a.fn_ot5 ) * 6 ) + (sum(a.fn_ot6 ) * 8 ) total_ot,	
sum( length(substring( a.fc_daytype,1,1)) ) total_workday,
sum( '1' in (a.fc_shift1,a.fc_shift2 ,a.fc_shift3) ) total_shift1,
sum( '2' in (a.fc_shift1,a.fc_shift2 ,a.fc_shift3) )  total_shift2,
sum( '3' in (a.fc_shift1,a.fc_shift2 ,a.fc_shift3) ) total_shift3,
sum(a.fn_jumlahshift ) totalshift,
concat( "uang makan puasa tanggal ",f.mindate, " s/d ", f.maxdate," : ",sum(a.fl_fastingshift )," hari" ) uangmakanpuasa
from t_attendance a
left join t_member b on a.fc_membercode = b.fc_membercode
left join t_trxtype c on b.fc_memberposition = c.fc_trxcode and c.fc_trxid = "EMP_POSITION"
left join t_division d on b.fc_divisioncode = d.fc_divisioncode 
left join ( 
	select fc_membercode,fd_date,fc_periode ,if(fd_date < concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" ) , 
	concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" ) ,fd_date ) 
	periodedate from t_attendance
) e on e.fc_membercode = a.fc_membercode and a.fd_date = e.fd_date and a.fc_periode = e.fc_periode
left join (
	select min(fd_date) mindate,max(fd_date) maxdate,
	fv_description 
	from t_fastingday group by fc_fastingdayno 
) f on month(e.periodedate) = month(f.mindate) and year(e.periodedate) = year(f.mindate) 
	or month(e.periodedate) = month(f.maxdate) and year(e.periodedate) = year(f.maxdate)
	where e.periodedate >= :datestart and e.periodedate <= :datefinish
	group by a.fc_membercode ,a.fc_periode 
)b on a.fc_membercode = b.fc_membercode and a.fc_periode = b.fc_periode
left join
(
select 
a.fc_periode,a.fd_date ,
a.fc_membercode,b.fc_membername1,
(sum(a.fn_ot1 ) * 1.5 ) + (sum(a.fn_ot2 ) * 2 ) + (sum(a.fn_ot3 ) * 3 ) + (sum(a.fn_ot4 ) * 4 ) + (sum(a.fn_ot5 ) * 6 ) + (sum(a.fn_ot6 ) * 8 ) total_ot
from t_attendance a
left join t_member b on a.fc_membercode = b.fc_membercode
left join t_trxtype c on b.fc_memberposition = c.fc_trxcode and c.fc_trxid = "EMP_POSITION"
left join t_division d on b.fc_divisioncode = d.fc_divisioncode 
left join ( 
	select fc_membercode,fd_date,fc_periode ,if(fd_date < concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" ) , 
	concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" ) ,fd_date ) 
	periodedate from t_attendance
) e on e.fc_membercode = a.fc_membercode and a.fd_date = e.fd_date and a.fc_periode = e.fc_periode
left join (
	select min(fd_date) mindate,max(fd_date) maxdate,
	fv_description 
	from t_fastingday group by fc_fastingdayno 
) f on month(e.periodedate) = month(f.mindate) and year(e.periodedate) = year(f.mindate) 
	or month(e.periodedate) = month(f.maxdate) and year(e.periodedate) = year(f.maxdate)
	where e.periodedate >= :datestart and e.periodedate <= last_day(:datestart)
group by a.fc_membercode ,a.fc_periode 
)c on a.fc_membercode = c.fc_membercode and a.fc_periode = c.fc_periode
where 
a.fc_membercode  like :member and b.fv_trxdescription like :position and b.fc_divisioncode like :division and b.periodedate >= :datestart and b.periodedate <= :datefinish
or 
b.fc_membername1 like :member and b.fv_trxdescription like :position and b.fc_divisioncode like :division and b.periodedate >= :datestart and b.periodedate <= :datefinish
order by b.fn_memberothernumber1,a.fc_membercode,a.fd_date,b.periodedate

-- Revisi REPORT SCRIPT ATTENDANCE


select 
a.fc_periode,a.fc_membercode,
b.fc_membername1,b.fv_trxdescription,b.fc_divisionname,
b.fn_memberothernumber1,cast(b.uangmakanpuasa as char) uangmakanpuasa,
b.fc_memberattendancetype ,

a.fd_date,b.periodedate,a.fc_daytype,
concat(a.fc_shift1,a.fc_shift2,a.fc_shift3) shift ,
a.fc_timeinshift1 timein1,
a.fc_timeoutshift1 timeout1,
a.fc_timeinshift2 timein2,
a.fc_timeoutshift2 timeout2,
a.fc_timeinshift3 timein3,
a.fc_timeoutshift3 timeout3,
a.fn_totalhour ,a.fn_effectivehour, 
a.fn_ot1 ,a.fn_ot2 ,a.fn_ot3 ,a.fn_ot4 ,a.fn_ot5 ,a.fn_ot6,
a.fn_jumlahshift ,
a.fc_ti ,
a.fv_description, 

b.total_ot1,
b.total_ot2,
b.total_ot3,
b.total_ot4,
b.total_ot5,
b.total_ot6,
b.totalshift,

b.total_workday,
b.total_shift1,b.total_shift2,b.total_shift3,
b.total_ot alltotalot,
c.total_ot fptotalot,
b.total_ot - c.total_ot ltotalot
from t_attendance a
left join
(
select 
a.fc_periode,a.fd_date ,
a.fc_membercode,b.fc_membername1 , b.fn_memberothernumber1 ,b.fc_memberattendancetype ,
c.fv_trxdescription,b.fc_memberposition ,d.fc_divisionname,b.fc_divisioncode,
e.periodedate,
month(e.periodedate) monthperiode,
sum(a.fn_ot1 ) total_ot1,
sum(a.fn_ot2 ) total_ot2,
sum(a.fn_ot3 ) total_ot3 ,
sum(a.fn_ot4 ) total_ot4,
sum(a.fn_ot5 ) total_ot5,
sum(a.fn_ot6 ) total_ot6,
(sum(a.fn_ot1 ) * 1.5 ) + (sum(a.fn_ot2 ) * 2 ) + (sum(a.fn_ot3 ) * 3 ) + (sum(a.fn_ot4 ) * 4 ) + (sum(a.fn_ot5 ) * 6 ) + (sum(a.fn_ot6 ) * 8 ) total_ot,	
sum( length(substring( a.fc_daytype,1,1)) ) total_workday,
sum( '1' in (a.fc_shift1,a.fc_shift2 ,a.fc_shift3) ) total_shift1,
sum( '2' in (a.fc_shift1,a.fc_shift2 ,a.fc_shift3) )  total_shift2,
sum( '3' in (a.fc_shift1,a.fc_shift2 ,a.fc_shift3) ) total_shift3,
sum(a.fn_jumlahshift ) totalshift,
concat( "uang makan puasa tanggal ",f.mindate, " s/d ", f.maxdate," : ",sum(a.fl_fastingshift )," hari" ) uangmakanpuasa
from t_attendance a
left join t_member b on a.fc_membercode = b.fc_membercode
left join t_trxtype c on b.fc_memberposition = c.fc_trxcode and c.fc_trxid = "EMP_POSITION"
left join t_division d on b.fc_divisioncode = d.fc_divisioncode 
left join ( 
	select fc_membercode,fd_date,fc_periode ,if(fd_date < concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" ) , 
	concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" ) ,fd_date ) 
	periodedate from t_attendance
) e on e.fc_membercode = a.fc_membercode and a.fd_date = e.fd_date and a.fc_periode = e.fc_periode
left join (
	select min(fd_date) mindate,max(fd_date) maxdate,
	fv_description 
	from t_fastingday group by fc_fastingdayno 
) f on month(e.periodedate) = month(f.mindate) and year(e.periodedate) = year(f.mindate) 
	or month(e.periodedate) = month(f.maxdate) and year(e.periodedate) = year(f.maxdate)
	where e.periodedate >= :datestart and e.periodedate <= :datefinish
	group by a.fc_membercode ,a.fc_periode 
)b on a.fc_membercode = b.fc_membercode and a.fc_periode = b.fc_periode
left join
(
select 
a.fc_periode,a.fd_date ,
a.fc_membercode,b.fc_membername1,
(sum(a.fn_ot1 ) * 1.5 ) + (sum(a.fn_ot2 ) * 2 ) + (sum(a.fn_ot3 ) * 3 ) + (sum(a.fn_ot4 ) * 4 ) + (sum(a.fn_ot5 ) * 6 ) + (sum(a.fn_ot6 ) * 8 ) total_ot
from t_attendance a
left join t_member b on a.fc_membercode = b.fc_membercode
left join t_trxtype c on b.fc_memberposition = c.fc_trxcode and c.fc_trxid = "EMP_POSITION"
left join t_division d on b.fc_divisioncode = d.fc_divisioncode 
left join ( 
	select fc_membercode,fd_date,fc_periode ,if(fd_date < concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" ) , 
	concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" ) ,fd_date ) 
	periodedate from t_attendance
) e on e.fc_membercode = a.fc_membercode and a.fd_date = e.fd_date and a.fc_periode = e.fc_periode
left join (
	select min(fd_date) mindate,max(fd_date) maxdate,
	fv_description 
	from t_fastingday group by fc_fastingdayno 
) f on month(e.periodedate) = month(f.mindate) and year(e.periodedate) = year(f.mindate) 
	or month(e.periodedate) = month(f.maxdate) and year(e.periodedate) = year(f.maxdate)
	where e.periodedate >= :datestart and e.periodedate <= last_day(:datestart)
group by a.fc_membercode ,a.fc_periode 
)c on a.fc_membercode = c.fc_membercode and a.fc_periode = c.fc_periode
where 
a.fc_membercode  like :member and b.fv_trxdescription like :position and b.fc_divisioncode like :division and b.periodedate >= :datestart and b.periodedate <= :datefinish
or 
b.fc_membername1 like :member and b.fv_trxdescription like :position and b.fc_divisioncode like :division and b.periodedate >= :datestart and b.periodedate <= :datefinish
order by b.fn_memberothernumber1,a.fc_membercode,a.fd_date,b.periodedate




