-- NOte CHEK DI ID EMPLOYEE 77220
-- TSI 66316 		-> TSI 71818 DEDI (06)
update t_member set fc_membercode = 'TSI 71818' where fc_membercode = 'TSI 66316';
update t_overtime set fc_membercode = 'TSI 71818' where fc_membercode = 'TSI 66316';
update t_leave set fc_membercode = 'TSI 71818' where fc_membercode = 'TSI 66316';
update t_attendance set fc_membercode = 'TSI 71818' where fc_membercode = 'TSI 66316';
update t_attendance_backup set fc_membercode = 'TSI 71818' where fc_membercode = 'TSI 66316';
-- TSI 65815 		-> TSI 73218 EDDY LESMONO (07)
update t_member set fc_membercode = 'TSI 73218' where fc_membercode = 'TSI 65815';
update t_overtime set fc_membercode = 'TSI 73218' where fc_membercode = 'TSI 65815';
update t_leave set fc_membercode = 'TSI 73218' where fc_membercode = 'TSI 65815';
update t_attendance set fc_membercode = 'TSI 73218' where fc_membercode = 'TSI 65815';
update t_attendance_backup set fc_membercode = 'TSI 73218' where fc_membercode = 'TSI 65815';
-- TSI 75520 		-> TSI 25819 ULFA (31)
update t_member set fc_membercode = 'TSI 25819' where fc_membercode = 'TSI 75520';
update t_overtime set fc_membercode = 'TSI 25819' where fc_membercode = 'TSI 75520';
update t_leave set fc_membercode = 'TSI 25819' where fc_membercode = 'TSI 75520';
update t_attendance set fc_membercode = 'TSI 25819' where fc_membercode = 'TSI 75520';
update t_attendance_backup set fc_membercode = 'TSI 25819' where fc_membercode = 'TSI 75520';
-- TSI 66216	 	-> TSI 71918 RIAN (36)
update t_member set fc_membercode = 'TSI 71918' where fc_membercode = 'TSI 66216';
update t_overtime set fc_membercode = 'TSI 71918' where fc_membercode = 'TSI 66216';
update t_leave set fc_membercode = 'TSI 71918' where fc_membercode = 'TSI 66216';
update t_attendance set fc_membercode = 'TSI 71918' where fc_membercode = 'TSI 66216';
update t_attendance_backup set fc_membercode = 'TSI 71918' where fc_membercode = 'TSI 66216';
-- TSI 71418 		-> TSI 77220 M.Hariyadi (43)
update t_member set fc_membercode = 'TSI 77220' where fc_membercode = 'TSI 71418';
update t_overtime set fc_membercode = 'TSI 77220' where fc_membercode = 'TSI 71418';
update t_leave set fc_membercode = 'TSI 77220' where fc_membercode = 'TSI 71418';
update t_attendance set fc_membercode = 'TSI 77220' where fc_membercode = 'TSI 71418';
update t_attendance_backup set fc_membercode = 'TSI 77220' where fc_membercode = 'TSI 71418';
-- TSI 69417 		-> TSI 78220 Supriadin (49)
update t_member set fc_membercode = 'TSI 78220' where fc_membercode = 'TSI 69417';
update t_overtime set fc_membercode = 'TSI 78220' where fc_membercode = 'TSI 69417';
update t_leave set fc_membercode = 'TSI 78220' where fc_membercode = 'TSI 69417';
update t_attendance set fc_membercode = 'TSI 78220' where fc_membercode = 'TSI 69417';
update t_attendance_backup set fc_membercode = 'TSI 78220' where fc_membercode = 'TSI 69417';
-- TSI 72318 		-> TSI 78320 Ainul (65)
update t_member set fc_membercode = 'TSI 78320' where fc_membercode = 'TSI 72318';
update t_overtime set fc_membercode = 'TSI 78320' where fc_membercode = 'TSI 72318';
update t_leave set fc_membercode = 'TSI 78320' where fc_membercode = 'TSI 72318';
update t_attendance set fc_membercode = 'TSI 78320' where fc_membercode = 'TSI 72318';
update t_attendance_backup set fc_membercode = 'TSI 78320' where fc_membercode = 'TSI 72318';
-- TSI 68717 		-> TSI 77520 ISMED INUNU (77)
update t_member set fc_membercode = 'TSI 77520' where fc_membercode = 'TSI 68717';
update t_overtime set fc_membercode = 'TSI 77520' where fc_membercode = 'TSI 68717';
update t_leave set fc_membercode = 'TSI 77520' where fc_membercode = 'TSI 68717';
update t_attendance set fc_membercode = 'TSI 77520' where fc_membercode = 'TSI 68717';
update t_attendance_backup set fc_membercode = 'TSI 77520' where fc_membercode = 'TSI 68717';
-- TSI 64915 		-> TSI 71618 Dimas (123)
update t_member set fc_membercode = 'TSI 71618' where fc_membercode = 'TSI 64915';
update t_overtime set fc_membercode = 'TSI 71618' where fc_membercode = 'TSI 64915';
update t_leave set fc_membercode = 'TSI 71618' where fc_membercode = 'TSI 64915';
update t_attendance set fc_membercode = 'TSI 71618' where fc_membercode = 'TSI 64915';
update t_attendance_backup set fc_membercode = 'TSI 71618' where fc_membercode = 'TSI 64915';
-- TSI 66016 		-> TSI 71518 Prasetyo T.U (132)
update t_member set fc_membercode = 'TSI 71518' where fc_membercode = 'TSI 66016';
update t_overtime set fc_membercode = 'TSI 71518' where fc_membercode = 'TSI 66016';
update t_leave set fc_membercode = 'TSI 71518' where fc_membercode = 'TSI 66016';
update t_attendance set fc_membercode = 'TSI 71518' where fc_membercode = 'TSI 66016';
update t_attendance_backup set fc_membercode = 'TSI 71518' where fc_membercode = 'TSI 66016';
-- TSI 66416 		-> TSI 72018 M. Fikri Ro. (157)
update t_member set fc_membercode = 'TSI 72018' where fc_membercode = 'TSI 66416';
update t_overtime set fc_membercode = 'TSI 72018' where fc_membercode = 'TSI 66416';
update t_leave set fc_membercode = 'TSI 72018' where fc_membercode = 'TSI 66416';
update t_attendance set fc_membercode = 'TSI 72018' where fc_membercode = 'TSI 66416';
update t_attendance_backup set fc_membercode = 'TSI 72018' where fc_membercode = 'TSI 66416';
-- TSI 68817 		-> TSI 77620 M.Rusli (161)
update t_member set fc_membercode = 'TSI 77620' where fc_membercode = 'TSI 68817';
update t_overtime set fc_membercode = 'TSI 77620' where fc_membercode = 'TSI 68817';
update t_leave set fc_membercode = 'TSI 77620' where fc_membercode = 'TSI 68817';
update t_attendance set fc_membercode = 'TSI 77620' where fc_membercode = 'TSI 68817';
update t_attendance_backup set fc_membercode = 'TSI 77620' where fc_membercode = 'TSI 68817';
-- TSI 69217 		-> TSI 75919 M. Irfansyah (165)
update t_member set fc_membercode = 'TSI 75919' where fc_membercode = 'TSI 69217';
update t_overtime set fc_membercode = 'TSI 75919' where fc_membercode = 'TSI 69217';
update t_leave set fc_membercode = 'TSI 75919' where fc_membercode = 'TSI 69217';
update t_attendance set fc_membercode = 'TSI 75919' where fc_membercode = 'TSI 69217';
update t_attendance_backup set fc_membercode = 'TSI 75919' where fc_membercode = 'TSI 69217';
-- TSI 69317 		-> TSI 75619 Adhek tama (166)
update t_member set fc_membercode = 'TSI 75619' where fc_membercode = 'TSI 69317';
update t_overtime set fc_membercode = 'TSI 75619' where fc_membercode = 'TSI 69317';
update t_leave set fc_membercode = 'TSI 75619' where fc_membercode = 'TSI 69317';
update t_attendance set fc_membercode = 'TSI 75619' where fc_membercode = 'TSI 69317';
update t_attendance_backup set fc_membercode = 'TSI 75619' where fc_membercode = 'TSI 69317';
-- TSI 77220 		-> TSI 77320 Nurhadi (171)
update t_member set fc_membercode = 'TSI 77320' where fc_membercode = 'TSI 77220';
update t_overtime set fc_membercode = 'TSI 77320' where fc_membercode = 'TSI 77220';
update t_leave set fc_membercode = 'TSI 77320' where fc_membercode = 'TSI 77220';
update t_attendance set fc_membercode = 'TSI 77320' where fc_membercode = 'TSI 77220';
update t_attendance_backup set fc_membercode = 'TSI 77320' where fc_membercode = 'TSI 77220';
-- TSI 68017 		-> TSI 76019 Dedi apri (178)
update t_member set fc_membercode = 'TSI 76019' where fc_membercode = 'TSI 68017';
update t_overtime set fc_membercode = 'TSI 76019' where fc_membercode = 'TSI 68017';
update t_leave set fc_membercode = 'TSI 76019' where fc_membercode = 'TSI 68017';
update t_attendance set fc_membercode = 'TSI 76019' where fc_membercode = 'TSI 68017';
update t_attendance_backup set fc_membercode = 'TSI 76019' where fc_membercode = 'TSI 68017';
-- TSI 67817 		-> TSI 75319 M. Arif (180)
update t_member set fc_membercode = 'TSI 75319' where fc_membercode = 'TSI 67817';
update t_overtime set fc_membercode = 'TSI 75319' where fc_membercode = 'TSI 67817';
update t_leave set fc_membercode = 'TSI 75319' where fc_membercode = 'TSI 67817';
update t_attendance set fc_membercode = 'TSI 75319' where fc_membercode = 'TSI 67817';
update t_attendance_backup set fc_membercode = 'TSI 75319' where fc_membercode = 'TSI 67817';
-- TSI 67717 		-> TSI 75719 Nur Huda (181)
update t_member set fc_membercode = 'TSI 75719' where fc_membercode = 'TSI 67717';
update t_overtime set fc_membercode = 'TSI 75719' where fc_membercode = 'TSI 67717';
update t_leave set fc_membercode = 'TSI 75719' where fc_membercode = 'TSI 67717';
update t_attendance set fc_membercode = 'TSI 75719' where fc_membercode = 'TSI 67717';
update t_attendance_backup set fc_membercode = 'TSI 75719' where fc_membercode = 'TSI 67717';
-- TSI 70517 		-> TSI 70518 M.Irfan fanani (182)
update t_member set fc_membercode = 'TSI 70518' where fc_membercode = 'TSI 70517';
update t_overtime set fc_membercode = 'TSI 70518' where fc_membercode = 'TSI 70517';
update t_leave set fc_membercode = 'TSI 70518' where fc_membercode = 'TSI 70517';
update t_attendance set fc_membercode = 'TSI 70518' where fc_membercode = 'TSI 70517';
update t_attendance_backup set fc_membercode = 'TSI 70518' where fc_membercode = 'TSI 70517';