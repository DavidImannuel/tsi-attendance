
update t_attendance a 
left join y_attendance_date b on 
a.fc_membercode = b.fc_membercode 
and DATE_FORMAT(a.fc_workin,'%Y-%m-%d' ) =  DATE_FORMAT(b.fd_date ,'%Y-%m-%d' ) and b.fc_status = 'I'
left join y_attendance_date c on 
a.fc_membercode = c.fc_membercode 
and DATE_FORMAT(a.fc_workout,'%Y-%m-%d' ) =  DATE_FORMAT(c.fd_date ,'%Y-%m-%d' ) and b.fc_status = 'O'
set 
a.fd_realdatein = COALESCE(b.fd_date,'0000-00-00 00:00:00'),
a.fd_realdateout = COALESCE(c.fd_date,'0000-00-00 00:00:00')
where a.fc_periode = '201912' 

update t_attendance a 
left join y_attendance_date b on 
a.fc_membercode = b.fc_membercode 
and DATE_FORMAT(a.fc_workin,'%Y-%m-%d' ) =  DATE_FORMAT(b.fd_date ,'%Y-%m-%d' ) and b.fc_status = 'I'
set 
a.fd_realdatein = b.fd_date
where a.fc_periode = '201912'

update t_attendance a 
left join y_attendance_date b on 
a.fc_membercode = b.fc_membercode 
and DATE_FORMAT(a.fc_workout,'%Y-%m-%d' ) =  DATE_FORMAT(b.fd_date ,'%Y-%m-%d' ) and b.fc_status = 'O'
set 
a.fd_realdateout = b.fd_date
where a.fc_periode = '201912'




select 
'001' as fc_branch,
DATE_FORMAT(date_add("2020-03-16", INTERVAL 4 day),'%Y%m') as fc_periode,
(select fc_membercode from t_member where fc_membername1 = a.fc_nameis) as membercode,
SYSDATE() as fd_dategenerate, 
CASE when a.fc_date6 = '' then '-'
else a.fc_date6 end	as fc_timerangecode,
@fc_workin := CONCAT(date_add("2020-03-16", INTERVAL 4 day)," ",b.fc_workin) as fc_workin,
@fc_workout := 
case when length(a.fc_date6) > 1 then
	case 
	when (select fc_workout from t_dailytimerange where fc_timerangecode = right(a.fc_date6,1) ) > b.fc_workin then
		CONCAT(date_add("2020-03-16", INTERVAL 4 day),' ',(select fc_workout from t_dailytimerange where fc_timerangecode = right(a.fc_date6,1)) )
	else 
		CONCAT(date_add("2020-03-16", INTERVAL (4 + 1) day),' ',(select fc_workout from t_dailytimerange where fc_timerangecode = right(a.fc_date6,1)))
	end 
else CONCAT(date_add("2020-03-16", INTERVAL 4 day),' ',b.fc_workout) end as fc_workout,
timediff(@fc_workout,@fc_workin) as totalhour
from t_tempyudharotateshift a
left join t_dailytimerange b on
CASE when a.fc_date6 = '' then '-'
else left(fc_date6,1) end
= b.fc_timerangecode
where a.fc_blank != 'O'



select 
'001' as fc_branch,
DATE_FORMAT(date_add("2020-03-16", INTERVAL 4 day),'%Y%m%d') as fc_periode,
c.fc_membercode as fc_membercode,
CASE when a.fc_date5 = '' then '-'
else a.fc_date5 end	as fc_timerangecode,
@fc_workin := CONCAT(date_add("2020-03-16", INTERVAL 4 day)," ",b.fc_workin) as fc_workin,
@fc_workout :=case when length(a.fc_date5) > 1 then
	case 
	when (select fc_workout from t_dailytimerange where fc_timerangecode = right(a.fc_date5,1) ) > b.fc_workin then
		CONCAT(date_add("2020-03-16", INTERVAL 4 day),' ',(select fc_workout from t_dailytimerange where fc_timerangecode = right(a.fc_date5,1)) )
	else 
		CONCAT(date_add("2020-03-16", INTERVAL (4 + 1) day),' ',(select fc_workout from t_dailytimerange where fc_timerangecode = right(a.fc_date5,1)))
	end 
else CONCAT(date_add("2020-03-16", INTERVAL 4 day),' ',b.fc_workout) end as fc_workout,
@total_hours := case when timediff(@fc_workout,@fc_workin) = '00:00:00' then
	 addtime('24:00:00',timediff(@fc_workout,@fc_workin))
	else timediff(@fc_workout,@fc_workin) end total_hours,
@total_break := case when length(a.fc_date5 ) = 1 then 
	@break1 := subtime(b.fc_workbreakout ,b.fc_workbreakin )
	when length(a.fc_date5 )= 2 then
	@break2 :=	addtime(@break1,timediff(
			(select fc_workbreakout from t_dailytimerange where fc_timerangecode = mid(a.fc_date5,2,1) ),
			(select fc_workbreakin from t_dailytimerange where fc_timerangecode = mid(a.fc_date5,2,1))
			) )
	 when length(a.fc_date5) > 1 then 
	@break3 := addtime(@break2,timediff(
		(select fc_workbreakout from t_dailytimerange where fc_timerangecode = right(a.fc_date5,1) ),
		(select fc_workbreakin from t_dailytimerange where fc_timerangecode = right(a.fc_date5,1))
	)) else '00:00' end as total_break,
subtime(@total_hours,@total_break) as effective_hours,
d.fd_date fd_realdatein,
e.fd_date fd_realdateout
from t_tempyudharotateshift a
left join t_dailytimerange b on
	CASE when a.fc_date5 = '' then '-'
	else left(fc_date5,1) end
= b.fc_timerangecode
left join t_member c on a.fc_nameis = c.fc_membername1
left join y_attendance_date d on c.fc_membercode = d.fc_membercode
-- 	and DATE_FORMAT(fc_workin ,'%Y-%m-%d' ) =  DATE_FORMAT(d.fd_date ,'%Y-%m-%d' ) and d.fc_status = 'I'
left join y_attendance_date e on c.fc_membercode = e.fc_membercode
	a/**/nd DATE_FORMAT(fc_workin ,'%Y-%m-%d' ) =  DATE_FORMAT(e.fd_date ,'%Y-%m-%d' ) and d.fc_status = 'O'
where a.fc_blank != 'O'




############################################################################

select 
b.fc_membercode as fc_membercode ,
a.fc_date5 as shift,
c.fc_timerangecode shift1,
d.fc_timerangecode shift2,
e.fc_timerangecode shift3,
@fc_workin := CONCAT(date_add("2019-12-16", INTERVAL 4 day)," ",c.fc_workin) fc_workin,
@fc_workout := case 
	when length( a.fc_date5 ) = 1 then
		CONCAT(date_add("2019-12-16", INTERVAL 4 day)," ",c.fc_workout)
	when length( a.fc_date5 ) = 2 then
		CONCAT(date_add("2019-12-16", INTERVAL 4 day)," ",d.fc_workout)
	when length( a.fc_date5 ) = 3 then
		case 
			when e.fc_workout > e.fc_workin then CONCAT(date_add("2019-12-16", INTERVAL 4 day)," ",d.fc_workout)
			else CONCAT(date_add("2019-12-16", INTERVAL (4 + 1)day)," ",e.fc_workout)
		end
end fc_workout,
@total_hours := case 
	when timediff(@fc_workout ,@fc_workin) = '00:00:00' then addtime( '24:00:00',timediff(@fc_workout ,@fc_workin) )
	else timediff(@fc_workout ,@fc_workin)
	end total_hours,	
@total_breaks := case
	when length(a.fc_date5) = 1 then
		@break1 := subtime(c.fc_workbreakout,c.fc_workbreakin )
	when length(a.fc_date5) = 2 then
		@break2 := addtime( @break1 ,subtime(d.fc_workbreakout,d.fc_workbreakin ) )
	when length(a.fc_date5) = 3 then
		@break3 := addtime( @break2 ,subtime(d.fc_workbreakout,d.fc_workbreakin ) )
	end total_breaks,
timediff(@total_hours,@total_breaks) effective_hours
from t_tempyudharotateshift a
left join t_member b on a.fc_nameis = b.fc_membername1
left join t_dailytimerange c on left(a.fc_date5,1) = c.fc_timerangecode
left join t_dailytimerange d on mid(a.fc_date5,2,1) = d.fc_timerangecode
left join t_dailytimerange e on if(length(a.fc_date5) = 3, right(a.fc_date5,1),'' ) = e.fc_timerangecode
where a.fc_blank != 'O'

#################################################################################################################
select 
b.fc_membercode as fc_membercode ,
case 
when a.fc_date5 = '' then '-'  
else a.fc_date5
end as shift,
c.fc_timerangecode shift1,
d.fc_timerangecode shift2,
e.fc_timerangecode shift3,
@fc_workin := CONCAT(date_add("2019-12-16", INTERVAL 4 day)," ",c.fc_workin) fc_workin,
@fc_workout := case 
	when length( a.fc_date5 ) = 1 then
		CONCAT(date_add("2019-12-16", INTERVAL 4 day)," ",c.fc_workout)
	when length( a.fc_date5 ) = 2 then
		CONCAT(date_add("2019-12-16", INTERVAL 4 day)," ",d.fc_workout)
	when length( a.fc_date5 ) = 3 then
		case 
			when e.fc_workout > e.fc_workin then CONCAT(date_add("2019-12-16", INTERVAL 4 day)," ",d.fc_workout)
			else CONCAT(date_add("2019-12-16", INTERVAL (4 + 1)day)," ",e.fc_workout)
		end
end fc_workout,
@total_hours := case 
	when timediff(@fc_workout ,@fc_workin) = '00:00:00' then addtime( '24:00:00',timediff(@fc_workout ,@fc_workin) )
	else timediff(@fc_workout ,@fc_workin)
	end total_hours,	
@total_breaks := case
	when length(a.fc_date5) = 1 then
		@break1 := subtime(c.fc_workbreakout,c.fc_workbreakin )
	when length(a.fc_date5) = 2 then
		@break2 := addtime( @break1 ,subtime(d.fc_workbreakout,d.fc_workbreakin ) )
	when length(a.fc_date5) = 3 then
		@break3 := addtime( @break2 ,subtime(d.fc_workbreakout,d.fc_workbreakin ) )
	end total_breaks,
timediff(@total_hours,@total_breaks) effective_hours,
f.fd_date fd_realdatein ,
g.fd_date fd_realdateout 
from t_tempyudharotateshift a
left join t_member b on a.fc_nameis = b.fc_membername1
left join t_dailytimerange c on 
left(a.fc_date5,1) = c.fc_timerangecode
left join t_dailytimerange d on mid(a.fc_date5,2,1) = d.fc_timerangecode
left join t_dailytimerange e on if(length(a.fc_date5) = 3, right(a.fc_date5,1),'' ) = e.fc_timerangecode
left join t_attendance_date f 
	on b.fc_membercode = f.fc_membercode 
	and date_format(@fc_workin,'%Y-%m-%d') = date_format(f.fd_date ,'%Y-%m-%d')
	and f.fc_status = 'I'
left join t_attendance_date g 
	on b.fc_membercode = g.fc_membercode 
	and date_format(@fc_workout,'%Y-%m-%d') = date_format(g.fd_date ,'%Y-%m-%d')
	and g.fc_status = 'O'
where a.fc_blank != 'O'

################################################################################################################
select 
b.fc_membercode as fc_membercode ,
case when :fc_date = '' then '-'  
else :fc_date end as shift,
@fc_workin := CONCAT(date_add(:datestart, INTERVAL :i day)," ",c.fc_workin) fc_workin,
@fc_workout := case 
	when length( :fc_date ) = '-' then
		CONCAT(date_add(:datestart, INTERVAL :i day)," ",c.fc_workout)
	when length( :fc_date ) = 1 then
		CONCAT(date_add(:datestart, INTERVAL :i day)," ",c.fc_workout)
	when length( :fc_date ) = 2 then
		CONCAT(date_add(:datestart, INTERVAL :i day)," ",d.fc_workout)
	when length( :fc_date ) = 3 then
		case 
			when e.fc_workout > e.fc_workin then CONCAT(date_add(:datestart, INTERVAL :i day)," ",d.fc_workout)
			else CONCAT(date_add(:datestart, INTERVAL (:i + 1)day)," ",e.fc_workout)
		end
end fc_workout,
@total_hours := case 
	when timediff(@fc_workout ,@fc_workin) = '00:00:00' then addtime( '24:00:00',timediff(@fc_workout ,@fc_workin) )
	else timediff(@fc_workout ,@fc_workin)
	end total_hours,	
@total_breaks := case
	when length(:fc_date) = 1 then
		@break1 := subtime(c.fc_workbreakout,c.fc_workbreakin )
	when length(:fc_date) = 2 then
		@break2 := addtime( @break1 ,subtime(d.fc_workbreakout,d.fc_workbreakin ) )
	when length(:fc_date) = 3 then
		@break3 := addtime( @break2 ,subtime(d.fc_workbreakout,d.fc_workbreakin ) )
	end total_breaks,
timediff(@total_hours,@total_breaks) effective_hours,
f.fd_date fd_realdatein ,
g.fd_date fd_realdateout  
from t_tempyudharotateshift a
left join t_member b on a.fc_nameis = b.fc_membername1
left join t_dailytimerange c on 
case when :fc_date = '' then '-'  
else left(:fc_date,1) end = c.fc_timerangecode
left join t_dailytimerange d on mid(:fc_date,2,1) = d.fc_timerangecode
left join t_dailytimerange e on if(length(:fc_date) = 3, right(:fc_date,1),'' ) = e.fc_timerangecode
left join t_attendance_date f 
	on b.fc_membercode = f.fc_membercode 
	and date_format(@fc_workin,'%Y-%m-%d') = date_format(f.fd_date ,'%Y-%m-%d')
	and f.fc_status = 'I'
left join t_attendance_date g 
	on b.fc_membercode = g.fc_membercode 
	and date_format(@fc_workout,'%Y-%m-%d') = date_format(g.fd_date ,'%Y-%m-%d')
	and g.fc_status = 'O'
where a.fc_blank != 'O'
#################################################################################################################
insert into t_attendance(fc_branch,fc_periode,fc_membercode ,fd_dategenerate ,fc_timerangecode ,fc_workin ,fc_workout,fd_realdatein ,fd_realdateout ) 
select 
"001" fc_branch ,
date_format(:datestart,'%Y%m') fc_periode,
b.fc_membercode as fc_membercode ,
sysdate() as fd_dategenerate ,
case 
when :fc_date = "" then "-"  
else :fc_date
end as fc_timerangecode ,
@fd_workin := CONCAT(date_add(:datestart, INTERVAL :i day)," ",c.fc_workin) fd_workin ,
@fd_workout := case
	when length( :fc_date ) = 2 then
		CONCAT(date_add(:datestart, INTERVAL :i day)," ",d.fc_workout)
	when length( :fc_date ) = 3 then
		case 
			when e.fc_workout > e.fc_workin then CONCAT(date_add(:datestart, INTERVAL :i day)," ",d.fc_workout)
			else CONCAT(date_add(:datestart, INTERVAL (:i + 1)day)," ",e.fc_workout)
		end
	else 
		CONCAT(date_add(:datestart, INTERVAL :i day)," ",c.fc_workout) 
end fd_workout,
f.fd_date fd_realdatein,
g.fd_date fd_realdateout
from t_tempyudharotateshift a
left join t_member b on a.fc_nameis = b.fc_membername1
left join t_dailytimerange c on if(:fc_date = "","-",left(:fc_date,1)) = c.fc_timerangecode
left join t_dailytimerange d on mid(:fc_date,2,1) = d.fc_timerangecode
left join t_dailytimerange e on if(length(:fc_date) = 3, right(:fc_date,1),"" ) = e.fc_timerangecode
left join y_attendance_date f on f.fc_membercode  = b.fc_membercode and DATE_FORMAT(@fd_workin,"%Y-%m-%d") =  DATE_FORMAT(f.fd_date,"%Y-%m-%d") and f.fc_status = "I"
left join y_attendance_date g on g.fc_membercode  = b.fc_membercode and DATE_FORMAT(@fd_workin,"%Y-%m-%d") =  DATE_FORMAT(g.fd_date,"%Y-%m-%d") and g.fc_status = "O"
where a.fc_blank != 'O'


##########################################################################################################

insert into t_attendance(fc_branch,fc_periode,fc_membercode ,fd_dategenerate ,fc_timerangecode ,fc_workin ,fc_workout,fd_realdatein,fd_realdateout )  
select  "001" fc_branch ,  date_format(:datestart,"%Y%m") fc_periode,  
b.fc_membercode as fc_membercode ,  sysdate() as fd_dategenerate ,  
case  when :fc_date = "" then "-"  
else :fc_date end as fc_timerangecode ,  
CONCAT(date_add(:datestart, INTERVAL :i day)," ",c.fc_workin )  fd_workin ,  
case  when length( :fc_date ) = 2 then  CONCAT(date_add(:datestart, INTERVAL :i day)," ",d.fc_workout )  
		case when :fc_date in ('21','31','32') then 
		end
	when length( :fc_date ) = 3 then  
		case  when e.fc_workout > e.fc_workin then CONCAT(date_add(:datestart, INTERVAL :i day)," ",d.fc_workout)  
		else CONCAT(date_add(:datestart, INTERVAL (:i + 1)day)," ",e.fc_workout)  
end  else  CONCAT(date_add(:datestart, INTERVAL :i day)," ",c.fc_workout)  end 
fd_workout , 
COALESCE(f.fd_date,'0000-00-00 00:00:00') fd_realdatein,  COALESCE(g.fd_date,'0000-00-00 00:00:00') fd_realdateout  
from t_tempyudharotateshift a  
left join t_member b on a.fc_nameis = b.fc_membername1  
left join t_dailytimerange c on if(:fc_date = "","-",left(:fc_date,1)) = c.fc_timerangecode  
left join t_dailytimerange d on mid(:fc_date,2,1) = d.fc_timerangecode  
left join t_dailytimerange e on if(length(:fc_date) = 3, right(:fc_date,1),"" ) = e.fc_timerangecode  
left join t_attendance_date f 
on f.fc_membercode  = b.fc_membercode and DATE_FORMAT( concat(date_add(:datestart, INTERVAL :i day)," ",c.fc_workin) ,"%Y-%m-%d") =  DATE_FORMAT(f.fd_date,"%Y-%m-%d") and f.fc_status = "I"  
left join t_attendance_date g on g.fc_membercode  = b.fc_membercode 
and DATE_FORMAT( case  when length( :fc_date ) = 2 then  CONCAT(date_add(:datestart, INTERVAL :i day)," ",d.fc_workout )  
when length( :fc_date ) = 3 then  case  when e.fc_workout > e.fc_workin then CONCAT(date_add(:datestart, INTERVAL :i day)," ",d.fc_workout)  
else CONCAT(date_add(":datestart", INTERVAL (0 + 1)day)," ",e.fc_workout)  end  else  CONCAT(date_add(:datestart, INTERVAL :i day)," ",c.fc_workout)  end ,"%Y-%m-%d") =  DATE_FORMAT(g.fd_date,"%Y-%m-%d") 
and g.fc_status = "O"  where a.fc_blank != "O" ;




############################################################################################
-- overtime

select 
b.fc_membername1,a.fc_branch,a.fc_periode,a.fc_membercode ,a.fd_dategenerate ,a.fc_timerangecode ,a.fc_workin 
,a.fc_workout,c.fc_divisionname ,
coalesce(a.fd_realdatein,'0000-00-00 00:00:00') fd_realdatein,coalesce(a.fd_realdateout,'0000-00-00 00:00:00') fd_realdateout
from t_attendance a
left join t_member b on a.fc_membercode = b.fc_membercode 
left join t_division c on b.fc_divisioncode = c.fc_divisioncode 
order by a.fd_realdatein 


select 
a.fc_branch,a.fc_periode,a.fc_membercode ,a.fd_dategenerate ,a.fc_timerangecode ,a.fc_workin 
,a.fc_workout,
coalesce(a.fd_realdatein,'0000-00-00 00:00:00') fd_realdatein,coalesce(a.fd_realdateout,'0000-00-00 00:00:00') fd_realdateout,
COALESCE(b.fv_description,'Not Holiday') 
from t_attendance a 
left join t_holiday b on DATE_FORMAT(a.fd_realdatein,'%Y-%m-%d') = DATE_FORMAT( b.fd_datestart,'%Y-%m-%d' )
order by a.fd_realdatein 





select * from t_holiday where date_format(fd_datestart,'%Y') = date_format(SYSDATE() ,'%Y') 
select * from t_holiday where fv_description like :search and DATE_FORMAT(fd_datestart,'%Y-%m-%d') >= date_format(:date_start,"%Y-%m-%d") and DATE_FORMAT(fd_datestart,'%Y-%m-%d') <= date_format(:date_finish,"%Y-%m-%d") order by fd_datestart

select * from t_holiday where DATE_FORMAT(fd_datestart,'%Y-%m-%d') >= '20129-12-01' 

insert into t_attendance_date(fc_branch ,fc_membercode ,fd_date ,fc_status )
select '001' fc_branch,a.fc_membercode ,a.fd_date,a.fc_status from
(select 
concat('TSI ',fc_membercode)fc_membercode ,
DATE_FORMAT( STR_TO_DATE(fc_datein ,'%m/%d/%Y %H:%i'),'%Y-%m-%d %H:%i') fd_date,
'I' fc_status 
from t_pull_attendance
union all 
select 
concat('TSI ',fc_membercode)fc_membercode ,
DATE_FORMAT( STR_TO_DATE(fc_datein ,'%m/%d/%Y %H:%i'),'%Y-%m-%d %H:%i') fd_date,
'O' fc_status 
from t_pull_attendance) a
order by  a.fc_membercode,a.fd_date desc ,a.fc_status asc

insert into t_attendance_date
select "001" fc_branch,a.fc_membercode ,
DATE_FORMAT( STR_TO_DATE(a.fd_date,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d %H:%i:%s") fd_date,a.fc_status 
from
(select 
concat("TSI ",fc_membercode)fc_membercode ,
fc_datein  fd_date,
"I" fc_status 
from t_pull_attendance
union all 
select 
concat("TSI ",fc_membercode)fc_membercode ,
fc_dateout fd_date,
"O" fc_status 
from t_pull_attendance) a
order by  a.fc_membercode,a.fd_date desc ,a.fc_status asc

select SYSDATE() )

insert into t_member(fc_membercode )
select distinct fc_membercode 
from t_attendance_date  





create table x_date ( xdate datetime)
select * from x_date;
insert into x_date values( sysdate());


SET @@SESSION.sql_mode="NO_ZERO_DATE,NO_ZERO_IN_DATE";
insert into x_date 
select 
DATE_FORMAT( STR_TO_DATE(fc_datein,'%m/%d/%Y %H:%i'),'%Y-%m-%d %H:%i:%s')
from t_pull_attendance;



select a.fc_membercode,a.fc_timerangecode ,a.fc_workin,
case 
	when a.fc_timerangecode = '-' then
		str_to_date(a.fc_workout,"%Y-%m-%d %H:%i")
	when str_to_date(a.fc_workin,"%Y-%m-%d %H:%i") >= str_to_date(a.fc_workout,"%Y-%m-%d %H:%i") then
		date_add(str_to_date(a.fc_workout,"%Y-%m-%d %H:%i"),interval 1 day)
	else a.fc_workout 
end fc_workout
from t_attendance a

select  "001" fc_branch ,  date_format(:datestart,"%Y%m") fc_periode,  
b.fc_membercode as fc_membercode ,  sysdate() as fd_dategenerate ,  
case  when :fc_date = "" then "-"  
else :fc_date end as fc_timerangecode ,  
CONCAT(date_add(:datestart, INTERVAL :i day)," ",c.fc_workin )  fd_workin ,  
case  when length( :fc_date ) = 2 then  CONCAT(date_add(:datestart, INTERVAL :i day)," ",d.fc_workout )  
	when length( :fc_date ) = 3 then  
		case  when e.fc_workout > e.fc_workin then CONCAT(date_add(:datestart, INTERVAL :i day)," ",d.fc_workout)  
		else CONCAT(date_add(:datestart, INTERVAL (:i + 1)day)," ",e.fc_workout)  
end  else  CONCAT(date_add(:datestart, INTERVAL :i day)," ",c.fc_workout)  end 
fd_workout , 
COALESCE(f.fd_date,'0000-00-00 00:00:00') fd_realdatein,  COALESCE(g.fd_date,'0000-00-00 00:00:00') fd_realdateout  
from t_tempyudharotateshift a  
left join t_member b on a.fc_nameis = b.fc_membername1  
left join t_dailytimerange c on if(:fc_date = "","-",left(:fc_date,1)) = c.fc_timerangecode  
left join t_dailytimerange d on mid(:fc_date,2,1) = d.fc_timerangecode  
left join t_dailytimerange e on if(length(:fc_date) = 3, right(:fc_date,1),"" ) = e.fc_timerangecode  
left join t_attendance_date f 
on f.fc_membercode  = b.fc_membercode and DATE_FORMAT( concat(date_add(:datestart, INTERVAL :i day)," ",c.fc_workin) ,"%Y-%m-%d") =  DATE_FORMAT(f.fd_date,"%Y-%m-%d") and f.fc_status = "I"  
left join t_attendance_date g on g.fc_membercode  = b.fc_membercode 
and DATE_FORMAT( case  when length( :fc_date ) = 2 then  CONCAT(date_add(:datestart, INTERVAL :i day)," ",d.fc_workout )  
when length( :fc_date ) = 3 then  case  when e.fc_workout > e.fc_workin then CONCAT(date_add(:datestart, INTERVAL :i day)," ",d.fc_workout)  
else CONCAT(date_add(":datestart", INTERVAL (0 + 1)day)," ",e.fc_workout)  end  else  CONCAT(date_add(:datestart, INTERVAL :i day)," ",c.fc_workout)  end ,"%Y-%m-%d") =  DATE_FORMAT(g.fd_date,"%Y-%m-%d") 
and g.fc_status = "O"  where a.fc_blank != "O" ;


insert into 
t_attendance(fc_branch ,fc_periode ,fc_membercode ,fc_timerangecode,fc_workin ,fc_workout,fn_totalhour ,fn_totalbreak ,fn_effectivehour ,fc_daytype ,fd_realdatein ,fd_realdateout )
select 
'001' fc_branch,
date_format(:datestart,"%Y%m") fc_periode,
b.fc_membercode ,
if(:fc_date = "","O",:fc_date) fc_timerangecode,
@fc_workin := CONCAT(date_add(:datestart, INTERVAL :i day)," ",c.fc_workin )  fc_workin ,
@fc_workout := case when e.fc_workout = 'O' then 
	CONCAT(date_add(:datestart, INTERVAL :i day)," ",e.fc_workout )  
	when CONCAT(date_add(:datestart, INTERVAL :i day)," ",c.fc_workin ) >= CONCAT(date_add(:datestart, INTERVAL :i day)," ",e.fc_workout ) then
	CONCAT(date_add(:datestart, INTERVAL (:i + 1) day)," ",e.fc_workout )
	else CONCAT(date_add(:datestart, INTERVAL :i day)," ",e.fc_workout ) 
end fc_workout ,
@total_hour := timestampdiff(MINUTE ,@fc_workin,@fc_workout)/60 fn_totalhour,
@total_break :=cast( case
	when length(:fc_date) = 1 then
		@break1 := subtime(c.fc_workbreakout,c.fc_workbreakin )
	when length(:fc_date) = 2 then
		@break2 := addtime( @break1 ,subtime(d.fc_workbreakout,d.fc_workbreakin ) )
	when length(:fc_date) = 3 then
		@break3 := addtime( @break2 ,subtime(e.fc_workbreakout,e.fc_workbreakin ) )
	end as unsigned )fn_totalbreak,
@total_hour - @total_break fn_effectivehour,
case 
when :fc_date = '' then 'O'
when f.fc_status = 'P' then 'H+'
when f.fc_status = 'U' then 'H'
else 'N' end fc_daytype,
g.fd_date fd_realdatein ,
h.fd_date fd_realdateout
from t_tempyudharotateshift a
left join t_member b on a.fc_nameis = b.fc_membername1 
left join t_dailytimerange c on c.fc_timerangecode = if(:fc_date = "","O",left(:fc_date,1))
left join t_dailytimerange d on d.fc_timerangecode = if(:fc_date = "","O",mid(:fc_date,2,1))
left join t_dailytimerange e on e.fc_timerangecode = if(:fc_date = "","O",right(:fc_date,1)) 
left join t_holiday f on date_format(@fc_workin,'%Y-%m-%d') = f.fd_date 
left join t_attendance_date g on g.fc_membercode = b.fc_membercode 
	and date_format(
	CONCAT(date_add(:datestart, INTERVAL :i day)," ",c.fc_workin ) 
	,'%Y-%m-%d') = date_format(g.fd_date ,'%Y-%m-%d') 
	and g.fc_status = 'I'
left join t_attendance_date h on h.fc_membercode = b.fc_membercode 
	and date_format(
	case when e.fc_workout = 'O' then 
		CONCAT(date_add(:datestart, INTERVAL :i day)," ",e.fc_workout )  
		when CONCAT(date_add(:datestart, INTERVAL :i day)," ",c.fc_workin ) >= CONCAT(date_add(:datestart, INTERVAL :i day)," ",e.fc_workout ) then
		CONCAT(date_add(:datestart, INTERVAL (:i + 1) day)," ",e.fc_workout )
		else CONCAT(date_add(:datestart, INTERVAL :i day)," ",e.fc_workout ) 
	end	
	,'%Y-%m-%d') = date_format(h.fd_date ,'%Y-%m-%d') 
	and h.fc_status = 'O'
where a.fc_blank <> 'O' 



insert into 
t_attendance(fc_branch,fd_dategenerate ,fc_periode ,fc_membercode ,fc_timerangecode,fc_workin,fc_workoutshift1 ,fn_totalhourshift1 ,fc_workout,fn_totalhour,fn_totalbreak,fn_effectivehour ,fc_daytype ,fd_realdatein ,fd_realdateout )
select 
'001' fc_branch,
SYSDATE() fd_dategenerate, 
date_format(:datestart,"%Y%m") fc_periode,
b.fc_membercode ,
if(:fc_date = "","O",:fc_date) fc_timerangecode,
@fc_workin := CONCAT(date_add(:datestart, INTERVAL :i day)," ",c.fc_workin )  fc_workin ,
@fc_workoutshift1 := case when e.fc_timerangecode = 'O'   then 
	CONCAT(date_add(:datestart, INTERVAL :i day)," ",c.fc_workout ) 
	when CONCAT(date_add(:datestart, INTERVAL :i day)," ",c.fc_workin ) >= CONCAT(date_add(:datestart, INTERVAL :i day)," ",c.fc_workout ) then
	CONCAT(date_add(:datestart, INTERVAL (:i + 1) day)," ",c.fc_workout )
	else CONCAT(date_add(:datestart, INTERVAL :i day)," ",c.fc_workout ) 
end fc_workoutshift1,
@total_hourshift1 := timestampdiff(MINUTE ,@fc_workin,@fc_workoutshift1)/60 fn_totalhourshift1,	
@fc_workout := case when e.fc_timerangecode = 'O'   then 
	CONCAT(date_add(:datestart, INTERVAL :i day)," ",e.fc_workout ) 
	when CONCAT(date_add(:datestart, INTERVAL :i day)," ",c.fc_workin ) >= CONCAT(date_add(:datestart, INTERVAL :i day)," ",e.fc_workout ) then
	CONCAT(date_add(:datestart, INTERVAL (:i + 1) day)," ",e.fc_workout )
	else CONCAT(date_add(:datestart, INTERVAL :i day)," ",e.fc_workout ) 
end fc_workout ,
@total_hour := timestampdiff(MINUTE ,@fc_workin,@fc_workout)/60 fn_totalhour,
@total_break := coalesce (cast( case
	when length(:fc_date) = 1 then
		@break1 := subtime(c.fc_workbreakout,c.fc_workbreakin)
	when length(:fc_date) = 2 then
		@break2 := addtime( @break1 ,subtime(d.fc_workbreakout,d.fc_workbreakin ) )
	when length(:fc_date) = 3 then
		@break3 := addtime( @break2 ,subtime(e.fc_workbreakout,e.fc_workbreakin ) )
	end as decimal ),0 ) fn_totalbreak,
coalesce(@total_hour - @total_break,0) fn_effectivehour,
case 
when :fc_date = '' then 'O'
when f.fc_status = 'P' then 'H+'
when f.fc_status = 'U' then 'H'
else 'N' end fc_daytype,
g.fd_date fd_realdatein ,
h.fd_date fd_realdateout
from t_tempyudharotateshift a
left join t_member b on a.fc_nameis = b.fc_membername1 
left join t_dailytimerange c on c.fc_timerangecode = if(:fc_date = "","O",left(:fc_date,1))
left join t_dailytimerange d on d.fc_timerangecode = if(:fc_date = "","O",mid(:fc_date,2,1))
left join t_dailytimerange e on e.fc_timerangecode = if(:fc_date = "","O",right(:fc_date,1)) 
left join t_holiday f on date_format(CONCAT(date_add(:datestart, INTERVAL :i day)," ",c.fc_workin ),'%Y-%m-%d') = f.fd_date 
left join t_attendance_date g on g.fc_membercode = b.fc_membercode 
	and date_format(
	CONCAT(date_add(:datestart, INTERVAL :i day)," ",c.fc_workin ) 
	,'%Y-%m-%d') = date_format(g.fd_date ,'%Y-%m-%d') 
	and g.fc_status = 'I'
left join t_attendance_date h on h.fc_membercode = b.fc_membercode 
	and date_format(
	case when e.fc_timerangecode = 'O' then 
		CONCAT(date_add(:datestart, INTERVAL :i day)," ",e.fc_workout )  
		when CONCAT(date_add(:datestart, INTERVAL :i day)," ",c.fc_workin ) >= CONCAT(date_add(:datestart, INTERVAL :i day)," ",e.fc_workout ) then
		CONCAT(date_add(:datestart, INTERVAL (:i + 1) day)," ",e.fc_workout )
		else CONCAT(date_add(:datestart, INTERVAL :i day)," ",e.fc_workout ) 
	end	
	,'%Y-%m-%d') = date_format(h.fd_date ,'%Y-%m-%d') 
	and h.fc_status = 'O'
where a.fc_blank <> 'O' 


select 
b.fc_membername1
,a.fd_dategenerate ,a.fc_periode ,a.fc_membercode ,a.fc_timerangecode,a.fc_workin ,a.fc_workout,a.fn_totalhour,a.fn_totalbreak,a.fn_effectivehour ,a.fc_daytype ,a.fd_realdatein ,a.fd_realdateout
from t_attendance a left join t_member b on a.fc_membercode = b.fc_membercode 
#####################################################################################################################################


######################################################################################################################################
update t_attendance  
set fn_totalhour = timestampdiff(SECOND ,fd_realdatein ,fd_realdateout )/60 / 60


select 
fd_dategenerate ,fc_periode ,fc_membercode ,fc_timerangecode,
fc_workin ,fc_workout,fn_totalhour,fn_totalbreak,
fn_effectivehour ,fc_daytype ,fd_realdatein ,fd_realdateout,
fn_ot1,fn_ot2,fn_ot3,fn_ot4,fn_ot5,fn_ot6,fc_ti ,fc_timerangedescription 
from t_attendance


select
fc_membercode ,
fc_timerangecode ,
fc_daytype ,
fc_workin ,
fd_realdatein ,
fd_realdateout ,
fc_workout
from t_attendance  ORDER BY fc_workin ,fc_membercode ASC;


select "001" fc_branch,a.fc_membercode , 
DATE_FORMAT( STR_TO_DATE(a.fd_date,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d %H:%i:%s") fd_date,a.fc_status 
from 
(select 
concat("TSI ",fc_membercode)fc_membercode , 
fc_datein  fd_date, 
"I" fc_status 
from  t_tempodavidattendancedate tt 
union all 
select 
concat("TSI ",fc_membercode)fc_membercode , 
fc_dateout fd_date, 
"O" fc_status 
from t_tempodavidattendancedate tt2 ) a 
order by  a.fc_membercode,a.fd_date desc ,a.fc_status asc 




update t_attendance
set 
fn_ot1 = case when fc_daytype = 'N'



select
fc_timerangecode ,
fc_daytype ,
@workin := 
	case when coalesce(fd_realdatein,'0000-00-00 00:00:00') <= fc_workin then
		fc_workin 
	when fd_realdatein >= fc_workin then
		fd_realdatein
	end workin,
@workout := 
	case 
	when coalesce(fd_realdateout,fc_workout ) <= fc_workout then
		coalesce(fd_realdateout,fc_workout ) 
	when fd_realdateout >= fc_workout then
		fc_workout
	end workout,
fn_totalhour, 
@total_ot := timestampdiff(MINUTE ,fc_workoutshift1,@workout )/60   total_ot_nr,
@total_ot_r := if (@total_ot > 0, 
  	@total_ot := fn_totalhourshift1 - 1	
,0) total_ot_r,
@ot1 := case 
when fc_daytype = 'N' and @total_ot > 1 then 1 
else 0 end ot1,
@ot2 := case 
when fc_daytype = 'N' and @total_ot > 1 then @total_ot_r - @ot1
when fc_daytype = 'H' or fc_daytype = 'O' and @total_ot > 1 then @total_ot_r
else 0 end ot2
from t_attendance ;

select 
'001' fc_branch,
SYSDATE() fd_dategenerate, 
date_format(:datestart,"%Y%m") fc_periode,
b.fc_membercode ,
if(:fc_date = "","O",:fc_date) fc_timerangecode,
@fc_workin := CONCAT(date_add(:datestart, INTERVAL :i day)," ",c.fc_workin )  fc_workin ,
@fc_workoutshift1 := case when e.fc_timerangecode = 'O'   then 
	CONCAT(date_add(:datestart, INTERVAL :i day)," ",c.fc_workout ) 
	when CONCAT(date_add(:datestart, INTERVAL :i day)," ",c.fc_workin ) >= CONCAT(date_add(:datestart, INTERVAL :i day)," ",c.fc_workout ) then
	CONCAT(date_add(:datestart, INTERVAL (:i + 1) day)," ",c.fc_workout )
	else CONCAT(date_add(:datestart, INTERVAL :i day)," ",c.fc_workout ) 
end fc_workoutshift1,
@total_hourshift1 := timestampdiff(MINUTE ,@fc_workin,@fc_workoutshift1)/60 fn_totalhourshift1,	
@fc_workout := case when e.fc_timerangecode = 'O'   then 
	CONCAT(date_add(:datestart, INTERVAL :i day)," ",e.fc_workout ) 
	when CONCAT(date_add(:datestart, INTERVAL :i day)," ",c.fc_workin ) >= CONCAT(date_add(:datestart, INTERVAL :i day)," ",e.fc_workout ) then
	CONCAT(date_add(:datestart, INTERVAL (:i + 1) day)," ",e.fc_workout )
	else CONCAT(date_add(:datestart, INTERVAL :i day)," ",e.fc_workout ) 
end fc_workout ,
@total_hour := timestampdiff(MINUTE ,@fc_workin,@fc_workout)/60 fn_totalhour,
@total_break := coalesce (cast( case
	when length(:fc_date) = 1 then
		@break1 := subtime(c.fc_workbreakout,c.fc_workbreakin)
	when length(:fc_date) = 2 then
		@break2 := addtime( @break1 ,subtime(d.fc_workbreakout,d.fc_workbreakin ) )
	when length(:fc_date) = 3 then
		@break3 := addtime( @break2 ,subtime(e.fc_workbreakout,e.fc_workbreakin ) )
	end as decimal ),0 ) fn_totalbreak,
coalesce(@total_hour - @total_break,0) fn_effectivehour,
case 
when :fc_date = '' then 'O'
when f.fc_status = 'P' then 'H+'
when f.fc_status = 'U' then 'H'
else 'N' end fc_daytype
from t_tempyudharotateshift a
left join t_member b on a.fc_nameis = b.fc_membername1 
left join t_dailytimerange c on c.fc_timerangecode = if(:fc_date = "","O",left(:fc_date,1))
left join t_dailytimerange d on d.fc_timerangecode = if(:fc_date = "","O",mid(:fc_date,2,1))
left join t_dailytimerange e on e.fc_timerangecode = if(:fc_date = "","O",right(:fc_date,1)) 
left join t_holiday f on date_format(CONCAT(date_add(:datestart, INTERVAL :i day)," ",c.fc_workin ),'%Y-%m-%d') = f.fd_date 
where a.fc_blank <> 'O' 

update t_attendance 
set fn_ot1 = 
case when fc_daytype = 'N' AND timestampdiff(MINUTE ,fc_workoutshift1,fc_workout )/60 > 0 then
	1
else 0 end
,fn_ot2 = 
case when fc_daytype = 'N' AND timestampdiff(MINUTE ,fc_workoutshift1,fc_workout )/60 > 0 then
	fn_effectivehour - (fn_totalhourshift1 - 1 ) - fn_ot1 
	when fc_daytype = 'O' OR fc_daytype = 'H' then 
	(timestampdiff(MINUTE ,fc_workin ,fc_workoutshift1 )/60) - 1 
else 0 end
,fn_ot3 = case when fc_daytype = 'O' OR fc_daytype = 'H' then
	1
else 0 end
,fn_ot4 = 
case when fc_daytype = 'O' OR fc_daytype = 'H' then
	fn_totalhour - fn_ot3 - 1
	when fc_daytype = 'H+' then
	(timestampdiff(MINUTE ,fc_workin ,fc_workout )/60) - 1
else 0 end
,fn_ot5 = case when fc_daytype = 'H+' then
	1
else 0 end
,fn_ot6 = 
case when fc_daytype = 'H+' then
	(timestampdiff(MINUTE ,fc_workin ,fc_workoutshift1 )/60) - 1
else 0 end



select 
fc_membercode ,fc_timerangecode ,fc_daytype ,
fc_workin ,fc_workout ,
fn_ot1 ,fn_ot2 ,fn_ot3 ,fn_ot4 ,fn_ot5 ,fn_ot6 
from t_attendance ta 


select 
( TIME_TO_SEC('23:00') - TIME_TO_SEC('07:00') )/ 60 /60 

insert into x_attendance(fc_branch ,fd_dategenerate,fc_periode ,fd_date, fc_membercode ,fc_shift1 ,fc_shift2 ,fc_shift3,fc_daytype )
select 
'001' fc_branch,
SYSDATE() fd_dategenerate, 
date_format(:datestart,"%Y%m") fc_periode,
date_add(:datestart, INTERVAL :i day) fd_date,
b.fc_membercode ,
left(:fc_date,1) fc_shift1 ,
case 
	when length(:fc_date) > 1  then mid(:fc_date,2,1) 
	else ''
end fc_shift2,
case 
	when length(:fc_date) > 2  then right(:fc_date,1) 
	else ''
end fc_shift3,case 
when :fc_date = '' then 'O'
when c.fc_status = 'P' then 'H+'
when c.fc_status = 'U' then 'H'
else 'N' end fc_daytype
from t_tempdavidrotateshift a 
left join t_member b on a.fc_nameis = b.fc_membername1
left join t_holiday c on date_add(:datestart, INTERVAL :i day) = c.fd_date 
where a.fc_blank <> 'O' 

update x_attendance a
left join t_dailytimerange b on b.fc_timerangecode = a.fc_shift1 
left join t_dailytimerange c on c.fc_timerangecode = a.fc_shift2
left join t_dailytimerange d on d.fc_timerangecode = a.fc_shift3 
set 
	a.fc_workinshift1 = COALESCE(b.fc_workin,"00:00"),
	a.fc_workoutshift1 = COALESCE(b.fc_workout,"00:00") ,
	a.fn_totalbreakshift1 =COALESCE(TIME_TO_SEC( 
			if( b.fc_workbreakin > b.fc_workbreakout ,
				subtime(b.fc_workbreakin,b.fc_workbreakout ) ,
				subtime(b.fc_workbreakout,b.fc_workbreakin )
			) )/60/60,0), 
	a.fc_workinshift2 = COALESCE(c.fc_workin,"00:00"),
	a.fc_workoutshift2 = COALESCE(c.fc_workout,"00:00") ,
	a.fn_totalbreakshift2 = COALESCE(TIME_TO_SEC( 
			if( b.fc_workbreakin > b.fc_workbreakout ,
				subtime(c.fc_workbreakin,c.fc_workbreakout ) ,
				subtime(c.fc_workbreakout,c.fc_workbreakin )
			) )/60/60,0) ,
	a.fc_workinshift3 = COALESCE(d.fc_workin,"00:00"),
	a.fc_workoutshift3 = COALESCE(d.fc_workout,"00:00") ,
	a.fn_totalbreakshift3 = COALESCE (TIME_TO_SEC( 
			if( d.fc_workbreakin > d.fc_workbreakout ,
				subtime(d.fc_workbreakin,d.fc_workbreakout ) ,
				subtime(d.fc_workbreakout,d.fc_workbreakin )
			) 
		)/60/60,0 )
where a.fc_periode = date_format(:datestart,"%Y%m")

update x_attendance 
set 
	fc_timeinshift1 = COALESCE(fc_workinshift1,"00:00") ,
	fc_timeoutshift1 = COALESCE(fc_workoutshift1,"00:00"),
	fc_timeinshift2 = coalesce(fc_workinshift2,"00:00") ,
	fc_timeoutshift2 = COALESCE(fc_workoutshift2,"00:00"),
	fc_timeinshift3 = coalesce(fc_workinshift3,"00:00") ,
	fc_timeoutshift3 = coalesce(fc_workoutshift3,"00:00")
where fc_periode = date_format(:datestart,"%Y%m");

update x_attendance 
set 
	fn_totalhourshift1 = TIME_TO_SEC( 
			if( fc_workinshift1 > fc_workoutshift1 ,
				subtime(addtime(fc_workoutshift1 ,"24:00"),fc_workinshift1 ) ,
				subtime(fc_workoutshift1,fc_workinshift1 )
			) )/60/60,
	fn_totalhourshift2 = TIME_TO_SEC( 
			if( fc_workinshift2 > fc_workoutshift2 ,
				subtime(addtime(fc_workoutshift2 ,"24:00"),fc_workinshift2 ) ,
				subtime(fc_workoutshift2,fc_workinshift2 )
			) )/60/60,
	fn_totalhourshift3 = TIME_TO_SEC( 
			if( fc_workinshift3 > fc_workoutshift3 ,
				subtime(addtime(fc_workoutshift3 ,"24:00"),fc_workinshift3 ) ,
				subtime(fc_workoutshift3,fc_workinshift3 )
			) )/60/60
where fc_periode = date_format(:datestart,"%Y%m");

update x_attendance 
set 
	fn_totaltimeshift1 = TIME_TO_SEC( 
			if( fc_timeinshift1 > fc_timeoutshift1 ,
				subtime(addtime(fc_timeoutshift1 ,"24:00"),fc_timeinshift1 ) ,
				subtime(fc_timeoutshift1,fc_timeinshift1 )
			) )/60/60,
	fn_totaltimeshift2  = TIME_TO_SEC( 
			if( fc_timeinshift2 > fc_timeoutshift2 ,
				subtime(addtime(fc_timeoutshift2 ,"24:00"),fc_timeinshift2 ) ,
				subtime(fc_timeoutshift2,fc_timeinshift2 )
			) )/60/60,
	fn_totaltimeshift3  = TIME_TO_SEC( 
			if( fc_timeinshift3 > fc_timeoutshift3 ,
				subtime(addtime(fc_timeoutshift3 ,"24:00"),fc_timeinshift3 ) ,
				subtime(fc_timeoutshift3,fc_timeinshift3 )
			) )/60/60
where fc_periode = date_format(:datestart,"%Y%m");

##OT1
update x_attendance 
set fn_ot1 =
case 
	when fc_daytype = 'N' and (fn_totaltimeshift2 + fn_totaltimeshift3 + (fn_totaltimeshift1 - fn_totalhourshift1  ) ) != 0
	then
		1 
	else 0
end
where fc_periode = date_format(:datestart,"%Y%m");
#OT2
update x_attendance 
set fn_ot2 = 
case 
	when fc_daytype = 'N'  
	then (fn_totaltimeshift2 + fn_totaltimeshift3) - (fn_totalbreakshift2 + fn_totalbreakshift3 ) + (fn_totaltimeshift1 - fn_totalhourshift1 ) - fn_ot1 
	when fc_daytype = 'H' OR fc_daytype = 'O'
	then fn_totaltimeshift1 - fn_totalbreakshift1 
	else 0
end
where fc_periode = date_format(:datestart,"%Y%m");
#OT3
update x_attendance 
set fn_ot3 = 
case 
	when fc_daytype = 'H' and (fn_totaltimeshift2 + fn_totaltimeshift3) != 0  or fc_daytype = 'O' and (fn_totaltimeshift2 + fn_totaltimeshift3) != 0
	then 1 
	else 0
end 
where fc_periode = date_format(:datestart,"%Y%m");
#OT4
update x_attendance 
set fn_ot4 = 
case 
	when fc_daytype = 'H' or fc_daytype = 'O'
	then (fn_totaltimeshift2 + fn_totaltimeshift3) - (fn_totalbreakshift2 + fn_totalbreakshift3 ) - fn_ot3 
	when fc_daytype = 'H+'
	then fn_totalhourshift1 - fn_totalbreakshift1 
	else 0
end
where fc_periode = date_format(:datestart,"%Y%m");
#OT5
update x_attendance 
set fn_ot5 = 
case 
	when fc_daytype = 'H+' and (fn_totaltimeshift2 + fn_totaltimeshift3) != 0  
	then 1 
	else 0
end
where fc_periode = date_format(:datestart,"%Y%m");
#OT6
update x_attendance 
set fn_ot6 = 
case when fc_daytype = 'H+'
	then (fn_totaltimeshift2 + fn_totaltimeshift3) - (fn_totalbreakshift2 + fn_totalbreakshift3 ) - fn_ot5 
	else 0
end 
where fc_periode = date_format(:datestart,"%Y%m");

select
fc_periode, fd_dategenerate ,
fc_membercode,fd_date,fc_daytype, 
fc_shift1 ,fc_shift2,fc_shift3,
fc_timeinshift1,fc_timeoutshift1,
fc_timeinshift2,fc_timeoutshift2,
fc_timeinshift3,fc_timeoutshift3,
( fn_totaltimeshift1 + fn_totaltimeshift2 + fn_totaltimeshift3 ) total_hour,
( fn_totaltimeshift1 + fn_totaltimeshift2 + fn_totaltimeshift3 ) - (fn_totalbreakshift1 + fn_totalbreakshift2 + fn_totalbreakshift3  ) effective_hour,
fn_ot1 '1,5',
fn_ot2 '2',
fn_ot3 '3',
fn_ot4 '4',
fn_ot5 '6',
fn_ot6 '8',
length(concat(fc_shift1,fc_shift2,fc_shift3)) jmlh_shift,
fv_description keterangan
from x_attendance order by fd_date 


select 
a.fd_date, a.fc_membercode ,
concat(a.fc_shift1 ,a.fc_shift2 ,a.fc_shift3 ) shift ,
b.fc_datein ,b.fc_dateout ,b.fc_membercode 
from x_attendance a
left join t_tempodavidattendancedate b 
on a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d")
and a.fc_membercode = concat('TSI ',b.fc_membercode)


update x_attendance a 
left join t_tempodavidattendancedate b 
on a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d")
and a.fc_membercode = concat('TSI ',b.fc_membercode)
set 
a.fd_realdatein = STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),
a.fd_realdateout = STR_TO_DATE(b.fc_dateout ,"%m/%d/%Y %H:%i:%s")
where a.fc_periode = '201912' 

UPDATE x_attendance a
left join t_dailytimerange b on left(concat(fc_shift1 ,fc_shift2,fc_shift3),1) = b.fc_timerangecode 
left join t_dailytimerange c on right(concat(fc_shift1 ,fc_shift2,fc_shift3),1) = c.fc_timerangecode 
set
a.fc_datein = concat(DATE_FORMAT(a.fd_realdatein ,'%Y-%m-%d') ,' ',coalesce(b.fc_workin,'00:00')) ,
a.fc_dateout = concat(DATE_FORMAT(a.fd_realdateout ,'%Y-%m-%d') ,' ',coalesce(c.fc_workout ,'00:00'))
where a.fc_periode = '201912'

select
fc_membercode ,fd_date ,fc_daytype, concat(fc_shift1,fc_shift2,fc_shift3 ) shift,
fn_ot1 '1,5',fn_ot2 '2',fn_ot3 '3',fn_ot4 '4',fn_ot5 '6',fn_ot6 '8'
from x_attendance 

update x_attendance 
set 
fn_totalhour = time_to_sec(timediff(fc_dateout ,fc_datein ) ) / 60 /60,
fn_effectivehour = fn_totalhour - (fn_totalbreakshift1 + fn_totalbreakshift2 +fn_totalbreakshift3 )


UPDATE x_attendance a
left join t_dailytimerange b on left(concat(fc_shift1 ,fc_shift2,fc_shift3),1) = b.fc_timerangecode 
left join t_dailytimerange c on right(concat(fc_shift1 ,fc_shift2,fc_shift3),1) = c.fc_timerangecode 
set
a.fc_datein = 
case
when 
	 a.fd_realdatein < concat(DATE_FORMAT(a.fd_realdatein ,'%Y-%m-%d') ,' ',coalesce(b.fc_workin,'00:00'))
	then concat(DATE_FORMAT(a.fd_realdatein ,'%Y-%m-%d') ,' ',coalesce(b.fc_workin,'00:00'))
else a.fd_realdatein
end,
a.fc_dateout = 
case 
when time_to_sec(timediff(a.fd_realdateout, concat(DATE_FORMAT(a.fd_realdateout ,'%Y-%m-%d') ,' ',coalesce(c.fc_workout ,'00:00') ) ) )/60/60 > 1.5
	then a.fd_realdateout
when a.fd_realdateout > concat(DATE_FORMAT(a.fd_realdateout ,'%Y-%m-%d') ,' ',coalesce(c.fc_workout ,'00:00'))
 then  concat(DATE_FORMAT(a.fd_realdateout ,'%Y-%m-%d') ,' ',coalesce(c.fc_workout ,'00:00')) 
when a.fd_realdateout < concat(DATE_FORMAT(a.fd_realdateout ,'%Y-%m-%d') ,' ',coalesce(c.fc_workout ,'00:00'))
 then a.fd_realdateout
else a.fd_realdateout 
end
where a.fc_periode = '201912'

update x_attendance 
set 
fn_ot1 =case 
	when fc_daytype = 'N' and time_to_sec( timediff(fc_dateout , fc_datein ) )/60/60 -(fn_totalbreakshift1 + fn_totalbreakshift2 + fn_totalbreakshift3 ) > 7 then
	 1
	else 0
end,
fn_ot2 = case 
	when fc_daytype = 'N' and fn_ot1 >= 1
		then time_to_sec( timediff(fc_dateout,fc_datein) )/60/60 - fn_ot1 - (fn_totalbreakshift1 + fn_totalbreakshift2 + fn_totalbreakshift3 ) - (fn_totalhourshift1 - fn_totalbreakshift1 )
	when fc_daytype = 'H' and time_to_sec( timediff(fc_dateout ,fc_datein ) )/60/60 - (fn_totalbreakshift1 + fn_totalbreakshift2 + fn_totalbreakshift3 )  > 7
		then 7
	else 0
end,
fn_ot3 = case 
	when fc_daytype in ('H','O') and time_to_sec( timediff(fc_dateout , fc_datein ) )/60/60 -(fn_totalbreakshift1 + fn_totalbreakshift2 + fn_totalbreakshift3 )  > 7 
	and fn_ot2 > 0 
	then
		1
	else 0
end,
fn_ot4 = case
	when fc_daytype in ('H','O') and fn_ot3 > 0 then
		time_to_sec( timediff(fc_dateout,fc_datein) )/60/60 - fn_ot3 - (fn_totalbreakshift1 + fn_totalbreakshift2 + fn_totalbreakshift3 ) - (fn_totalhourshift1 - fn_totalbreakshift1 ) 
	when fc_daytype = 'H+'
	then
		(fn_totalhourshift1 - fn_totalbreakshift1 )
	else 0
end,
fn_ot5 = case 
	when fc_daytype = 'H+' and time_to_sec( timediff(fc_dateout , fc_datein ) )/60/60 -(fn_totalbreakshift1 + fn_totalbreakshift2 + fn_totalbreakshift3 )  > 7
	then
		1
	else 0
end,
fn_ot6 = case 
	when fc_daytype = 'H+' and fn_ot4 > 0
	then
		time_to_sec( timediff(fc_dateout,fc_datein) )/60/60 - fn_ot5 - (fn_totalbreakshift1 + fn_totalbreakshift2 + fn_totalbreakshift3 ) - (fn_totalhourshift1 - fn_totalbreakshift1 )
	else 0
end


select  
case 
	when fc_daytype = 'N' and fn_ot1 >= 1
		then time_to_sec( timediff(fc_dateout ,fc_datein ) )/60/60
	else 0
end ot2,
fc_periode ,fd_dategenerate ,
fc_membercode ,fd_date ,fc_daytype ,concat(fc_shift1,fc_shift2,fc_shift3 ) shift,
fd_realdatein ,fd_realdateout,
fn_totalhour ,fn_effectivehour
from x_attendance where fc_daytype in ('N')

select fd_date,fc_daytype, 
concat(fc_shift1,fc_shift2,fc_shift3 ) shift,
fd_realdatein,fc_datein ,fd_realdateout,fc_dateout, 
fn_totalhourshift1+fn_totalhourshift2+fn_totalhourshift3  total_hour,
fn_ot1,fn_ot2 ,fn_ot3,fn_ot4,fn_ot5,fn_ot6 
from x_attendance




