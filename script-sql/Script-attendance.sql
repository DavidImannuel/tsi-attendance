alter table t_attendance 
add primary key (fd_date,fc_membercode,fc_periode)

insert into t_attendance(fc_branch ,fd_dategenerate,fc_periode ,fd_date, fc_membercode,fc_memberattendancetype ,fc_shift1 ,fc_shift2 ,fc_shift3,fc_daytype )
select 
'001' fc_branch,
SYSDATE() fd_dategenerate, 
date_format(:datestart,"%Y%m") fc_periode,
date_add(:datestart, INTERVAL :i day) fd_date,
b.fc_membercode ,
b.fc_memberattendancetype, 
left(:fc_date,1) fc_shift1 ,
case 
	when length(:fc_date) > 1  then mid(:fc_date,2,1) 
	else ''
end fc_shift2,
case 
	when length(:fc_date) > 2  then right(:fc_date,1) 
	else ''
end fc_shift3,case 
when :fc_date = '' then 'O'
when c.fc_status = 'P' then 'H+'
when c.fc_status = 'U' then 'H'
when dayofweek( date_add(:datestart, INTERVAL :i day) ) = 7 then 'S'
else 'N' end fc_daytype
from t_tempdavidrotateshift a 
left join t_member b on a.fc_nameis = b.fc_membername1
left join t_holiday c on date_add(:datestart, INTERVAL :i day) = c.fd_date 
where a.fc_blank <> 'O'
on duplicate key update 
fc_shift1 = left(:fc_date,1),
fc_shift2 = 
case 
	when length(:fc_date) > 1  then mid(:fc_date,2,1) 
	else ''
end,
fc_shift3 = 
case 
	when length(:fc_date) > 2  then right(:fc_date,1) 
	else ''
end

select * from t_member where fc_membername1 =  rtrim(ltrim('ZAINUL AMIN'))

update t_member 
set fc_membername1 = cast(fc_membername1 as string)

update x_attendance a
left join t_dailytimerange b on b.fc_timerangecode = a.fc_shift1 
left join t_dailytimerange c on c.fc_timerangecode = a.fc_shift2
left join t_dailytimerange d on d.fc_timerangecode = a.fc_shift3 
set 
	a.fc_workinshift1 = COALESCE(b.fc_workin,"00:00"),
	a.fc_workoutshift1 = COALESCE(b.fc_workout,"00:00") ,
	a.fn_totalbreakshift1 =COALESCE(TIME_TO_SEC( 
			if( b.fc_workbreakin > b.fc_workbreakout ,
				subtime(b.fc_workbreakin,b.fc_workbreakout ) ,
				subtime(b.fc_workbreakout,b.fc_workbreakin )
			) )/60/60,0), 
	a.fc_workinshift2 = COALESCE(c.fc_workin,"00:00"),
	a.fc_workoutshift2 = COALESCE(c.fc_workout,"00:00") ,
	a.fn_totalbreakshift2 = COALESCE(TIME_TO_SEC( 
			if( b.fc_workbreakin > b.fc_workbreakout ,
				subtime(c.fc_workbreakin,c.fc_workbreakout ) ,
				subtime(c.fc_workbreakout,c.fc_workbreakin )
			) )/60/60,0) ,
	a.fc_workinshift3 = COALESCE(d.fc_workin,"00:00"),
	a.fc_workoutshift3 = COALESCE(d.fc_workout,"00:00") ,
	a.fn_totalbreakshift3 = COALESCE (TIME_TO_SEC( 
			if( d.fc_workbreakin > d.fc_workbreakout ,
				subtime(d.fc_workbreakin,d.fc_workbreakout ) ,
				subtime(d.fc_workbreakout,d.fc_workbreakin )
			) 
		)/60/60,0 )
where a.fc_periode = date_format(:datestart,"%Y%m")


update x_attendance 
set 
	fn_totalhourshift1 = TIME_TO_SEC( 
			if( fc_workinshift1 > fc_workoutshift1 ,
				subtime(addtime(fc_workoutshift1 ,"24:00"),fc_workinshift1 ) ,
				subtime(fc_workoutshift1,fc_workinshift1 )
			) )/60/60,
	fn_totalhourshift2 = TIME_TO_SEC( 
			if( fc_workinshift2 > fc_workoutshift2 ,
				subtime(addtime(fc_workoutshift2 ,"24:00"),fc_workinshift2 ) ,
				subtime(fc_workoutshift2,fc_workinshift2 )
			) )/60/60,
	fn_totalhourshift3 = TIME_TO_SEC( 
			if( fc_workinshift3 > fc_workoutshift3 ,
				subtime(addtime(fc_workoutshift3 ,"24:00"),fc_workinshift3 ) ,
				subtime(fc_workoutshift3,fc_workinshift3 )
			) )/60/60
where fc_periode = date_format(:datestart,"%Y%m");


update x_attendance a 
left join t_tempodavidattendancedate b 
on a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d")
and a.fc_membercode = concat('TSI ',b.fc_membercode)
set 
a.fd_realdatein = STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),
a.fd_realdateout = STR_TO_DATE(b.fc_dateout ,"%m/%d/%Y %H:%i:%s")
-- where a.fc_periode = '201912'
where a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d")
and a.fc_membercode = concat('TSI ',b.fc_membercode)

UPDATE x_attendance a
left join t_dailytimerange b on left(concat(fc_shift1 ,fc_shift2,fc_shift3),1) = b.fc_timerangecode 
left join t_dailytimerange c on right(concat(fc_shift1 ,fc_shift2,fc_shift3),1) = c.fc_timerangecode 
set
a.fc_datein = concat(DATE_FORMAT(a.fd_realdatein ,'%Y-%m-%d') ,' ',coalesce(b.fc_workin,'00:00')) ,
a.fc_dateout = concat(DATE_FORMAT(a.fd_realdateout ,'%Y-%m-%d') ,' ',coalesce(c.fc_workout ,'00:00'))
where a.fc_periode = '201912'


UPDATE x_attendance a
left join t_dailytimerange b on left(concat(fc_shift1 ,fc_shift2,fc_shift3),1) = b.fc_timerangecode 
left join t_dailytimerange c on right(concat(fc_shift1 ,fc_shift2,fc_shift3),1) = c.fc_timerangecode 
set
a.fc_datein = 
case
when 
	 a.fd_realdatein < concat(DATE_FORMAT(a.fd_realdatein ,'%Y-%m-%d') ,' ',coalesce(b.fc_workin,'00:00'))
	then concat(DATE_FORMAT(a.fd_realdatein ,'%Y-%m-%d') ,' ',coalesce(b.fc_workin,'00:00'))
else a.fd_realdatein
end,
a.fc_dateout = 
case 
when time_to_sec(timediff(a.fd_realdateout, concat(DATE_FORMAT(a.fd_realdateout ,'%Y-%m-%d') ,' ',coalesce(c.fc_workout ,'00:00') ) ) )/60/60 > 1.5
	then a.fd_realdateout
when a.fd_realdateout > concat(DATE_FORMAT(a.fd_realdateout ,'%Y-%m-%d') ,' ',coalesce(c.fc_workout ,'00:00'))
 then  concat(DATE_FORMAT(a.fd_realdateout ,'%Y-%m-%d') ,' ',coalesce(c.fc_workout ,'00:00')) 
when a.fd_realdateout < concat(DATE_FORMAT(a.fd_realdateout ,'%Y-%m-%d') ,' ',coalesce(c.fc_workout ,'00:00'))
 then a.fd_realdateout
else a.fd_realdateout 
end
where a.fc_periode = '201912'

update x_attendance 
set 
fn_totalhour = time_to_sec(timediff(fc_dateout ,fc_datein ) ) / 60 /60,
fn_effectivehour = fn_totalhour - (fn_totalbreakshift1 + fn_totalbreakshift2 +fn_totalbreakshift3 )

update x_attendance 
set
fn_effectivehour = 
case 
	when fc_ti in ('TI1','NR1') then 
		fn_effectivehour + 1
	when fc_ti in ('TI2','NR2') then 
		fn_effectivehour + 2
	when fc_ti in ('TI3','NR3') then 
		fn_effectivehour + 3
	else 
		fn_effectivehour
end 

-- Overtime Formula
update x_attendance
set 
fn_ot1 =
case when fc_daytype = 'N' and fn_effectivehour > (fn_totalhourshift1 - fn_totalbreakshift1 ) then
	case when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) >= 1
	then 1
	else 0
	end
when fc_daytype = 'S' and fn_effectivehour > ( ( fn_totalhourshift1 - 2 ) - fn_totalbreakshift1 ) then
	case when fn_effectivehour - ( ( fn_totalhourshift1 - 2 )- fn_totalbreakshift1 ) >= 1
	then 1
	else 0
	end
else 0
end,
fn_ot2 =
case when fc_daytype = 'N' and fn_ot1 >= 1 then 
	fn_effectivehour - fn_ot1 - (fn_totalhourshift1 - fn_totalbreakshift1 ) 
case when fc_daytype = 'S' and fn_ot1 >= 1 then 
	fn_effectivehour - fn_ot1 - ( ( fn_totalhourshift1 - 2 ) - fn_totalbreakshift1 )
when fc_daytype in ('H','O') and 
		fn_effectivehour - (fn_totalhourshift2 - fn_totalbreakshift2 ) - (fn_totalhourshift3 - fn_totalbreakshift3 ) >= (fn_totalhourshift1 - fn_totalbreakshift1 )
	then (fn_totalhourshift1 - fn_totalbreakshift1 )
when fc_daytype in ('H','O') and 
		fn_effectivehour - (fn_totalhourshift2 - fn_totalbreakshift2 ) - (fn_totalhourshift3 - fn_totalbreakshift3 ) < (fn_totalhourshift1 - fn_totalbreakshift1 )
	then fn_effectivehour - (fn_totalhourshift2 - fn_totalbreakshift2 ) - (fn_totalhourshift3 - fn_totalbreakshift3 )
else 0 end,
fn_ot3 = 
case when fc_daytype in ('H','O') and fn_effectivehour > (fn_totalhourshift1 - fn_totalbreakshift1 ) then
	case when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) >= 1
	then 1
	else 0
	end
else 0
end,
fn_ot4 = 
case when fc_daytype in ('H','O') and fn_ot3 >= 1 then 
	fn_effectivehour -  fn_ot3 - (fn_totalhourshift1 - fn_totalbreakshift1 ) 
when fc_daytype = 'H+' and 
		fn_effectivehour - (fn_totalhourshift2 - fn_totalbreakshift2 ) - (fn_totalhourshift3 - fn_totalbreakshift3 ) >= (fn_totalhourshift1 - fn_totalbreakshift1 )
	then (fn_totalhourshift1 - fn_totalbreakshift1 )
when fc_daytype = 'H+' and 
		fn_effectivehour - (fn_totalhourshift2 - fn_totalbreakshift2 ) - (fn_totalhourshift3 - fn_totalbreakshift3 ) < (fn_totalhourshift1 - fn_totalbreakshift1 )
	then fn_effectivehour - (fn_totalhourshift2 - fn_totalbreakshift2 ) - (fn_totalhourshift3 - fn_totalbreakshift3 )
else 0 end,
fn_ot5 =
case when fc_daytype = 'H+' and fn_effectivehour > (fn_totalhourshift1 - fn_totalbreakshift1 ) then
	case when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) >= 1
	then 1
	else 0
	end
else 0
end,
fn_ot6 =
case when fc_daytype = 'H+' and fn_ot5 >= 1 then 
	fn_effectivehour -  fn_ot5 - (fn_totalhourshift1 - fn_totalbreakshift1 ) 
else 0 end

select 
-- fc_periode ,fd_dategenerate ,
a.fc_membercode,b.fc_membername1 ,
-- b.fc_memberposition, 
fd_date ,fc_daytype ,
concat(fc_shift1,fc_shift2,fc_shift3) shift ,
fd_realdatein ,fd_realdateout,
fn_totalhour ,fn_effectivehour, 
-- fn_ot1 ,fn_ot2 ,fn_ot3 ,fn_ot4 ,fn_ot5 ,fn_ot6,
-- fv_description, 
fc_ti 
from x_attendance a
left join t_member b on a.fc_membercode = b.fc_membercode
left join t_trxtype c on b.fc_memberposition = c.fc_trxcode and c.fc_trxcode = "EMP_POSITION"
order by fd_date 




UPDATE x_attendance a
left join t_dailytimerange b on left(concat(fc_shift1 ,fc_shift2,fc_shift3),1) = b.fc_timerangecode 
left join t_dailytimerange c on right(concat(fc_shift1 ,fc_shift2,fc_shift3),1) = c.fc_timerangecode 
set
a.fc_datein = 
case
when 
	 a.fd_realdatein < concat(DATE_FORMAT(a.fd_realdatein ,'%Y-%m-%d') ,' ',coalesce(b.fc_workin,'00:00'))
	then concat(DATE_FORMAT(a.fd_realdatein ,'%Y-%m-%d') ,' ',coalesce(b.fc_workin,'00:00'))
else a.fd_realdatein
end,
a.fc_dateout = 
case 
when time_to_sec(timediff(a.fd_realdateout, concat(DATE_FORMAT(a.fd_realdateout ,'%Y-%m-%d') ,' ',coalesce(c.fc_workout ,'00:00') ) ) )/60/60 > 1.5
	then a.fd_realdateout
when a.fd_realdateout > concat(DATE_FORMAT(a.fd_realdateout ,'%Y-%m-%d') ,' ',coalesce(c.fc_workout ,'00:00'))
 then  concat(DATE_FORMAT(a.fd_realdateout ,'%Y-%m-%d') ,' ',coalesce(c.fc_workout ,'00:00')) 
when a.fd_realdateout < concat(DATE_FORMAT(a.fd_realdateout ,'%Y-%m-%d') ,' ',coalesce(c.fc_workout ,'00:00'))
 then a.fd_realdateout
else a.fd_realdateout 
end
where a.fc_periode = '201912'

update x_attendance 
set 
fn_totalhour = time_to_sec(timediff(fc_dateout ,fc_datein ) ) / 60 /60,
fn_effectivehour = fn_totalhour - (fn_totalbreakshift1 + fn_totalbreakshift2 +fn_totalbreakshift3 )

update x_attendance 
set 
fn_ot1 = 0,
fn_ot2 = 0,
fn_ot3 = 0,
fn_ot4 = 0,
fn_ot5 = 0;
-- new formula for shift and non shift 
update t_attendance
set
fn_ot1 =
case when (fn_totalhourshift1 - fn_totalbreakshift1 ) = 8  then 
	case when fc_daytype = 'N' then
		case when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) >= 1 then
			1
		when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) >= 0 then 
			fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 )
		else 0 end
	else 0 end 
when (fn_totalhourshift1 - fn_totalbreakshift1 ) = 7 then
	case when fc_daytype = 'N' then
		case when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) >= 1 then
			1
		when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) >= 0 then 
			fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 )
		else 0 end
	when fc_daytype = 'S' then
		case when fn_effectivehour > 5 then
			case when fn_effectivehour - 5 >= 1 then
				1
			when fn_effectivehour - 5 <= 1 then 
				fn_effectivehour - 5
			else 0 end
		else 0 end
	else 0 end
else 0 end,
fn_ot2 =
case when (fn_totalhourshift1 - fn_totalbreakshift1 ) = 8  then 
	case when fc_daytype = 'N' then
		case when fn_ot1 >=1 then
			fn_effectivehour - fn_ot1 - (fn_totalhourshift1 - fn_totalbreakshift1 )
		else 0 end
	when fc_daytype in ('S','H','O') then
		case when fn_effectivehour - 7 >= 0 then
			7
		when fn_effectivehour - 7 <= 0 then 
			fn_effectivehour 
		else 0 end
	else 0 end 
when (fn_totalhourshift1 - fn_totalbreakshift1 ) = 7 then
	case when fc_daytype = 'N' then
		case when fn_ot1 >= 1 then 
			fn_effectivehour - fn_ot1 - (fn_totalhourshift1 - fn_totalbreakshift1 )
		else 0 end
	when fc_daytype = 'S' then
		case when fn_ot1 >= 1 then
			case when fn_effectivehour - 5 >= 0 then
				fn_effectivehour - fn_ot1 - 5
			when fn_effectivehour - 5 <= 0 then
				fn_effectivehour
			else 0 end
		else 0 end
	when fc_daytype in ('H','O') then
		case when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) >= 0 then
			7
		when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) <= 0 then 
			fn_effectivehour 
		else 0 end
	else 0 end 
else 0 end,
fn_ot3 =
case when (fn_totalhourshift1 - fn_totalbreakshift1 ) = 8  then 
	case when fc_daytype in ('S','H','O') then
		case when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) >= 1 then
			1
		when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) <= 1 then 
			fn_effectivehour - 7
		else 0 end
	else 0 end 
when (fn_totalhourshift1 - fn_totalbreakshift1 ) = 7 then
	case when fc_daytype in ('H','O') then
		case when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) >= 1 then
			1
		when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) <= 1 then 
			fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 )
		else 0 end
	else 0 end
else 0 end,
fn_ot4 =
case when (fn_totalhourshift1 - fn_totalbreakshift1 ) = 8  then 
	case when fc_daytype in ('S','H','O') then
		case when fn_ot3 >=1 then
			fn_effectivehour - fn_ot3 - 7
		else 0 end
	when fc_daytype in ('H+') then
		case when fn_effectivehour - 7 >= 0 then
			7
		when fn_effectivehour - 7 <= 0 then 
			fn_effectivehour 
		else 0 end
	else 0 end 
when (fn_totalhourshift1 - fn_totalbreakshift1 ) = 7 then
	case when fc_daytype in ('H','O') then
		case when fn_ot3 >= 1 then 
			fn_effectivehour - fn_ot3 - (fn_totalhourshift1 - fn_totalbreakshift1 )
		else 0 end
	when fc_daytype in ('H+') then
		case when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) >= 0 then
			7
		when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) <= 0 then 
			fn_effectivehour 
		else 0 end
	else 0 end 
else 0 end,
fn_ot5 =
case when (fn_totalhourshift1 - fn_totalbreakshift1 ) = 8  then 
	case when fc_daytype in ('H+') then
		case when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) >= 1 then
			1
		when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) <= 1 then 
			fn_effectivehour - 7
		else 0 end
	else 0 end 
when (fn_totalhourshift1 - fn_totalbreakshift1 ) = 7 then
	case when fc_daytype in ('H+') then
		case when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) >= 1 then
			1
		when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) <= 1 then 
			fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 )
		else 0 end
	else 0 end
else 0 end,
fn_ot6 =
case when (fn_totalhourshift1 - fn_totalbreakshift1 ) = 8  then 
	case when fc_daytype in ('H+') then
		case when fn_ot5 >=1 then
			fn_effectivehour - fn_ot5 - 7
		else 0 end
	else 0 end 
when (fn_totalhourshift1 - fn_totalbreakshift1 ) = 7 then
	case when fc_daytype in ('H+') then
		case when fn_ot5 >= 1 then 
			fn_effectivehour - fn_ot5 - (fn_totalhourshift1 - fn_totalbreakshift1 )
		else 0 end
	else 0 end 
else 0 end

###############################################################################################################################################################
-- Refactoring Here 
###############################################################################################################################################################
-- update real date in date out
update t_attendance a 
left join t_tempodavidattendancedate b 
on a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d")
and a.fc_membercode = concat('TSI ',b.fc_membercode) 
set 
a.fd_realdatein = STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),
a.fd_realdateout = STR_TO_DATE(b.fc_dateout ,"%m/%d/%Y %H:%i:%s")
where a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d")
and a.fc_membercode = concat('TSI ',b.fc_membercode) ; 
-- update datein date out
update t_attendance a 
left join t_tempodavidattendancedate b 
on a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d")
and a.fc_membercode = concat('TSI ',b.fc_membercode)
left join t_dailytimerange c on left(concat(a.fc_shift1 ,a.fc_shift2,a.fc_shift3),1) = c.fc_timerangecode 
left join t_dailytimerange d on right(concat(a.fc_shift1 ,a.fc_shift2,a.fc_shift3),1) = d.fc_timerangecode 
set 
a.fc_datein = 
case
when 
	 a.fd_realdatein < concat(DATE_FORMAT(a.fd_realdatein ,'%Y-%m-%d') ,' ',coalesce(c.fc_workin,'00:00'))
	then concat(DATE_FORMAT(a.fd_realdatein ,'%Y-%m-%d') ,' ',coalesce(c.fc_workin,'00:00'))
else a.fd_realdatein
end,
a.fc_dateout = 
case 
when time_to_sec(timediff(a.fd_realdateout, concat(DATE_FORMAT(a.fd_realdateout ,'%Y-%m-%d') ,' ',coalesce(d.fc_workout ,'00:00') ) ) )/60/60 > 1.5
	then a.fd_realdateout
when a.fd_realdateout > concat(DATE_FORMAT(a.fd_realdateout ,'%Y-%m-%d') ,' ',coalesce(d.fc_workout ,'00:00'))
 then  concat(DATE_FORMAT(a.fd_realdateout ,'%Y-%m-%d') ,' ',coalesce(d.fc_workout ,'00:00')) 
when a.fd_realdateout < concat(DATE_FORMAT(a.fd_realdateout ,'%Y-%m-%d') ,' ',coalesce(d.fc_workout ,'00:00'))
 then a.fd_realdateout
else a.fd_realdateout 
end
where a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d")
and a.fc_membercode = concat('TSI ',b.fc_membercode) ;
-- update total hour
update t_attendance a 
left join t_tempodavidattendancedate b 
on a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d")
and a.fc_membercode = concat('TSI ',b.fc_membercode)
set 
a.fn_totalhour = time_to_sec(timediff(a.fc_dateout ,a.fc_datein ) ) / 60 /60
where a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d")
and a.fc_membercode = concat('TSI ',b.fc_membercode) ;
-- update effective hour
update t_attendance a 
left join t_tempodavidattendancedate b 
on a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d")
and a.fc_membercode = concat('TSI ',b.fc_membercode)
set 
a.fn_effectivehour = a.fn_totalhour - (a.fn_totalbreakshift1 + a.fn_totalbreakshift2 + a.fn_totalbreakshift3 )
where a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d")
and a.fc_membercode = concat('TSI ',b.fc_membercode) ;
-- update ot1
update t_attendance a 
left join t_tempodavidattendancedate b 
on a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d")
and a.fc_membercode = concat('TSI ',b.fc_membercode)
set 
a.fn_ot1 =
case when (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) = 8  then 
	case when a.fc_daytype = 'N' then
		case when a.fn_effectivehour - ( a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) >= 1 then
			1
		when a.fn_effectivehour - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) >= 0 then 
			a.fn_effectivehour - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 )
		else 0 end
	else 0 end 
when (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) = 7 then
	case when a.fc_daytype = 'N' then
		case when a.fn_effectivehour - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) >= 1 then
			1
		when a.fn_effectivehour - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) >= 0 then 
			a.fn_effectivehour - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 )
		else 0 end
	when a.fc_daytype = 'S' then
		case when a.fn_effectivehour > 5 then
			case when a.fn_effectivehour - 5 >= 1 then
				1
			when a.fn_effectivehour - 5 <= 1 then 
				a.fn_effectivehour - 5
			else 0 end
		else 0 end
	else 0 end
else 0 end
where a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d")
and a.fc_membercode = concat('TSI ',b.fc_membercode) ;
-- update ot 2
update t_attendance a 
left join t_tempodavidattendancedate b 
on a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d")
and a.fc_membercode = concat('TSI ',b.fc_membercode)
set 
a.fn_ot2 =
case when (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) = 8  then 
	case when a.fc_daytype = 'N' then
		case when a.fn_ot1 >=1 then
			a.fn_effectivehour - a.fn_ot1 - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 )
		else 0 end
	when a.fc_daytype in ('S','H','O') then
		case when a.fn_effectivehour - 7 >= 0 then
			7
		when a.fn_effectivehour - 7 <= 0 then 
			a.fn_effectivehour 
		else 0 end
	else 0 end 
when (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) = 7 then
	case when a.fc_daytype = 'N' then
		case when a.fn_ot1 >= 1 then 
			a.fn_effectivehour - a.fn_ot1 - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 )
		else 0 end
	when a.fc_daytype = 'S' then
		case when a.fn_ot1 >= 1 then
			case when a.fn_effectivehour - 5 >= 0 then
				a.fn_effectivehour - a.fn_ot1 - 5
			when a.fn_effectivehour - 5 <= 0 then
				a.fn_effectivehour
			else 0 end
		else 0 end
	when a.fc_daytype in ('H','O') then
		case when a.fn_effectivehour - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) >= 0 then
			7
		when a.fn_effectivehour - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) <= 0 then 
			a.fn_effectivehour 
		else 0 end
	else 0 end 
else 0 end
where a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d")
and a.fc_membercode = concat('TSI ',b.fc_membercode) ;
-- update ot 3
update t_attendance a 
left join t_tempodavidattendancedate b 
on a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d")
and a.fc_membercode = concat('TSI ',b.fc_membercode)
set
a.fn_ot3 =
case when (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) = 8  then 
	case when a.fc_daytype in ('S','H','O') then
		case when a.fn_effectivehour - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) >= 1 then
			1
		when a.fn_effectivehour - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) >= 0 then 
			a.fn_effectivehour - 7
		else 0 end
	else 0 end 
when (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) = 7 then
	case when a.fc_daytype in ('H','O') then
		case when a.fn_effectivehour - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) >= 1 then
			1
		when a.fn_effectivehour - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) >= 0 then 
			a.fn_effectivehour - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 )
		else 0 end
	else 0 end
else 0 end
where a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d")
and a.fc_membercode = concat('TSI ',b.fc_membercode) ;
-- update ot 4
update t_attendance a 
left join t_tempodavidattendancedate b 
on a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d")
and a.fc_membercode = concat('TSI ',b.fc_membercode)
set
fn_ot4 =
case when (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) = 8  then 
	case when a.fc_daytype in ('S','H','O') then
		case when a.fn_ot3 >=1 then
			a.fn_effectivehour - fn_ot3 - 7
		else 0 end
	when a.fc_daytype in ('H+') then
		case when a.fn_effectivehour - 7 >= 0 then
			7
		when a.fn_effectivehour - 7 <= 0 then 
			a.fn_effectivehour 
		else 0 end
	else 0 end 
when (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) = 7 then
	case when a.fc_daytype in ('H','O') then
		case when a.fn_ot3 >= 1 then 
			a.fn_effectivehour - a.fn_ot3 - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 )
		else 0 end
	when a.fc_daytype in ('H+') then
		case when a.fn_effectivehour - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) >= 0 then
			7
		when a.fn_effectivehour - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) <= 0 then 
			a.fn_effectivehour 
		else 0 end
	else 0 end 
else 0 end
where a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d")
and a.fc_membercode = concat('TSI ',b.fc_membercode) ;
-- update ot 5
update t_attendance a 
left join t_tempodavidattendancedate b 
on a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d")
and a.fc_membercode = concat('TSI ',b.fc_membercode)
set
a.fn_ot5 =
case when (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) = 8  then 
	case when a.fc_daytype in ('H+') then
		case when a.fn_effectivehour - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) >= 1 then
			1
		when a.fn_effectivehour - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) >= 0 then 
			a.fn_effectivehour - 7
		else 0 end
	else 0 end 
when (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) = 7 then
	case when a.fc_daytype in ('H+') then
		case when a.fn_effectivehour - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) >= 1 then
			1
		when a.fn_effectivehour - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) >= 0 then 
			a.fn_effectivehour - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 )
		else 0 end
	else 0 end
else 0 end
where a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d")
and a.fc_membercode = concat('TSI ',b.fc_membercode) ;
-- update ot 6
update t_attendance a 
left join t_tempodavidattendancedate b 
on a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d")
and a.fc_membercode = concat('TSI ',b.fc_membercode)
set
a.fn_ot6 =
case when (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) = 8  then 
	case when a.fc_daytype in ('H+') then
		case when a.fn_ot5 >=1 then
			a.fn_effectivehour - a.fn_ot5 - 7
		else 0 end
	else 0 end 
when (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) = 7 then
	case when a.fc_daytype in ('H+') then
		case when a.fn_ot5 >= 1 then 
			a.fn_effectivehour - a.fn_ot5 - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 )
		else 0 end
	else 0 end 
else 0 end
where a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d")
and a.fc_membercode = concat('TSI ',b.fc_membercode) ;


select 
fc_periode ,fd_dategenerate ,
a.fc_membercode,b.fc_membername1,
b.fc_memberattendancetype ,b.fn_memberothernumber1 ,
b.fc_memberposition, 
a.fd_date ,a.fc_daytype ,
concat(fc_shift1,fc_shift2,fc_shift3) shift ,
fc_shift1 ,fc_shift2,fc_shift3,
fc_workinshift1 timein1,
if(fc_workoutshift2 in ("","00:00",NULL), fc_workoutshift1 ,fc_workoutshift2 ) timeout1,
fc_workinshift3 timein2,
fc_workoutshift3 timeout2,
fd_realdatein ,fd_realdateout,
fn_totalhour ,fn_effectivehour, 
fn_ot1 ,fn_ot2 ,fn_ot3 ,fn_ot4 ,fn_ot5 ,fn_ot6,
length(concat(fc_shift1,fc_shift2,fc_shift3)) jmlh_shift,
fv_description, 
fc_ti ,
( select count(fd_date) from t_attendance where fc_daytype not in ("") and fc_membercode = b.fc_membercode and fc_periode = a.fc_periode ) totalworkday,
( fn_ot1 + fn_ot2 + fn_ot3 + fn_ot4 + fn_ot5 + fn_ot6 )  total_ot 
from t_attendance a
left join t_member b on a.fc_membercode = b.fc_membercode
left join t_trxtype c on b.fc_memberposition = c.fc_trxcode and c.fc_trxcode = "EMP_POSITION"
where 
a.fc_membercode like :member and b.fl_memberblacklist = 'F' and a.fd_date  >= :datestart and a.fd_date <= :datefinish
or 
a.fc_membercode like :member and b.fl_memberblacklist = 'F' and a.fc_periode = DATE_FORMAT(:datestart,"%Y%m" )   
or
b.fc_membername1 like :member and b.fl_memberblacklist = 'F' and a.fd_date  >= :datestart and a.fd_date <= :datefinish
or 
b.fc_membername1  like :member and b.fl_memberblacklist = 'F' and a.fc_periode = DATE_FORMAT(:datestart,"%Y%m" )   
order by b.fc_membercode,a.fd_date


DATE_FORMAT(NOW(),"%Y%m" )

-- REVISI
update t_attendance a 
set 
a.fc_timeinshift1 = if( a.fc_timeinshift1 = "" OR a.fc_timeinshift1 is NULL,a.fc_workinshift1 ,a.fc_timeinshift1 ),
a.fc_timeoutshift1 = if( a.fc_timeoutshift1 = "" OR a.fc_timeoutshift1 is NULL,a.fc_workoutshift1 ,a.fc_timeoutshift1 ),
a.fc_timeinshift2 = if( a.fc_timeinshift2 = "" or a.fc_timeinshift2 is NULL,a.fc_workinshift2 ,a.fc_timeinshift2 ),
a.fc_timeoutshift2 = if( a.fc_timeoutshift2 = "" or a.fc_timeoutshift2 is NULL,a.fc_workoutshift2 ,a.fc_timeoutshift2 ),
a.fc_timeinshift3 = if( a.fc_timeinshift3 = "" or a.fc_timeinshift3 is NULL,a.fc_workinshift3 ,a.fc_timeinshift3 ),
a.fc_timeoutshift3 = if( a.fc_timeoutshift3 = "" or a.fc_timeoutshift3 is NULL,a.fc_workoutshift3 ,a.fc_timeoutshift3 )

update t_attendance a
set 
a.fn_totaltimeshift1 = TIME_TO_SEC( 
	if( coalesce(a.fc_timeinshift1,"00:00") > coalesce(a.fc_timeoutshift1,"00:00") ,
		subtime(coalesce(a.fc_timeinshift1,"00:00") ,coalesce(a.fc_timeoutshift1,"00:00") ) ,
		subtime(coalesce(a.fc_timeoutshift1,"00:00"), coalesce(a.fc_timeinshift1,"00:00") )
	) )/60/60, 
a.fn_totaltimeshift2 = TIME_TO_SEC( 
	if( coalesce(a.fc_timeinshift2,"00:00") > coalesce(a.fc_timeoutshift2,"00:00") ,
		subtime(coalesce(a.fc_timeinshift2,"00:00") ,coalesce(a.fc_timeoutshift2,"00:00") ) ,
		subtime(coalesce(a.fc_timeoutshift2,"00:00"), coalesce(a.fc_timeinshift2,"00:00") )
	) )/60/60,
a.fn_totaltimeshift3 = TIME_TO_SEC( 
	if( coalesce(a.fc_timeinshift3,"00:00") > coalesce(a.fc_timeoutshift3,"00:00") ,
		subtime(coalesce(a.fc_timeinshift3,"00:00") ,coalesce(a.fc_timeoutshift3,"00:00") ) ,
		subtime(coalesce(a.fc_timeoutshift3,"00:00"), coalesce(a.fc_timeinshift3,"00:00") )
	) )/60/60,
a.fn_totalhour = a.fn_totaltimeshift1 + coalesce(a.fn_totaltimeshift2,0) + coalesce(a.fn_totaltimeshift3,0),
a.fn_effectivehour = coalesce(a.fn_totaltimeshift1,0) + coalesce(a.fn_totaltimeshift2,0) + coalesce(a.fn_totaltimeshift3,0) 
- ( coalesce(a.fn_totalbreakshift1,0) + coalesce(a.fn_totalbreakshift2,0) + coalesce(a.fn_totalbreakshift3,0)  )
where a.fc_periode = date_format(:datestart,"%Y%m")

update t_attendance a
set 
a.fn_totaltimeshift1 = TIME_TO_SEC( 
	if( a.fc_timeinshift1 > a.fc_timeoutshift1 ,
		subtime( addtime(a.fc_timeoutshift1 ,"24:00") ,a.fc_timeinshift1 ) ,
		subtime(a.fc_timeoutshift1, a.fc_timeinshift1 )
	) )/60/60, 
a.fn_totaltimeshift2 = TIME_TO_SEC( 
	if( a.fc_timeinshift2 > a.fc_timeoutshift2 ,
		subtime( subtime(a.fc_timeoutshift2 ,"24:00") , a.fc_timeinshift2 ) ,
		subtime( a.fc_timeoutshift2, a.fc_timeinshift2 )
	) )/60/60,
a.fn_totaltimeshift3 = TIME_TO_SEC( 
	if( a.fc_timeinshift3 > a.fc_timeoutshift3 ,
		subtime(addtime(a.fc_timeoutshift3,"24:00") , a.fc_timeinshift3) ,
		subtime(a.fc_timeoutshift3, a.fc_timeinshift3 )
	) )/60/60,
a.fn_totalhour = a.fn_totaltimeshift1 + coalesce(a.fn_totaltimeshift2,0) + coalesce(a.fn_totaltimeshift3,0),
a.fn_effectivehour = a.fn_totaltimeshift1 + coalesce(a.fn_totaltimeshift2,0) + coalesce(a.fn_totaltimeshift3,0) 
- ( coalesce(a.fn_totalbreakshift1,0) + coalesce(a.fn_totalbreakshift2,0) + coalesce(a.fn_totalbreakshift3,0)  )
where a.fc_periode = date_format(:datestart,"%Y%m")


select 
fc_periode ,fd_dategenerate ,
a.fc_membercode,b.fc_membername1,
b.fc_memberattendancetype ,b.fn_memberothernumber1 ,
b.fc_memberposition, 
a.fd_date ,a.fc_daytype ,
concat(fc_shift1,fc_shift2,fc_shift3) shift ,
fc_shift1 ,fc_shift2,fc_shift3,
fc_timeinshift1,fc_timeoutshift1 ,
fc_timeinshift2,fc_timeoutshift2 ,
fc_timeinshift3,fc_timeoutshift3 ,
fc_workinshift1 timein1,
if(fc_workoutshift2 in ("","00:00",NULL), fc_workoutshift1 ,fc_workoutshift2 ) timeout1,
fc_workinshift3 timein2,
fc_workoutshift3 timeout2,
fd_realdatein ,fd_realdateout,
fn_totalhour ,fn_effectivehour, 
fn_ot1 ,fn_ot2 ,fn_ot3 ,fn_ot4 ,fn_ot5 ,fn_ot6,
length(concat(fc_shift1,fc_shift2,fc_shift3)) jmlh_shift,
fv_description, 
fc_ti ,
( select count(fd_date) from t_attendance where 
"1" in (fc_shift1,fc_shift2,fc_shift3 ) and fc_membercode = a.fc_membercode  and fd_date  >= :datestart and fd_date <= :datefinish
or  
"1" in (fc_shift1,fc_shift2,fc_shift3 ) and fc_membercode = a.fc_membercode  and fc_periode = DATE_FORMAT(:datestart,"%Y%m" ) and  fd_date  <= :datestart
) totalshift1,
( select count(fd_date) from t_attendance where 
"2" in (fc_shift1,fc_shift2,fc_shift3 ) and fc_membercode = a.fc_membercode  and fd_date  >= :datestart and fd_date <= :datefinish
or  
"3" in (fc_shift1,fc_shift2,fc_shift3 ) and fc_membercode = a.fc_membercode  and fc_periode = DATE_FORMAT(:datestart,"%Y%m" ) and  fd_date  <= :datestart
) totalshift2,
( select count(fd_date) from t_attendance where 
"3" in (fc_shift1,fc_shift2,fc_shift3 ) and fc_membercode = a.fc_membercode  and fd_date  >= :datestart and fd_date <= :datefinish
or  
"3" in (fc_shift1,fc_shift2,fc_shift3 ) and fc_membercode = a.fc_membercode  and fc_periode = DATE_FORMAT(:datestart,"%Y%m" ) and  fd_date  <= :datestart
) totalshift3,
( fn_ot1 + fn_ot2 + fn_ot3 + fn_ot4 + fn_ot5 + fn_ot6 ) total_ot,
( select sum( fn_ot1 + fn_ot2 + fn_ot3 + fn_ot4 + fn_ot5 + fn_ot6 ) from t_attendance where 
 fc_membercode = a.fc_membercode  and fd_date  >= :datestart and fd_date <= :datefinish
or  
 fc_membercode = a.fc_membercode  and fc_periode = DATE_FORMAT(:datestart,"%Y%m" ) and  fd_date  <= :datestart
) sum_total_ot,
( select count(fd_date) from t_attendance where 
fc_daytype not in ("") and fc_membercode = a.fc_membercode  and fd_date  >= :datestart and fd_date <= :datefinish
or  
fc_daytype not in ("") and fc_membercode = a.fc_membercode  and fc_periode = DATE_FORMAT(:datestart,"%Y%m" ) and  fd_date  <= :datestart
) totalworkday,
( select count(fd_date) from t_attendance where 
fl_fastingshift = 1 and fc_membercode = a.fc_membercode  
and fd_date  >= 
if ( (select fv_trxdescription from t_trxtype where fc_trxid = 'FASTING_VAR' and fc_trxcode = 'BEGIN_DATE') > :datestart,
(select fv_trxdescription from t_trxtype where fc_trxid = 'FASTING_VAR' and fc_trxcode = 'BEGIN_DATE')
, :datestart )
and fd_date <= (select fv_trxdescription from t_trxtype where fc_trxid = 'FASTING_VAR' and fc_trxcode = 'FINISH_DATE')
) total_fasting_shift
from t_attendance a
left join t_member b on a.fc_membercode = b.fc_membercode
left join t_trxtype c on b.fc_memberposition = c.fc_trxcode and c.fc_trxcode = "EMP_POSITION"
where 
a.fc_membercode like :member and b.fl_memberblacklist = 'F' and a.fd_date  >= :datestart and a.fd_date <= :datefinish
or  
a.fc_membercode like :member and b.fl_memberblacklist = 'F' and a.fc_periode = DATE_FORMAT(:datestart,"%Y%m" ) 
and  a.fd_date  <= CONCAT(substring(a.fc_periode ,1,4),'-',substring(a.fc_periode ,5,6),'-','15') 
or
b.fc_membername1 like :member and b.fl_memberblacklist = 'F' and a.fd_date  >= :datestart and a.fd_date <= :datefinish
or  
b.fc_membername1 like :member and b.fl_memberblacklist = 'F' and a.fc_periode = DATE_FORMAT(:datestart,"%Y%m" ) 
and  a.fd_date  <= CONCAT(substring(a.fc_periode ,1,4),'-',substring(a.fc_periode ,5,6),'-','15') 



select fd_date from t_attendance where fc_periode = DATE_FORMAT(:datestart,"%Y%m" )  
and fd_date = CONCAT(substring('202004',1,4),'-',substring('202004',5,6),'-','15') 
and fc_membercode = :member



select count(fd_date) from t_attendance where 
fl_fastingshift = 1 and fc_membercode = 'TSI 52713'  
and fd_date  >= 
if ( (select fv_trxdescription from t_trxtype where fc_trxid = 'FASTING_VAR' and fc_trxcode = 'BEGIN_DATE') > :datestart,
(select fv_trxdescription from t_trxtype where fc_trxid = 'FASTING_VAR' and fc_trxcode = 'BEGIN_DATE')
, :datestart )
and fd_date <= (select fv_trxdescription from t_trxtype where fc_trxid = 'FASTING_VAR' and fc_trxcode = 'FINISH_DATE')


update t_attendance a
left join t_leave b on a.fd_date = b.fd_leavedate 
set 
a.fv_description = coalesce(
case when b.fc_leavetype = "L" then
"an leave"
when b.fc_leavetype = "PE" then
"permit early"
when b.fc_leavetype = "PH" then
"permit holiday"
when b.fc_leavetype = "PL" then
"permit late"
when b.fc_leavetype = "PLO" then
"permit leave office"
end ,a.fv_description ,""),
a.fc_daytype = if (b.fc_leavetype in ("L","PH"),"",a.fc_daytype ),
a.fc_shift1 = if (b.fc_leavetype in ("L","PH"),"",a.fc_shift1 ),
a.fc_shift2 = if (b.fc_leavetype in ("L","PH"),"",a.fc_shift2 ),
a.fc_shift3 = if (b.fc_leavetype in ("L","PH"),"",a.fc_shift3 ),
a.fc_timeinshift1 = if (b.fc_leavetype in ("L","PH"),"",a.fc_timeinshift1 ),
a.fc_timeoutshift1 = if (b.fc_leavetype in ("L","PH"),"",a.fc_timeoutshift1 ),
a.fn_totaltimeshift1 = if (b.fc_leavetype in ("L","PH"),0,a.fn_totaltimeshift1 ),
a.fc_timeinshift2 = if (b.fc_leavetype in ("L","PH"),"",a.fc_timeinshift2 ),
a.fc_timeoutshift2 = if (b.fc_leavetype in ("L","PH"),"",a.fc_timeoutshift2 ),
a.fn_totaltimeshift2 = if (b.fc_leavetype in ("L","PH"),0,a.fn_totaltimeshift2 ),
a.fc_timeinshift3 = if (b.fc_leavetype in ("L","PH"),"",a.fc_timeinshift3 ),
a.fc_timeoutshift3 = if (b.fc_leavetype in ("L","PH"),"",a.fc_timeoutshift3 ),
a.fn_totaltimeshift3 = if (b.fc_leavetype in ("L","PH"),0,a.fn_totaltimeshift3 ),
a.fn_totalhour = if (b.fc_leavetype in ("L","PH"),"",a.fn_totalhour ),
a.fn_effectivehour = if (b.fc_leavetype in ("L","PH"),"",a.fn_effectivehour ),

-- jumlahshift formula
update t_attendance 
set
fn_jumlahshift1 = if(fn_totaltimeshift1 >= 4,1,0),
fn_jumlahshift2 = if(fn_totaltimeshift2 >= 4,1,0),
fn_jumlahshift3 = if(fn_totaltimeshift3 >= 4,1,0),
fn_jumlahshift = if(fc_timeinshift1 is null,null,fn_jumlahshift1 ) + coalesce(fn_jumlahshift2,0) + coalesce(fn_jumlahshift3,0)




-- leave script
select a.fc_branch, a.fc_leaveno, a.fd_leavecreatedate, 
       a.fc_membercode, b.fc_membername1, a.fc_inputby,
       a.fc_leavestatus, a.fc_leaveprogress, a.fc_leavetype,
       c.fv_trxdescription,
       min(a.fd_leavedate) as DateStart, 
       max(a.fd_leavedate) as DateFinish,
       DATEDIFF(MAX(a.fd_leavedate), MIN(a.fd_leavedate)) + 1 as TotDay,
       CASE WHEN (DATE_FORMAT(MIN(a.fd_leavedate),'%Y%m%d') <= DATE_FORMAT(SYSDATE(),'%Y%m%d'))
            AND  (DATE_FORMAT(MAX(a.fd_leavedate),'%Y%m%d') >= DATE_FORMAT(SYSDATE(),'%Y%m%d'))
            THEN 'ON LEAVING'
            
            WHEN (DATE_FORMAT(MIN(a.fd_leavedate),'%Y%m%d') > DATE_FORMAT(SYSDATE(),'%Y%m%d'))
            AND  (DATE_FORMAT(MAX(a.fd_leavedate),'%Y%m%d') > DATE_FORMAT(SYSDATE(),'%Y%m%d'))       
            THEN CONCAT(DATEDIFF(MIN(a.fd_leavedate), SYSDATE()) ,' DAYS REMAINING') 
            
            WHEN (DATE_FORMAT(MIN(a.fd_leavedate),'%Y%m%d') < DATE_FORMAT(SYSDATE(),'%Y%m%d'))
            AND  (DATE_FORMAT(MAX(a.fd_leavedate),'%Y%m%d') < DATE_FORMAT(SYSDATE(),'%Y%m%d'))       
            THEN 'LEAVE IS OVER'
       END as LeavePosition,            
       a.fv_description            
from t_leave a
left outer join t_member b on a.fc_membercode = b.fc_membercode
left outer join t_trxtype c on a.fc_leavetype = c.fc_trxcode and c.fc_trxid = 'LEAVE_TYPE'
group by a.fc_branch, a.fc_leaveno
order by a.fc_leaveno;

select a.fc_branch, a.fc_leaveno, a.fd_leavecreatedate, 
       a.fc_membercode, b.fc_membername1, a.fc_inputby,
       a.fc_leavestatus, a.fc_leaveprogress, a.fc_leavetype,
       c.fv_trxdescription,
       min(a.fd_leavedate) as DateStart, 
       max(a.fd_leavedate) as DateFinish,
       DATEDIFF(MAX(a.fd_leavedate), MIN(a.fd_leavedate)) + 1 as TotDay,
       CASE WHEN (DATE_FORMAT(MIN(a.fd_leavedate),'%Y%m%d') <= DATE_FORMAT(SYSDATE(),'%Y%m%d'))
            AND  (DATE_FORMAT(MAX(a.fd_leavedate),'%Y%m%d') >= DATE_FORMAT(SYSDATE(),'%Y%m%d'))
            THEN 'ON LEAVING'
            
            WHEN (DATE_FORMAT(MIN(a.fd_leavedate),'%Y%m%d') > DATE_FORMAT(SYSDATE(),'%Y%m%d'))
            AND  (DATE_FORMAT(MAX(a.fd_leavedate),'%Y%m%d') > DATE_FORMAT(SYSDATE(),'%Y%m%d'))       
            THEN CONCAT(DATEDIFF(MIN(a.fd_leavedate), SYSDATE()) ,' DAYS REMAINING') 
            
            WHEN (DATE_FORMAT(MIN(a.fd_leavedate),'%Y%m%d') < DATE_FORMAT(SYSDATE(),'%Y%m%d'))
            AND  (DATE_FORMAT(MAX(a.fd_leavedate),'%Y%m%d') < DATE_FORMAT(SYSDATE(),'%Y%m%d'))       
            THEN 'LEAVE IS OVER'
       END as LeavePosition,            
       a.fv_description            
from t_leave a
left outer join t_member b on a.fc_membercode = b.fc_membercode
left outer join t_trxtype c on a.fc_leavetype = c.fc_trxcode and c.fc_trxid = 'LEAVE_TYPE'
where 
(select min(fd_leavedate) from t_leave where fc_leaveno = a.fc_leaveno group by fc_branch, fc_leaveno) >= :datestart 
and 
(select max(fd_leavedate) from t_leave where fc_leaveno = a.fc_leaveno group by fc_branch, fc_leaveno) <= :datefinish
group by a.fc_branch, a.fc_leaveno order by a.fc_leaveno;


-- fastingshift
update t_attendance a
left join t_dailytimerange b on  b.fc_timerangecode = concat(a.fc_shift1,a.fc_shift2,a.fc_shift3 )
set 
	a.fl_fastingshift = b.fl_fastingshift 	
	
update t_attendance a
left join t_fastingday b on a.fd_date = b.fd_date
left join t_dailytimerange c on  c.fc_timerangecode = concat(a.fc_shift1,a.fc_shift2,a.fc_shift3 )
set 
	a.fl_fastingshift = if( b.fd_date is not null and c.fl_fastingshift is true , 1 , 0)
where 
a.fd_date = b.fd_date 
	
	

select concat(a.fc_shift1,a.fc_shift2,a.fc_shift3 )
,a.fd_date ,b.fd_date 
from
t_attendance a
left join t_fastingday b on a.fd_date = b.fd_date 
	
	
	
select 
fc_periode ,fd_dategenerate ,
a.fc_membercode,b.fc_membername1,
b.fc_memberattendancetype ,b.fn_memberothernumber1 ,
b.fc_memberposition, 
a.fd_date ,a.fc_daytype ,
concat(fc_shift1,fc_shift2,fc_shift3) shift ,
fc_shift1 ,fc_shift2,fc_shift3,
fc_timeinshift1,fc_timeoutshift1 ,
fc_timeinshift2,fc_timeoutshift2 ,
fc_timeinshift3,fc_timeoutshift3 ,
fc_workinshift1 timein1,
if(fc_workoutshift2 in ("","00:00",NULL), fc_workoutshift1 ,fc_workoutshift2 ) timeout1,
fc_workinshift3 timein2,
fc_workoutshift3 timeout2,
fd_realdatein ,fd_realdateout,
fn_totalhour ,fn_effectivehour, 
fn_ot1 ,fn_ot2 ,fn_ot3 ,fn_ot4 ,fn_ot5 ,fn_ot6,
length(concat(fc_shift1,fc_shift2,fc_shift3)) jmlh_shift,
fv_description, 
fc_ti ,
( select count(fd_date) from t_attendance where 
"1" in (fc_shift1,fc_shift2,fc_shift3 ) and fc_membercode = a.fc_membercode  and fd_date  >= :datestart and fd_date <= :datefinish
or  
"1" in (fc_shift1,fc_shift2,fc_shift3 ) and fc_membercode = a.fc_membercode  and fc_periode = DATE_FORMAT(:datestart,"%Y%m" ) and  fd_date  <= :datestart
) totalshift1,
( select count(fd_date) from t_attendance where 
"2" in (fc_shift1,fc_shift2,fc_shift3 ) and fc_membercode = a.fc_membercode  and fd_date  >= :datestart and fd_date <= :datefinish
or  
"3" in (fc_shift1,fc_shift2,fc_shift3 ) and fc_membercode = a.fc_membercode  and fc_periode = DATE_FORMAT(:datestart,"%Y%m" ) and  fd_date  <= :datestart
) totalshift2,
( select count(fd_date) from t_attendance where 
"3" in (fc_shift1,fc_shift2,fc_shift3 ) and fc_membercode = a.fc_membercode  and fd_date  >= :datestart and fd_date <= :datefinish
or  
"3" in (fc_shift1,fc_shift2,fc_shift3 ) and fc_membercode = a.fc_membercode  and fc_periode = DATE_FORMAT(:datestart,"%Y%m" ) and  fd_date  <= :datestart
) totalshift3,
( fn_ot1 + fn_ot2 + fn_ot3 + fn_ot4 + fn_ot5 + fn_ot6 ) total_ot,
( select sum( fn_ot1 + fn_ot2 + fn_ot3 + fn_ot4 + fn_ot5 + fn_ot6 ) from t_attendance where 
 fc_membercode = a.fc_membercode  and fd_date  >= :datestart and fd_date <= :datefinish
or  
 fc_membercode = a.fc_membercode  and fc_periode = DATE_FORMAT(:datestart,"%Y%m" ) and  fd_date  <= :datestart
) sum_total_ot,
( select count(fd_date) from t_attendance where 
fc_daytype not in ("") and fc_membercode = a.fc_membercode  and fd_date  >= :datestart and fd_date <= :datefinish
or  
fc_daytype not in ("") and fc_membercode = a.fc_membercode  and fc_periode = DATE_FORMAT(:datestart,"%Y%m" ) and  fd_date  <= :datestart
) totalworkday,
( select count(fd_date) from t_attendance where 
fl_fastingshift = 1 and fc_membercode = a.fc_membercode  
and fd_date  >= 
if ( (select fv_trxdescription from t_trxtype where fc_trxid = 'FASTING_VAR' and fc_trxcode = 'BEGIN_DATE') > :datestart,
(select fv_trxdescription from t_trxtype where fc_trxid = 'FASTING_VAR' and fc_trxcode = 'BEGIN_DATE')
, :datestart )
and fd_date <= (select fv_trxdescription from t_trxtype where fc_trxid = 'FASTING_VAR' and fc_trxcode = 'FINISH_DATE')
) total_fasting_shift
from t_attendance a
left join t_member b on a.fc_membercode = b.fc_membercode
left join t_trxtype c on b.fc_memberposition = c.fc_trxcode and c.fc_trxcode = "EMP_POSITION"
where 
a.fc_membercode like :member and b.fl_memberblacklist = 'F' and a.fd_date  >= :datestart and a.fd_date <= :datefinish
or  
a.fc_membercode like :member and b.fl_memberblacklist = 'F' and a.fc_periode = DATE_FORMAT(:datestart,"%Y%m" ) 
and  a.fd_date  <= CONCAT(substring(a.fc_periode ,1,4),'-',substring(a.fc_periode ,5,6),'-','15') 
or
b.fc_membername1 like :member and b.fl_memberblacklist = 'F' and a.fd_date  >= :datestart and a.fd_date <= :datefinish
or  
b.fc_membername1 like :member and b.fl_memberblacklist = 'F' and a.fc_periode = DATE_FORMAT(:datestart,"%Y%m" ) 
and  a.fd_date  <= CONCAT(substring(a.fc_periode ,1,4),'-',substring(a.fc_periode ,5,6),'-','15') 
order by fc_membercode 


select 
-- a.fc_branch,a.fc_periode ,a.fd_dategenerate ,
a.fc_membercode,b.fc_membername1,
-- b.fc_memberattendancetype ,b.fn_memberothernumber1 ,
b.fc_memberposition, 
a.fd_date ,a.fc_daytype ,
concat(a.fc_shift1,a.fc_shift2,a.fc_shift3) shift ,
-- a.fc_shift1 ,a.fc_shift2,a.fc_shift3,
-- a.fc_timeinshift1,a.fc_timeoutshift1 ,
-- a.fc_timeinshift2,a.fc_timeoutshift2 ,
-- a.fc_timeinshift3,a.fc_timeoutshift3 ,
a.fc_timeinshift1 timein1,
if(a.fc_timeinshift2  in ("","00:00") or a.fc_timeinshift2 is null , a.fc_timeoutshift1 ,a.fc_timeoutshift2 ) timeout1,
a.fc_timeinshift3 timein2,
a.fc_timeoutshift3 timeout2,
-- a.fd_realdatein ,a.fd_realdateout,
-- a.fn_totalhour ,a.fn_effectivehour, 
a.fn_ot1 ,a.fn_ot2 ,a.fn_ot3 ,a.fn_ot4 ,a.fn_ot5 ,a.fn_ot6,
a.fn_jumlahshift ,
a.fv_description, 
a.fc_ti,
d.workday,
d.total_ot,
e.total_ot total_ot1
from t_attendance a
left join t_member b on a.fc_membercode = b.fc_membercode

left join t_trxtype c on b.fc_memberposition = c.fc_trxcode and c.fc_trxcode = "EMP_POSITION"

left join ( 
select count(fd_date) workday, 
(fn_ot1 +  fn_ot2 + fn_ot3 + fn_ot4 + fn_ot5 + fn_ot6) total_ot,
fc_membercode,fc_daytype,fd_date  from t_attendance 
where fc_daytype not in ("")
group by fc_membercode,fc_daytype,fd_date 
) d 
on a.fc_membercode = d.fc_membercode and a.fc_daytype = d.fc_daytype and a.fd_date = d.fd_date

left join ( 
select fc_periode,fc_membercode,fd_date from t_attendance  
where  fd_date  >= :datestart and fd_date <= :datefinish
group by fc_membercode,fc_periode
) f 
on a.fc_membercode = f.fc_membercode and a.fc_periode = f.fc_periode

left join ( 
select 
fc_periode,
-- fd_date ,
-- count(fd_date) workday, 
(fn_ot1 +  fn_ot2 + fn_ot3 + fn_ot4 + fn_ot5 + fn_ot6) total_ot,
fc_membercode,fc_daytype,fd_date  from t_attendance 
where fc_daytype not in ("")
and fd_date  >=  (select min(fd_date) from t_attendance where fc_periode = DATE_FORMAT(:datestart,"%Y%m" ) ) 
and fd_date <= last_day(:datestart)
group by fc_membercode,fc_daytype,fd_date 
) e 
on a.fc_membercode = e.fc_membercode and a.fc_daytype = e.fc_daytype and a.fd_date = e.fd_date

where 
a.fc_membercode like :member and b.fl_memberblacklist = 'F' and a.fd_date  >= :datestart and a.fd_date <= :datefinish
or  
a.fc_membercode like :member and b.fl_memberblacklist = 'F' 
and a.fc_periode = f.fc_periode 
and  a.fd_date  <= CONCAT(substring(f.fc_periode ,1,4),'-',substring(f.fc_periode ,5,6),'-','15')
order by a.fc_membercode ,a.fc_periode ,a.fd_date 




select 
a.fc_branch,a.fc_periode ,a.fd_dategenerate ,
a.fc_membercode,b.fc_membername1,
b.fc_memberposition, 
a.fd_date ,a.fc_daytype ,
a.fc_shift1 ,a.fc_shift2,a.fc_shift3,
a.fc_timeinshift1,a.fc_timeoutshift1 ,
a.fc_timeinshift2,a.fc_timeoutshift2 ,
a.fc_timeinshift3,a.fc_timeoutshift3 ,
a.fd_realdatein ,a.fd_realdateout,
a.fn_totalhour ,a.fn_effectivehour, 
a.fn_ot1 ,a.fn_ot2 ,a.fn_ot3 ,a.fn_ot4 ,a.fn_ot5 ,a.fn_ot6,
a.fc_ti,
a.fv_description, 
a.fl_ischecked1 ,
a.fl_ischeked2 
from t_attendance a
left join t_member b on a.fc_membercode = b.fc_membercode
left join t_trxtype c on b.fc_memberposition = c.fc_trxcode and c.fc_trxcode = "EMP_POSITION"
left join ( 
select fc_periode,fc_membercode,fd_date from t_attendance  
where  fd_date  >= :datestart and fd_date <= :datefinish
group by fc_membercode,fc_periode
) d 
on a.fc_membercode = d.fc_membercode and a.fc_periode = d.fc_periode
where 
a.fc_membercode like :member and b.fl_memberblacklist = :status and a.fd_date  >= :datestart and a.fd_date <= :datefinish
or  
a.fc_membercode like :member and b.fl_memberblacklist = :status 
and a.fc_periode = d.fc_periode 
and  a.fd_date  <= CONCAT(substring(d.fc_periode ,1,4),'-',substring(d.fc_periode ,5,6),'-','15')
or
b.fc_membername1 like :member and b.fl_memberblacklist = :status and a.fd_date  >= :datestart and a.fd_date <= :datefinish
or  
b.fc_membername1 like :member and b.fl_memberblacklist = :status 
and a.fc_periode = d.fc_periode 
and  a.fd_date  <= CONCAT(substring(d.fc_periode ,1,4),'-',substring(d.fc_periode ,5,6),'-','15')
order by a.fc_membercode ,a.fc_periode ,a.fd_date 



select 
a.fc_periode ,a.fd_date ,
a.fc_membercode,b.fc_membername1,
b.fc_memberattendancetype ,b.fn_memberothernumber1 ,
b.fc_memberposition, 
a.fc_daytype ,
concat(a.fc_shift1,a.fc_shift2,a.fc_shift3) shift ,
a.fc_timeinshift1 timein1,
if(a.fc_timeinshift2 in ("","00:00") or a.fc_timeinshift2 is null , a.fc_timeoutshift1 ,a.fc_timeoutshift2 ) timeout1,
a.fc_timeinshift3 timein2,
a.fc_timeoutshift3 timeout2,
a.fn_totalhour ,a.fn_effectivehour, 
a.fn_ot1 ,a.fn_ot2 ,a.fn_ot3 ,a.fn_ot4 ,a.fn_ot5 ,a.fn_ot6,
a.fn_jumlahshift ,
a.fv_description, 
a.fc_ti ,
a.fl_fastingshift ,
month(fd_date )
from t_attendance a
left join t_member b on a.fc_membercode = b.fc_membercode
left join t_trxtype c on b.fc_memberposition = c.fc_trxcode and c.fc_trxcode = "EMP_POSITION"
where 
a.fc_membercode like "%%" and b.fl_memberblacklist = 'F' and a.fc_periode = DATE_FORMAT(now(),"%Y%m" ) 
or
b.fc_membername1 like "%%" and b.fl_memberblacklist = 'F' and a.fc_periode = DATE_FORMAT(now(),"%Y%m" )
order by a.fc_membercode,a.fc_periode ,a.fd_date


select 
a.fc_periode ,a.fd_date ,
a.fc_membercode,b.fc_membername1,
b.fc_memberattendancetype ,b.fn_memberothernumber1 ,
b.fc_memberposition, 
a.fc_daytype ,
concat(a.fc_shift1,a.fc_shift2,a.fc_shift3) shift ,
a.fc_timeinshift1 timein1,
if(a.fc_timeinshift2 in ("","00:00") or a.fc_timeinshift2 is null , a.fc_timeoutshift1 ,a.fc_timeoutshift2 ) timeout1,
a.fc_timeinshift3 timein2,
a.fc_timeoutshift3 timeout2,
a.fn_totalhour ,a.fn_effectivehour, 
a.fn_ot1 ,a.fn_ot2 ,a.fn_ot3 ,a.fn_ot4 ,a.fn_ot5 ,a.fn_ot6,
a.fn_jumlahshift ,
a.fv_description, 
a.fc_ti ,
a.fl_fastingshift 
from t_attendance a
left join t_member b on a.fc_membercode = b.fc_membercode
left join t_trxtype c on b.fc_memberposition = c.fc_trxcode and c.fc_trxcode = "EMP_POSITION"
where 
a.fc_membercode like "%%" and b.fl_memberblacklist = 'F' and a.fc_periode = DATE_FORMAT(now(),"%Y%m" ) 
or
b.fc_membername1 like "%%" and b.fl_memberblacklist = 'F' and a.fc_periode = DATE_FORMAT(now(),"%Y%m" )
order by a.fc_membercode,a.fc_periode ,a.fd_date


select fc_periode ,
fc_membercode,
min(fd_date) ,max(fd_date),
month(fd_date) month,
sum(fn_ot1 ) total_ot1,
sum(fn_ot2 ) total_ot2,
sum(fn_ot3 ) total_ot3,
sum(fn_ot4 ) total_ot4,
sum(fn_ot5 ) total_ot5,
sum(fn_ot6 ) total_ot6,
(sum(fn_ot1 ) * 1.5 ) + (sum(fn_ot2 ) * 2 ) + (sum(fn_ot3 ) * 3 ) + (sum(fn_ot4 ) * 4 ) + (sum(fn_ot5 ) * 6 ) + (sum(fn_ot6 ) * 8 ) total_ot,
sum( length(fc_daytype) ) total_workday,
sum( '1' in (fc_shift1,fc_shift2 ,fc_shift3) ) total_shift1,
sum( '2' in (fc_shift1,fc_shift2 ,fc_shift3) ) total_shift2,
sum( '3' in (fc_shift1,fc_shift2 ,fc_shift3) ) total_shift3,
sum(fl_fastingshift )
from t_attendance
group by fc_periode,fc_membercode 
-- ,month(fd_date) 
order by fc_membercode,fc_periode 

select fc_fastingdayno, min(fd_date) as StartDate, 
       max(fd_date) as EndDate, 
       fv_description
from t_fastingday 
where date_format(fd_date,'%Y') = date_format(SYSDATE() ,'%Y')  
group by fc_fastingdayno, fv_description
order by fd_date


select fc_fastingdayno, min(fd_date) StartDate, 
       max(fd_date) EndDate, 
       fv_description,date_format(fd_date,"%Y%m")
from t_fastingday 
group by fc_fastingdayno, fv_description,date_format(fd_date,"%Y%m")
order by fd_date


select 
a.fc_periode ,a.fd_date ,
g.mindate,
a.fc_membercode,b.fc_membername1,
b.fc_memberattendancetype ,b.fn_memberothernumber1 ,
b.fc_memberposition, 
a.fc_daytype ,
concat(a.fc_shift1,a.fc_shift2,a.fc_shift3) shift ,
a.fc_timeinshift1 timein1,
if(a.fc_timeinshift2 in ("","00:00") or a.fc_timeinshift2 is null , a.fc_timeoutshift1 ,a.fc_timeoutshift2 ) timeout1,
a.fc_timeinshift3 timein2,
a.fc_timeoutshift3 timeout2,
a.fn_totalhour ,a.fn_effectivehour, 
a.fn_ot1 ,a.fn_ot2 ,a.fn_ot3 ,a.fn_ot4 ,a.fn_ot5 ,a.fn_ot6,
a.fn_jumlahshift ,
a.fv_description, 
a.fc_ti ,
a.fl_fastingshift ,
if( d.fc_fastingdayno is not null,concat("Uang makan Puasa (",e.StartDate," - ",e.EndDate,") ",f.fastingshift," hari" ),"" ) uang_makanpuasa,
f.total_ot1,
f.total_ot2,
f.total_ot3,
f.total_ot4,
f.total_ot5,
f.total_ot6,
f.total_ot,
f.total_shift1,
f.total_shift2,
f.total_shift3,
h.total_ot1 total_ot1PrevPeriode ,
h.total_ot2 total_ot2PrevPeriode,
h.total_ot3 total_ot3PrevPeriode,
h.total_ot4 total_ot4PrevPeriode,
h.total_ot5 total_ot5PrevPeriode,
h.total_ot6 total_ot6PrevPeriode,
h.total_ot total_otPrevPeriode
from t_attendance a
left join t_member b on a.fc_membercode = b.fc_membercode
left join t_trxtype c on b.fc_memberposition = c.fc_trxcode and c.fc_trxcode = "EMP_POSITION"
left join (
select fc_fastingdayno, min(fd_date) StartDate, 
       max(fd_date) EndDate, 
       fv_description,
       date_format(fd_date,"%Y%m") fastingPeriode
from t_fastingday 
group by fc_fastingdayno, fv_description,date_format(fd_date,"%Y%m")
order by fd_date
) d on d.fastingPeriode = a.fc_periode 
left join (
select fc_fastingdayno, min(fd_date) as StartDate, 
       max(fd_date) as EndDate, 
       fv_description
from t_fastingday   
group by fc_fastingdayno, fv_description
order by fd_date
) e on e.fc_fastingdayno = d.fc_fastingdayno
left join 
(
select fc_periode ,
fc_membercode,
month(fd_date) monthperiode,
sum(fn_ot1 ) total_ot1,
sum(fn_ot2 ) total_ot2,
sum(fn_ot3 ) total_ot3,
sum(fn_ot4 ) total_ot4,
sum(fn_ot5 ) total_ot5,
sum(fn_ot6 ) total_ot6,
(sum(fn_ot1 ) * 1.5 ) + (sum(fn_ot2 ) * 2 ) + (sum(fn_ot3 ) * 3 ) + (sum(fn_ot4 ) * 4 ) + (sum(fn_ot5 ) * 6 ) + (sum(fn_ot6 ) * 8 ) total_ot,
sum( length(fc_daytype) ) total_workday,
sum( '1' in (fc_shift1,fc_shift2 ,fc_shift3) ) total_shift1,
sum( '2' in (fc_shift1,fc_shift2 ,fc_shift3) ) total_shift2,
sum( '3' in (fc_shift1,fc_shift2 ,fc_shift3) ) total_shift3,
sum(fl_fastingshift ) fastingshift
from t_attendance
group by fc_periode,fc_membercode
order by fc_membercode,fc_periode 
) f on a.fc_membercode = f.fc_membercode and a.fc_periode = f.fc_periode
left join (
select 
fd_date ,fc_periode,fc_membercode,
if(fd_date < concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" ) , 
concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" )
,fd_date ) mindate
from t_attendance
group by fc_periode ,fc_membercode,fd_date
order by fc_membercode,fc_periode,fd_date 
) g on  g.fc_membercode = a.fc_membercode and a.fc_periode = g.fc_periode and a.fd_date = g.fd_date
left join 
(
select fc_periode ,
fc_membercode,
sum(fn_ot1 ) total_ot1,
sum(fn_ot2 ) total_ot2,
sum(fn_ot3 ) total_ot3,
sum(fn_ot4 ) total_ot4,
sum(fn_ot5 ) total_ot5,
sum(fn_ot6 ) total_ot6,
(sum(fn_ot1 ) * 1.5 ) + (sum(fn_ot2 ) * 2 ) + (sum(fn_ot3 ) * 3 ) + (sum(fn_ot4 ) * 4 ) + (sum(fn_ot5 ) * 6 ) + (sum(fn_ot6 ) * 8 ) total_ot,
sum( length(fc_daytype) ) total_workday,
sum( '1' in (fc_shift1,fc_shift2 ,fc_shift3) ) total_shift1,
sum( '2' in (fc_shift1,fc_shift2 ,fc_shift3) ) total_shift2,
sum( '3' in (fc_shift1,fc_shift2 ,fc_shift3) ) total_shift3,
sum(fl_fastingshift ) fastingshift
from t_attendance
where month(fd_date) < month(:datefinsih) 
group by fc_periode,fc_membercode
order by fc_membercode,fc_periode
) h on a.fc_membercode = h.fc_membercode and a.fc_periode = h.fc_periode
where 
a.fc_membercode like :member and b.fl_memberblacklist = :active 
-- and a.fc_periode = :periode
and g.mindate >= :datestart and g.mindate <= :datefinsih
or
b.fc_membername1 like :member and b.fl_memberblacklist = :active 
and a.fc_periode = :periode
order by a.fc_membercode,a.fc_periode ,a.fd_date





select t1.fc_periode ,
t1.fc_membercode,
sum(t1.fn_ot1 ) total_ot1,
sum(t1.fn_ot2 ) total_ot2,
sum(t1.fn_ot3 ) total_ot3,
sum(t1.fn_ot4 ) total_ot4,
sum(t1.fn_ot5 ) total_ot5,
sum(t1.fn_ot6 ) total_ot6,
(sum(t1.fn_ot1 ) * 1.5 ) + (sum(t1.fn_ot2 ) * 2 ) + (sum(t1.fn_ot3 ) * 3 ) + (sum(t1.fn_ot4 ) * 4 ) + (sum(t1.fn_ot5 ) * 6 ) + (sum(t1.fn_ot6 ) * 8 ) total_ot,
sum( length(t1.fc_daytype) ) total_workday,
sum( '1' in (t1.fc_shift1,t1.fc_shift2 ,t1.fc_shift3) ) total_shift1,
sum( '2' in (t1.fc_shift1,t1.fc_shift2 ,t1.fc_shift3) ) total_shift2,
sum( '3' in (t1.fc_shift1,t1.fc_shift2 ,t1.fc_shift3) ) total_shift3,
sum(t1.fl_fastingshift ) fastingshift,
t2.mindate
from t_attendance t1
left join (
select 
fd_date ,fc_periode,fc_membercode,
if(fd_date < concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" ) , 
concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" )
,fd_date ) mindate
from t_attendance
group by fc_periode ,fc_membercode,fd_date
order by fc_membercode,fc_periode,fd_date 
) t2 on  t2.fc_membercode = t1.fc_membercode and t2.fc_periode = t1.fc_periode and t1.fd_date = t2.fd_date 
where t1.fc_membercode like :member 
and t2.mindate >= :datestart
and t2.mindate <= :datefinish
group by t1.fc_periode,t1.fc_membercode
-- ,t1.fd_date
order by t1.fc_membercode,t1.fc_periode
-- ,t1.fd_date






select fc_periode ,fc_membercode,
max(fd_date) maxdate
from t_attendance 
group by fc_membercode,fc_periode 




select 
a.fc_periode ,a.fd_date ,
g.mindate,
a.fc_membercode,b.fc_membername1,
b.fc_memberattendancetype ,b.fn_memberothernumber1 ,
b.fc_memberposition, 
a.fc_daytype ,
concat(a.fc_shift1,a.fc_shift2,a.fc_shift3) shift ,
a.fc_timeinshift1 timein1,
if(a.fc_timeinshift2 in ("","00:00") or a.fc_timeinshift2 is null , a.fc_timeoutshift1 ,a.fc_timeoutshift2 ) timeout1,
a.fc_timeinshift3 timein2,
a.fc_timeoutshift3 timeout2,
a.fn_totalhour ,a.fn_effectivehour, 
a.fn_ot1 ,a.fn_ot2 ,a.fn_ot3 ,a.fn_ot4 ,a.fn_ot5 ,a.fn_ot6,
a.fn_jumlahshift ,
a.fv_description, 
a.fc_ti ,
a.fl_fastingshift ,
if( d.fc_fastingdayno is not null,concat("Uang makan Puasa (",d.StartDate," - ",d.EndDate,") ",f.fastingshift," hari" ),"" ) uang_makanpuasa
from t_attendance a
left join t_member b on a.fc_membercode = b.fc_membercode
left join t_trxtype c on b.fc_memberposition = c.fc_trxcode and c.fc_trxcode = "EMP_POSITION"
left join (
select fc_fastingdayno, min(fd_date) StartDate, 
       max(fd_date) EndDate, 
       fv_description,
       date_format(fd_date,"%Y%m") fastingPeriode
from t_fastingday 
group by fc_fastingdayno, fv_description,date_format(fd_date,"%Y%m")
order by fd_date
) d on d.fastingPeriode = a.fc_periode 
-- left join (
-- select fc_fastingdayno, min(fd_date) as StartDate, 
--        max(fd_date) as EndDate, 
--        fv_description
-- from t_fastingday   
-- group by fc_fastingdayno, fv_description
-- order by fd_date
-- ) e on e.fc_fastingdayno = d.fc_fastingdayno
left join 
(
select fc_periode ,
fc_membercode,
month(fd_date) monthperiode,
sum(fn_ot1 ) total_ot1,
sum(fn_ot2 ) total_ot2,
sum(fn_ot3 ) total_ot3,
sum(fn_ot4 ) total_ot4,
sum(fn_ot5 ) total_ot5,
sum(fn_ot6 ) total_ot6,
(sum(fn_ot1 ) * 1.5 ) + (sum(fn_ot2 ) * 2 ) + (sum(fn_ot3 ) * 3 ) + (sum(fn_ot4 ) * 4 ) + (sum(fn_ot5 ) * 6 ) + (sum(fn_ot6 ) * 8 ) total_ot,
sum( length(fc_daytype) ) total_workday,
sum( '1' in (fc_shift1,fc_shift2 ,fc_shift3) ) total_shift1,
sum( '2' in (fc_shift1,fc_shift2 ,fc_shift3) ) total_shift2,
sum( '3' in (fc_shift1,fc_shift2 ,fc_shift3) ) total_shift3,
sum(fl_fastingshift ) fastingshift
from t_attendance
group by fc_periode,fc_membercode
order by fc_membercode,fc_periode 
) f on a.fc_membercode = f.fc_membercode and a.fc_periode = f.fc_periode
left join (
select 
fd_date ,fc_periode,fc_membercode,
if(fd_date < concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" ) , 
concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" )
,fd_date ) mindate
from t_attendance
group by fc_periode ,fc_membercode,fd_date
order by fc_membercode,fc_periode,fd_date 
) g on  g.fc_membercode = a.fc_membercode and a.fc_periode = g.fc_periode and a.fd_date = g.fd_date
left join 
(
select fc_periode ,
fc_membercode,
sum(fn_ot1 ) total_ot1,
sum(fn_ot2 ) total_ot2,
sum(fn_ot3 ) total_ot3,
sum(fn_ot4 ) total_ot4,
sum(fn_ot5 ) total_ot5,
sum(fn_ot6 ) total_ot6,
(sum(fn_ot1 ) * 1.5 ) + (sum(fn_ot2 ) * 2 ) + (sum(fn_ot3 ) * 3 ) + (sum(fn_ot4 ) * 4 ) + (sum(fn_ot5 ) * 6 ) + (sum(fn_ot6 ) * 8 ) total_ot,
sum( length(fc_daytype) ) total_workday,
sum( '1' in (fc_shift1,fc_shift2 ,fc_shift3) ) total_shift1,
sum( '2' in (fc_shift1,fc_shift2 ,fc_shift3) ) total_shift2,
sum( '3' in (fc_shift1,fc_shift2 ,fc_shift3) ) total_shift3,
sum(fl_fastingshift ) fastingshift
from t_attendance
where month(fd_date) < month(:datefinsih) 
group by fc_periode,fc_membercode
order by fc_membercode,fc_periode
) h on a.fc_membercode = h.fc_membercode and a.fc_periode = h.fc_periode
where 
a.fc_membercode like :member and b.fl_memberblacklist = :active
and g.mindate >= :datestart and g.mindate <= :datefinsih
or
b.fc_membername1 like :member and b.fl_memberblacklist = :active 
order by a.fc_membercode,a.fc_periode ,a.fd_date




select 
t1.fc_periode ,
t1.fc_membercode,
sum(t1.fn_ot1 ) "1,5",
sum(t1.fn_ot2 ) "2",
sum(t1.fn_ot3 ) "3" ,
sum(t1.fn_ot4 ) "4",
sum(t1.fn_ot5 ) "5",
sum(t1.fn_ot6 ) "6",
(sum(t1.fn_ot1 ) * 1.5 ) + (sum(t1.fn_ot2 ) * 2 ) + (sum(t1.fn_ot3 ) * 3 ) + (sum(t1.fn_ot4 ) * 4 ) + (sum(t1.fn_ot5 ) * 6 ) + (sum(t1.fn_ot6 ) * 8 ) "total OT",
sum( length(t1.fc_daytype) ) "Total workday",
sum( '1' in (t1.fc_shift1,t1.fc_shift2 ,t1.fc_shift3) ) +
sum( '2' in (t1.fc_shift1,t1.fc_shift2 ,t1.fc_shift3) )  " Total shift 1 & 2 ",
sum( '3' in (t1.fc_shift1,t1.fc_shift2 ,t1.fc_shift3) ) " Total shift 3 ",
sum(t1.fl_fastingshift ) "Uang makan puasa (hari)"
from t_attendance t1
left join (
select 
fd_date ,fc_periode,fc_membercode,
if(fd_date < concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" ) , 
concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" )
,fd_date ) mindate
from t_attendance
group by fc_periode ,fc_membercode,fd_date
order by fc_membercode,fc_periode,fd_date 
) t2 on  t2.fc_membercode = t1.fc_membercode and t2.fc_periode = t1.fc_periode and t1.fd_date = t2.fd_date 
where t1.fc_periode = :periode
and t2.mindate >= :datestart
and t2.mindate <= :datefinish
group by t1.fc_periode,t1.fc_membercode
order by t1.fc_membercode,t1.fc_periode



select 
a.fc_periode ,a.fd_date ,
a.fc_membercode,b.fc_membername1,
-- b.fc_memberattendancetype ,b.fn_memberothernumber1 ,
-- b.fc_memberposition, 
-- a.fc_daytype ,
-- concat(a.fc_shift1,a.fc_shift2,a.fc_shift3) shift ,
-- a.fc_timeinshift1 timein1,
-- if(a.fc_timeinshift2 in ("","00:00") or a.fc_timeinshift2 is null , a.fc_timeoutshift1 ,a.fc_timeoutshift2 ) timeout1,
-- a.fc_timeinshift3 timein2,
-- a.fc_timeoutshift3 timeout2,
-- a.fn_totalhour ,a.fn_effectivehour, 
-- a.fn_ot1 ,a.fn_ot2 ,a.fn_ot3 ,a.fn_ot4 ,a.fn_ot5 ,a.fn_ot6,
-- a.fn_jumlahshift ,
-- a.fv_description, 
-- a.fc_ti ,
-- a.fl_fastingshift ,
e.mindate,
if( d.fc_fastingdayno is not null,concat("Uang makan Puasa (",d.StartDate," - ",d.EndDate,") ","gatahu"," hari" ),"" ) uang_makanpuasa
from t_attendance a
left join t_member b on a.fc_membercode = b.fc_membercode
left join t_trxtype c on b.fc_memberposition = c.fc_trxcode and c.fc_trxcode = "EMP_POSITION"
left join (
select fc_fastingdayno, min(fd_date) StartDate, 
       max(fd_date) EndDate, 
       fv_description,
       date_format(fd_date,"%Y%m") fastingPeriode
from t_fastingday 
group by fc_fastingdayno, fv_description,date_format(fd_date,"%Y%m")
order by fd_date
) d on d.fastingPeriode = a.fc_periode 
left join (
select 
fd_date ,fc_periode,fc_membercode,
if(fd_date < concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" ) , 
concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" )
,fd_date ) mindate
from t_attendance
group by fc_periode ,fc_membercode,fd_date
order by fc_membercode,fc_periode,fd_date 
) e on  e.fc_membercode = a.fc_membercode and a.fc_periode = e.fc_periode and a.fd_date = e.fd_date
where 
a.fc_membercode like :member and b.fl_memberblacklist = :active 
and e.mindate >= :datestart and e.mindate <= :datefinish
order by a.fc_membercode,a.fc_periode ,a.fd_date

select 
t1.fc_periode ,
t1.fd_date ,
month(t2.mindate) monthperiode,
t1.fc_membercode,
sum(t1.fn_ot1 ) "1,5",
sum(t1.fn_ot2 ) "2",
sum(t1.fn_ot3 ) "3" ,
sum(t1.fn_ot4 ) "4",
sum(t1.fn_ot5 ) "5",
sum(t1.fn_ot6 ) "6",
(sum(t1.fn_ot1 ) * 1.5 ) + (sum(t1.fn_ot2 ) * 2 ) + (sum(t1.fn_ot3 ) * 3 ) + (sum(t1.fn_ot4 ) * 4 ) + (sum(t1.fn_ot5 ) * 6 ) + (sum(t1.fn_ot6 ) * 8 ) total_ot,
sum( length(substring( t1.fc_daytype,1,1)) ) total_workday,
sum( '1' in (t1.fc_shift1,t1.fc_shift2 ,t1.fc_shift3) ) total_shift1,
sum( '2' in (t1.fc_shift1,t1.fc_shift2 ,t1.fc_shift3) )  total_shift2,
sum( '3' in (t1.fc_shift1,t1.fc_shift2 ,t1.fc_shift3) ) total_shift3,
sum(t1.fl_fastingshift ) "Uang makan puasa (hari)"
from t_attendance t1
left join (
select 
fd_date ,fc_periode,fc_membercode,
if(fd_date < concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" ) , 
concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" )
,fd_date ) mindate
from t_attendance
group by fc_periode ,fc_membercode,fd_date
order by fc_membercode,fc_periode,fd_date 
) t2 on  t2.fc_membercode = t1.fc_membercode and t2.fc_periode = t1.fc_periode and t1.fd_date = t2.fd_date 
where t1.fc_periode = :periode
and t1.fc_membercode like "%TSI 52713%"
and t2.mindate >= :datestart
and t2.mindate <= :datefinish
group by t1.fc_periode,t1.fc_membercode,month(t2.mindate)
order by t1.fc_membercode,t1.fc_periode,month(t2.mindate)




############################################################################################################3

select 
a.fc_periode ,a.fd_date ,
a.fc_membercode,b.fc_membername1,
e.mindate,
if( d.fc_fastingdayno is not null,concat("Uang makan Puasa (",d.StartDate," - ",d.EndDate,") ",f.fastingshift," hari" ),"" ) uang_makanpuasa,
f.total_ot1 fmot1,
f.total_ot2 fmot2,
f.total_ot3 fmot3,
f.total_ot4 fmot4,
f.total_ot5 fmot5,
f.total_ot6 fmot6,
f.total_ot fmot,
g.total_ot1 lmot1,
g.total_ot2 lmot2,
g.total_ot3 lmot3,
g.total_ot4 lmot4,
g.total_ot5 lmot5,
g.total_ot6 lmot6,
g.total_ot lmot
from t_attendance a
left join t_member b on a.fc_membercode = b.fc_membercode
left join t_trxtype c on b.fc_memberposition = c.fc_trxcode and c.fc_trxcode = "EMP_POSITION"
left join (
select fc_fastingdayno, min(fd_date) StartDate, 
       max(fd_date) EndDate, 
       fv_description,
       date_format(fd_date,"%Y%m") fastingPeriode
from t_fastingday 
group by fc_fastingdayno, fv_description,date_format(fd_date,"%Y%m")
) d on  a.fc_periode = d.fastingPeriode
left join (
select 
fd_date ,fc_periode,fc_membercode,
if(fd_date < concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" ) , 
concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" )
,fd_date ) mindate
from t_attendance
group by fc_periode ,fc_membercode,fd_date
order by fc_membercode,fc_periode,fd_date 
) e on  e.fc_membercode = a.fc_membercode and a.fc_periode = e.fc_periode and a.fd_date = e.fd_date
left join (
	select 
	t1.fc_periode ,
	t1.fd_date ,
	month(t2.mindate) monthperiode,
	last_day( min(t2.mindate) ) mindateperiode,
	t1.fc_membercode,
	sum(t1.fn_ot1 ) total_ot1,
	sum(t1.fn_ot2 ) total_ot2,
	sum(t1.fn_ot3 ) total_ot3 ,
	sum(t1.fn_ot4 ) total_ot4,
	sum(t1.fn_ot5 ) total_ot5,
	sum(t1.fn_ot6 ) total_ot6,
	(sum(t1.fn_ot1 ) * 1.5 ) + (sum(t1.fn_ot2 ) * 2 ) + (sum(t1.fn_ot3 ) * 3 ) + (sum(t1.fn_ot4 ) * 4 ) + (sum(t1.fn_ot5 ) * 6 ) + (sum(t1.fn_ot6 ) * 8 ) total_ot,	
	sum( length(substring( t1.fc_daytype,1,1)) ) total_workday,
	sum( '1' in (t1.fc_shift1,t1.fc_shift2 ,t1.fc_shift3) ) total_shift1,
	sum( '2' in (t1.fc_shift1,t1.fc_shift2 ,t1.fc_shift3) )  total_shift2,
	sum( '3' in (t1.fc_shift1,t1.fc_shift2 ,t1.fc_shift3) ) total_shift3,
	sum(t1.fl_fastingshift ) fastingshift
	from t_attendance t1
	left join (
		select 
		fd_date ,fc_periode,fc_membercode,
		if(fd_date < concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" ) , 
		concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" )
		,fd_date ) mindate,
		min( '2020-04-16' ) mindateperiode
		from t_attendance
		group by fc_periode ,fc_membercode,fd_date
		order by fc_membercode,fc_periode,fd_date 
		) t2 on  t2.fc_membercode = t1.fc_membercode and t2.fc_periode = t1.fc_periode and t1.fd_date = t2.fd_date 
	where 
-- 	t2.mindate >= :datestart
-- 	and t2.mindate <= last_day( :datestart )	
	t2.mindate >= last_day( :datestart )
	and t2.mindate <= :datefinish
	group by t1.fc_periode,t1.fc_membercode
	order by t1.fc_membercode,t1.fc_periode
) f on  f.fc_membercode = a.fc_membercode and a.fc_periode = f.fc_periode 
left join (
	select 
	t1.fc_periode ,
	t1.fd_date ,
	month(t2.mindate) monthperiode,
	last_day( min(t2.mindate) ) mindateperiode,
	t1.fc_membercode,
	sum(t1.fn_ot1 ) total_ot1,
	sum(t1.fn_ot2 ) total_ot2,
	sum(t1.fn_ot3 ) total_ot3 ,
	sum(t1.fn_ot4 ) total_ot4,
	sum(t1.fn_ot5 ) total_ot5,
	sum(t1.fn_ot6 ) total_ot6,
	(sum(t1.fn_ot1 ) * 1.5 ) + (sum(t1.fn_ot2 ) * 2 ) + (sum(t1.fn_ot3 ) * 3 ) + (sum(t1.fn_ot4 ) * 4 ) + (sum(t1.fn_ot5 ) * 6 ) + (sum(t1.fn_ot6 ) * 8 ) total_ot,	
	sum( length(substring( t1.fc_daytype,1,1)) ) total_workday,
	sum( '1' in (t1.fc_shift1,t1.fc_shift2 ,t1.fc_shift3) ) total_shift1,
	sum( '2' in (t1.fc_shift1,t1.fc_shift2 ,t1.fc_shift3) )  total_shift2,
	sum( '3' in (t1.fc_shift1,t1.fc_shift2 ,t1.fc_shift3) ) total_shift3,
	sum(t1.fl_fastingshift ) fastingshift
	from t_attendance t1
	left join (
		select 
		fd_date ,fc_periode,fc_membercode,
		if(fd_date < concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" ) , 
		concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" )
		,fd_date ) mindate
		from t_attendance
		group by fc_periode ,fc_membercode,fd_date
		order by fc_membercode,fc_periode,fd_date 
		) t2 on  t2.fc_membercode = t1.fc_membercode and t2.fc_periode = t1.fc_periode and t1.fd_date = t2.fd_date 
	where 
	t2.mindate >= :datestart
	and t2.mindate <= :datefinish
	group by t1.fc_periode,t1.fc_membercode
	order by t1.fc_membercode,t1.fc_periode
) g on  g.fc_membercode = a.fc_membercode and a.fc_periode = g.fc_periode 
where 
a.fc_membercode like :member and b.fl_memberblacklist = :active 
and e.mindate >= :datestart and e.mindate <= :datefinish
order by a.fc_membercode,a.fc_periode ,a.fd_date






-- current script
select 
a.fc_periode ,a.fd_date ,
a.fc_membercode,b.fc_membername1,
b.fc_memberattendancetype ,b.fn_memberothernumber1 ,
b.fc_memberposition, 
a.fc_daytype ,
concat(a.fc_shift1,a.fc_shift2,a.fc_shift3) shift ,
a.fc_timeinshift1 timein1,
if(a.fc_timeinshift2 in ("","00:00") or a.fc_timeinshift2 is null , a.fc_timeoutshift1 ,a.fc_timeoutshift2 ) timeout1,
a.fc_timeinshift3 timein2,
a.fc_timeoutshift3 timeout2,
a.fn_totalhour ,a.fn_effectivehour, 
a.fn_ot1 ,a.fn_ot2 ,a.fn_ot3 ,a.fn_ot4 ,a.fn_ot5 ,a.fn_ot6,
a.fn_jumlahshift ,
a.fv_description, 
a.fc_ti ,
a.fl_fastingshift ,
e.mindate,
if( d.fc_fastingdayno is not null,concat("Uang makan Puasa (",d.StartDate," - ",d.EndDate,") ",f.fastingshift," hari" ),"" ) uang_makanpuasa,
f.total_ot1 fmot1,
f.total_ot2 fmot2,
f.total_ot3 fmot3,
f.total_ot4 fmot4,
f.total_ot5 fmot5,
f.total_ot6 fmot6,
f.total_ot fmot,
g.total_ot1 lmot1,
g.total_ot2 lmot2,
g.total_ot3 lmot3,
g.total_ot4 lmot4,
g.total_ot5 lmot5,
g.total_ot6 lmot6,
g.total_ot lmot,
(g.total_ot - f.total_ot) rlmot,
g.total_workday,
g.total_shift1,
g.total_shift2,
g.total_shift3
from t_attendance a
left join t_member b on a.fc_membercode = b.fc_membercode
left join t_trxtype c on b.fc_memberposition = c.fc_trxcode and c.fc_trxcode = "EMP_POSITION"
left join (
select fc_fastingdayno, min(fd_date) StartDate, 
       max(fd_date) EndDate, 
       fv_description,
       date_format(fd_date,"%Y%m") fastingPeriode
from t_fastingday 
group by fc_fastingdayno, fv_description,date_format(fd_date,"%Y%m")
) d on  a.fc_periode = d.fastingPeriode
left join (
select 
fd_date ,fc_periode,fc_membercode,
if(fd_date < concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" ) , 
concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" )
,fd_date ) mindate
from t_attendance
group by fc_periode ,fc_membercode,fd_date
order by fc_membercode,fc_periode,fd_date 
) e on  e.fc_membercode = a.fc_membercode and a.fc_periode = e.fc_periode and a.fd_date = e.fd_date
left join (
	select 
	t1.fc_periode ,
	t1.fd_date ,
	month(t2.mindate) monthperiode,
	last_day( min(t2.mindate) ) mindateperiode,
	t1.fc_membercode,
	sum(t1.fn_ot1 ) total_ot1,
	sum(t1.fn_ot2 ) total_ot2,
	sum(t1.fn_ot3 ) total_ot3 ,
	sum(t1.fn_ot4 ) total_ot4,
	sum(t1.fn_ot5 ) total_ot5,
	sum(t1.fn_ot6 ) total_ot6,
	(sum(t1.fn_ot1 ) * 1.5 ) + (sum(t1.fn_ot2 ) * 2 ) + (sum(t1.fn_ot3 ) * 3 ) + (sum(t1.fn_ot4 ) * 4 ) + (sum(t1.fn_ot5 ) * 6 ) + (sum(t1.fn_ot6 ) * 8 ) total_ot,	
	sum( length(substring( t1.fc_daytype,1,1)) ) total_workday,
	sum( '1' in (t1.fc_shift1,t1.fc_shift2 ,t1.fc_shift3) ) total_shift1,
	sum( '2' in (t1.fc_shift1,t1.fc_shift2 ,t1.fc_shift3) )  total_shift2,
	sum( '3' in (t1.fc_shift1,t1.fc_shift2 ,t1.fc_shift3) ) total_shift3,
	sum(t1.fl_fastingshift ) fastingshift
	from t_attendance t1
	left join (
		select 
		fd_date ,fc_periode,fc_membercode,
		if(fd_date < concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" ) , 
		concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" )
		,fd_date ) mindate
		from t_attendance
		group by fc_periode ,fc_membercode,fd_date
		order by fc_membercode,fc_periode,fd_date 
		) t2 on  t2.fc_membercode = t1.fc_membercode and t2.fc_periode = t1.fc_periode and t1.fd_date = t2.fd_date 
	where 
	t2.mindate >= :datestart
	and t2.mindate <= last_day( :datestart )
	group by t1.fc_periode,t1.fc_membercode
	order by t1.fc_membercode,t1.fc_periode
) f on  f.fc_membercode = a.fc_membercode and a.fc_periode = f.fc_periode 
left join (
	select 
	t1.fc_periode ,
	t1.fd_date ,
	month(t2.mindate) monthperiode,
	last_day( min(t2.mindate) ) mindateperiode,
	t1.fc_membercode,
	sum(t1.fn_ot1 ) total_ot1,
	sum(t1.fn_ot2 ) total_ot2,
	sum(t1.fn_ot3 ) total_ot3 ,
	sum(t1.fn_ot4 ) total_ot4,
	sum(t1.fn_ot5 ) total_ot5,
	sum(t1.fn_ot6 ) total_ot6,
	(sum(t1.fn_ot1 ) * 1.5 ) + (sum(t1.fn_ot2 ) * 2 ) + (sum(t1.fn_ot3 ) * 3 ) + (sum(t1.fn_ot4 ) * 4 ) + (sum(t1.fn_ot5 ) * 6 ) + (sum(t1.fn_ot6 ) * 8 ) total_ot,	
	sum( length(substring( t1.fc_daytype,1,1)) ) total_workday,
	sum( '1' in (t1.fc_shift1,t1.fc_shift2 ,t1.fc_shift3) ) total_shift1,
	sum( '2' in (t1.fc_shift1,t1.fc_shift2 ,t1.fc_shift3) )  total_shift2,
	sum( '3' in (t1.fc_shift1,t1.fc_shift2 ,t1.fc_shift3) ) total_shift3,
	sum(t1.fl_fastingshift ) fastingshift
	from t_attendance t1
	left join (
		select 
		fd_date ,fc_periode,fc_membercode,
		if(fd_date < concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" ) , 
		concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" )
		,fd_date ) mindate
		from t_attendance
		group by fc_periode ,fc_membercode,fd_date
		order by fc_membercode,fc_periode,fd_date 
		) t2 on  t2.fc_membercode = t1.fc_membercode and t2.fc_periode = t1.fc_periode and t1.fd_date = t2.fd_date 
	where 
	t2.mindate >= :datestart
	and t2.mindate <= :datefinish
	group by t1.fc_periode,t1.fc_membercode
	order by t1.fc_membercode,t1.fc_periode
) g on  g.fc_membercode = a.fc_membercode and a.fc_periode = g.fc_periode 
where 
a.fc_membercode like :member and b.fl_memberblacklist = :active 
and e.mindate >= :datestart and e.mindate <= :datefinish
order by a.fc_membercode,a.fc_periode ,a.fd_date






select 
a.fc_branch,a.fc_periode ,a.fd_dategenerate ,
a.fc_membercode,b.fc_membername1,
b.fc_memberposition, 
a.fd_date ,a.fc_daytype ,
a.fc_shift1 ,a.fc_shift2,a.fc_shift3,
a.fc_timeinshift1,a.fc_timeoutshift1 ,
a.fc_timeinshift2,a.fc_timeoutshift2 ,
a.fc_timeinshift3,a.fc_timeoutshift3 ,
a.fd_realdatein ,a.fd_realdateout,
a.fn_totalhour ,a.fn_effectivehour, 
a.fn_ot1 ,a.fn_ot2 ,a.fn_ot3 ,a.fn_ot4 ,a.fn_ot5 ,a.fn_ot6,
a.fc_ti,
a.fv_description, 
a.fl_ischecked1 ,
a.fl_ischeked2 
from t_attendance a
left join t_member b on a.fc_membercode = b.fc_membercode
left join t_trxtype c on b.fc_memberposition = c.fc_trxcode and c.fc_trxcode = "EMP_POSITION"
left join ( 
select fc_periode,fc_membercode,fd_date from t_attendance  
where  fd_date  >= NOW() and fd_date <= NOW()
group by fc_membercode,fc_periode
) d 
on a.fc_membercode = d.fc_membercode and a.fc_periode = d.fc_periode
where 
a.fc_membercode like "%%" and b.fl_memberblacklist ="F" and a.fd_date  >= NOW() and a.fd_date <= NOW()
or  
a.fc_membercode like "%%" and b.fl_memberblacklist = "F"
and a.fc_periode = d.fc_periode 
and  a.fd_date  <= CONCAT(substring(d.fc_periode ,1,4),'-',substring(d.fc_periode ,5,6),'-','15')
or
b.fc_membername1 like "%%" and b.fl_memberblacklist = "F" and a.fd_date  >= NOW() and a.fd_date <= NOW()
or  
b.fc_membername1 like "%%" and b.fl_memberblacklist = "F"
and a.fc_periode = d.fc_periode 
and  a.fd_date  <= CONCAT(substring(d.fc_periode ,1,4),'-',substring(d.fc_periode ,5,6),'-','15')
order by a.fc_membercode ,a.fc_periode ,a.fd_date 






#leave script
select 
fc_trxid,
fv_trxdescription,
concat(date_format( now(),"%Y" ),"-",fv_trxdescription)
from t_trxtype 
where fc_trxid = "ADD_LEAVE_DATE"
where :date >= (select concat(date_format( now(),"%Y" ),"-",fv_trxdescription) from t_trxtype where fc_trxid = "RESET_LEAVE_DATE")
group by fc_trxid ,fc_trxcode 
order by date1

select 
fc_trxaction1 ,
if( concat(date_format( now(),"%Y" ),"-",fv_trxdescription) > (select concat(date_format( now(),"%Y" ),"-",fv_trxdescription) from t_trxtype where fc_trxid = "RESET_LEAVE_DATE")
, date_sub(concat(date_format( now(),"%Y" ),"-",fv_trxdescription), interval 1 year ) 
, concat(date_format( now(),"%Y" ),"-",fv_trxdescription)) date_addleave 
from t_trxtype 
where fc_trxid = "ADD_LEAVE_DATE"
order by date_addleave

select 
a.fc_trxaction1 ,
if( concat(date_format( now(),"%Y" ),"-",a.fv_trxdescription) > concat(date_format( now(),"%Y" ),"-",b.fv_trxdescription)
, date_sub(concat(date_format( now(),"%Y" ),"-",a.fv_trxdescription), interval 1 year ) 
, concat(date_format( now(),"%Y" ),"-",a.fv_trxdescription)) addleave_date ,
concat(date_format( now(),"%Y" ),"-",b.fv_trxdescription) resetleave_date
from t_trxtype a
left join 
(
select fv_trxdescription
from t_trxtype where fc_trxid = "RESET_LEAVE_DATE"
) b on a.fv_trxdescription > b.fv_trxdescription or a.fv_trxdescription < b.fv_trxdescription
where fc_trxid = "ADD_LEAVE_DATE"
and 
if( concat(date_format( now(),"%Y" ),"-",a.fv_trxdescription) > concat(date_format( now(),"%Y" ),"-",b.fv_trxdescription)
, date_sub(concat(date_format( now(),"%Y" ),"-",a.fv_trxdescription), interval 1 year ) 
, concat(date_format( now(),"%Y" ),"-",a.fv_trxdescription)) between :datestart and :datefinish
order by addleave_date

-- used script
select 
sum(a.fc_trxaction1) amount_leave ,
if( concat(date_format( now(),"%Y" ),"-",a.fv_trxdescription) > concat(date_format( now(),"%Y" ),"-",b.fv_trxdescription)
, date_sub(concat(date_format( now(),"%Y" ),"-",a.fv_trxdescription), interval 1 year ) 
, concat(date_format( now(),"%Y" ),"-",a.fv_trxdescription)) addleave_date ,
concat(date_format( now(),"%Y" ),"-",b.fv_trxdescription) resetleave_date_thisyear,
date_sub(concat(date_format( now(),"%Y" ),"-",b.fv_trxdescription),interval 1 year) resetleave_date_pastyear
from t_trxtype a
left join 
(
select fv_trxdescription
from t_trxtype where fc_trxid = "RESET_LEAVE_DATE"
) b on a.fv_trxdescription > b.fv_trxdescription or a.fv_trxdescription < b.fv_trxdescription
where fc_trxid = "ADD_LEAVE_DATE"
and 
:datestart <= if( concat(date_format( now(),"%Y" ),"-",a.fv_trxdescription) > concat(date_format( now(),"%Y" ),"-",b.fv_trxdescription)
, date_sub(concat(date_format( now(),"%Y" ),"-",a.fv_trxdescription), interval 1 year ) 
, concat(date_format( now(),"%Y" ),"-",a.fv_trxdescription))
and 
if( concat(date_format( now(),"%Y" ),"-",a.fv_trxdescription) > concat(date_format( now(),"%Y" ),"-",b.fv_trxdescription)
, date_sub(concat(date_format( now(),"%Y" ),"-",a.fv_trxdescription), interval 1 year ) 
, concat(date_format( now(),"%Y" ),"-",a.fv_trxdescription)) <= :now
order by addleave_date

select date_sub( concat(date_format( now(),"%Y" ),"-",fv_trxdescription),interval 1 YEAR ) from t_trxtype where fc_trxid = "RESET_LEAVE_DATE" 


update t_member set fn_memberleavecount = 0

update 
t_member a 
left join 
(
select 
a.fc_membercode ,
a.fc_membername1,
sum(b.amount_leave) amount_leave
from t_member a
left join 
(
select 
t1.fc_trxaction1 amount_leave ,
t1.fv_trxdescription fv_trxdescription1 ,
t2.fv_trxdescription fv_trxdescription2 
from t_trxtype t1
left join 
(
select fv_trxdescription
from t_trxtype where fc_trxid = "RESET_LEAVE_DATE"
) t2 on t1.fv_trxdescription > t2.fv_trxdescription or t1.fv_trxdescription < t2.fv_trxdescription
where fc_trxid = "ADD_LEAVE_DATE"
) b on 
a.fd_memberjoindate <= if( concat(date_format( now(),"%Y" ),"-",b.fv_trxdescription1) > concat(date_format( now(),"%Y" ),"-",b.fv_trxdescription2)
, date_sub(concat(date_format( now(),"%Y" ),"-",b.fv_trxdescription1), interval 1 year ) 
, concat(date_format( now(),"%Y" ),"-",b.fv_trxdescription1))
and 
if( concat(date_format( now(),"%Y" ),"-",b.fv_trxdescription1) > concat(date_format( now(),"%Y" ),"-",b.fv_trxdescription2)
, date_sub(concat(date_format( now(),"%Y" ),"-",b.fv_trxdescription1), interval 1 year ) 
, concat(date_format( now(),"%Y" ),"-",b.fv_trxdescription1)) <= :now
group by fc_membercode
) b on a.fc_membercode = b.fc_membercode
set a.fn_memberleavecount = b.amount_leave





select 
a.fc_membername1 ,
a.fc_membercode ,
b.amount_leave,
from 
t_member a 
left join 
(
	select 
	a.fc_membercode ,
	a.fc_membername1,
	sum(b.amount_leave) amount_leave,
	b.leave_date_add
	from t_member a
	left join 
	(
		select 
		t1.fc_trxaction1 amount_leave ,
		t1.fv_trxdescription fv_trxdescription1 ,
		t2.fv_trxdescription fv_trxdescription2 ,
		if( concat(date_format( now(),"%Y" ),"-",t1.fv_trxdescription) > concat(date_format( now(),"%Y" ),"-",t2.fv_trxdescription)
		, date_sub(concat(date_format( now(),"%Y" ),"-",t1.fv_trxdescription), interval 1 year ) 
		, concat(date_format( now(),"%Y" ),"-",t1.fv_trxdescription)) leave_date_add
		from t_trxtype t1
		left join 
		(
			select fv_trxdescription
			from t_trxtype where fc_trxid = "RESET_LEAVE_DATE"
		) t2 on t1.fv_trxdescription > t2.fv_trxdescription or t1.fv_trxdescription < t2.fv_trxdescription
		where fc_trxid = "ADD_LEAVE_DATE"
	) b on 
	a.fd_memberjoindate <= b.leave_date_add
	and 
	b.leave_date_add <= :now
	group by fc_membercode
) b on a.fc_membercode = b.fc_membercode



update 
t_member a
left join 
(
	select 
	fc_membercode,
	count(fd_leavedate) countleave,
	fd_leavedate 
	from t_leave
	where 
	fd_leavedate >= (
		select date_sub( concat(date_format( now(),"%Y" ),"-",fv_trxdescription),interval 1 year )
		from t_trxtype where fc_trxid = "RESET_LEAVE_DATE"
	)
	and 
	fd_leavedate <= (
		select concat(date_format( now(),"%Y" ),"-",fv_trxdescription)
		from t_trxtype where fc_trxid = "RESET_LEAVE_DATE"
	)
	and fc_leavetype in ("L")
	group by fc_membercode
) b on a.fc_membercode = b.fc_membercode
set 
a.fn_memberleavecount =  if( b.countleave >= a.fn_memberleaveadd, ( ( a.fn_memberleavecount + a.fn_memberleaveadd  )- b.countleave) , a.fn_memberleavecount ),
a.fn_memberleaveadd = if( b.countleave >= a.fn_memberleaveadd, 0 , a.fn_memberleaveadd - b.countleave )
where a.fc_membercode = b.fc_membercode

select 
fc_trxcode ,
fc_trxid  ,
fv_trxdescription 
from t_trxtype 
where fc_trxid = "ADD_LEAVE_DATE"



select count(*) leavethismonth
from t_leave where 
fc_membercode = "TS170718" and month(fd_leavecreatedate) = month(now())

update t_member set 
fn_memberleaveadd = :amountleave,
fd_expiredleaveadd = :date
where fc_membercode = :member


update t_member set fn_memberleaveadd = 0 where fd_expiredleaveadd < now()

insert into t_oldmember select * from t_member where fc_membercode = :member;
update t_member  set fn_memberothernumber1 = fn_memberothernumber1 - 1 where fn_memberothernumber1 > :noselip;
delete from t_member  where fc_membercode = :member;

insert into t_member select * from t_backupmember limit 10
delete from t_member





update t_member a
left join t_trxtype b on a.fc_memberposition = b.fv_trxdescription and b.fc_trxid = "EMP_POSITION"
set a.fc_memberposition = coalesce(b.fc_trxcode,"")




select 
a.fc_periode,a.fc_membercode,
b.fc_membername1,b.fv_trxdescription,b.fc_divisionname,
b.fn_memberothernumber1,cast(b.uangmakanpuasa as char) uangmakanpuasa,
b.fc_memberattendancetype ,

a.fd_date,b.periodedate,a.fc_daytype,
concat(a.fc_shift1,a.fc_shift2,a.fc_shift3) shift ,
a.fc_timeinshift1 timein1,
if(a.fc_timeinshift2 in ("","00:00") or a.fc_timeinshift2 is null , a.fc_timeoutshift1 ,a.fc_timeoutshift2 ) timeout1,
a.fc_timeinshift3 timein2,
a.fc_timeoutshift3 timeout2,
a.fn_totalhour ,a.fn_effectivehour, 
a.fn_ot1 ,a.fn_ot2 ,a.fn_ot3 ,a.fn_ot4 ,a.fn_ot5 ,a.fn_ot6,
a.fn_jumlahshift ,
a.fc_ti ,
a.fv_description, 

b.total_ot1,
b.total_ot2,
b.total_ot3,
b.total_ot4,
b.total_ot5,
b.total_ot6,
b.totalshift,

b.total_workday,
b.total_shift1,b.total_shift2,b.total_shift3,
b.total_ot alltotalot,
c.total_ot fptotalot,
b.total_ot - c.total_ot ltotalot
from t_attendance a
left join
(
select 
a.fc_periode,a.fd_date ,
a.fc_membercode,b.fc_membername1 , b.fn_memberothernumber1 ,b.fc_memberattendancetype ,
c.fv_trxdescription,b.fc_memberposition ,d.fc_divisionname,b.fc_divisioncode,
e.periodedate,
month(e.periodedate) monthperiode,
sum(a.fn_ot1 ) total_ot1,
sum(a.fn_ot2 ) total_ot2,
sum(a.fn_ot3 ) total_ot3 ,
sum(a.fn_ot4 ) total_ot4,
sum(a.fn_ot5 ) total_ot5,
sum(a.fn_ot6 ) total_ot6,
(sum(a.fn_ot1 ) * 1.5 ) + (sum(a.fn_ot2 ) * 2 ) + (sum(a.fn_ot3 ) * 3 ) + (sum(a.fn_ot4 ) * 4 ) + (sum(a.fn_ot5 ) * 6 ) + (sum(a.fn_ot6 ) * 8 ) total_ot,	
sum( length(substring( a.fc_daytype,1,1)) ) total_workday,
sum( '1' in (a.fc_shift1,a.fc_shift2 ,a.fc_shift3) ) total_shift1,
sum( '2' in (a.fc_shift1,a.fc_shift2 ,a.fc_shift3) )  total_shift2,
sum( '3' in (a.fc_shift1,a.fc_shift2 ,a.fc_shift3) ) total_shift3,
sum(a.fn_jumlahshift ) totalshift,
concat( "uang makan puasa tanggal ",f.mindate, " s/d ", f.maxdate," : ",sum(a.fl_fastingshift )," hari" ) uangmakanpuasa
from t_attendance a
left join t_member b on a.fc_membercode = b.fc_membercode
left join t_trxtype c on b.fc_memberposition = c.fc_trxcode and c.fc_trxid = "EMP_POSITION"
left join t_division d on b.fc_divisioncode = d.fc_divisioncode 
left join ( 
	select fc_membercode,fd_date,fc_periode ,if(fd_date < concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" ) , 
	concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" ) ,fd_date ) 
	periodedate from t_attendance
) e on e.fc_membercode = a.fc_membercode and a.fd_date = e.fd_date and a.fc_periode = e.fc_periode
left join (
	select min(fd_date) mindate,max(fd_date) maxdate,
	fv_description 
	from t_fastingday group by fc_fastingdayno 
) f on month(e.periodedate) = month(f.mindate) and year(e.periodedate) = year(f.mindate) 
	or month(e.periodedate) = month(f.maxdate) and year(e.periodedate) = year(f.maxdate)
	where e.periodedate >= :datestart and e.periodedate <= :datefinish
	group by a.fc_membercode ,a.fc_periode 
)b on a.fc_membercode = b.fc_membercode and a.fc_periode = b.fc_periode
left join
(
select 
a.fc_periode,a.fd_date ,
a.fc_membercode,b.fc_membername1,
(sum(a.fn_ot1 ) * 1.5 ) + (sum(a.fn_ot2 ) * 2 ) + (sum(a.fn_ot3 ) * 3 ) + (sum(a.fn_ot4 ) * 4 ) + (sum(a.fn_ot5 ) * 6 ) + (sum(a.fn_ot6 ) * 8 ) total_ot
from t_attendance a
left join t_member b on a.fc_membercode = b.fc_membercode
left join t_trxtype c on b.fc_memberposition = c.fc_trxcode and c.fc_trxid = "EMP_POSITION"
left join t_division d on b.fc_divisioncode = d.fc_divisioncode 
left join ( 
	select fc_membercode,fd_date,fc_periode ,if(fd_date < concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" ) , 
	concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" ) ,fd_date ) 
	periodedate from t_attendance
) e on e.fc_membercode = a.fc_membercode and a.fd_date = e.fd_date and a.fc_periode = e.fc_periode
left join (
	select min(fd_date) mindate,max(fd_date) maxdate,
	fv_description 
	from t_fastingday group by fc_fastingdayno 
) f on month(e.periodedate) = month(f.mindate) and year(e.periodedate) = year(f.mindate) 
	or month(e.periodedate) = month(f.maxdate) and year(e.periodedate) = year(f.maxdate)
	where e.periodedate >= :datestart and e.periodedate <= last_day(:datestart)
group by a.fc_membercode ,a.fc_periode 
)c on a.fc_membercode = c.fc_membercode and a.fc_periode = c.fc_periode
where 
a.fc_membercode  like :member and  b.periodedate >= :datestart and b.periodedate <= :datefinish    and b.fc_divisionname like coalesce(:division,'') and b.fv_trxdescription like :position
or 
b.fc_membername1 like :member and b.periodedate >= :datestart and b.periodedate <= :datefinish   and b.fc_divisionname like coalesce(:division,'') and b.fv_trxdescription like :position
order by b.fn_memberothernumber1,a.fc_membercode,a.fd_date,b.periodedate







select 
a.fc_branch,a.fc_periode ,a.fd_dategenerate , 
a.fc_membercode,b.fc_membername1, 
c.fv_trxdescription fc_memberposition, 
a.fd_date ,a.fc_daytype , 
a.fc_shift1 ,a.fc_shift2,a.fc_shift3,
a.fc_timeinshift1,a.fc_timeoutshift1 , 
a.fc_timeinshift2,a.fc_timeoutshift2 , 
a.fc_timeinshift3,a.fc_timeoutshift3 , 
a.fn_jumlahshift ,
a.fd_realdatein ,a.fd_realdateout, 
a.fn_totalhour ,a.fn_effectivehour, 
a.fn_ot1 ,a.fn_ot2 ,a.fn_ot3 ,a.fn_ot4 ,a.fn_ot5 ,a.fn_ot6, 
a.fc_ti, 
a.fv_description, 
a.fl_ischecked1 , 
a.fl_ischeked2 
from t_attendance a 
left join t_member b on a.fc_membercode = b.fc_membercode  
left join t_trxtype c on b.fc_memberposition = c.fc_trxcode and c.fc_trxid = "EMP_POSITION" 
left join ( 
select fc_periode,fc_membercode,fd_date from t_attendance         
where fd_date >= :datestart
and fd_date <= :datefinish
group by
fc_membercode,
fc_periode ) d on
a.fc_membercode = d.fc_membercode
and a.fc_periode = d.fc_periode
where
a.fc_membercode like :member
and b.fl_memberblacklist = :active
and a.fd_date >= :datestart
and a.fd_date <= :datefinish
or a.fc_membercode like :member
and b.fl_memberblacklist = :active
and a.fc_periode = d.fc_periode
and a.fd_date <= CONCAT(substring(d.fc_periode , 1, 4), "-", substring(d.fc_periode , 5, 6), "-", "15")
or b.fc_membername1 like :member and c.fv_trxdescription like :position
and a.fd_date >= :datestart
and a.fd_date <= :datefinish
or b.fc_membername1 like :member and c.fv_trxdescription like :position
and a.fc_periode = d.fc_periode
and a.fd_date <= CONCAT(substring(d.fc_periode , 1, 4), "-", substring(d.fc_periode , 5, 6), "-", "15")
order by
a.fc_membercode ,
a.fc_periode ,
a.fd_date


##################################################################################################################################################
-- o8 juni 2020 perbaikan attendance
select a.* ,
b.fv_trxdescription 
from
t_leave a
left join t_trxtype b on a.fc_leavetype = b.fc_trxcode and b.fc_trxid = "LEAVE_TYPE"

update t_attendance a 
left join t_leave b on a.fd_date = b.fd_leavedate 
set 
a.fv_description = coalesce( 
case when b.fc_leavetype = "L" then 
"an leave" 
when b.fc_leavetype = "PE" then 
"permit early" 
when b.fc_leavetype = "PH" then 
"permit holiday" 
when b.fc_leavetype = "PL" then 
"permit late"
when b.fc_leavetype = "PLO" then 
"permit leave office" 
end ,a.fv_description ,"")
a.fc_daytype = if (b.fc_leavetype in ("L","PH"),"",a.fc_daytype ), 
a.fc_shift1 = if (b.fc_leavetype in ("L","PH"),"",a.fc_shift1 ), 
a.fc_shift2 = if (b.fc_leavetype in ("L","PH"),"",a.fc_shift2 ), 
a.fc_shift3 = if (b.fc_leavetype in ("L","PH"),"",a.fc_shift3 ), 
a.fc_workinshift1 = if (b.fc_leavetype in ("L","PH"),NULL,a.fc_workinshift1 ), 
a.fc_workoutshift1 = if (b.fc_leavetype in ("L","PH"),NULL,a.fc_workoutshift1 ),  
a.fn_totalbreakshift1 = if (b.fc_leavetype in ("L","PH"),0,a.fn_totalbreakshift1 ),
a.fc_workinshift2 = if (b.fc_leavetype in ("L","PH"),NULL,a.fc_workinshift2 ), 
a.fc_workoutshift2 = if (b.fc_leavetype in ("L","PH"),NULL,a.fc_workoutshift2 ),  
a.fn_totalbreakshift2 = if (b.fc_leavetype in ("L","PH"),0,a.fn_totalbreakshift2 ),
a.fc_workinshift3 = if (b.fc_leavetype in ("L","PH"),NULL,a.fc_workinshift3 ), 
a.fc_workoutshift3 = if (b.fc_leavetype in ("L","PH"),NULL,a.fc_workoutshift3 ),  
a.fn_totalbreakshift3 = if (b.fc_leavetype in ("L","PH"),0,a.fn_totalbreakshift3 ),
a.fc_timeinshift1 = if (b.fc_leavetype in ("L","PH"),NULL,a.fc_timeinshift1 ), 
a.fc_timeoutshift1 = if (b.fc_leavetype in ("L","PH"),NULL,a.fc_timeoutshift1 ),  
a.fc_timeinshift2 = if (b.fc_leavetype in ("L","PH"),NULL,a.fc_timeinshift2 ), 
a.fc_timeoutshift2 = if (b.fc_leavetype in ("L","PH"),NULL,a.fc_timeoutshift2 ),
a.fc_timeinshift3 = if (b.fc_leavetype in ("L","PH"),NULL,a.fc_timeinshift3 ), 
a.fc_timeoutshift3 = if (b.fc_leavetype in ("L","PH"),NULL,a.fc_timeoutshift3 )  
where fc_periode = date_format(:datestart,"%Y%m") and b.fc_membercode = a.fc_membercode;  


select a.fd_date,b.fc_membercode ,b.fc_leavetype
from t_attendance a
inner join t_leave b on a.fc_membercode = b.fc_membercode and a.fd_date = b.fd_leavedate 
left join t_trxtype c on b.fc_leavetype = c.fc_trxcode and c.fc_trxid = "LEAVE_TYPE"

update t_attendance a
inner join t_leave b on a.fc_membercode = b.fc_membercode and a.fd_date = b.fd_leavedate 
inner join t_trxtype c on b.fc_leavetype = c.fc_trxcode and c.fc_trxid = "LEAVE_TYPE"
set 
a.fc_shift1 = b.fc_shift1 ,
a.fc_timeinshift1 = b.fc_timeinshift1 ,
a.fc_timeoutshift1 = b.fc_timeoutshift1, 
a.fc_shift2 = b.fc_shift2 ,
a.fc_timeinshift2 = b.fc_timeinshift2 ,
a.fc_timeoutshift2 = b.fc_timeoutshift2,
a.fc_shift3 = b.fc_shift3 ,
a.fc_timeinshift3 = b.fc_timeinshift3 ,
a.fc_timeoutshift3 = b.fc_timeoutshift3,
a.fv_description = c.fv_trxdescription,
a.fc_daytype = if(b.fc_leavetype in ("AL","IP"), "",a.fc_daytype )
where a.fd_date = :date and a.fc_membercode = :membercode;


update t_attendance a
set 
 fn_totalhourshift1 = TIME_TO_SEC( 
if( fc_workinshift1 > fc_workoutshift1 , 
subtime(addtime(fc_workoutshift1 ,"24:00"),fc_workinshift1 ) , 
 subtime(fc_workoutshift1,fc_workinshift1 ) 
 	) )/60/60, 
 fn_totalhourshift2 = TIME_TO_SEC( 
if( fc_workinshift2 > fc_workoutshift2 , 
 subtime(addtime(fc_workoutshift2 ,"24:00"),fc_workinshift2 ) , 
 subtime(fc_workoutshift2,fc_workinshift2 ) 
 ) )/60/60, 
 fn_totalhourshift3 = TIME_TO_SEC( 
 if( fc_workinshift3 > fc_workoutshift3 , 
 subtime(addtime(fc_workoutshift3 ,"24:00"),fc_workinshift3 ) , 
subtime(fc_workoutshift3,fc_workinshift3 ) 
 ) )/60/60 
where a.fd_date = :date and a.fc_membercode = :membercode;


update t_attendance a 
left join t_fastingday b on a.fd_date = b.fd_date 
left join t_dailytimerange c on  c.fc_timerangecode = concat(a.fc_shift1,a.fc_shift2,a.fc_shift3 ) 
set
a.fl_fastingshift = if( b.fd_date is not null and c.fl_fastingshift is true , 1 , 0) 
where 
a.fd_date = b.fd_date ;

