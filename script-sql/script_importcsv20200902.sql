select 
"001" fc_branch, SYSDATE() fd_dategenerate, date_format('2020-09-11',"%Y%m") fc_periode,
date_add('2020-08-16', INTERVAL 0 day) fd_date, b.fc_membercode , b.fc_memberattendancetype, 
left(a.fc_date1,1) fc_shift1 , 
case  when length(a.fc_date1) > 1  then mid(a.fc_date1,2,1)  else ""  end fc_shift2, 
case when length(a.fc_date1) > 2  then right(a.fc_date1,1) else "" end fc_shift3,
@fc_daytype := case when a.fc_date1 = "" then "" when c.fc_status = "P" then "H+" when c.fc_status = "U" then "H" else "N" end fc_daytype ,
d.fc_workin fc_workinshift1,d.fc_workout fc_workoutshift1, 
@fn_totalbreakshift1 := coalesce(TIME_TO_SEC( if( d.fc_workbreakin > d.fc_workbreakout , subtime(d.fc_workbreakin,d.fc_workbreakout ) , 
subtime(d.fc_workbreakout,d.fc_workbreakin ) ) )/60/60,NULL) fn_totalbreakshift1, 
e.fc_workin fc_workinshift2,e.fc_workout fc_workoutshift2, 
@fn_totalbreakshift2 := coalesce(TIME_TO_SEC( if( e.fc_workbreakin > e.fc_workbreakout , subtime(e.fc_workbreakin,e.fc_workbreakout ) , 
subtime(e.fc_workbreakout,e.fc_workbreakin ) ) )/60/60,NULL) fn_totalbreakshift2 ,
f.fc_workin fc_workinshift3,f.fc_workout fc_workoutshift3, 
@fn_totalbreakshift3 := coalesce(TIME_TO_SEC( if( f.fc_workbreakin > f.fc_workbreakout , subtime(f.fc_workbreakin,f.fc_workbreakout ) , 
subtime(f.fc_workbreakout,f.fc_workbreakin ) ) )/60/60,NULL) fn_totalbreakshift3, 
d.fc_workin fc_timeinshift1,d.fc_workout fc_timeoutshift1, 
e.fc_workin fc_timeinshift2,e.fc_workout fc_timeoutshift2, 
f.fc_workin fc_timeinshift3,f.fc_workout fc_timeoutshift3,
@fn_totalhourshift1 := coalesce(TIME_TO_SEC( if( d.fc_workin > d.fc_workout , subtime(addtime(d.fc_workout,"24:00"),d.fc_workin ) , 
subtime(d.fc_workout,d.fc_workin ) ) )/60/60,NULL) fn_totalhourshift1,
@fn_totalhourshift2 := coalesce(TIME_TO_SEC( if( e.fc_workin > e.fc_workout , subtime(addtime(e.fc_workout ,"24:00"),e.fc_workin ) , 
subtime(e.fc_workout,e.fc_workin ) ) )/60/60,NULL) fn_totalhourshift2,
@fn_totalhourshift3 := coalesce(TIME_TO_SEC( if( f.fc_workin > f.fc_workout , subtime(addtime(f.fc_workout ,"24:00"),f.fc_workin ), 
subtime(f.fc_workout,f.fc_workin ) ) )/60/60,NULL) fn_totalhourshift3,
@fn_totalhourshift1 fn_totaltimeshift1, @fn_totalhourshift2 fn_totaltimeshift2, @fn_totalhourshift3 fn_totaltimeshift3,
@fn_totalhour := @fn_totalhourshift1  + coalesce(@fn_totalhourshift2,0 ) + coalesce(@fn_totalhourshift3,0) fn_totalhour,
@fn_effectivehour := @fn_totalhour - ( coalesce(@fn_totalbreakshift1,0) + coalesce(@fn_totalbreakshift2,0) + coalesce(@fn_totalbreakshift3,0)  )   fn_effectivehour ,
"" fc_ti,
@fn_ot1 :=  case when @fc_daytype = "N" then 
case when @fn_effectivehour - (@fn_totalhourshift1 - @fn_totalbreakshift1 ) >= 1 then 1 
when @fn_effectivehour - (@fn_totalhourshift1 - @fn_totalbreakshift1 ) >= 0 then 
@fn_effectivehour - (@fn_totalhourshift1 - @fn_totalbreakshift1 ) 
else 0 end 
else if(@fc_daytype is null or @fc_daytype = "" ,NULL,0) end fn_ot1  ,
@fn_ot2 := case when @fc_daytype = "N" then 
case when @fn_ot1 >= 1 then @fn_effectivehour - @fn_ot1 - (@fn_totalhourshift1 - @fn_totalbreakshift1 ) 
else 0 end when @fc_daytype in ("H","O") then case when @fn_effectivehour - 7 >= 0 then 7 
when @fn_effectivehour - 7 <= 0 then @fn_effectivehour else 0 end 
else if(@fc_daytype is  null or @fc_daytype = "" ,NULL,0) end fn_ot2,
@fn_ot3 :=  case when @fc_daytype in ("H","O") then case when @fn_effectivehour - 7 >= 1 then 1 
when @fn_effectivehour - 7 <= 1 then if(@fn_effectivehour - 7 > 0, @fn_effectivehour - 7 ,0 )  
else 0 end else if(@fc_daytype is  null or @fc_daytype = "" ,NULL,0) end fn_ot3 ,
@fn_ot4 := case when @fc_daytype in ("H","O") then case when @fn_ot3 >= 1 then @fn_effectivehour - @fn_ot3 - 7 else 0 end 
when @fc_daytype in ("H+") then case when @fn_effectivehour - 7 >= 0 then 7 when @fn_effectivehour - 7 <= 0 then @fn_effectivehour else 0 end 
else if(@fc_daytype is  null or @fc_daytype = "" ,NULL,0) end fn_ot4 ,
@fn_ot5 := case when @fc_daytype in ("H+") then case when @fn_effectivehour - 7 >= 1 then 1 
when @fn_effectivehour - 7 <= 1 then if( @fn_effectivehour - 7 > 0 ,@fn_effectivehour - 7,0) else 0 end 
else if(@fc_daytype is  null or @fc_daytype = "" ,NULL,0) end fn_ot5 ,
@fn_ot6 :=case when @fc_daytype in ("H+") then case when @fn_ot5 >=1 then 
@fn_effectivehour - @fn_ot5 - 7 else 0 end else if(@fc_daytype is  null or @fc_daytype = "" ,NULL,0) end fn_ot6 
from  t_tempdavidrotateshift  a 
left join t_member b on trim( lower(a.fc_nameis) ) = trim( lower(b.fc_membername1) ) 
left join t_holiday c on date_add('2020-08-16', INTERVAL 0 day) = c.fd_date 
left join t_dailytimerange d on d.fc_timerangecode = left(a.fc_date1,1) 
left join t_dailytimerange e on e.fc_timerangecode = case  when length(a.fc_date1) > 1  then mid(a.fc_date1,2,1)  else ""  end 
left join t_dailytimerange f on f.fc_timerangecode = case when length(a.fc_date1) > 2  then right(a.fc_date1,1) else "" end 
where a.fc_blank <> "O" 