alter table t_attendance 
add primary key (fd_date,fc_membercode,fc_periode)

insert into t_attendance(fc_branch ,fd_dategenerate,fc_periode ,fd_date, fc_membercode,fc_memberattendancetype ,fc_shift1 ,fc_shift2 ,fc_shift3,fc_daytype )
select 
'001' fc_branch,
SYSDATE() fd_dategenerate, 
date_format(:datestart,"%Y%m") fc_periode,
date_add(:datestart, INTERVAL :i day) fd_date,
b.fc_membercode ,
b.fc_memberattendancetype, 
left(:fc_date,1) fc_shift1 ,
case 
	when length(:fc_date) > 1  then mid(:fc_date,2,1) 
	else ''
end fc_shift2,
case 
	when length(:fc_date) > 2  then right(:fc_date,1) 
	else ''
end fc_shift3,case 
when :fc_date = '' then 'O'
when c.fc_status = 'P' then 'H+'
when c.fc_status = 'U' then 'H'
when dayofweek( date_add(:datestart, INTERVAL :i day) ) = 7 then 'S'
else 'N' end fc_daytype
from t_tempdavidrotateshift a 
left join t_member b on a.fc_nameis = b.fc_membername1
left join t_holiday c on date_add(:datestart, INTERVAL :i day) = c.fd_date 
where a.fc_blank <> 'O'
on duplicate key update 
fc_shift1 = left(:fc_date,1),
fc_shift2 = 
case 
	when length(:fc_date) > 1  then mid(:fc_date,2,1) 
	else ''
end,
fc_shift3 = 
case 
	when length(:fc_date) > 2  then right(:fc_date,1) 
	else ''
end


update t_attendance a
left join t_dailytimerange b on b.fc_timerangecode = a.fc_shift1 
left join t_dailytimerange c on c.fc_timerangecode = a.fc_shift2
left join t_dailytimerange d on d.fc_timerangecode = a.fc_shift3 
set 
	a.fc_workinshift1 = COALESCE(b.fc_workin,"00:00"),
	a.fc_workoutshift1 = COALESCE(b.fc_workout,"00:00") ,
	a.fn_totalbreakshift1 =COALESCE(TIME_TO_SEC( 
			if( b.fc_workbreakin > b.fc_workbreakout ,
				subtime(b.fc_workbreakin,b.fc_workbreakout ) ,
				subtime(b.fc_workbreakout,b.fc_workbreakin )
			) )/60/60,0), 
	a.fc_workinshift2 = COALESCE(c.fc_workin,"00:00"),
	a.fc_workoutshift2 = COALESCE(c.fc_workout,"00:00") ,
	a.fn_totalbreakshift2 = COALESCE(TIME_TO_SEC( 
			if( b.fc_workbreakin > b.fc_workbreakout ,
				subtime(c.fc_workbreakin,c.fc_workbreakout ) ,
				subtime(c.fc_workbreakout,c.fc_workbreakin )
			) )/60/60,0) ,
	a.fc_workinshift3 = COALESCE(d.fc_workin,"00:00"),
	a.fc_workoutshift3 = COALESCE(d.fc_workout,"00:00") ,
	a.fn_totalbreakshift3 = COALESCE (TIME_TO_SEC( 
			if( d.fc_workbreakin > d.fc_workbreakout ,
				subtime(d.fc_workbreakin,d.fc_workbreakout ) ,
				subtime(d.fc_workbreakout,d.fc_workbreakin )
			) 
		)/60/60,0 )
where a.fc_periode = date_format(:datestart,"%Y%m")


update t_attendance 
set 
	fn_totalhourshift1 = TIME_TO_SEC( 
			if( fc_workinshift1 > fc_workoutshift1 ,
				subtime(addtime(fc_workoutshift1 ,"24:00"),fc_workinshift1 ) ,
				subtime(fc_workoutshift1,fc_workinshift1 )
			) )/60/60,
	fn_totalhourshift2 = TIME_TO_SEC( 
			if( fc_workinshift2 > fc_workoutshift2 ,
				subtime(addtime(fc_workoutshift2 ,"24:00"),fc_workinshift2 ) ,
				subtime(fc_workoutshift2,fc_workinshift2 )
			) )/60/60,
	fn_totalhourshift3 = TIME_TO_SEC( 
			if( fc_workinshift3 > fc_workoutshift3 ,
				subtime(addtime(fc_workoutshift3 ,"24:00"),fc_workinshift3 ) ,
				subtime(fc_workoutshift3,fc_workinshift3 )
			) )/60/60
where fc_periode = date_format(:datestart,"%Y%m");


update t_attendance a 
left join t_tempodavidattendancedate b 
on a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d")
and a.fc_membercode = concat('TSI ',b.fc_membercode)
set 
a.fd_realdatein = STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),
a.fd_realdateout = STR_TO_DATE(b.fc_dateout ,"%m/%d/%Y %H:%i:%s")
-- where a.fc_periode = '201912'
where a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d")
and a.fc_membercode = concat('TSI ',b.fc_membercode)

UPDATE t_attendance a
left join t_dailytimerange b on left(concat(fc_shift1 ,fc_shift2,fc_shift3),1) = b.fc_timerangecode 
left join t_dailytimerange c on right(concat(fc_shift1 ,fc_shift2,fc_shift3),1) = c.fc_timerangecode 
set
a.fc_datein = concat(DATE_FORMAT(a.fd_realdatein ,'%Y-%m-%d') ,' ',coalesce(b.fc_workin,'00:00')) ,
a.fc_dateout = concat(DATE_FORMAT(a.fd_realdateout ,'%Y-%m-%d') ,' ',coalesce(c.fc_workout ,'00:00'))
where a.fc_periode = '201912'


UPDATE t_attendance a
left join t_dailytimerange b on left(concat(fc_shift1 ,fc_shift2,fc_shift3),1) = b.fc_timerangecode 
left join t_dailytimerange c on right(concat(fc_shift1 ,fc_shift2,fc_shift3),1) = c.fc_timerangecode 
set
a.fc_datein = 
case
when 
	 a.fd_realdatein < concat(DATE_FORMAT(a.fd_realdatein ,'%Y-%m-%d') ,' ',coalesce(b.fc_workin,'00:00'))
	then concat(DATE_FORMAT(a.fd_realdatein ,'%Y-%m-%d') ,' ',coalesce(b.fc_workin,'00:00'))
else a.fd_realdatein
end,
a.fc_dateout = 
case 
when time_to_sec(timediff(a.fd_realdateout, concat(DATE_FORMAT(a.fd_realdateout ,'%Y-%m-%d') ,' ',coalesce(c.fc_workout ,'00:00') ) ) )/60/60 > 1.5
	then a.fd_realdateout
when a.fd_realdateout > concat(DATE_FORMAT(a.fd_realdateout ,'%Y-%m-%d') ,' ',coalesce(c.fc_workout ,'00:00'))
 then  concat(DATE_FORMAT(a.fd_realdateout ,'%Y-%m-%d') ,' ',coalesce(c.fc_workout ,'00:00')) 
when a.fd_realdateout < concat(DATE_FORMAT(a.fd_realdateout ,'%Y-%m-%d') ,' ',coalesce(c.fc_workout ,'00:00'))
 then a.fd_realdateout
else a.fd_realdateout 
end
where a.fc_periode = '201912'

update x_attendance 
set 
fn_totalhour = time_to_sec(timediff(fc_dateout ,fc_datein ) ) / 60 /60,
fn_effectivehour = fn_totalhour - (fn_totalbreakshift1 + fn_totalbreakshift2 +fn_totalbreakshift3 )

update x_attendance 
set
fn_effectivehour = 
case 
	when fc_ti in ('TI1','NR1') then 
		fn_effectivehour + 1
	when fc_ti in ('TI2','NR2') then 
		fn_effectivehour + 2
	when fc_ti in ('TI3','NR3') then 
		fn_effectivehour + 3
	else 
		fn_effectivehour
end 

-- Overtime Formula
update t_attendance
set 
fn_ot1 =
case when fc_daytype = 'N' and fn_effectivehour > (fn_totalhourshift1 - fn_totalbreakshift1 ) then
	case when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) >= 1
	then 1
	else 0
	end
when fc_daytype = 'S' and fn_effectivehour > ( ( fn_totalhourshift1 - 2 ) - fn_totalbreakshift1 ) then
	case when fn_effectivehour - ( ( fn_totalhourshift1 - 2 )- fn_totalbreakshift1 ) >= 1
	then 1
	else 0
	end
else 0
end,
fn_ot2 =
case when fc_daytype = 'N' and fn_ot1 >= 1 then 
	fn_effectivehour - fn_ot1 - (fn_totalhourshift1 - fn_totalbreakshift1 ) 
case when fc_daytype = 'S' and fn_ot1 >= 1 then 
	fn_effectivehour - fn_ot1 - ( ( fn_totalhourshift1 - 2 ) - fn_totalbreakshift1 )
when fc_daytype in ('H','O') and 
		fn_effectivehour - (fn_totalhourshift2 - fn_totalbreakshift2 ) - (fn_totalhourshift3 - fn_totalbreakshift3 ) >= (fn_totalhourshift1 - fn_totalbreakshift1 )
	then (fn_totalhourshift1 - fn_totalbreakshift1 )
when fc_daytype in ('H','O') and 
		fn_effectivehour - (fn_totalhourshift2 - fn_totalbreakshift2 ) - (fn_totalhourshift3 - fn_totalbreakshift3 ) < (fn_totalhourshift1 - fn_totalbreakshift1 )
	then fn_effectivehour - (fn_totalhourshift2 - fn_totalbreakshift2 ) - (fn_totalhourshift3 - fn_totalbreakshift3 )
else 0 end,
fn_ot3 = 
case when fc_daytype in ('H','O') and fn_effectivehour > (fn_totalhourshift1 - fn_totalbreakshift1 ) then
	case when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) >= 1
	then 1
	else 0
	end
else 0
end,
fn_ot4 = 
case when fc_daytype in ('H','O') and fn_ot3 >= 1 then 
	fn_effectivehour -  fn_ot3 - (fn_totalhourshift1 - fn_totalbreakshift1 ) 
when fc_daytype = 'H+' and 
		fn_effectivehour - (fn_totalhourshift2 - fn_totalbreakshift2 ) - (fn_totalhourshift3 - fn_totalbreakshift3 ) >= (fn_totalhourshift1 - fn_totalbreakshift1 )
	then (fn_totalhourshift1 - fn_totalbreakshift1 )
when fc_daytype = 'H+' and 
		fn_effectivehour - (fn_totalhourshift2 - fn_totalbreakshift2 ) - (fn_totalhourshift3 - fn_totalbreakshift3 ) < (fn_totalhourshift1 - fn_totalbreakshift1 )
	then fn_effectivehour - (fn_totalhourshift2 - fn_totalbreakshift2 ) - (fn_totalhourshift3 - fn_totalbreakshift3 )
else 0 end,
fn_ot5 =
case when fc_daytype = 'H+' and fn_effectivehour > (fn_totalhourshift1 - fn_totalbreakshift1 ) then
	case when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) >= 1
	then 1
	else 0
	end
else 0
end,
fn_ot6 =
case when fc_daytype = 'H+' and fn_ot5 >= 1 then 
	fn_effectivehour -  fn_ot5 - (fn_totalhourshift1 - fn_totalbreakshift1 ) 
else 0 end

select 
-- fc_periode ,fd_dategenerate ,
a.fc_membercode,b.fc_membername1 ,
-- b.fc_memberposition, 
fd_date ,fc_daytype ,
concat(fc_shift1,fc_shift2,fc_shift3) shift ,
fd_realdatein ,fd_realdateout,
fn_totalhour ,fn_effectivehour, 
-- fn_ot1 ,fn_ot2 ,fn_ot3 ,fn_ot4 ,fn_ot5 ,fn_ot6,
-- fv_description, 
fc_ti 
from x_attendance a
left join t_member b on a.fc_membercode = b.fc_membercode
left join t_trxtype c on b.fc_memberposition = c.fc_trxcode and c.fc_trxcode = "EMP_POSITION"
order by fd_date 




UPDATE x_attendance a
left join t_dailytimerange b on left(concat(fc_shift1 ,fc_shift2,fc_shift3),1) = b.fc_timerangecode 
left join t_dailytimerange c on right(concat(fc_shift1 ,fc_shift2,fc_shift3),1) = c.fc_timerangecode 
set
a.fc_datein = 
case
when 
	 a.fd_realdatein < concat(DATE_FORMAT(a.fd_realdatein ,'%Y-%m-%d') ,' ',coalesce(b.fc_workin,'00:00'))
	then concat(DATE_FORMAT(a.fd_realdatein ,'%Y-%m-%d') ,' ',coalesce(b.fc_workin,'00:00'))
else a.fd_realdatein
end,
a.fc_dateout = 
case 
when time_to_sec(timediff(a.fd_realdateout, concat(DATE_FORMAT(a.fd_realdateout ,'%Y-%m-%d') ,' ',coalesce(c.fc_workout ,'00:00') ) ) )/60/60 > 1.5
	then a.fd_realdateout
when a.fd_realdateout > concat(DATE_FORMAT(a.fd_realdateout ,'%Y-%m-%d') ,' ',coalesce(c.fc_workout ,'00:00'))
 then  concat(DATE_FORMAT(a.fd_realdateout ,'%Y-%m-%d') ,' ',coalesce(c.fc_workout ,'00:00')) 
when a.fd_realdateout < concat(DATE_FORMAT(a.fd_realdateout ,'%Y-%m-%d') ,' ',coalesce(c.fc_workout ,'00:00'))
 then a.fd_realdateout
else a.fd_realdateout 
end
where a.fc_periode = '201912'

update x_attendance 
set 
fn_totalhour = time_to_sec(timediff(fc_dateout ,fc_datein ) ) / 60 /60,
fn_effectivehour = fn_totalhour - (fn_totalbreakshift1 + fn_totalbreakshift2 +fn_totalbreakshift3 )

update x_attendance 
set 
fn_ot1 = 0,
fn_ot2 = 0,
fn_ot3 = 0,
fn_ot4 = 0,
fn_ot5 = 0;
-- new formula for shift and non shift 
update t_attendance
set
fn_ot1 =
case when (fn_totalhourshift1 - fn_totalbreakshift1 ) = 8  then 
	case when fc_daytype = 'N' then
		case when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) >= 1 then
			1
		when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) >= 0 then 
			fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 )
		else 0 end
	else 0 end 
when (fn_totalhourshift1 - fn_totalbreakshift1 ) = 7 then
	case when fc_daytype = 'N' then
		case when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) >= 1 then
			1
		when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) >= 0 then 
			fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 )
		else 0 end
	when fc_daytype = 'S' then
		case when fn_effectivehour > 5 then
			case when fn_effectivehour - 5 >= 1 then
				1
			when fn_effectivehour - 5 <= 1 then 
				fn_effectivehour - 5
			else 0 end
		else 0 end
	else 0 end
else 0 end,
fn_ot2 =
case when (fn_totalhourshift1 - fn_totalbreakshift1 ) = 8  then 
	case when fc_daytype = 'N' then
		case when fn_ot1 >=1 then
			fn_effectivehour - fn_ot1 - (fn_totalhourshift1 - fn_totalbreakshift1 )
		else 0 end
	when fc_daytype in ('S','H','O') then
		case when fn_effectivehour - 7 >= 0 then
			7
		when fn_effectivehour - 7 <= 0 then 
			fn_effectivehour 
		else 0 end
	else 0 end 
when (fn_totalhourshift1 - fn_totalbreakshift1 ) = 7 then
	case when fc_daytype = 'N' then
		case when fn_ot1 >= 1 then 
			fn_effectivehour - fn_ot1 - (fn_totalhourshift1 - fn_totalbreakshift1 )
		else 0 end
	when fc_daytype = 'S' then
		case when fn_ot1 >= 1 then
			case when fn_effectivehour - 5 >= 0 then
				fn_effectivehour - fn_ot1 - 5
			when fn_effectivehour - 5 <= 0 then
				fn_effectivehour
			else 0 end
		else 0 end
	when fc_daytype in ('H','O') then
		case when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) >= 0 then
			7
		when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) <= 0 then 
			fn_effectivehour 
		else 0 end
	else 0 end 
else 0 end,
fn_ot3 =
case when (fn_totalhourshift1 - fn_totalbreakshift1 ) = 8  then 
	case when fc_daytype in ('S','H','O') then
		case when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) >= 1 then
			1
		when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) <= 1 then 
			fn_effectivehour - 7
		else 0 end
	else 0 end 
when (fn_totalhourshift1 - fn_totalbreakshift1 ) = 7 then
	case when fc_daytype in ('H','O') then
		case when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) >= 1 then
			1
		when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) <= 1 then 
			fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 )
		else 0 end
	else 0 end
else 0 end,
fn_ot4 =
case when (fn_totalhourshift1 - fn_totalbreakshift1 ) = 8  then 
	case when fc_daytype in ('S','H','O') then
		case when fn_ot3 >=1 then
			fn_effectivehour - fn_ot3 - 7
		else 0 end
	when fc_daytype in ('H+') then
		case when fn_effectivehour - 7 >= 0 then
			7
		when fn_effectivehour - 7 <= 0 then 
			fn_effectivehour 
		else 0 end
	else 0 end 
when (fn_totalhourshift1 - fn_totalbreakshift1 ) = 7 then
	case when fc_daytype in ('H','O') then
		case when fn_ot3 >= 1 then 
			fn_effectivehour - fn_ot3 - (fn_totalhourshift1 - fn_totalbreakshift1 )
		else 0 end
	when fc_daytype in ('H+') then
		case when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) >= 0 then
			7
		when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) <= 0 then 
			fn_effectivehour 
		else 0 end
	else 0 end 
else 0 end,
fn_ot5 =
case when (fn_totalhourshift1 - fn_totalbreakshift1 ) = 8  then 
	case when fc_daytype in ('H+') then
		case when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) >= 1 then
			1
		when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) <= 1 then 
			fn_effectivehour - 7
		else 0 end
	else 0 end 
when (fn_totalhourshift1 - fn_totalbreakshift1 ) = 7 then
	case when fc_daytype in ('H+') then
		case when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) >= 1 then
			1
		when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) <= 1 then 
			fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 )
		else 0 end
	else 0 end
else 0 end,
fn_ot6 =
case when (fn_totalhourshift1 - fn_totalbreakshift1 ) = 8  then 
	case when fc_daytype in ('H+') then
		case when fn_ot5 >=1 then
			fn_effectivehour - fn_ot5 - 7
		else 0 end
	else 0 end 
when (fn_totalhourshift1 - fn_totalbreakshift1 ) = 7 then
	case when fc_daytype in ('H+') then
		case when fn_ot5 >= 1 then 
			fn_effectivehour - fn_ot5 - (fn_totalhourshift1 - fn_totalbreakshift1 )
		else 0 end
	else 0 end 
else 0 end

###############################################################################################################################################################
-- Refactoring Here 
###############################################################################################################################################################
-- update real date in date out
update t_attendance a 
left join t_tempodavidattendancedate b 
on a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d")
and a.fc_membercode = concat('TSI ',b.fc_membercode) 
set 
a.fd_realdatein = STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),
a.fd_realdateout = STR_TO_DATE(b.fc_dateout ,"%m/%d/%Y %H:%i:%s")
where a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d")
and a.fc_membercode = concat('TSI ',b.fc_membercode) ; 
-- update datein date out
update t_attendance a 
left join t_tempodavidattendancedate b 
on a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d")
and a.fc_membercode = concat('TSI ',b.fc_membercode)
left join t_dailytimerange c on left(concat(a.fc_shift1 ,a.fc_shift2,a.fc_shift3),1) = c.fc_timerangecode 
left join t_dailytimerange d on right(concat(a.fc_shift1 ,a.fc_shift2,a.fc_shift3),1) = d.fc_timerangecode 
set 
a.fc_datein = 
case
when 
	 a.fd_realdatein < concat(DATE_FORMAT(a.fd_realdatein ,'%Y-%m-%d') ,' ',coalesce(c.fc_workin,'00:00'))
	then concat(DATE_FORMAT(a.fd_realdatein ,'%Y-%m-%d') ,' ',coalesce(c.fc_workin,'00:00'))
else a.fd_realdatein
end,
a.fc_dateout = 
case 
when time_to_sec(timediff(a.fd_realdateout, concat(DATE_FORMAT(a.fd_realdateout ,'%Y-%m-%d') ,' ',coalesce(d.fc_workout ,'00:00') ) ) )/60/60 > 1.5
	then a.fd_realdateout
when a.fd_realdateout > concat(DATE_FORMAT(a.fd_realdateout ,'%Y-%m-%d') ,' ',coalesce(d.fc_workout ,'00:00'))
 then  concat(DATE_FORMAT(a.fd_realdateout ,'%Y-%m-%d') ,' ',coalesce(d.fc_workout ,'00:00')) 
when a.fd_realdateout < concat(DATE_FORMAT(a.fd_realdateout ,'%Y-%m-%d') ,' ',coalesce(d.fc_workout ,'00:00'))
 then a.fd_realdateout
else a.fd_realdateout 
end
where a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d")
and a.fc_membercode = concat('TSI ',b.fc_membercode) ;
-- update total hour
update t_attendance a 
left join t_tempodavidattendancedate b 
on a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d")
and a.fc_membercode = concat('TSI ',b.fc_membercode)
set 
a.fn_totalhour = time_to_sec(timediff(a.fc_dateout ,a.fc_datein ) ) / 60 /60
where a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d")
and a.fc_membercode = concat('TSI ',b.fc_membercode) ;
-- update effective hour
update t_attendance a 
left join t_tempodavidattendancedate b 
on a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d")
and a.fc_membercode = concat('TSI ',b.fc_membercode)
set 
a.fn_effectivehour = a.fn_totalhour - (a.fn_totalbreakshift1 + a.fn_totalbreakshift2 + a.fn_totalbreakshift3 )
where a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d")
and a.fc_membercode = concat('TSI ',b.fc_membercode) ;
-- update ot1
update t_attendance a 
left join t_tempodavidattendancedate b 
on a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d")
and a.fc_membercode = concat('TSI ',b.fc_membercode)
set 
a.fn_ot1 =
case when (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) = 8  then 
	case when a.fc_daytype = 'N' then
		case when a.fn_effectivehour - ( a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) >= 1 then
			1
		when a.fn_effectivehour - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) >= 0 then 
			a.fn_effectivehour - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 )
		else 0 end
	else 0 end 
when (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) = 7 then
	case when a.fc_daytype = 'N' then
		case when a.fn_effectivehour - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) >= 1 then
			1
		when a.fn_effectivehour - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) >= 0 then 
			a.fn_effectivehour - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 )
		else 0 end
	when a.fc_daytype = 'S' then
		case when a.fn_effectivehour > 5 then
			case when a.fn_effectivehour - 5 >= 1 then
				1
			when a.fn_effectivehour - 5 <= 1 then 
				a.fn_effectivehour - 5
			else 0 end
		else 0 end
	else 0 end
else 0 end
where a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d")
and a.fc_membercode = concat('TSI ',b.fc_membercode) ;
-- update ot 2
update t_attendance a 
left join t_tempodavidattendancedate b 
on a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d")
and a.fc_membercode = concat('TSI ',b.fc_membercode)
set 
a.fn_ot2 =
case when (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) = 8  then 
	case when a.fc_daytype = 'N' then
		case when a.fn_ot1 >=1 then
			a.fn_effectivehour - a.fn_ot1 - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 )
		else 0 end
	when a.fc_daytype in ('S','H','O') then
		case when a.fn_effectivehour - 7 >= 0 then
			7
		when a.fn_effectivehour - 7 <= 0 then 
			a.fn_effectivehour 
		else 0 end
	else 0 end 
when (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) = 7 then
	case when a.fc_daytype = 'N' then
		case when a.fn_ot1 >= 1 then 
			a.fn_effectivehour - a.fn_ot1 - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 )
		else 0 end
	when a.fc_daytype = 'S' then
		case when a.fn_ot1 >= 1 then
			case when a.fn_effectivehour - 5 >= 0 then
				a.fn_effectivehour - a.fn_ot1 - 5
			when a.fn_effectivehour - 5 <= 0 then
				a.fn_effectivehour
			else 0 end
		else 0 end
	when a.fc_daytype in ('H','O') then
		case when a.fn_effectivehour - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) >= 0 then
			7
		when a.fn_effectivehour - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) <= 0 then 
			a.fn_effectivehour 
		else 0 end
	else 0 end 
else 0 end
where a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d")
and a.fc_membercode = concat('TSI ',b.fc_membercode) ;
-- update ot 3
update t_attendance a 
left join t_tempodavidattendancedate b 
on a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d")
and a.fc_membercode = concat('TSI ',b.fc_membercode)
set
a.fn_ot3 =
case when (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) = 8  then 
	case when a.fc_daytype in ('S','H','O') then
		case when a.fn_effectivehour - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) >= 1 then
			1
		when a.fn_effectivehour - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) >= 0 then 
			a.fn_effectivehour - 7
		else 0 end
	else 0 end 
when (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) = 7 then
	case when a.fc_daytype in ('H','O') then
		case when a.fn_effectivehour - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) >= 1 then
			1
		when a.fn_effectivehour - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) >= 0 then 
			a.fn_effectivehour - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 )
		else 0 end
	else 0 end
else 0 end
where a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d")
and a.fc_membercode = concat('TSI ',b.fc_membercode) ;
-- update ot 4
update t_attendance a 
left join t_tempodavidattendancedate b 
on a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d")
and a.fc_membercode = concat('TSI ',b.fc_membercode)
set
fn_ot4 =
case when (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) = 8  then 
	case when a.fc_daytype in ('S','H','O') then
		case when a.fn_ot3 >=1 then
			a.fn_effectivehour - fn_ot3 - 7
		else 0 end
	when a.fc_daytype in ('H+') then
		case when a.fn_effectivehour - 7 >= 0 then
			7
		when a.fn_effectivehour - 7 <= 0 then 
			a.fn_effectivehour 
		else 0 end
	else 0 end 
when (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) = 7 then
	case when a.fc_daytype in ('H','O') then
		case when a.fn_ot3 >= 1 then 
			a.fn_effectivehour - a.fn_ot3 - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 )
		else 0 end
	when a.fc_daytype in ('H+') then
		case when a.fn_effectivehour - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) >= 0 then
			7
		when a.fn_effectivehour - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) <= 0 then 
			a.fn_effectivehour 
		else 0 end
	else 0 end 
else 0 end
where a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d")
and a.fc_membercode = concat('TSI ',b.fc_membercode) ;
-- update ot 5
update t_attendance a 
left join t_tempodavidattendancedate b 
on a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d")
and a.fc_membercode = concat('TSI ',b.fc_membercode)
set
a.fn_ot5 =
case when (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) = 8  then 
	case when a.fc_daytype in ('H+') then
		case when a.fn_effectivehour - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) >= 1 then
			1
		when a.fn_effectivehour - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) >= 0 then 
			a.fn_effectivehour - 7
		else 0 end
	else 0 end 
when (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) = 7 then
	case when a.fc_daytype in ('H+') then
		case when a.fn_effectivehour - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) >= 1 then
			1
		when a.fn_effectivehour - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) >= 0 then 
			a.fn_effectivehour - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 )
		else 0 end
	else 0 end
else 0 end
where a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d")
and a.fc_membercode = concat('TSI ',b.fc_membercode) ;
-- update ot 6
update t_attendance a 
left join t_tempodavidattendancedate b 
on a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d")
and a.fc_membercode = concat('TSI ',b.fc_membercode)
set
a.fn_ot6 =
case when (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) = 8  then 
	case when a.fc_daytype in ('H+') then
		case when a.fn_ot5 >=1 then
			a.fn_effectivehour - a.fn_ot5 - 7
		else 0 end
	else 0 end 
when (a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) = 7 then
	case when a.fc_daytype in ('H+') then
		case when a.fn_ot5 >= 1 then 
			a.fn_effectivehour - a.fn_ot5 - (a.fn_totalhourshift1 - a.fn_totalbreakshift1 )
		else 0 end
	else 0 end 
else 0 end
where a.fd_date = DATE_FORMAT( STR_TO_DATE(b.fc_datein ,"%m/%d/%Y %H:%i:%s"),"%Y-%m-%d")
and a.fc_membercode = concat('TSI ',b.fc_membercode) ;


select 
fc_periode ,fd_dategenerate ,
a.fc_membercode,b.fc_membername1,
b.fc_memberattendancetype ,b.fn_memberothernumber1 ,
b.fc_memberposition, 
a.fd_date ,a.fc_daytype ,
concat(fc_shift1,fc_shift2,fc_shift3) shift ,
fc_shift1 ,fc_shift2,fc_shift3,
fc_workinshift1 timein1,
if(fc_workoutshift2 in ("","00:00",NULL), fc_workoutshift1 ,fc_workoutshift2 ) timeout1,
fc_workinshift3 timein2,
fc_workoutshift3 timeout2,
fd_realdatein ,fd_realdateout,
fn_totalhour ,fn_effectivehour, 
fn_ot1 ,fn_ot2 ,fn_ot3 ,fn_ot4 ,fn_ot5 ,fn_ot6,
length(concat(fc_shift1,fc_shift2,fc_shift3)) jmlh_shift,
fv_description, 
fc_ti ,
( select count(fd_date) from t_attendance where fc_daytype not in ("") and fc_membercode = b.fc_membercode and fc_periode = a.fc_periode ) totalworkday,
( fn_ot1 + fn_ot2 + fn_ot3 + fn_ot4 + fn_ot5 + fn_ot6 )  total_ot 
from t_attendance a
left join t_member b on a.fc_membercode = b.fc_membercode
left join t_trxtype c on b.fc_memberposition = c.fc_trxcode and c.fc_trxcode = "EMP_POSITION"
where 
a.fc_membercode like :member and b.fl_memberblacklist = 'F' and a.fd_date  >= :datestart and a.fd_date <= :datefinish
or 
a.fc_membercode like :member and b.fl_memberblacklist = 'F' and a.fc_periode = DATE_FORMAT(:datestart,"%Y%m" )   
or
b.fc_membername1 like :member and b.fl_memberblacklist = 'F' and a.fd_date  >= :datestart and a.fd_date <= :datefinish
or 
b.fc_membername1  like :member and b.fl_memberblacklist = 'F' and a.fc_periode = DATE_FORMAT(:datestart,"%Y%m" )   
order by b.fc_membercode,a.fd_date


DATE_FORMAT(NOW(),"%Y%m" )

-- REVISI
update t_attendance a 
set 
a.fc_timeinshift1 = if( a.fc_timeinshift1 = "" OR a.fc_timeinshift1 is NULL,a.fc_workinshift1 ,a.fc_timeinshift1 ),
a.fc_timeoutshift1 = if( a.fc_timeoutshift1 = "" OR a.fc_timeoutshift1 is NULL,a.fc_workoutshift1 ,a.fc_timeoutshift1 ),
a.fc_timeinshift2 = if( a.fc_timeinshift2 = "" or a.fc_timeinshift2 is NULL,a.fc_workinshift2 ,a.fc_timeinshift2 ),
a.fc_timeoutshift2 = if( a.fc_timeoutshift2 = "" or a.fc_timeoutshift2 is NULL,a.fc_workoutshift2 ,a.fc_timeoutshift2 ),
a.fc_timeinshift3 = if( a.fc_timeinshift3 = "" or a.fc_timeinshift3 is NULL,a.fc_workinshift3 ,a.fc_timeinshift3 ),
a.fc_timeoutshift3 = if( a.fc_timeoutshift3 = "" or a.fc_timeoutshift3 is NULL,a.fc_workoutshift3 ,a.fc_timeoutshift3 )

update t_attendance a
set 
a.fn_totaltimeshift1 = TIME_TO_SEC( 
	if( coalesce(a.fc_timeinshift1,"00:00") > coalesce(a.fc_timeoutshift1,"00:00") ,
		subtime(coalesce(a.fc_timeinshift1,"00:00") ,coalesce(a.fc_timeoutshift1,"00:00") ) ,
		subtime(coalesce(a.fc_timeoutshift1,"00:00"), coalesce(a.fc_timeinshift1,"00:00") )
	) )/60/60, 
a.fn_totaltimeshift2 = TIME_TO_SEC( 
	if( coalesce(a.fc_timeinshift2,"00:00") > coalesce(a.fc_timeoutshift2,"00:00") ,
		subtime(coalesce(a.fc_timeinshift2,"00:00") ,coalesce(a.fc_timeoutshift2,"00:00") ) ,
		subtime(coalesce(a.fc_timeoutshift2,"00:00"), coalesce(a.fc_timeinshift2,"00:00") )
	) )/60/60,
a.fn_totaltimeshift3 = TIME_TO_SEC( 
	if( coalesce(a.fc_timeinshift3,"00:00") > coalesce(a.fc_timeoutshift3,"00:00") ,
		subtime(coalesce(a.fc_timeinshift3,"00:00") ,coalesce(a.fc_timeoutshift3,"00:00") ) ,
		subtime(coalesce(a.fc_timeoutshift3,"00:00"), coalesce(a.fc_timeinshift3,"00:00") )
	) )/60/60,
a.fn_totalhour = a.fn_totaltimeshift1 + coalesce(a.fn_totaltimeshift2,0) + coalesce(a.fn_totaltimeshift3,0),
a.fn_effectivehour = coalesce(a.fn_totaltimeshift1,0) + coalesce(a.fn_totaltimeshift2,0) + coalesce(a.fn_totaltimeshift3,0) 
- ( coalesce(a.fn_totalbreakshift1,0) + coalesce(a.fn_totalbreakshift2,0) + coalesce(a.fn_totalbreakshift3,0)  )
where a.fc_periode = date_format(:datestart,"%Y%m")

update t_attendance a
set 
a.fn_totaltimeshift1 = TIME_TO_SEC( 
	if( a.fc_timeinshift1 > a.fc_timeoutshift1 ,
		subtime( a.fc_timeinshift1 ,a.fc_timeoutshift1 ) ,
		subtime(a.fc_timeoutshift1, a.fc_timeinshift1 )
	) )/60/60, 
a.fn_totaltimeshift2 = TIME_TO_SEC( 
	if( a.fc_timeinshift2 > a.fc_timeoutshift2 ,
		subtime( a.fc_timeinshift2 , a.fc_timeoutshift2 ) ,
		subtime( a.fc_timeoutshift2, a.fc_timeinshift2 )
	) )/60/60,
a.fn_totaltimeshift3 = TIME_TO_SEC( 
	if( a.fc_timeinshift3 > a.fc_timeoutshift3 ,
		subtime(a.fc_timeinshift3 , a.fc_timeoutshift3) ,
		subtime(a.fc_timeoutshift3, a.fc_timeinshift3 )
	) )/60/60,
a.fn_totalhour = a.fn_totaltimeshift1 + coalesce(a.fn_totaltimeshift2,0) + coalesce(a.fn_totaltimeshift3,0),
a.fn_effectivehour = a.fn_totaltimeshift1 + coalesce(a.fn_totaltimeshift2,0) + coalesce(a.fn_totaltimeshift3,0) 
- ( coalesce(a.fn_totalbreakshift1,0) + coalesce(a.fn_totalbreakshift2,0) + coalesce(a.fn_totalbreakshift3,0)  )
where a.fc_periode = date_format(:datestart,"%Y%m")

select 
fc_periode ,fd_dategenerate ,
a.fc_membercode,b.fc_membername1,
b.fc_memberattendancetype ,b.fn_memberothernumber1 ,
b.fc_memberposition, 
a.fd_date ,a.fc_daytype ,
concat(fc_shift1,fc_shift2,fc_shift3) shift ,
fc_shift1 ,fc_shift2,fc_shift3,
fc_timeinshift1,fc_timeoutshift1 ,
fc_timeinshift2,fc_timeoutshift2 ,
fc_timeinshift3,fc_timeoutshift3 ,
fc_workinshift1 timein1,
if(fc_workoutshift2 in ("","00:00",NULL), fc_workoutshift1 ,fc_workoutshift2 ) timeout1,
fc_workinshift3 timein2,
fc_workoutshift3 timeout2,
fd_realdatein ,fd_realdateout,
fn_totalhour ,fn_effectivehour, 
fn_ot1 ,fn_ot2 ,fn_ot3 ,fn_ot4 ,fn_ot5 ,fn_ot6,
length(concat(fc_shift1,fc_shift2,fc_shift3)) jmlh_shift,
fv_description, 
fc_ti ,
( select count(fd_date) from t_attendance where 
"1" in (fc_shift1,fc_shift2,fc_shift3 ) and fc_membercode = a.fc_membercode  and fd_date  >= :datestart and fd_date <= :datefinish
or  
"1" in (fc_shift1,fc_shift2,fc_shift3 ) and fc_membercode = a.fc_membercode  and fc_periode = DATE_FORMAT(:datestart,"%Y%m" ) and  fd_date  <= :datestart
) totalshift1,
( select count(fd_date) from t_attendance where 
"2" in (fc_shift1,fc_shift2,fc_shift3 ) and fc_membercode = a.fc_membercode  and fd_date  >= :datestart and fd_date <= :datefinish
or  
"3" in (fc_shift1,fc_shift2,fc_shift3 ) and fc_membercode = a.fc_membercode  and fc_periode = DATE_FORMAT(:datestart,"%Y%m" ) and  fd_date  <= :datestart
) totalshift2,
( select count(fd_date) from t_attendance where 
"3" in (fc_shift1,fc_shift2,fc_shift3 ) and fc_membercode = a.fc_membercode  and fd_date  >= :datestart and fd_date <= :datefinish
or  
"3" in (fc_shift1,fc_shift2,fc_shift3 ) and fc_membercode = a.fc_membercode  and fc_periode = DATE_FORMAT(:datestart,"%Y%m" ) and  fd_date  <= :datestart
) totalshift3,
( fn_ot1 + fn_ot2 + fn_ot3 + fn_ot4 + fn_ot5 + fn_ot6 )  total_ot,
( select count(fd_date) from t_attendance where 
fc_daytype not in ("") and fc_membercode = a.fc_membercode  and fd_date  >= :datestart and fd_date <= :datefinish
or  
fc_daytype not in ("") and fc_membercode = a.fc_membercode  and fc_periode = DATE_FORMAT(:datestart,"%Y%m" ) and  fd_date  <= :datestart
) totalworkday
from t_attendance a
left join t_member b on a.fc_membercode = b.fc_membercode
left join t_trxtype c on b.fc_memberposition = c.fc_trxcode and c.fc_trxcode = "EMP_POSITION"
where 
a.fc_membercode like :member and b.fl_memberblacklist = 'F' and a.fd_date  >= :datestart and a.fd_date <= :datefinish
or  
a.fc_membercode like :member and b.fl_memberblacklist = 'F' and a.fc_periode = DATE_FORMAT(:datestart,"%Y%m" ) and  a.fd_date  <= :datestart
or
b.fc_membername1 like :member and b.fl_memberblacklist = 'F' and a.fd_date  >= :datestart and a.fd_date <= :datefinish
or  
b.fc_membername1 like :member and b.fl_memberblacklist = 'F' and a.fc_periode = DATE_FORMAT(:datestart,"%Y%m" ) and  a.fd_date  <= :datestart  
order by b.fc_membercode,a.fd_date



