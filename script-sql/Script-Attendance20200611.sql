drop table t_overtime

select * from t_overtime where fd_otdate >= :datestart and fd_otdate >= :datefinish

delete from t_attendance_backup 

select fc_timeinshift1 ,fv_description from t_attendance_backup where fd_date >= '2020-01-120' and fd_date <= '2020-01-23'

select * from t_attendance where  fd_date = '2020-01-20' and fc_membercode = 'TSI 55214';
delete from t_attendance where  fd_date = '2020-01-16' and fc_membercode = 'TSI 55214';
insert into t_attendance select * from t_attendance_backup where  fd_date = '2020-01-20' and fc_membercode = 'TSI 55214';

update t_attendance_backup 
 set fv_description = ''
 where  fd_date = '2020-01-20' and fc_membercode = 'TSI 55214';
 

#new Overtime Formula
                      
#OT Update
                  
update t_attendance 
set 
fn_ot1 = 
case when fc_daytype = "N" then 
	case when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) >= 1 then 
		1 
	when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) >= 0 then 
		fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) 
	else 0 end 
else if(fc_daytype is null or fc_daytype = "" ,NULL,0) end ,	                     
fn_ot2 = 
case when fc_daytype = "N" then 
	case when fn_ot1 >= 1 then 
		fn_effectivehour - fn_ot1 - (fn_totalhourshift1 - fn_totalbreakshift1 ) 
	else 0 end 
when fc_daytype in ("H","O") then 
	case when fn_effectivehour - 7 >= 0 then 
		7 
	when fn_effectivehour - 7 <= 0 then 
		fn_effectivehour 
	else 0 end 
else if(fc_daytype is  null or fc_daytype = "" ,NULL,0) end ,
fn_ot3 = 
case when fc_daytype in ("H","O") then 
	case when fn_effectivehour - 7 >= 1 then 
		1 
	when fn_effectivehour - 7 <= 1 then 
		if(fn_effectivehour - 7 > 0, fn_effectivehour - 7 ,0 )
	else 0 end 
else if(fc_daytype is  null or fc_daytype = "" ,NULL,0) end ,
fn_ot4 = 
case when fc_daytype in ("H","O") then 
	case when fn_ot3 >= 1 then 
		fn_effectivehour - fn_ot3 - 7
	else 0 end 
when fc_daytype in ("H+") then 
	case when fn_effectivehour - 7 >= 0 then 
		7 
	when fn_effectivehour - 7 <= 0 then 
		fn_effectivehour 
	else 0 end 
else if(fc_daytype is  null or fc_daytype = "" ,NULL,0) end,
fn_ot5 = 
case when fc_daytype in ("H+") then 
	case when fn_effectivehour - 7 >= 1 then 
		1 
	when fn_effectivehour - 7 <= 1 then 
		if( fn_effectivehour - 7 > 0 ,fn_effectivehour - 7,0) 
	else 0 end 
else if(fc_daytype is  null or fc_daytype = "" ,NULL,0) end ,                     
fn_ot6 = 
case when fc_daytype in ("H+") then 
	case when fn_ot5 >=1 then 
		fn_effectivehour - fn_ot5 - 7 
	else 0 end 
else if(fc_daytype is  null or fc_daytype = "" ,NULL,0) end
where fc_periode = :periode and fd_date = :date and fc_membercode = :membercode;  


select a.*
from t_overtime a 
left join t_member b on a.fc_membercode = b.fc_membercode 
where a.fd_otdate >= :datestart and a.fd_otdate >= :datefinish



















select 
a.fc_periode,a.fc_membercode,
b.fc_membername1,b.fv_trxdescription,b.fc_divisionname,
b.fn_memberothernumber1,cast(b.uangmakanpuasa as char) uangmakanpuasa,
b.fc_memberattendancetype ,

a.fd_date,b.periodedate,a.fc_daytype,
concat(a.fc_shift1,a.fc_shift2,a.fc_shift3) shift ,
a.fc_timeinshift1 timein1,
a.fc_timeoutshift1 timeout1,
a.fc_timeinshift2 timein2,
a.fc_timeoutshift2 timeout2,
a.fc_timeinshift3 timein3,
a.fc_timeoutshift3 timeout3,
a.fn_totalhour ,a.fn_effectivehour, 
a.fn_ot1 ,a.fn_ot2 ,a.fn_ot3 ,a.fn_ot4 ,a.fn_ot5 ,a.fn_ot6,
a.fn_jumlahshift ,
a.fc_ti ,
a.fv_description, 

b.total_ot1,
b.total_ot2,
b.total_ot3,
b.total_ot4,
b.total_ot5,
b.total_ot6,
b.totalshift,

b.total_workday,
b.total_shift1,b.total_shift2,b.total_shift3,
b.total_ot alltotalot,
c.total_ot fptotalot,
b.total_ot - c.total_ot ltotalot
from t_attendance a
left join
(
select 
a.fc_periode,a.fd_date ,
a.fc_membercode,b.fc_membername1 , b.fn_memberothernumber1 ,b.fc_memberattendancetype ,
c.fv_trxdescription,b.fc_memberposition ,d.fc_divisionname,b.fc_divisioncode,
e.periodedate,
month(e.periodedate) monthperiode,
sum(a.fn_ot1 ) total_ot1,
sum(a.fn_ot2 ) total_ot2,
sum(a.fn_ot3 ) total_ot3 ,
sum(a.fn_ot4 ) total_ot4,
sum(a.fn_ot5 ) total_ot5,
sum(a.fn_ot6 ) total_ot6,
(sum(a.fn_ot1 ) * 1.5 ) + (sum(a.fn_ot2 ) * 2 ) + (sum(a.fn_ot3 ) * 3 ) + (sum(a.fn_ot4 ) * 4 ) + (sum(a.fn_ot5 ) * 6 ) + (sum(a.fn_ot6 ) * 8 ) total_ot,	
sum( length(substring( a.fc_daytype,1,1)) ) total_workday,
sum( '1' in (a.fc_shift1,a.fc_shift2 ,a.fc_shift3) ) total_shift1,
sum( '2' in (a.fc_shift1,a.fc_shift2 ,a.fc_shift3) )  total_shift2,
sum( '3' in (a.fc_shift1,a.fc_shift2 ,a.fc_shift3) ) total_shift3,
sum(a.fn_jumlahshift ) totalshift,
concat( "uang makan puasa tanggal ",f.mindate, " s/d ", f.maxdate," : ",sum(a.fl_fastingshift )," hari" ) uangmakanpuasa
from t_attendance a
left join t_member b on a.fc_membercode = b.fc_membercode
left join t_trxtype c on b.fc_memberposition = c.fc_trxcode and c.fc_trxid = "EMP_POSITION"
left join t_division d on b.fc_divisioncode = d.fc_divisioncode 
left join ( 
	select fc_membercode,fd_date,fc_periode ,if(fd_date < concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" ) , 
	concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" ) ,fd_date ) 
	periodedate from t_attendance
) e on e.fc_membercode = a.fc_membercode and a.fd_date = e.fd_date and a.fc_periode = e.fc_periode
inner join (
	select min(fd_date) mindate,max(fd_date) maxdate,
	fv_description 
	from t_fastingday group by fc_fastingdayno 
) f on month(e.periodedate) = month(f.mindate) and year(e.periodedate) = year(f.mindate) 
	or month(e.periodedate) = month(f.maxdate) and year(e.periodedate) = year(f.maxdate)
	where e.periodedate >= :datestart and e.periodedate <= :datefinish
	group by a.fc_membercode ,a.fc_periode 
)b on a.fc_membercode = b.fc_membercode and a.fc_periode = b.fc_periode
left join
(
select 
a.fc_periode,a.fd_date ,
a.fc_membercode,b.fc_membername1,
(sum(a.fn_ot1 ) * 1.5 ) + (sum(a.fn_ot2 ) * 2 ) + (sum(a.fn_ot3 ) * 3 ) + (sum(a.fn_ot4 ) * 4 ) + (sum(a.fn_ot5 ) * 6 ) + (sum(a.fn_ot6 ) * 8 ) total_ot
from t_attendance a
left join t_member b on a.fc_membercode = b.fc_membercode
left join t_trxtype c on b.fc_memberposition = c.fc_trxcode and c.fc_trxid = "EMP_POSITION"
left join t_division d on b.fc_divisioncode = d.fc_divisioncode 
left join ( 
	select fc_membercode,fd_date,fc_periode ,if(fd_date < concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" ) , 
	concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" ) ,fd_date ) 
	periodedate from t_attendance
) e on e.fc_membercode = a.fc_membercode and a.fd_date = e.fd_date and a.fc_periode = e.fc_periode
left join (
	select min(fd_date) mindate,max(fd_date) maxdate,
	fv_description 
	from t_fastingday group by fc_fastingdayno 
) f on month(e.periodedate) = month(f.mindate) and year(e.periodedate) = year(f.mindate) 
	or month(e.periodedate) = month(f.maxdate) and year(e.periodedate) = year(f.maxdate)
	where e.periodedate >= :datestart and e.periodedate <= last_day(:datestart)
group by a.fc_membercode ,a.fc_periode 
)c on a.fc_membercode = c.fc_membercode and a.fc_periode = c.fc_periode
where 
a.fc_membercode  like :member and b.fv_trxdescription like :position and b.fc_divisioncode like :division and b.periodedate >= :datestart and b.periodedate <= :datefinish
or 
b.fc_membername1 like :member and b.fv_trxdescription like :position and b.fc_divisioncode like :division and b.periodedate >= :datestart and b.periodedate <= :datefinish
order by b.fn_memberothernumber1,a.fc_membercode,a.fd_date,b.periodedate


update t_attendance set fl_fastingshift = 0

update t_attendance a
left join t_fastingday b on a.fd_date = b.fd_date 
left join t_dailytimerange c on  c.fc_timerangecode = a.fc_shift1
left join t_dailytimerange d on  d.fc_timerangecode = a.fc_shift2
left join t_dailytimerange e on  e.fc_timerangecode = a.fc_shift3
set 
a.fl_fastingshift = if ( a.fd_date = b.fd_date ,if( c.fl_fastingshift = 1 or d.fl_fastingshift = 1 or e.fl_fastingshift = 1,1,0 ),0)

select 
a.fc_membercode ,a.fd_date ,concat(a.fc_shift1 ,a.fc_shift2,a.fc_shift3) shift,
c.fl_fastingshift,d.fl_fastingshift, e.fl_fastingshift,
if( c.fl_fastingshift = 1 or d.fl_fastingshift = 1 or e.fl_fastingshift = 1,1,0 ) test
from t_attendance a
inner join t_fastingday b on a.fd_date = b.fd_date 
left join t_dailytimerange c on  c.fc_timerangecode = a.fc_shift1
left join t_dailytimerange d on  d.fc_timerangecode = a.fc_shift2
left join t_dailytimerange e on  e.fc_timerangecode = a.fc_shift3
order by a.fc_membercode ,fd_date

select fc_membercode,sum(fl_fastingshift) 
from t_attendance
group by fc_membercode


select 
fc_membercode,fd_date ,concat(fc_shift1,fc_shift2,fc_shift3),fl_fastingshift 
from t_attendance ta 
order by fc_membercode,fd_date



#21 06 2020


select
a.fc_periode,a.fc_membercode,
b.fc_membername1,b.fv_trxdescription,b.fc_divisionname,
b.fn_memberothernumber1,cast(b.uangmakanpuasa as char) uangmakanpuasa,
b.fc_memberattendancetype ,

a.fd_date,b.periodedate,
if( a.fc_daytype != '' or a.fc_daytype is null ,
 if(dayofweek(a.fd_date) = 7 and a.fc_daytype not in ('H','H+') ,'S' ,a.fc_daytype) 
, a.fc_daytype) fc_daytype,
concat(coalesce(d.fc_shiftnamealias,''),coalesce(e.fc_shiftnamealias,''),coalesce(f.fc_shiftnamealias,'')) shift ,
a.fc_timeinshift1 timein1,
a.fc_timeoutshift1 timeout1,
a.fc_timeinshift2 timein2,
a.fc_timeoutshift2 timeout2,
a.fc_timeinshift3 timein3,
a.fc_timeoutshift3 timeout3,
a.fn_totalhour ,a.fn_effectivehour, 
a.fn_ot1 ,a.fn_ot2 ,a.fn_ot3 ,a.fn_ot4 ,a.fn_ot5 ,a.fn_ot6,
a.fn_jumlahshift ,
a.fc_ti ,
a.fv_description, 

b.total_ot1,
b.total_ot2,
b.total_ot3,
b.total_ot4,
b.total_ot5,
b.total_ot6,
b.totalshift,

b.total_workday,
b.total_shift1,b.total_shift2,b.total_shift3,
b.total_ot alltotalot,
c.total_ot fptotalot,
b.total_ot - c.total_ot ltotalot
from t_attendance a
left join
(
select 
a.fc_periode,a.fd_date ,
a.fc_membercode,b.fc_membername1 , b.fn_memberothernumber1 ,b.fc_memberattendancetype ,
c.fv_trxdescription,b.fc_memberposition ,d.fc_divisionname,b.fc_divisioncode,
e.periodedate,
month(e.periodedate) monthperiode,
sum(a.fn_ot1 ) total_ot1,
sum(a.fn_ot2 ) total_ot2,
sum(a.fn_ot3 ) total_ot3 ,
sum(a.fn_ot4 ) total_ot4,
sum(a.fn_ot5 ) total_ot5,
sum(a.fn_ot6 ) total_ot6,
(sum(a.fn_ot1 ) * 1.5 ) + (sum(a.fn_ot2 ) * 2 ) + (sum(a.fn_ot3 ) * 3 ) + (sum(a.fn_ot4 ) * 4 ) + (sum(a.fn_ot5 ) * 6 ) + (sum(a.fn_ot6 ) * 8 ) total_ot,	
sum( length(substring( a.fc_daytype,1,1)) ) total_workday,
sum( '1' in (coalesce(g.fc_shiftnamealias,'') ,coalesce(h.fc_shiftnamealias,'')  ,coalesce(i.fc_shiftnamealias,'') ) ) total_shift1,
sum( '2' in (coalesce(g.fc_shiftnamealias,'') ,coalesce(h.fc_shiftnamealias,'')  ,coalesce(i.fc_shiftnamealias,'') ) )  total_shift2,
sum( '3' in (coalesce(g.fc_shiftnamealias,'') ,coalesce(h.fc_shiftnamealias,'')  ,coalesce(i.fc_shiftnamealias,'') ) ) total_shift3,
sum(a.fn_jumlahshift ) totalshift,
concat( "uang makan puasa tanggal ",f.mindate, " s/d ", f.maxdate," : ",sum(a.fl_fastingshift )," hari" ) uangmakanpuasa
from t_attendance a
left join t_member b on a.fc_membercode = b.fc_membercode
left join t_trxtype c on b.fc_memberposition = c.fc_trxcode and c.fc_trxid = "EMP_POSITION"
left join t_division d on b.fc_divisioncode = d.fc_divisioncode 
left join ( 
	select fc_membercode,fd_date,fc_periode ,if(fd_date < concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" ) , 
	concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" ) ,fd_date ) 
	periodedate from t_attendance
) e on e.fc_membercode = a.fc_membercode and a.fd_date = e.fd_date and a.fc_periode = e.fc_periode
left join (
	select min(fd_date) mindate,max(fd_date) maxdate,
	fv_description 
	from t_fastingday group by fc_fastingdayno 
) f on month(e.periodedate) = month(f.mindate) and year(e.periodedate) = year(f.mindate) 
	or month(e.periodedate) = month(f.maxdate) and year(e.periodedate) = year(f.maxdate)
left join t_dailytimerange g on a.fc_shift1 = g.fc_timerangecode 
left join t_dailytimerange h on a.fc_shift2 = h.fc_timerangecode 
left join t_dailytimerange i on a.fc_shift3 = i.fc_timerangecode 
where e.periodedate >= :datestart and e.periodedate <= :datefinish
group by a.fc_membercode ,a.fc_periode 
)b on a.fc_membercode = b.fc_membercode and a.fc_periode = b.fc_periode
left join
(
select 
a.fc_periode,a.fd_date ,
a.fc_membercode,b.fc_membername1,
(sum(a.fn_ot1 ) * 1.5 ) + (sum(a.fn_ot2 ) * 2 ) + (sum(a.fn_ot3 ) * 3 ) + (sum(a.fn_ot4 ) * 4 ) + (sum(a.fn_ot5 ) * 6 ) + (sum(a.fn_ot6 ) * 8 ) total_ot
from t_attendance a
left join t_member b on a.fc_membercode = b.fc_membercode
left join t_trxtype c on b.fc_memberposition = c.fc_trxcode and c.fc_trxid = "EMP_POSITION"
left join t_division d on b.fc_divisioncode = d.fc_divisioncode 
left join ( 
	select fc_membercode,fd_date,fc_periode ,if(fd_date < concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" ) , 
	concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" ) ,fd_date ) 
	periodedate from t_attendance
) e on e.fc_membercode = a.fc_membercode and a.fd_date = e.fd_date and a.fc_periode = e.fc_periode
left join (
	select min(fd_date) mindate,max(fd_date) maxdate,
	fv_description 
	from t_fastingday group by fc_fastingdayno 
) f on month(e.periodedate) = month(f.mindate) and year(e.periodedate) = year(f.mindate) 
	or month(e.periodedate) = month(f.maxdate) and year(e.periodedate) = year(f.maxdate)
	where e.periodedate >= :datestart and e.periodedate <= last_day(:datestart)
group by a.fc_membercode ,a.fc_periode 
)c on a.fc_membercode = c.fc_membercode and a.fc_periode = c.fc_periode
left join t_dailytimerange d on a.fc_shift1 = d.fc_timerangecode 
left join t_dailytimerange e on a.fc_shift2 = e.fc_timerangecode 
left join t_dailytimerange f on a.fc_shift3 = f.fc_timerangecode 
where 
a.fc_membercode  like :member and b.fv_trxdescription like :position and b.fc_divisioncode like :division and b.periodedate >= :datestart and b.periodedate <= :datefinish
or 
b.fc_membername1 like :member and b.fv_trxdescription like :position and b.fc_divisioncode like :division and b.periodedate >= :datestart and b.periodedate <= :datefinish
order by b.fn_memberothernumber1,a.fc_membercode,a.fd_date,b.periodedate


#22 06 2020

select
a.fc_periode,a.fc_membercode,
b.fc_membername1,b.fv_trxdescription,b.fc_divisionname,
b.fn_memberothernumber1,cast(b.uangmakanpuasa as char) uangmakanpuasa,
b.fc_memberattendancetype ,

a.fd_date,b.periodedate,
if( coalesce(a.fc_daytype,'') != '' and a.fc_daytype not in ('H','H+') ,
 if(
 time_to_sec( if( fc_workinshift1 > fc_workoutshift1 , 
  subtime(addtime(fc_workoutshift1 ,"24:00"),fc_workinshift1 ) , 
  subtime(fc_workoutshift1,fc_workinshift1 ) 
  ) )/60/60 = 5,
 "S",
 a.fc_daytype)
, a.fc_daytype) fc_daytype,
concat(coalesce(d.fc_shiftnamealias,''),coalesce(e.fc_shiftnamealias,''),coalesce(f.fc_shiftnamealias,'')) shift ,
a.fc_timeinshift1 timein1,
a.fc_timeoutshift1 timeout1,
a.fc_timeinshift2 timein2,
a.fc_timeoutshift2 timeout2,
a.fc_timeinshift3 timein3,
a.fc_timeoutshift3 timeout3,
a.fn_totalhour ,a.fn_effectivehour, 
a.fn_ot1 ,a.fn_ot2 ,a.fn_ot3 ,a.fn_ot4 ,a.fn_ot5 ,a.fn_ot6,
a.fn_jumlahshift ,
a.fc_ti ,
a.fv_description, 

b.total_ot1,
b.total_ot2,
b.total_ot3,
b.total_ot4,
b.total_ot5,
b.total_ot6,
b.totalshift,

b.total_workday,
b.total_shift1,b.total_shift2,b.total_shift3,
b.total_ot alltotalot,
c.total_ot fptotalot,
b.total_ot - c.total_ot ltotalot
from t_attendance a
left join
(
select 
a.fc_periode,a.fd_date ,
a.fc_membercode,b.fc_membername1 , b.fn_memberothernumber1 ,b.fc_memberattendancetype ,
c.fv_trxdescription,b.fc_memberposition ,d.fc_divisionname,b.fc_divisioncode,
e.periodedate,
month(e.periodedate) monthperiode,
sum(a.fn_ot1 ) total_ot1,
sum(a.fn_ot2 ) total_ot2,
sum(a.fn_ot3 ) total_ot3 ,
sum(a.fn_ot4 ) total_ot4,
sum(a.fn_ot5 ) total_ot5,
sum(a.fn_ot6 ) total_ot6,
(sum(a.fn_ot1 ) * 1.5 ) + (sum(a.fn_ot2 ) * 2 ) + (sum(a.fn_ot3 ) * 3 ) + (sum(a.fn_ot4 ) * 4 ) + (sum(a.fn_ot5 ) * 6 ) + (sum(a.fn_ot6 ) * 8 ) total_ot,	
sum( length(substring( a.fc_daytype,1,1)) ) total_workday,
sum( '1' in (coalesce(g.fc_shiftnamealias,'') ,coalesce(h.fc_shiftnamealias,'')  ,coalesce(i.fc_shiftnamealias,'') ) ) total_shift1,
sum( '2' in (coalesce(g.fc_shiftnamealias,'') ,coalesce(h.fc_shiftnamealias,'')  ,coalesce(i.fc_shiftnamealias,'') ) )  total_shift2,
sum( '3' in (coalesce(g.fc_shiftnamealias,'') ,coalesce(h.fc_shiftnamealias,'')  ,coalesce(i.fc_shiftnamealias,'') ) ) total_shift3,
sum(a.fn_jumlahshift ) totalshift,
concat( "uang makan puasa tanggal ",f.mindate, " s/d ", f.maxdate," : ",sum(a.fl_fastingshift )," hari" ) uangmakanpuasa
from t_attendance a
left join t_member b on a.fc_membercode = b.fc_membercode
left join t_trxtype c on b.fc_memberposition = c.fc_trxcode and c.fc_trxid = "EMP_POSITION"
left join t_division d on b.fc_divisioncode = d.fc_divisioncode 
left join ( 
	select fc_membercode,fd_date,fc_periode ,if(fd_date < concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" ) , 
	concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" ) ,fd_date ) 
	periodedate from t_attendance
) e on e.fc_membercode = a.fc_membercode and a.fd_date = e.fd_date and a.fc_periode = e.fc_periode
left join (
	select min(fd_date) mindate,max(fd_date) maxdate,
	fv_description 
	from t_fastingday group by fc_fastingdayno 
) f on month(e.periodedate) = month(f.mindate) and year(e.periodedate) = year(f.mindate) 
	or month(e.periodedate) = month(f.maxdate) and year(e.periodedate) = year(f.maxdate)
left join t_dailytimerange g on a.fc_shift1 = g.fc_timerangecode 
left join t_dailytimerange h on a.fc_shift2 = h.fc_timerangecode 
left join t_dailytimerange i on a.fc_shift3 = i.fc_timerangecode 
where e.periodedate >= :datestart and e.periodedate <= :datefinish
group by a.fc_membercode ,a.fc_periode 
)b on a.fc_membercode = b.fc_membercode and a.fc_periode = b.fc_periode
left join
(
select 
a.fc_periode,a.fd_date ,
a.fc_membercode,b.fc_membername1,
(sum(a.fn_ot1 ) * 1.5 ) + (sum(a.fn_ot2 ) * 2 ) + (sum(a.fn_ot3 ) * 3 ) + (sum(a.fn_ot4 ) * 4 ) + (sum(a.fn_ot5 ) * 6 ) + (sum(a.fn_ot6 ) * 8 ) total_ot
from t_attendance a
left join t_member b on a.fc_membercode = b.fc_membercode
left join t_trxtype c on b.fc_memberposition = c.fc_trxcode and c.fc_trxid = "EMP_POSITION"
left join t_division d on b.fc_divisioncode = d.fc_divisioncode 
left join ( 
	select fc_membercode,fd_date,fc_periode ,if(fd_date < concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" ) , 
	concat(substring(fc_periode ,1,4),"-",substring(fc_periode ,5,6),"-","16" ) ,fd_date ) 
	periodedate from t_attendance
) e on e.fc_membercode = a.fc_membercode and a.fd_date = e.fd_date and a.fc_periode = e.fc_periode
left join (
	select min(fd_date) mindate,max(fd_date) maxdate,
	fv_description 
	from t_fastingday group by fc_fastingdayno 
) f on month(e.periodedate) = month(f.mindate) and year(e.periodedate) = year(f.mindate) 
	or month(e.periodedate) = month(f.maxdate) and year(e.periodedate) = year(f.maxdate)
	where e.periodedate >= :datestart and e.periodedate <= last_day(:datestart)
group by a.fc_membercode ,a.fc_periode 
)c on a.fc_membercode = c.fc_membercode and a.fc_periode = c.fc_periode
left join t_dailytimerange d on a.fc_shift1 = d.fc_timerangecode 
left join t_dailytimerange e on a.fc_shift2 = e.fc_timerangecode 
left join t_dailytimerange f on a.fc_shift3 = f.fc_timerangecode 
where 
a.fc_membercode  like :member and b.fv_trxdescription like :position and b.fc_divisioncode like :division and b.periodedate >= :datestart and b.periodedate <= :datefinish
or 
b.fc_membername1 like :member and b.fv_trxdescription like :position and b.fc_divisioncode like :division and b.periodedate >= :datestart and b.periodedate <= :datefinish
order by b.fn_memberothernumber1,a.fc_membercode,a.fd_date,b.periodedate



 select 
  time_to_sec( if( '07:00' > '12:00', 
  subtime(addtime('12:00' ,"24:00"),'07:00' ) , 
  subtime('12:00','07:00' ) 
  ) )/60/60 = 5

select subtime('12:00','07:00') /60/60
