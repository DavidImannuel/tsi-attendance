unit u_setupposition;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, db, Forms, Controls, Graphics, Dialogs, ExtCtrls, DBGrids,
  ZDataset;

type

  { Tf_setupposition }

  Tf_setupposition = class(TForm)
    ds_trxposition: TDataSource;
    DBGrid1: TDBGrid;
    Panel1: TPanel;
    Panel2: TPanel;
    qt_trxposition: TZQuery;
    qt_trxpositionfc_branch: TStringField;
    qt_trxpositionfc_trxcode: TStringField;
    qt_trxpositionfc_trxid: TStringField;
    qt_trxpositionfn_trxnumber: TSmallintField;
    qt_trxpositionfv_trxdescription: TStringField;
    procedure DBGrid1ColExit(Sender: TObject);
    procedure DBGrid2ColExit(Sender: TObject);
    procedure qt_trxpositionNewRecord(DataSet: TDataSet);
  private

  public

  end;

var
  f_setupposition: Tf_setupposition;

implementation

uses u_datamodule;

{$R *.lfm}

{ Tf_setupposition }

procedure Tf_setupposition.DBGrid1ColExit(Sender: TObject);
begin
  qt_trxposition.ApplyUpdates;
end;

procedure Tf_setupposition.DBGrid2ColExit(Sender: TObject);
begin

end;

procedure Tf_setupposition.qt_trxpositionNewRecord(DataSet: TDataSet);
begin
  qt_trxpositionfc_branch.AsString:='001';
  qt_trxpositionfc_trxid.AsString:='EMP_POSITION';
end;

end.

