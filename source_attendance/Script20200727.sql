select 
a.fc_branch,a.fc_periode ,a.fd_dategenerate , 
a.fc_membercode,b.fc_membername1, 
c.fv_trxdescription fc_memberposition, 
a.fd_date ,a.fc_daytype , 
a.fc_shift1 ,a.fc_shift2,a.fc_shift3,
a.fc_timeinshift1,a.fc_timeoutshift1 , 
a.fc_timeinshift2,a.fc_timeoutshift2 , 
a.fc_timeinshift3,a.fc_timeoutshift3 , 
a.fn_jumlahshift ,
a.fd_realdatein ,a.fd_realdateout, 
a.fn_totalhour ,a.fn_effectivehour, 
a.fn_ot1 ,a.fn_ot2 ,a.fn_ot3 ,a.fn_ot4 ,a.fn_ot5 ,a.fn_ot6, 
a.fc_ti, 
a.fv_description, 
a.fl_ischecked1 , 
a.fl_ischeked2,
a.fl_isremindagain 
from t_attendance a 
left join t_member b on a.fc_membercode = b.fc_membercode  
left join t_trxtype c on b.fc_memberposition = c.fc_trxcode and c.fc_trxid = "EMP_POSITION" 
where
a.fc_membercode like :member and c.fv_trxdescription like :position and a.fd_date >= :datestart and a.fd_date <= :datefinish
or 
b.fc_membername1 like :member and c.fv_trxdescription like :position and a.fd_date >= :datestart and a.fd_date <= :datefinish
order by a.fc_membercode ,c.fv_trxdescription, a.fc_periode ,a.fd_date

ALTER TABLE db_tatsumi.t_attendance_backup ADD fl_isremindagain BOOL DEFAULT FALSE NOT NULL;
ALTER TABLE db_tatsumi.t_attendance_backup ADD fc_attendancetype varchar(30) DEFAULT 'ATTENDANCE' NOT NULL;

-- import csv

 update t_tempdavidrotateshift set fc_date31 = "2" where fc_date31 in ( "2C" ) and fc_blank <> "O" 
 
select fc_date31 in ("2C\n") from t_tempdavidrotateshift

select TRIM(TRAILING '\n' FROM fc_date31) from t_tempdavidrotateshift

select TRIM(fc_date31) from t_tempdavidrotateshift

select fc_date31 from t_tempdavidrotateshift
select fc_date31,trim(REPLACE(fc_date31, CHAR(13), '') ) = "2C" from t_tempdavidrotateshift
select fc_date31,trim(REPLACE(fc_date31, CHAR(23), '') ) = "2C" from t_tempdavidrotateshift



