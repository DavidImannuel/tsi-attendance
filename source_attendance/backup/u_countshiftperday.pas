unit u_countshiftperday;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, DBGrids, ExtCtrls,
  StdCtrls, DateTimePicker, ZDataset, db;

type

  { Tf_counshiftperday }

  Tf_counshiftperday = class(TForm)
    Button1: TButton;
    DateTimePicker1: TDateTimePicker;
    DateTimePicker2: TDateTimePicker;
    ds_countshiftperday: TDataSource;
    DBGrid1: TDBGrid;
    Label1: TLabel;
    Panel1: TPanel;
    Panel2: TPanel;
    qt_countshiftperday: TZReadOnlyQuery;
    procedure Button1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private

  public

  end;

var
  f_counshiftperday: Tf_counshiftperday;

implementation

uses u_datamodule;

{$R *.lfm}

{ Tf_counshiftperday }

procedure Tf_counshiftperday.Button1Click(Sender: TObject);
begin
  with qt_countshiftperday do begin
    Close;
    ParamByName('datestart').AsDate:=DateTimePicker1.Date;
    ParamByName('datefinish').AsDate:=DateTimePicker2.Date;
    Open;
  end;
end;

procedure Tf_counshiftperday.FormShow(Sender: TObject);
begin
  DateTimePicker1.Date:=DM.NowX;
  DateTimePicker2.Date:=DM.NowX;
end;

end.

