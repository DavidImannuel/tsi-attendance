unit u_attendance;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, StdCtrls,
  Buttons, ComCtrls, DBGrids, DateTimePicker, DBDateTimePicker, LR_DBSet,
  LR_Class, ZConnection, ZSqlProcessor, ZDataset, ZSqlUpdate, dateutils, db,
  LCLType, Menus, DBCtrls, fpspreadsheetgrid,Variants, Grids, JSONPropStorage,StrUtils;
const
  vq_qt_attendance = ' select '+
                     ' a.fc_branch,a.fc_periode ,a.fd_dategenerate , ' +
                     ' a.fc_membercode,b.fc_membername1, c.fv_trxdescription fc_memberposition, '+
                     ' a.fd_date ,a.fc_daytype , a.fc_shift1 ,a.fc_shift2,a.fc_shift3, '+
                     ' a.fc_timeinshift1,a.fc_timeoutshift1 , '+
                     ' a.fc_timeinshift2,a.fc_timeoutshift2 , '+
                     ' a.fc_timeinshift3,a.fc_timeoutshift3 , '+
                     ' a.fn_jumlahshift , '+
                     ' a.fd_realdatein ,a.fd_realdateout, '+
                     ' a.fn_totalhour ,a.fn_effectivehour, '+
                     ' a.fn_ot1 ,a.fn_ot2 ,a.fn_ot3 ,a.fn_ot4 ,a.fn_ot5 ,a.fn_ot6, '+
                     ' a.fc_ti, a.fv_description, a.fl_ischecked1 , a.fl_ischeked2, a.fl_isremindagain,a.fc_adjustmenttype '+
                     ' from t_attendance a '+
                     ' left join t_member b on a.fc_membercode = b.fc_membercode '+
                     ' left join t_trxtype c on b.fc_memberposition = c.fc_trxcode and c.fc_trxid = "EMP_POSITION" '+
                     ' left join t_division d on b.fc_divisioncode = d.fc_divisioncode '+
                     ' where '+
                     ' a.fc_membercode like :member and c.fv_trxdescription like :position and d.fc_divisionname like :division and a.fd_date >= :datestart and a.fd_date <= :datefinish '+
                     ' or '+
                     ' b.fc_membername1 like :member and c.fv_trxdescription like :position and d.fc_divisionname like :division and a.fd_date >= :datestart and a.fd_date <= :datefinish '+
                     ' order by fc_memberposition,a.fc_membercode ,c.fv_trxdescription, a.fc_periode ,a.fd_date ';
  vq_qt_attendance_exportcsv = ' select '+
                     ' a.fc_branch,a.fc_periode,a.fd_dategenerate, '+
                     ' a.fd_date,a.fc_daytype, '+
                     ' a.fc_membercode,a.fc_memberattendancetype, '+
                     ' a.fc_shift1,a.fc_shift2,a.fc_shift3, '+
                     ' a.fc_workinshift1,a.fc_workoutshift1,a.fn_totalbreakshift1, '+
                     ' a.fc_workinshift2,a.fc_workoutshift2,a.fn_totalbreakshift2, '+
                     ' a.fc_workinshift3,a.fc_workoutshift3,a.fn_totalbreakshift3, '+
                     ' a.fn_totalhourshift1,a.fn_totalhourshift2,a.fn_totalhourshift3, '+
                     ' a.fn_ot1,a.fn_ot2,a.fn_ot3,a.fn_ot4,a.fn_ot5,a.fn_ot6, '+
                     ' a.fc_datein,a.fc_dateout,a.fd_realdatein,a.fd_realdateout, '+
                     ' a.fn_totalhour,a.fn_effectivehour, '+
                     ' a.fc_ti,a.fv_description, '+
                     ' a.fc_timeinshift1,a.fc_timeoutshift1, '+
                     ' a.fc_timeinshift2,a.fc_timeoutshift2, '+
                     ' a.fc_timeinshift3,a.fc_timeoutshift3, '+
                     ' a.fn_totaltimeshift1,a.fn_totaltimeshift2,a.fn_totaltimeshift3, '+
                     ' a.fn_jumlahshift1,a.fn_jumlahshift2,a.fn_jumlahshift3,a.fn_jumlahshift, '+
                     ' a.fl_fastingshift,a.fl_ischecked1,a.fl_ischeked2,a.fl_isremindagain, '+
                     ' a.fc_attendancetype,a.fc_adjustmenttype'+
                     ' from t_attendance a '+
                     ' left join t_member b on a.fc_membercode = b.fc_membercode '+
                     ' left join t_trxtype c on b.fc_memberposition = c.fc_trxcode and c.fc_trxid = "EMP_POSITION" '+
                     ' left join t_division d on b.fc_divisioncode = d.fc_divisioncode '+
                     ' where '+
                     ' a.fc_membercode like :member and c.fv_trxdescription like :position and d.fc_divisionname like :division and a.fd_date >= :datestart and a.fd_date <= :datefinish '+
                     ' or '+
                     ' b.fc_membername1 like :member and c.fv_trxdescription like :position and d.fc_divisionname like :division and a.fd_date >= :datestart and a.fd_date <= :datefinish '+
                     ' order by fc_memberposition,a.fc_membercode ,c.fv_trxdescription, a.fc_periode ,a.fd_date ';
   vq_qt_attendance_adjustment = ' select '+
                     ' a.fc_branch,a.fc_periode ,a.fd_dategenerate , ' +
                     ' a.fc_membercode,b.fc_membername1, c.fv_trxdescription fc_memberposition, '+
                     ' a.fd_date ,a.fc_daytype , a.fc_shift1 ,a.fc_shift2,a.fc_shift3, '+
                     ' a.fc_timeinshift1,a.fc_timeoutshift1 , '+
                     ' a.fc_timeinshift2,a.fc_timeoutshift2 , '+
                     ' a.fc_timeinshift3,a.fc_timeoutshift3 , '+
                     ' a.fn_jumlahshift , '+
                     ' a.fd_realdatein ,a.fd_realdateout, '+
                     ' a.fn_totalhour ,a.fn_effectivehour, '+
                     ' a.fn_ot1 ,a.fn_ot2 ,a.fn_ot3 ,a.fn_ot4 ,a.fn_ot5 ,a.fn_ot6, '+
                     ' a.fc_ti, a.fv_description, a.fl_ischecked1 , a.fl_ischeked2, a.fl_isremindagain,a.fc_adjustmenttype '+
                     ' from t_attendance a '+
                     ' left join t_member b on a.fc_membercode = b.fc_membercode '+
                     ' left join t_trxtype c on b.fc_memberposition = c.fc_trxcode and c.fc_trxid = "EMP_POSITION" '+
                     ' where '+
                     ' a.fc_membercode like :member and c.fv_trxdescription like :position and a.fc_periode = date_format(:date,"%Y%m") and a.fc_attendancetype = "ADJUSTMENT" '+
                     ' or '+
                     ' b.fc_membername1 like :member and c.fv_trxdescription like :position and a.fc_periode = date_format(:date,"%Y%m") and a.fc_attendancetype = "ADJUSTMENT" '+
                     ' order by fc_memberposition,a.fc_membercode ,c.fv_trxdescription, a.fc_periode ,a.fd_date ';
   vq_qt_attendance_adjustment_exportcsv = ' select a.* '+
                     ' from t_attendance a '+
                     ' left join t_member b on a.fc_membercode = b.fc_membercode '+
                     ' left join t_trxtype c on b.fc_memberposition = c.fc_trxcode and c.fc_trxid = "EMP_POSITION" '+
                     ' where '+
                     ' a.fc_membercode like :member and c.fv_trxdescription like :position and a.fc_periode = date_format(:date,"%Y%m") and a.fc_attendancetype = "ADJUSTMENT" '+
                     ' or '+
                     ' b.fc_membername1 like :member and c.fv_trxdescription like :position and a.fc_periode = date_format(:date,"%Y%m") and a.fc_attendancetype = "ADJUSTMENT" '+
                     ' order by fc_memberposition,a.fc_membercode ,c.fv_trxdescription, a.fc_periode ,a.fd_date ';
   vq_qt_attendance_remind = ' select '+
                     ' a.fc_branch,a.fc_periode ,a.fd_dategenerate , ' +
                     ' a.fc_membercode,b.fc_membername1, c.fv_trxdescription fc_memberposition, '+
                     ' a.fd_date ,a.fc_daytype , a.fc_shift1 ,a.fc_shift2,a.fc_shift3, '+
                     ' a.fc_timeinshift1,a.fc_timeoutshift1 , '+
                     ' a.fc_timeinshift2,a.fc_timeoutshift2 , '+
                     ' a.fc_timeinshift3,a.fc_timeoutshift3 , '+
                     ' a.fn_jumlahshift , '+
                     ' a.fd_realdatein ,a.fd_realdateout, '+
                     ' a.fn_totalhour ,a.fn_effectivehour, '+
                     ' a.fn_ot1 ,a.fn_ot2 ,a.fn_ot3 ,a.fn_ot4 ,a.fn_ot5 ,a.fn_ot6, '+
                     ' a.fc_ti, a.fv_description, a.fl_ischecked1 , a.fl_ischeked2, a.fl_isremindagain,a.fc_adjustmenttype '+
                     ' from t_attendance a '+
                     ' left join t_member b on a.fc_membercode = b.fc_membercode '+
                     ' left join t_trxtype c on b.fc_memberposition = c.fc_trxcode and c.fc_trxid = "EMP_POSITION" '+
                     ' left join t_division d on b.fc_divisioncode = d.fc_divisioncode '+
                     ' where '+
                     ' a.fc_membercode like :member and c.fv_trxdescription like :position and d.fc_divisionname like :division and fl_isremindagain = 1 and a.fd_date >= :datestart and a.fd_date <= :datefinish '+
                     ' or '+
                     ' b.fc_membername1 like :member and c.fv_trxdescription like :position and d.fc_divisionname like :division and fl_isremindagain = 1 and a.fd_date >= :datestart and a.fd_date <= :datefinish '+
                     ' order by fc_memberposition,a.fc_membercode ,c.fv_trxdescription, a.fc_periode ,a.fd_date ';
type

  { Tf_attendance }

  Tf_attendance = class(TForm)
    BitBtnAddAdjustment: TBitBtn;
    BitBtnFilter: TBitBtn;
    BitBtnFilter1: TBitBtn;
    BitBtnFilter2: TBitBtn;
    BitBtnPrint: TBitBtn;
    BitBtnRefreshAttendance: TBitBtn;
    BitBtnRefreshAttendance1: TBitBtn;
    BitBtnSaveAdjustment: TBitBtn;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    Button6: TButton;
    Button7: TButton;
    Button8: TButton;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    CheckBoxAttendanceFilter1: TCheckBox;
    ComboBox1: TComboBox;
    DateTimePicker1: TDateTimePicker;
    DateTimePickerAdjustmentPeriode: TDateTimePicker;
    DateTimePickerAdjustmentInput: TDateTimePicker;
    DateTimePickerDate: TDateTimePicker;
    DateTimePickerFinishAttendance: TDateTimePicker;
    DateTimePickerStartAttendance: TDateTimePicker;
    DBGridAttendance: TDBGrid;
    DBGridAttendance1: TDBGrid;
    DBLookupListBox1: TDBLookupListBox;
    ds_attendance: TDataSource;
    ds_membersearch: TDataSource;
    EditFixColumn: TEdit;
    EditFixColumn1: TEdit;
    EditSeacrhEmp: TEdit;
    EditSearchAttendance: TEdit;
    EditSearchAttendance1: TEdit;
    EditSearchPosition: TEdit;
    EditSearchPosition1: TEdit;
    EditSearchDivision: TEdit;
    EdtDaytype: TEdit;
    EdtNoRest: TEdit;
    EdtSHift1: TEdit;
    EdtShift2: TEdit;
    EdtShift3: TEdit;
    EdtTimeInShift1: TEdit;
    EdtTimeInShift2: TEdit;
    EdtTimeInShift3: TEdit;
    EdtTimeOutShift1: TEdit;
    EdtTimeOutShift2: TEdit;
    EdtTimeOutShift3: TEdit;
    frDBDataSetAttendance: TfrDBDataSet;
    frReport: TfrReport;
    ImageList: TImageList;
    Label11: TLabel;
    Label12: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label18: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label3: TLabel;
    Label36: TLabel;
    Label37: TLabel;
    Label38: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    MemoDescriptionInput: TMemo;
    Notebook1: TNotebook;
    OpenDialog: TOpenDialog;
    PageAdjustment: TPage;
    PageAttendance: TPage;
    Panel1: TPanel;
    Panel10: TPanel;
    Panel14: TPanel;
    Panel15: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel9: TPanel;
    PanelAdjustment: TPanel;
    qt_attendancefc_adjustmenttype: TStringField;
    qt_attendancefc_branch: TStringField;
    qt_attendancefc_daytype: TStringField;
    qt_attendancefc_membercode: TStringField;
    qt_attendancefc_membername1: TStringField;
    qt_attendancefc_memberposition: TStringField;
    qt_attendancefc_periode: TStringField;
    qt_attendancefc_shift1: TStringField;
    qt_attendancefc_shift2: TStringField;
    qt_attendancefc_shift3: TStringField;
    qt_attendancefc_ti: TStringField;
    qt_attendancefc_timeinshift1: TStringField;
    qt_attendancefc_timeinshift2: TStringField;
    qt_attendancefc_timeinshift3: TStringField;
    qt_attendancefc_timeoutshift1: TStringField;
    qt_attendancefc_timeoutshift2: TStringField;
    qt_attendancefc_timeoutshift3: TStringField;
    qt_attendancefd_date: TDateField;
    qt_attendancefd_dategenerate: TDateTimeField;
    qt_attendancefd_realdatein: TDateTimeField;
    qt_attendancefd_realdateout: TDateTimeField;
    qt_attendancefl_ischecked1: TSmallintField;
    qt_attendancefl_ischeked2: TSmallintField;
    qt_attendancefl_isremindagain: TSmallintField;
    qt_attendancefn_effectivehour: TFloatField;
    qt_attendancefn_jumlahshift: TLongintField;
    qt_attendancefn_ot1: TFloatField;
    qt_attendancefn_ot2: TFloatField;
    qt_attendancefn_ot3: TFloatField;
    qt_attendancefn_ot4: TFloatField;
    qt_attendancefn_ot5: TFloatField;
    qt_attendancefn_ot6: TFloatField;
    qt_attendancefn_totalhour: TFloatField;
    qt_attendancefv_description: TStringField;
    qt_membersearch: TZReadOnlyQuery;
    qt_membersearchfc_membercode: TStringField;
    qt_membersearchfc_membername1: TStringField;
    qt_membersearchfv_trxdescription: TStringField;
    qt_membersearchmemberandposition: TStringField;
    SaveDialog: TSaveDialog;
    MyProcessor: TZSQLProcessor;
    qt_attendance: TZQuery;
    qtu_attendance: TZUpdateSQL;
    UpDown1: TUpDown;
    UpDown2: TUpDown;
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtnAddAdjustmentClick(Sender: TObject);
    procedure BitBtnFilter1Click(Sender: TObject);
    procedure BitBtnFilter2Click(Sender: TObject);
    procedure BitBtnRefreshAttendanceClick(Sender: TObject);
    procedure BitBtnFilterClick(Sender: TObject);
    procedure BitBtnPrintClick(Sender: TObject);
    procedure BitBtnSaveAdjustmentClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure Button8Click(Sender: TObject);
    procedure CheckBox1Change(Sender: TObject);
    procedure CheckBox2Change(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure DateTimePicker1Change(Sender: TObject);
    procedure DateTimePickerDateChange(Sender: TObject);
    procedure DBGridAttendance1DrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure DBGridAttendanceColEnter(Sender: TObject);
    procedure DBGridAttendanceColExit(Sender: TObject);
    procedure DBGridAttendanceDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure ds_membersearchDataChange(Sender: TObject; Field: TField);
    procedure EditFixColumn1Change(Sender: TObject);
    procedure EditFixColumnChange(Sender: TObject);
    procedure EditSeacrhEmpKeyPress(Sender: TObject; var Key: char);
    procedure EditSearchAttendance1KeyPress(Sender: TObject; var Key: char);
    procedure EditSearchAttendanceKeyPress(Sender: TObject; var Key: char);
    procedure EditSearchPosition1KeyPress(Sender: TObject; var Key: char);
    procedure EditSearchPositionKeyPress(Sender: TObject; var Key: char);
    procedure EdtSHift1Change(Sender: TObject);
    procedure EdtShift2Change(Sender: TObject);
    procedure EdtShift3Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure frReportEnterRect(MemoDescription: TStringList; View: TfrView);
    procedure qt_attendancefc_adjustmenttypeChange(Sender: TField);
    procedure qt_attendancefc_daytypeChange(Sender: TField);
    procedure qt_attendancefc_shift1Change(Sender: TField);
    procedure qt_attendancefc_shift2Change(Sender: TField);
    procedure qt_attendancefc_shift3Change(Sender: TField);
    procedure qt_attendancefc_tiChange(Sender: TField);
    procedure qt_attendancefc_timeinshift1Change(Sender: TField);
    procedure qt_attendancefc_timeinshift2Change(Sender: TField);
    procedure qt_attendancefc_timeinshift3Change(Sender: TField);
    procedure qt_attendancefc_timeoutshift1Change(Sender: TField);
    procedure qt_attendancefc_timeoutshift2Change(Sender: TField);
    procedure qt_attendancefc_timeoutshift3Change(Sender: TField);
    procedure qt_attendancefl_ischecked1Change(Sender: TField);
    procedure qt_attendancefl_ischeked2Change(Sender: TField);
    procedure qt_attendancefl_isremindagainChange(Sender: TField);
    procedure qt_attendancefn_effectivehourChange(Sender: TField);
    procedure qt_attendancefn_ot1Change(Sender: TField);
    procedure qt_attendancefn_ot2Change(Sender: TField);
    procedure qt_attendancefn_ot3Change(Sender: TField);
    procedure qt_attendancefn_ot4Change(Sender: TField);
    procedure qt_attendancefn_ot5Change(Sender: TField);
    procedure qt_attendancefn_ot6Change(Sender: TField);
    procedure qt_attendancefn_totalhourChange(Sender: TField);
    procedure qt_attendanceNewRecord(DataSet: TDataSet);
    Procedure UpdateAttendanceRecord;
    procedure SetUpGridPickList(FieldName,SQL: String;DBGrid : TDBGrid);
    Procedure UpdateAdjustmentAttendance(membercode,periode:string;date:TDate);
    Procedure UpdateShift1;
    Procedure UpdateShift2;
    Procedure UpdateShift3;
    Procedure UpdateTime;
    Procedure UpdateOvertime;
    Procedure UpdateAdjType;
  private
    vTableNameTempRotateShift      : String;      // Variable Nama Table. Ini nanti iku USERID
    vTableNameTempAttendance      : String;      // Variable Nama Table. Ini nanti iku USERID
    vCountFieldDate : Integer;     // Total "hari" yang di block di Excel
    vTableNameAttendance : String;
    vUPD_FieldName, vUPD_OldValue, vUPD_EmployeeName, vUPD_Blank : String;
    curRecNo : LongInt;
    curField : TField;

  public

  end;

var
  f_attendance: Tf_attendance;

implementation

uses u_datamodule,u_countshiftperday,u_overtime,u_leave,u_checkattendancesave;

{$R *.lfm}

{ Tf_attendance }


procedure Tf_attendance.qt_attendancefc_daytypeChange(Sender: TField);
begin
  qt_attendance.ApplyUpdates;
  UpdateOvertime;
end;

procedure Tf_attendance.qt_attendancefc_shift1Change(Sender: TField);
begin
  qt_attendance.ApplyUpdates;
  UpdateShift1;
end;

procedure Tf_attendance.qt_attendancefc_shift2Change(Sender: TField);
begin
  qt_attendance.ApplyUpdates;
  UpdateShift2;
end;

procedure Tf_attendance.qt_attendancefc_shift3Change(Sender: TField);
begin
  qt_attendance.ApplyUpdates;
  //UpdateShift;
  UpdateShift3;
end;

procedure Tf_attendance.qt_attendancefc_tiChange(Sender: TField);
begin
  qt_attendance.ApplyUpdates;
  UpdateOvertime;
end;

procedure Tf_attendance.qt_attendancefc_timeinshift1Change(Sender: TField);
begin
  qt_attendance.ApplyUpdates;
  UpdateTime;
end;

procedure Tf_attendance.qt_attendancefc_timeinshift2Change(Sender: TField);
begin
  qt_attendance.ApplyUpdates;
  UpdateTime;
end;

procedure Tf_attendance.qt_attendancefc_timeinshift3Change(Sender: TField);
begin
  qt_attendance.ApplyUpdates;
  UpdateTime;
end;

procedure Tf_attendance.qt_attendancefc_timeoutshift1Change(Sender: TField);
begin
  qt_attendance.ApplyUpdates;
  UpdateTime;
end;

procedure Tf_attendance.qt_attendancefc_timeoutshift2Change(Sender: TField);
begin
  qt_attendance.ApplyUpdates;
  UpdateTime;
end;

procedure Tf_attendance.qt_attendancefc_timeoutshift3Change(Sender: TField);
begin
  qt_attendance.ApplyUpdates;
  UpdateTime;
end;

procedure Tf_attendance.qt_attendancefl_ischecked1Change(Sender: TField);
begin
  qt_attendance.ApplyUpdates;
end;

procedure Tf_attendance.qt_attendancefl_ischeked2Change(Sender: TField);
begin
  qt_attendance.ApplyUpdates;
end;

procedure Tf_attendance.qt_attendancefl_isremindagainChange(Sender: TField);
begin
  qt_attendance.ApplyUpdates;
end;

procedure Tf_attendance.qt_attendancefn_effectivehourChange(Sender: TField);
begin
  //qt_attendance.ApplyUpdates;
  //UpdateOvertime;
end;

procedure Tf_attendance.qt_attendancefn_ot1Change(Sender: TField);
begin
  qt_attendance.ApplyUpdates;
  if ComboBox1.ItemIndex = 1 then UpdateAdjType;
end;

procedure Tf_attendance.qt_attendancefn_ot2Change(Sender: TField);
begin
  qt_attendance.ApplyUpdates;
  if ComboBox1.ItemIndex = 1 then UpdateAdjType;
end;

procedure Tf_attendance.qt_attendancefn_ot3Change(Sender: TField);
begin
  qt_attendance.ApplyUpdates;
  if ComboBox1.ItemIndex = 1 then UpdateAdjType;
end;

procedure Tf_attendance.qt_attendancefn_ot4Change(Sender: TField);
begin
  qt_attendance.ApplyUpdates;
  if ComboBox1.ItemIndex = 1 then UpdateAdjType;
end;

procedure Tf_attendance.qt_attendancefn_ot5Change(Sender: TField);
begin
  qt_attendance.ApplyUpdates;
  if ComboBox1.ItemIndex = 1 then UpdateAdjType;
end;

procedure Tf_attendance.qt_attendancefn_ot6Change(Sender: TField);
begin
  qt_attendance.ApplyUpdates;
  if ComboBox1.ItemIndex = 1 then UpdateAdjType;
end;

procedure Tf_attendance.qt_attendancefn_totalhourChange(Sender: TField);
begin
  //qt_attendance.ApplyUpdates;
  //UpdateOvertime;
end;

procedure Tf_attendance.qt_attendanceNewRecord(DataSet: TDataSet);
begin
  //qt_attendance.FieldByName('fc_branch').AsString:='001';
  qt_attendance.Cancel;
end;

procedure Tf_attendance.UpdateAttendanceRecord;
begin
  With MyProcessor do begin
    Script.Clear;
    Script.Text:=   //update shift
                    ' update '+vTableNameAttendance+' a '
                    +' left join t_dailytimerange b on b.fc_timerangecode = a.fc_shift1 '
                    +' left join t_dailytimerange c on c.fc_timerangecode = a.fc_shift2 '
                    +' left join t_dailytimerange d on d.fc_timerangecode = a.fc_shift3  '
                    +' set  '
                    +' a.fc_workinshift1 = COALESCE(b.fc_workin,NULL), '
                    +' a.fc_workoutshift1 = COALESCE(b.fc_workout,NULL) , '
                    +' a.fn_totalbreakshift1 =COALESCE(TIME_TO_SEC( '
                    +' if( b.fc_workbreakin > b.fc_workbreakout , '
                    +' subtime(b.fc_workbreakin,b.fc_workbreakout ) , '
                    +' subtime(b.fc_workbreakout,b.fc_workbreakin ) '
                    +' ) )/60/60,NULL), '
                    +' a.fc_workinshift2 = COALESCE(c.fc_workin,NULL), '
                    +' a.fc_workoutshift2 = COALESCE(c.fc_workout,NULL) , '
                    +' a.fn_totalbreakshift2 = COALESCE(TIME_TO_SEC(  '
                    +' if( b.fc_workbreakin > b.fc_workbreakout , '
                    +' subtime(c.fc_workbreakin,c.fc_workbreakout ) , '
                    +' subtime(c.fc_workbreakout,c.fc_workbreakin ) '
                    +' ) )/60/60,NULL) , '
                    +' a.fc_workinshift3 = COALESCE(d.fc_workin,NULL), '
                    +' a.fc_workoutshift3 = COALESCE(d.fc_workout,NULL) , '
                    +' a.fn_totalbreakshift3 = COALESCE(TIME_TO_SEC(  '
                    +' if( d.fc_workbreakin > d.fc_workbreakout , '
                    +' subtime(d.fc_workbreakin,d.fc_workbreakout ) , '
                    +' subtime(d.fc_workbreakout,d.fc_workbreakin ) '
                    +' ) )/60/60,NULL) '
                    +' where a.fc_periode = :periode and a.fd_date = :date and a.fc_membercode = :membercode;'
                    // update totalhour reference
                    +' update '+vTableNameAttendance+' '
                    +' set '
	            +' fn_totalhourshift1 = TIME_TO_SEC( '
		    +'	if( fc_workinshift1 > fc_workoutshift1 , '
                    +' subtime(addtime(fc_workoutshift1 ,"24:00"),fc_workinshift1 ) , '
		    +' subtime(fc_workoutshift1,fc_workinshift1 ) '
		    +' 	) )/60/60, '
	            +' fn_totalhourshift2 = TIME_TO_SEC( '
		    +' if( fc_workinshift2 > fc_workoutshift2 , '
		    +' subtime(addtime(fc_workoutshift2 ,"24:00"),fc_workinshift2 ) , '
		    +' subtime(fc_workoutshift2,fc_workinshift2 ) '
		    +' ) )/60/60, '
	            +' fn_totalhourshift3 = TIME_TO_SEC( '
		    +' if( fc_workinshift3 > fc_workoutshift3 , '
		    +' subtime(addtime(fc_workoutshift3 ,"24:00"),fc_workinshift3 ) , '
		    +' subtime(fc_workoutshift3,fc_workinshift3 ) '
		    +' ) )/60/60 '
                    +' where fc_periode = :periode and fd_date = :date and fc_membercode = :membercode;'
                    //update timein / out shift 1,2,3 date out
                    +' update '+ vTableNameAttendance + ' a '
                    +' set '
                    +' a.fc_timeinshift1 = if( a.fc_timeinshift1 = "" or a.fc_timeinshift1 is NULL,a.fc_workinshift1 ,a.fc_timeinshift1 ), '
                    +' a.fc_timeoutshift1 = if( a.fc_timeoutshift1 = "" or a.fc_timeoutshift1 is NULL,a.fc_workoutshift1 ,a.fc_timeoutshift1 ), '
                    +' a.fc_timeinshift2 = if( a.fc_timeinshift2 = "" or a.fc_timeinshift2 is NULL,a.fc_workinshift2 ,a.fc_timeinshift2 ), '
                    +' a.fc_timeoutshift2 = if( a.fc_timeoutshift2 = "" or a.fc_timeoutshift2 is NULL,a.fc_workoutshift2 ,a.fc_timeoutshift2 ), '
                    +' a.fc_timeinshift3 = if( a.fc_timeinshift3 = "" or a.fc_timeinshift3 is NULL,a.fc_workinshift3 ,a.fc_timeinshift3 ), '
                    +' a.fc_timeoutshift3 = if( a.fc_timeoutshift3 = "" or a.fc_timeoutshift3 is NULL,a.fc_workoutshift3 ,a.fc_timeoutshift3 ) '
                    +' where a.fc_periode = :periode and a.fd_date = :date and a.fc_membercode = :membercode;'
                    //Update Totalhour  & effective Hour time
                    +' update '+vTableNameAttendance +' a '
                    +' set '
                    +' a.fn_totaltimeshift1 = TIME_TO_SEC( '
	            +' if( a.fc_timeinshift1 > a.fc_timeoutshift1 , '
		    +' subtime(addtime(a.fc_timeoutshift1,"24:00"), a.fc_timeinshift1 ) , '
		    +' subtime(a.fc_timeoutshift1, a.fc_timeinshift1 ) '
	            +' ) )/60/60, '
                    +' a.fn_totaltimeshift2 = TIME_TO_SEC( '
	            +' if( a.fc_timeinshift2 > a.fc_timeoutshift2 , '
		    +' subtime(addtime(a.fc_timeoutshift2,"24:00"), a.fc_timeinshift2 ) , '
		    +' subtime(a.fc_timeoutshift2, a.fc_timeinshift2 ) '
	            +' ) )/60/60, '
                    +' a.fn_totaltimeshift3 = TIME_TO_SEC( '
	            +' if( a.fc_timeinshift3 > a.fc_timeoutshift3 , '
		    +' subtime(addtime(a.fc_timeoutshift3,"24:00"), a.fc_timeinshift3 ) , '
		    +' subtime(a.fc_timeoutshift3, a.fc_timeinshift3 ) '
	            +' ) )/60/60, '
                    +' a.fn_totalhour = a.fn_totaltimeshift1 + coalesce(a.fn_totaltimeshift2,0 ) + coalesce(a.fn_totaltimeshift3,0), '
                    +' a.fn_effectivehour = a.fn_totaltimeshift1 + coalesce(a.fn_totaltimeshift2,0) + coalesce(a.fn_totaltimeshift3,0) - '
                    +' ( coalesce(a.fn_totalbreakshift1,0) + coalesce(a.fn_totalbreakshift2,0) + coalesce(a.fn_totalbreakshift3,0)  )  '
                    +' where a.fc_periode = :periode and a.fd_date = :date and a.fc_membercode = :membercode;'
                    //update jumlahshift
                    +' update ' + vTableNameAttendance +' a '
                    +' set '
                    +' fn_jumlahshift1 = if(fn_totaltimeshift1 >= 4,1,1), ' //hanya hitung total jam yang overtime saja
                    +' fn_jumlahshift2 = if(fn_totaltimeshift2 >= 4,1,0), '
                    +' fn_jumlahshift3 = if(fn_totaltimeshift3 >= 4,1,0), '
                    +' fn_jumlahshift = if(fc_timeinshift1 is null,null,fn_jumlahshift1 ) + coalesce(fn_jumlahshift2,0) + coalesce(fn_jumlahshift3,0) '
                    +' where a.fc_periode = :periode and a.fd_date = :date and a.fc_membercode = :membercode;'
                   //update TI1,TI2,TI3
                   +' update ' + vTableNameAttendance
                   +' set '
                   +' fn_effectivehour = '
                   +' case '
	           +' when fc_ti in ("TI1","NR1") then '
		   +' fn_effectivehour + 1 '
	           +' when fc_ti in ("TI2","NR2") then '
		   +' fn_effectivehour + 2 '
	           +' when fc_ti in ("TI3","NR3") then '
		   +' fn_effectivehour + 3 '
	           +' else '
		   +' fn_effectivehour '
                   +' end '
                   +' where fc_periode = :periode and fd_date = :date and fc_membercode = :membercode;'
                    //OT Update
                    +' update '+vTableNameAttendance
                   +' set '
                   +' fn_ot1 = '
                   +' case when fc_daytype = "N" then '
	           +' case when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) >= 1 then '
		   +' 1 '
	           +' when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) >= 0 then '
		   +' fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) '
	           +' else 0 end '
                   +' else if(fc_daytype is null or fc_daytype = "" ,NULL,0) end , '
                   +' fn_ot2 = '
                   +' case when fc_daytype = "N" then '
	           +' case when fn_ot1 >= 1 then '
		   +' fn_effectivehour - fn_ot1 - (fn_totalhourshift1 - fn_totalbreakshift1 ) '
	           +' else 0 end '
                   +' when fc_daytype in ("H","O") then '
	           +' case when fn_effectivehour - 7 >= 0 then '
		   +' 7 '
	           +' when fn_effectivehour - 7 <= 0 then '
		   +' fn_effectivehour '
	           +' else 0 end '
                   +' else if(fc_daytype is  null or fc_daytype = "" ,NULL,0) end , '
                   +' fn_ot3 = '
                   +' case when fc_daytype in ("H","O") then '
	           +' case when fn_effectivehour - 7 >= 1 then '
		   +' 1 '
	           +' when fn_effectivehour - 7 <= 1 then '
		   +' if(fn_effectivehour - 7 > 0, fn_effectivehour - 7 ,0 )  '
	           +' else 0 end '
                   +' else if(fc_daytype is  null or fc_daytype = "" ,NULL,0) end , '
                   +' fn_ot4 = '
                   +' case when fc_daytype in ("H","O") then '
	           +' case when fn_ot3 >= 1 then '
		   +' fn_effectivehour - fn_ot3 - 7 '
	           +' else 0 end '
                   +' when fc_daytype in ("H+") then '
	           +' case when fn_effectivehour - 7 >= 0 then '
		   +' 7 '
	           +' when fn_effectivehour - 7 <= 0 then '
		   +' fn_effectivehour '
	           +' else 0 end '
                   +' else if(fc_daytype is  null or fc_daytype = "" ,NULL,0) end, '
                   +' fn_ot5 = '
                   +' case when fc_daytype in ("H+") then '
	           +' case when fn_effectivehour - 7 >= 1 then '
		   +' 1 '
	           +' when fn_effectivehour - 7 <= 1 then '
		   +' if( fn_effectivehour - 7 > 0 ,fn_effectivehour - 7,0) '
	           +' else 0 end '
                   +' else if(fc_daytype is  null or fc_daytype = "" ,NULL,0) end , '
                   +' fn_ot6 = '
                   +' case when fc_daytype in ("H+") then '
	           +' case when fn_ot5 >=1 then '
		   +' fn_effectivehour - fn_ot5 - 7 '
	           +' else 0 end '
                   +' else if(fc_daytype is  null or fc_daytype = "" ,NULL,0) end '
                   +' where fc_periode = :periode and fd_date = :date and fc_membercode = :membercode;';
      ParamByName('periode').AsString:=qt_attendance.FieldByName('fc_periode').AsString;
      ParamByName('date').AsDate:=qt_attendance.FieldByName('fd_date').AsDateTime;
      ParamByName('membercode').AsString:=qt_attendance.FieldByName('fc_membercode').AsString;
    Execute;
  end;
  BitBtnRefreshAttendanceClick(Self);
end;

procedure Tf_attendance.SetUpGridPickList(FieldName, SQL: String;DBGrid :TDBGrid);
var
   SLPickList: TStringList;
   Query: TZQuery;
   i: Integer;
begin
  SLPickList:= TStringList.Create;
  Query:=TZQuery.Create(Self);
  try
    Query.Connection := DM.MyData;
    Query.SQL.Text:=SQL;
    Query.Open;
    while not Query.EOF do begin
      SLPickList.Add(Query.Fields[0].AsString);
      Query.Next;
    end;
    for i:= 0 to DBGrid.Columns.Count -1 do begin
      if DBGrid.Columns[i].FieldName = FieldName then begin
         DBGrid.Columns[i].PickList := SLPickList;
         Break;
      end;
    end;
  finally
    SLPickList.Free;
    Query.Free;
  end;
end;

procedure Tf_attendance.UpdateAdjustmentAttendance(membercode, periode: string;
  date: TDate);
begin
  With MyProcessor do begin
    Script.Clear;
    Script.Text:=   //update shift
                    ' update '+vTableNameAttendance+' a '
                    +' left join t_dailytimerange b on b.fc_timerangecode = a.fc_shift1 '
                    +' left join t_dailytimerange c on c.fc_timerangecode = a.fc_shift2 '
                    +' left join t_dailytimerange d on d.fc_timerangecode = a.fc_shift3  '
                    +' set  '
                    +' a.fc_workinshift1 = COALESCE(b.fc_workin,NULL), '
                    +' a.fc_workoutshift1 = COALESCE(b.fc_workout,NULL) , '
                    +' a.fn_totalbreakshift1 =COALESCE(TIME_TO_SEC( '
                    +' if( b.fc_workbreakin > b.fc_workbreakout , '
                    +' subtime(b.fc_workbreakin,b.fc_workbreakout ) , '
                    +' subtime(b.fc_workbreakout,b.fc_workbreakin ) '
                    +' ) )/60/60,NULL), '
                    +' a.fc_workinshift2 = COALESCE(c.fc_workin,NULL), '
                    +' a.fc_workoutshift2 = COALESCE(c.fc_workout,NULL) , '
                    +' a.fn_totalbreakshift2 = COALESCE(TIME_TO_SEC(  '
                    +' if( b.fc_workbreakin > b.fc_workbreakout , '
                    +' subtime(c.fc_workbreakin,c.fc_workbreakout ) , '
                    +' subtime(c.fc_workbreakout,c.fc_workbreakin ) '
                    +' ) )/60/60,NULL) , '
                    +' a.fc_workinshift3 = COALESCE(d.fc_workin,NULL), '
                    +' a.fc_workoutshift3 = COALESCE(d.fc_workout,NULL) , '
                    +' a.fn_totalbreakshift3 = COALESCE(TIME_TO_SEC(  '
                    +' if( d.fc_workbreakin > d.fc_workbreakout , '
                    +' subtime(d.fc_workbreakin,d.fc_workbreakout ) , '
                    +' subtime(d.fc_workbreakout,d.fc_workbreakin ) '
                    +' ) )/60/60,NULL) '
                    +' where a.fc_periode = :periode and a.fd_date = :date and a.fc_membercode = :membercode;'
                    // update totalhour reference
                    +' update '+vTableNameAttendance+' '
                    +' set '
	            +' fn_totalhourshift1 = TIME_TO_SEC( '
		    +'	if( fc_workinshift1 > fc_workoutshift1 , '
                    +' subtime(addtime(fc_workoutshift1 ,"24:00"),fc_workinshift1 ) , '
		    +' subtime(fc_workoutshift1,fc_workinshift1 ) '
		    +' 	) )/60/60, '
	            +' fn_totalhourshift2 = TIME_TO_SEC( '
		    +' if( fc_workinshift2 > fc_workoutshift2 , '
		    +' subtime(addtime(fc_workoutshift2 ,"24:00"),fc_workinshift2 ) , '
		    +' subtime(fc_workoutshift2,fc_workinshift2 ) '
		    +' ) )/60/60, '
	            +' fn_totalhourshift3 = TIME_TO_SEC( '
		    +' if( fc_workinshift3 > fc_workoutshift3 , '
		    +' subtime(addtime(fc_workoutshift3 ,"24:00"),fc_workinshift3 ) , '
		    +' subtime(fc_workoutshift3,fc_workinshift3 ) '
		    +' ) )/60/60 '
                    +' where fc_periode = :periode and fd_date = :date and fc_membercode = :membercode;'
                    //update timeinshift yang "" karena akan membuat error ubah "" menjadi null
                    +' update t_attendance  '
                    +' set fc_timeinshift1 = if(fc_timeinshift1 = "",null,fc_timeinshift1 ), '
                    +' fc_timeoutshift1 = if(fc_timeoutshift1 = "",null,fc_timeoutshift1 ), '
                    +' fc_timeinshift2 = if(fc_timeinshift2 = "",null,fc_timeinshift2 ), '
                    +' fc_timeoutshift2 = if(fc_timeoutshift2 = "",null,fc_timeoutshift2 ), '
                    +' fc_timeinshift3 = if(fc_timeinshift3 = "",null,fc_timeinshift3 ), '
                    +' fc_timeoutshift3 = if(fc_timeoutshift3 = "",null,fc_timeoutshift3 ) '
                    +' where fc_periode = :periode and fd_date = :date and fc_membercode = :membercode;'
                    //Update Totalhour  & effective Hour time
                    +' update '+vTableNameAttendance +' a '
                    +' set '
                    +' a.fn_totaltimeshift1 = TIME_TO_SEC( '
	            +' if( a.fc_timeinshift1 > a.fc_timeoutshift1 , '
		    +' subtime(addtime(a.fc_timeoutshift1,"24:00"), a.fc_timeinshift1 ) , '
		    +' subtime(a.fc_timeoutshift1, a.fc_timeinshift1 ) '
	            +' ) )/60/60, '
                    +' a.fn_totaltimeshift2 = TIME_TO_SEC( '
	            +' if( a.fc_timeinshift2 > a.fc_timeoutshift2 , '
		    +' subtime(addtime(a.fc_timeoutshift2,"24:00"), a.fc_timeinshift2 ) , '
		    +' subtime(a.fc_timeoutshift2, a.fc_timeinshift2 ) '
	            +' ) )/60/60, '
                    +' a.fn_totaltimeshift3 = TIME_TO_SEC( '
	            +' if( a.fc_timeinshift3 > a.fc_timeoutshift3 , '
		    +' subtime(addtime(a.fc_timeoutshift3,"24:00"), a.fc_timeinshift3 ) , '
		    +' subtime(a.fc_timeoutshift3, a.fc_timeinshift3 ) '
	            +' ) )/60/60, '
                    +' a.fn_totalhour = a.fn_totaltimeshift1 + coalesce(a.fn_totaltimeshift2,0 ) + coalesce(a.fn_totaltimeshift3,0), '
                    +' a.fn_effectivehour = a.fn_totaltimeshift1 + coalesce(a.fn_totaltimeshift2,0) + coalesce(a.fn_totaltimeshift3,0) - '
                    +' ( coalesce(a.fn_totalbreakshift1,0) + coalesce(a.fn_totalbreakshift2,0) + coalesce(a.fn_totalbreakshift3,0)  )  '
                    +' where a.fc_periode = :periode and a.fd_date = :date and a.fc_membercode = :membercode;'
                    //update jumlahshift
                    +' update ' + vTableNameAttendance +' a '
                    +' set '
                    +' fn_jumlahshift1 = if(fn_totaltimeshift1 >= 4,1,1), ' //hanya hitung total jam yang overtime saja
                    +' fn_jumlahshift2 = if(fn_totaltimeshift2 >= 4,1,0), '
                    +' fn_jumlahshift3 = if(fn_totaltimeshift3 >= 4,1,0), '
                    +' fn_jumlahshift = if(fc_timeinshift1 is null,null,fn_jumlahshift1 ) + coalesce(fn_jumlahshift2,0) + coalesce(fn_jumlahshift3,0) '
                    +' where a.fc_periode = :periode and a.fd_date = :date and a.fc_membercode = :membercode;'
                   //update TI1,TI2,TI3
                   +' update ' + vTableNameAttendance
                   +' set '
                   +' fn_effectivehour = '
                   +' case '
	           +' when fc_ti in ("TI1","NR1") then '
		   +' fn_effectivehour + 1 '
	           +' when fc_ti in ("TI2","NR2") then '
		   +' fn_effectivehour + 2 '
	           +' when fc_ti in ("TI3","NR3") then '
		   +' fn_effectivehour + 3 '
	           +' else '
		   +' fn_effectivehour '
                   +' end '
                   +' where fc_periode = :periode and fd_date = :date and fc_membercode = :membercode;'
                    //OT Update
                    +' update '+vTableNameAttendance
                   +' set '
                   +' fn_ot1 = '
                   +' case when fc_daytype in ("N") then '
	           +' case when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) >= 1 then '
		   +' 1 '
	           +' when fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) >= 0 then '
		   +' fn_effectivehour - (fn_totalhourshift1 - fn_totalbreakshift1 ) '
	           +' else 0 end '
                   +' else if(fc_daytype is null or fc_daytype = "" ,NULL,0) end , '
                   +' fn_ot2 = '
                   +' case when fc_daytype in ("N") then '
	           +' case when fn_ot1 >= 1 then '
		   +' fn_effectivehour - fn_ot1 - (fn_totalhourshift1 - fn_totalbreakshift1 ) '
	           +' else 0 end '
                   +' when fc_daytype in ("H","O") then '
	           +' case when fn_effectivehour - 7 >= 0 then '
		   +' 7 '
	           +' when fn_effectivehour - 7 <= 0 then '
		   +' fn_effectivehour '
	           +' else 0 end '
                   +' else if(fc_daytype is  null or fc_daytype = "" ,NULL,0) end , '
                   +' fn_ot3 = '
                   +' case when fc_daytype in ("H","O") then '
	           +' case when fn_effectivehour - 7 >= 1 then '
		   +' 1 '
	           +' when fn_effectivehour - 7 <= 1 then '
		   +' if(fn_effectivehour - 7 > 0, fn_effectivehour - 7 ,0 )  '
	           +' else 0 end '
                   +' else if(fc_daytype is  null or fc_daytype = "" ,NULL,0) end , '
                   +' fn_ot4 = '
                   +' case when fc_daytype in ("H","O") then '
	           +' case when fn_ot3 >= 1 then '
		   +' fn_effectivehour - fn_ot3 - 7 '
	           +' else 0 end '
                   +' when fc_daytype in ("H+") then '
	           +' case when fn_effectivehour - 7 >= 0 then '
		   +' 7 '
	           +' when fn_effectivehour - 7 <= 0 then '
		   +' fn_effectivehour '
	           +' else 0 end '
                   +' else if(fc_daytype is  null or fc_daytype = "" ,NULL,0) end, '
                   +' fn_ot5 = '
                   +' case when fc_daytype in ("H+") then '
	           +' case when fn_effectivehour - 7 >= 1 then '
		   +' 1 '
	           +' when fn_effectivehour - 7 <= 1 then '
		   +' if( fn_effectivehour - 7 > 0 ,fn_effectivehour - 7,0) '
	           +' else 0 end '
                   +' else if(fc_daytype is  null or fc_daytype = "" ,NULL,0) end , '
                   +' fn_ot6 = '
                   +' case when fc_daytype in ("H+") then '
	           +' case when fn_ot5 >=1 then '
		   +' fn_effectivehour - fn_ot5 - 7 '
	           +' else 0 end '
                   +' else if(fc_daytype is  null or fc_daytype = "" ,NULL,0) end '
                   +' where fc_periode = :periode and fd_date = :date and fc_membercode = :membercode;'
                   //update adjustment yg tipe ='-'
                   +' update '+vTableNameAttendance
                   +' set '
                   +' fn_jumlahshift = (abs(fn_jumlahshift) * -1), '
                   +' fn_ot1 = (abs(fn_ot1) * -1), '
                   +' fn_ot2 = (abs(fn_ot2) * -1), '
                   +' fn_ot3 = (abs(fn_ot3) * -1), '
                   +' fn_ot4 = (abs(fn_ot4) * -1), '
                   +' fn_ot5 = (abs(fn_ot5) * -1), '
                   +' fn_ot6 = (abs(fn_ot6) * -1), '
                   +' fn_totalhour = (abs(fn_totalhour) * -1), '
                   +' fn_effectivehour = (abs(fn_effectivehour) * -1) '
                   +' where fc_periode = :periode and fd_date = :date and fc_membercode = :membercode '
                   +' and fc_attendancetype = "ADJUSTMENT" and fc_adjustmenttype = "-" ; ' ;
      ParamByName('periode').AsString:=periode;
      ParamByName('date').AsDate:=date;
      ParamByName('membercode').AsString:=membercode;
    Execute;
  end;
  BitBtnRefreshAttendanceClick(Self);
end;

procedure Tf_attendance.UpdateShift1;
begin
  With MyProcessor do begin
    Script.Clear;
    Script.Text:=   //update shift
                    ' update '+vTableNameAttendance+' a '
                    +' left join t_dailytimerange b on b.fc_timerangecode = a.fc_shift1 '
                    +' set  '
                    +' a.fc_workinshift1 = COALESCE(b.fc_workin,NULL), '
                    +' a.fc_workoutshift1 = COALESCE(b.fc_workout,NULL) , '
                    +' a.fn_totalbreakshift1 =COALESCE(TIME_TO_SEC( '
                    +' if( b.fc_workbreakin > b.fc_workbreakout , '
                    +' subtime(b.fc_workbreakin,b.fc_workbreakout ) , '
                    +' subtime(b.fc_workbreakout,b.fc_workbreakin ) '
                    +' ) )/60/60,NULL) '
                    +' where a.fc_periode = :periode and a.fd_date = :date and a.fc_membercode = :membercode;'
                    // update totalhour reference
                    +' update '+vTableNameAttendance+' '
                    +' set '
	            +' fn_totalhourshift1 = TIME_TO_SEC( '
		    +'	if( fc_workinshift1 > fc_workoutshift1 , '
                    +' subtime(addtime(fc_workoutshift1 ,"24:00"),fc_workinshift1 ) , '
		    +' subtime(fc_workoutshift1,fc_workinshift1 ) '
		    +' 	) )/60/60 '
                    +' where fc_periode = :periode and fd_date = :date and fc_membercode = :membercode;'
                    //update timein / out shift 1,2,3 date out
                    +' update '+ vTableNameAttendance + ' a '
                    +' set '
                    +' a.fc_timeinshift1 = a.fc_workinshift1 , '
                    +' a.fc_timeoutshift1 = a.fc_workoutshift1  '
                    +' where a.fc_periode = :periode and a.fd_date = :date and a.fc_membercode = :membercode;'
                    //Update Totalhour  & effective Hour time
                    +' update '+vTableNameAttendance +' a '
                    +' set '
                    +' a.fn_totaltimeshift1 = TIME_TO_SEC( '
	            +' if( a.fc_timeinshift1 > a.fc_timeoutshift1 , '
		    +' subtime(addtime(a.fc_timeoutshift1,"24:00"), a.fc_timeinshift1 ) , '
		    +' subtime(a.fc_timeoutshift1, a.fc_timeinshift1 ) '
	            +' ) )/60/60, '
                    +' a.fn_totaltimeshift2 = TIME_TO_SEC( '
	            +' if( a.fc_timeinshift2 > a.fc_timeoutshift2 , '
		    +' subtime(addtime(a.fc_timeoutshift2,"24:00"), a.fc_timeinshift2 ) , '
		    +' subtime(a.fc_timeoutshift2, a.fc_timeinshift2 ) '
	            +' ) )/60/60, '
                    +' a.fn_totaltimeshift3 = TIME_TO_SEC( '
	            +' if( a.fc_timeinshift3 > a.fc_timeoutshift3 , '
		    +' subtime(addtime(a.fc_timeoutshift3,"24:00"), a.fc_timeinshift3 ) , '
		    +' subtime(a.fc_timeoutshift3, a.fc_timeinshift3 ) '
	            +' ) )/60/60, '
                    +' a.fn_totalhour = a.fn_totaltimeshift1 + coalesce(a.fn_totaltimeshift2,0 ) + coalesce(a.fn_totaltimeshift3,0), '
                    +' a.fn_effectivehour = a.fn_totalhour - '
                       +'case when ( a.fn_totalhour >= ( (2 * if( a.fn_totalhourshift1 < 8 , 8 , a.fn_totalhourshift1 ) ) + 5 ) ) then 3 '
                       +' when ( a.fn_totalhour >= ( if( a.fn_totalhourshift1 < 8 , 8 , a.fn_totalhourshift1 ) + 5 ) ) then 2 '
                                                              // ralat if(a.fn_totalhourshift1 = 5, if(a.fn_totalhour > 5,1,0) , 1)
                       +' when ( a.fn_totalhour >= 5 ) then if(a.fn_totalhourshift1 = 5, if(a.fn_totalhour > 5,1,0) , 1) else 0 end'
                    +' where a.fc_periode = :periode and a.fd_date = :date and a.fc_membercode = :membercode;'
                    +' update ' + vTableNameAttendance +' a '
                    +' set '
                    +' fn_jumlahshift = '                    // untuk mengatasi hari sabtu 8jamkerja dianggap 8+4 baru menambah 1 shift (excel)  (RECHECK lAGI)
                       +' case when ( a.fn_effectivehour >= ( (2 * if( a.fn_totalhourshift1 < 8 , 7 , a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) ) + 4 ) ) then 3 '
                       +' when ( a.fn_effectivehour >= ( if( a.fn_totalhourshift1 < 8 , 7 , a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) + 4 ) ) then 2 '
                       +' when ( a.fn_effectivehour >= 4   ) then 1 when ( a.fn_effectivehour < 4   ) then 0 else null end'
                    +' where a.fc_periode = :periode and a.fd_date = :date and a.fc_membercode = :membercode;'
                   //update TI1,TI2,TI3
                   +' update ' + vTableNameAttendance
                   +' set '
                   +' fn_effectivehour = '
                   +' case '
	           +' when fc_ti in ("TI1","NR1") then '
		   +' fn_effectivehour + 1 '
	           +' when fc_ti in ("TI2","NR2") then '
		   +' fn_effectivehour + 2 '
	           +' when fc_ti in ("TI3","NR3") then '
		   +' fn_effectivehour + 3 '
	           +' else '
		   +' fn_effectivehour '
                   +' end '
                   +' where fc_periode = :periode and fd_date = :date and fc_membercode = :membercode;'
                    //OT Update
                   +' update '+vTableNameAttendance
                   +' set '
                   +' fn_ot1 = '
                   +' case when fc_daytype = "N" then '
                                                      //untuk 5 jam kerja effective hour 5 jam karena istirahat setelah pulang   (RECHECK lAGI)
	           +' case when fn_effectivehour - ( fn_totalhourshift1 - if(fn_totalhourshift1 > 5,fn_totalbreakshift1,0) ) >= 1 then '
		   +' 1 '
	           +' when fn_effectivehour - (fn_totalhourshift1 - if(fn_totalhourshift1 > 5,fn_totalbreakshift1,0) ) >= 0 then '
		   +' fn_effectivehour - (fn_totalhourshift1 - if(fn_totalhourshift1 > 5,fn_totalbreakshift1,0) ) '
	           +' else 0 end '
                   +' else if(fc_daytype is null or fc_daytype = "" ,NULL,0) end , '
                   +' fn_ot2 = '
                   +' case when fc_daytype = "N" then '
	           +' case when fn_ot1 >= 1 then '
		   +' fn_effectivehour - fn_ot1 - (fn_totalhourshift1 - if(fn_totalhourshift1 > 5,fn_totalbreakshift1,0) ) '
	           +' else 0 end '
                   +' when fc_daytype in ("H","O") then '
	           +' case when fn_effectivehour - 7 >= 0 then '
		   +' 7 '
	           +' when fn_effectivehour - 7 <= 0 then '
		   +' fn_effectivehour '
	           +' else 0 end '
                   +' else if(fc_daytype is  null or fc_daytype = "" ,NULL,0) end , '
                   +' fn_ot3 = '
                   +' case when fc_daytype in ("H","O") then '
	           +' case when fn_effectivehour - 7 >= 1 then '
		   +' 1 '
	           +' when fn_effectivehour - 7 <= 1 then '
		   +' if(fn_effectivehour - 7 > 0, fn_effectivehour - 7 ,0 )  '
	           +' else 0 end '
                   +' else if(fc_daytype is  null or fc_daytype = "" ,NULL,0) end , '
                   +' fn_ot4 = '
                   +' case when fc_daytype in ("H","O") then '
	           +' case when fn_ot3 >= 1 then '
		   +' fn_effectivehour - fn_ot3 - 7 '
	           +' else 0 end '
                   +' when fc_daytype in ("H+") then '
	           +' case when fn_effectivehour - 7 >= 0 then '
		   +' 7 '
	           +' when fn_effectivehour - 7 <= 0 then '
		   +' fn_effectivehour '
	           +' else 0 end '
                   +' else if(fc_daytype is  null or fc_daytype = "" ,NULL,0) end, '
                   +' fn_ot5 = '
                   +' case when fc_daytype in ("H+") then '
	           +' case when fn_effectivehour - 7 >= 1 then '
		   +' 1 '
	           +' when fn_effectivehour - 7 <= 1 then '
		   +' if( fn_effectivehour - 7 > 0 ,fn_effectivehour - 7,0) '
	           +' else 0 end '
                   +' else if(fc_daytype is  null or fc_daytype = "" ,NULL,0) end , '
                   +' fn_ot6 = '
                   +' case when fc_daytype in ("H+") then '
	           +' case when fn_ot5 >=1 then '
		   +' fn_effectivehour - fn_ot5 - 7 '
	           +' else 0 end '
                   +' else if(fc_daytype is  null or fc_daytype = "" ,NULL,0) end '
                   +' where fc_periode = :periode and fd_date = :date and fc_membercode = :membercode;'
                   //update adjustment yg tipe ='-'
                   +' update '+vTableNameAttendance
                   +' set '
                   +' fn_jumlahshift = (abs(fn_jumlahshift) * -1), '
                   +' fn_ot1 = (abs(fn_ot1) * -1), '
                   +' fn_ot2 = (abs(fn_ot2) * -1), '
                   +' fn_ot3 = (abs(fn_ot3) * -1), '
                   +' fn_ot4 = (abs(fn_ot4) * -1), '
                   +' fn_ot5 = (abs(fn_ot5) * -1), '
                   +' fn_ot6 = (abs(fn_ot6) * -1), '
                   +' fn_totalhour = (abs(fn_totalhour) * -1), '
                   +' fn_effectivehour = (abs(fn_effectivehour) * -1) '
                   +' where fc_periode = :periode and fd_date = :date and fc_membercode = :membercode '
                   +' and fc_attendancetype = "ADJUSTMENT" and fc_adjustmenttype = "-" ; ';
      ParamByName('periode').AsString:=qt_attendance.FieldByName('fc_periode').AsString;
      ParamByName('date').AsDate:=qt_attendance.FieldByName('fd_date').AsDateTime;
      ParamByName('membercode').AsString:=qt_attendance.FieldByName('fc_membercode').AsString;
    Execute;
  end;
  BitBtnRefreshAttendanceClick(Self);
end;

procedure Tf_attendance.UpdateShift2;
begin
  With MyProcessor do begin
    Script.Clear;
    Script.Text:=   //update shift
                    ' update '+vTableNameAttendance+' a '
                    +' left join t_dailytimerange b on b.fc_timerangecode = a.fc_shift2 '
                    +' set  '
                    +' a.fc_workinshift2 = COALESCE(b.fc_workin,NULL), '
                    +' a.fc_workoutshift2 = COALESCE(b.fc_workout,NULL) , '
                    +' a.fn_totalbreakshift2 =COALESCE(TIME_TO_SEC( '
                    +' if( b.fc_workbreakin > b.fc_workbreakout , '
                    +' subtime(b.fc_workbreakin,b.fc_workbreakout ) , '
                    +' subtime(b.fc_workbreakout,b.fc_workbreakin ) '
                    +' ) )/60/60,NULL) '
                    +' where a.fc_periode = :periode and a.fd_date = :date and a.fc_membercode = :membercode;'
                    // update totalhour reference
                    +' update '+vTableNameAttendance+' '
                    +' set '
	            +' fn_totalhourshift2 = TIME_TO_SEC( '
		    +'	if( fc_workinshift2 > fc_workoutshift2 , '
                    +' subtime(addtime(fc_workoutshift2 ,"24:00"),fc_workinshift2 ) , '
		    +' subtime(fc_workoutshift2,fc_workinshift2 ) '
		    +' 	) )/60/60 '
                    +' where fc_periode = :periode and fd_date = :date and fc_membercode = :membercode;'
                    //update timein / out shift 1,2,3 date out
                    +' update '+ vTableNameAttendance + ' a '
                    +' set '
                    +' a.fc_timeinshift2 = a.fc_workinshift2 , '
                    +' a.fc_timeoutshift2 = a.fc_workoutshift2  '
                    +' where a.fc_periode = :periode and a.fd_date = :date and a.fc_membercode = :membercode;'
                    //Update Totalhour  & effective Hour time
                    +' update '+vTableNameAttendance +' a '
                    +' set '
                    +' a.fn_totaltimeshift1 = TIME_TO_SEC( '
	            +' if( a.fc_timeinshift1 > a.fc_timeoutshift1 , '
		    +' subtime(addtime(a.fc_timeoutshift1,"24:00"), a.fc_timeinshift1 ) , '
		    +' subtime(a.fc_timeoutshift1, a.fc_timeinshift1 ) '
	            +' ) )/60/60, '
                    +' a.fn_totaltimeshift2 = TIME_TO_SEC( '
	            +' if( a.fc_timeinshift2 > a.fc_timeoutshift2 , '
		    +' subtime(addtime(a.fc_timeoutshift2,"24:00"), a.fc_timeinshift2 ) , '
		    +' subtime(a.fc_timeoutshift2, a.fc_timeinshift2 ) '
	            +' ) )/60/60, '
                    +' a.fn_totaltimeshift3 = TIME_TO_SEC( '
	            +' if( a.fc_timeinshift3 > a.fc_timeoutshift3 , '
		    +' subtime(addtime(a.fc_timeoutshift3,"24:00"), a.fc_timeinshift3 ) , '
		    +' subtime(a.fc_timeoutshift3, a.fc_timeinshift3 ) '
	            +' ) )/60/60, '
                    +' a.fn_totalhour = a.fn_totaltimeshift1 + coalesce(a.fn_totaltimeshift2,0 ) + coalesce(a.fn_totaltimeshift3,0), '
                    +' a.fn_effectivehour = a.fn_totalhour - '
                       +'case when ( a.fn_totalhour >= ( (2 * if( a.fn_totalhourshift1 < 8 , 8 , a.fn_totalhourshift1 ) ) + 5 ) ) then 3 '
                       +' when ( a.fn_totalhour >= ( if( a.fn_totalhourshift1 < 8 , 8 , a.fn_totalhourshift1 ) + 5 ) ) then 2 '
                       +' when ( a.fn_totalhour >= 5 ) then if(a.fn_totalhourshift1 = 5, if(a.fn_totalhour > 5,1,0) , 1) else 0 end'
                    +' where a.fc_periode = :periode and a.fd_date = :date and a.fc_membercode = :membercode;'
                    +' update ' + vTableNameAttendance +' a '
                    +' set '
                    +' fn_jumlahshift = '                    // untuk mengatasi hari sabtu 8jamkerja dianggap 8+4 baru menambah 1 shift (excel)  (RECHECK lAGI)
                       +' case when ( a.fn_effectivehour >= ( (2 * if( a.fn_totalhourshift1 < 8 , 7 , a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) ) + 4 ) ) then 3 '
                       +' when ( a.fn_effectivehour >= ( if( a.fn_totalhourshift1 < 8 , 7 , a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) + 4 ) ) then 2 '
                       +' when ( a.fn_effectivehour >= 4   ) then 1 when ( a.fn_effectivehour < 4   ) then 0 else null end'
                    +' where a.fc_periode = :periode and a.fd_date = :date and a.fc_membercode = :membercode;'
                   //update TI1,TI2,TI3
                   +' update ' + vTableNameAttendance
                   +' set '
                   +' fn_effectivehour = '
                   +' case '
	           +' when fc_ti in ("TI1","NR1") then '
		   +' fn_effectivehour + 1 '
	           +' when fc_ti in ("TI2","NR2") then '
		   +' fn_effectivehour + 2 '
	           +' when fc_ti in ("TI3","NR3") then '
		   +' fn_effectivehour + 3 '
	           +' else '
		   +' fn_effectivehour '
                   +' end '
                   +' where fc_periode = :periode and fd_date = :date and fc_membercode = :membercode;'
                    //OT Update
                   +' update '+vTableNameAttendance
                   +' set '
                   +' fn_ot1 = '
                   +' case when fc_daytype = "N" then '
                                                      //untuk 5 jam kerja effective hour 5 jam karena istirahat setelah pulang   (RECHECK lAGI)
	           +' case when fn_effectivehour - ( fn_totalhourshift1 - if(fn_totalhourshift1 > 5,fn_totalbreakshift1,0) ) >= 1 then '
		   +' 1 '
	           +' when fn_effectivehour - (fn_totalhourshift1 - if(fn_totalhourshift1 > 5,fn_totalbreakshift1,0) ) >= 0 then '
		   +' fn_effectivehour - (fn_totalhourshift1 - if(fn_totalhourshift1 > 5,fn_totalbreakshift1,0) ) '
	           +' else 0 end '
                   +' else if(fc_daytype is null or fc_daytype = "" ,NULL,0) end , '
                   +' fn_ot2 = '
                   +' case when fc_daytype = "N" then '
	           +' case when fn_ot1 >= 1 then '
		   +' fn_effectivehour - fn_ot1 - (fn_totalhourshift1 - if(fn_totalhourshift1 > 5,fn_totalbreakshift1,0) ) '
	           +' else 0 end '
                   +' when fc_daytype in ("H","O") then '
	           +' case when fn_effectivehour - 7 >= 0 then '
		   +' 7 '
	           +' when fn_effectivehour - 7 <= 0 then '
		   +' fn_effectivehour '
	           +' else 0 end '
                   +' else if(fc_daytype is  null or fc_daytype = "" ,NULL,0) end , '
                   +' fn_ot3 = '
                   +' case when fc_daytype in ("H","O") then '
	           +' case when fn_effectivehour - 7 >= 1 then '
		   +' 1 '
	           +' when fn_effectivehour - 7 <= 1 then '
		   +' if(fn_effectivehour - 7 > 0, fn_effectivehour - 7 ,0 )  '
	           +' else 0 end '
                   +' else if(fc_daytype is  null or fc_daytype = "" ,NULL,0) end , '
                   +' fn_ot4 = '
                   +' case when fc_daytype in ("H","O") then '
	           +' case when fn_ot3 >= 1 then '
		   +' fn_effectivehour - fn_ot3 - 7 '
	           +' else 0 end '
                   +' when fc_daytype in ("H+") then '
	           +' case when fn_effectivehour - 7 >= 0 then '
		   +' 7 '
	           +' when fn_effectivehour - 7 <= 0 then '
		   +' fn_effectivehour '
	           +' else 0 end '
                   +' else if(fc_daytype is  null or fc_daytype = "" ,NULL,0) end, '
                   +' fn_ot5 = '
                   +' case when fc_daytype in ("H+") then '
	           +' case when fn_effectivehour - 7 >= 1 then '
		   +' 1 '
	           +' when fn_effectivehour - 7 <= 1 then '
		   +' if( fn_effectivehour - 7 > 0 ,fn_effectivehour - 7,0) '
	           +' else 0 end '
                   +' else if(fc_daytype is  null or fc_daytype = "" ,NULL,0) end , '
                   +' fn_ot6 = '
                   +' case when fc_daytype in ("H+") then '
	           +' case when fn_ot5 >=1 then '
		   +' fn_effectivehour - fn_ot5 - 7 '
	           +' else 0 end '
                   +' else if(fc_daytype is  null or fc_daytype = "" ,NULL,0) end '
                   +' where fc_periode = :periode and fd_date = :date and fc_membercode = :membercode;'
                   //update adjustment yg tipe ='-'
                   +' update '+vTableNameAttendance
                   +' set '
                   +' fn_jumlahshift = (abs(fn_jumlahshift) * -1), '
                   +' fn_ot1 = (abs(fn_ot1) * -1), '
                   +' fn_ot2 = (abs(fn_ot2) * -1), '
                   +' fn_ot3 = (abs(fn_ot3) * -1), '
                   +' fn_ot4 = (abs(fn_ot4) * -1), '
                   +' fn_ot5 = (abs(fn_ot5) * -1), '
                   +' fn_ot6 = (abs(fn_ot6) * -1), '
                   +' fn_totalhour = (abs(fn_totalhour) * -1), '
                   +' fn_effectivehour = (abs(fn_effectivehour) * -1) '
                   +' where fc_periode = :periode and fd_date = :date and fc_membercode = :membercode '
                   +' and fc_attendancetype = "ADJUSTMENT" and fc_adjustmenttype = "-" ; ';
      ParamByName('periode').AsString:=qt_attendance.FieldByName('fc_periode').AsString;
      ParamByName('date').AsDate:=qt_attendance.FieldByName('fd_date').AsDateTime;
      ParamByName('membercode').AsString:=qt_attendance.FieldByName('fc_membercode').AsString;
    Execute;
  end;
  BitBtnRefreshAttendanceClick(Self);
end;

procedure Tf_attendance.UpdateShift3;
begin
  With MyProcessor do begin
    Script.Clear;
    Script.Text:=   //update shift
                    ' update '+vTableNameAttendance+' a '
                    +' left join t_dailytimerange b on b.fc_timerangecode = a.fc_shift3 '
                    +' set  '
                    +' a.fc_workinshift3 = COALESCE(b.fc_workin,NULL), '
                    +' a.fc_workoutshift3 = COALESCE(b.fc_workout,NULL) , '
                    +' a.fn_totalbreakshift3 =COALESCE(TIME_TO_SEC( '
                    +' if( b.fc_workbreakin > b.fc_workbreakout , '
                    +' subtime(b.fc_workbreakin,b.fc_workbreakout ) , '
                    +' subtime(b.fc_workbreakout,b.fc_workbreakin ) '
                    +' ) )/60/60,NULL) '
                    +' where a.fc_periode = :periode and a.fd_date = :date and a.fc_membercode = :membercode;'
                    // update totalhour reference
                    +' update '+vTableNameAttendance+' '
                    +' set '
	            +' fn_totalhourshift3 = TIME_TO_SEC( '
		    +'	if( fc_workinshift3 > fc_workoutshift3 , '
                    +' subtime(addtime(fc_workoutshift3 ,"24:00"),fc_workinshift3 ) , '
		    +' subtime(fc_workoutshift3,fc_workinshift3 ) '
		    +' 	) )/60/60 '
                    +' where fc_periode = :periode and fd_date = :date and fc_membercode = :membercode;'
                    //update timein / out shift 1,2,3 date out
                    +' update '+ vTableNameAttendance + ' a '
                    +' set '
                    +' a.fc_timeinshift3 = a.fc_workinshift3 , '
                    +' a.fc_timeoutshift3 = a.fc_workoutshift3  '
                    +' where a.fc_periode = :periode and a.fd_date = :date and a.fc_membercode = :membercode;'
                    //Update Totalhour  & effective Hour time
                    +' update '+vTableNameAttendance +' a '
                    +' set '
                    +' a.fn_totaltimeshift1 = TIME_TO_SEC( '
	            +' if( a.fc_timeinshift1 > a.fc_timeoutshift1 , '
		    +' subtime(addtime(a.fc_timeoutshift1,"24:00"), a.fc_timeinshift1 ) , '
		    +' subtime(a.fc_timeoutshift1, a.fc_timeinshift1 ) '
	            +' ) )/60/60, '
                    +' a.fn_totaltimeshift2 = TIME_TO_SEC( '
	            +' if( a.fc_timeinshift2 > a.fc_timeoutshift2 , '
		    +' subtime(addtime(a.fc_timeoutshift2,"24:00"), a.fc_timeinshift2 ) , '
		    +' subtime(a.fc_timeoutshift2, a.fc_timeinshift2 ) '
	            +' ) )/60/60, '
                    +' a.fn_totaltimeshift3 = TIME_TO_SEC( '
	            +' if( a.fc_timeinshift3 > a.fc_timeoutshift3 , '
		    +' subtime(addtime(a.fc_timeoutshift3,"24:00"), a.fc_timeinshift3 ) , '
		    +' subtime(a.fc_timeoutshift3, a.fc_timeinshift3 ) '
	            +' ) )/60/60, '
                    +' a.fn_totalhour = a.fn_totaltimeshift1 + coalesce(a.fn_totaltimeshift2,0 ) + coalesce(a.fn_totaltimeshift3,0), '
                    +' a.fn_effectivehour = a.fn_totalhour - '
                       +'case when ( a.fn_totalhour >= ( (2 * if( a.fn_totalhourshift1 < 8 , 8 , a.fn_totalhourshift1 ) ) + 5 ) ) then 3 '
                       +' when ( a.fn_totalhour >= ( if( a.fn_totalhourshift1 < 8 , 8 , a.fn_totalhourshift1 ) + 5 ) ) then 2 '
                       +' when ( a.fn_totalhour >= 5 ) then if(a.fn_totalhourshift1 = 5, if(a.fn_totalhour > 5,1,0) , 1) else 0 end'
                    +' where a.fc_periode = :periode and a.fd_date = :date and a.fc_membercode = :membercode;'
                    +' update ' + vTableNameAttendance +' a '
                    +' set '
                    +' fn_jumlahshift = '                    // untuk mengatasi hari sabtu 8jamkerja dianggap 8+4 baru menambah 1 shift (excel)  (RECHECK lAGI)
                       +' case when ( a.fn_effectivehour >= ( (2 * if( a.fn_totalhourshift1 < 8 , 7 , a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) ) + 4 ) ) then 3 '
                       +' when ( a.fn_effectivehour >= ( if( a.fn_totalhourshift1 < 8 , 7 , a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) + 4 ) ) then 2 '
                       +' when ( a.fn_effectivehour >= 4   ) then 1 when ( a.fn_effectivehour < 4   ) then 0 else null end'
                    +' where a.fc_periode = :periode and a.fd_date = :date and a.fc_membercode = :membercode;'
                   //update TI1,TI2,TI3
                   +' update ' + vTableNameAttendance
                   +' set '
                   +' fn_effectivehour = '
                   +' case '
	           +' when fc_ti in ("TI1","NR1") then '
		   +' fn_effectivehour + 1 '
	           +' when fc_ti in ("TI2","NR2") then '
		   +' fn_effectivehour + 2 '
	           +' when fc_ti in ("TI3","NR3") then '
		   +' fn_effectivehour + 3 '
	           +' else '
		   +' fn_effectivehour '
                   +' end '
                   +' where fc_periode = :periode and fd_date = :date and fc_membercode = :membercode;'
                    //OT Update
                   +' update '+vTableNameAttendance
                   +' set '
                   +' fn_ot1 = '
                   +' case when fc_daytype = "N" then '
                                                      //untuk 5 jam kerja effective hour 5 jam karena istirahat setelah pulang   (RECHECK lAGI)
	           +' case when fn_effectivehour - ( fn_totalhourshift1 - if(fn_totalhourshift1 > 5,fn_totalbreakshift1,0) ) >= 1 then '
		   +' 1 '
	           +' when fn_effectivehour - (fn_totalhourshift1 - if(fn_totalhourshift1 > 5,fn_totalbreakshift1,0) ) >= 0 then '
		   +' fn_effectivehour - (fn_totalhourshift1 - if(fn_totalhourshift1 > 5,fn_totalbreakshift1,0) ) '
	           +' else 0 end '
                   +' else if(fc_daytype is null or fc_daytype = "" ,NULL,0) end , '
                   +' fn_ot2 = '
                   +' case when fc_daytype = "N" then '
	           +' case when fn_ot1 >= 1 then '
		   +' fn_effectivehour - fn_ot1 - (fn_totalhourshift1 - if(fn_totalhourshift1 > 5,fn_totalbreakshift1,0) ) '
	           +' else 0 end '
                   +' when fc_daytype in ("H","O") then '
	           +' case when fn_effectivehour - 7 >= 0 then '
		   +' 7 '
	           +' when fn_effectivehour - 7 <= 0 then '
		   +' fn_effectivehour '
	           +' else 0 end '
                   +' else if(fc_daytype is  null or fc_daytype = "" ,NULL,0) end , '
                   +' fn_ot3 = '
                   +' case when fc_daytype in ("H","O") then '
	           +' case when fn_effectivehour - 7 >= 1 then '
		   +' 1 '
	           +' when fn_effectivehour - 7 <= 1 then '
		   +' if(fn_effectivehour - 7 > 0, fn_effectivehour - 7 ,0 )  '
	           +' else 0 end '
                   +' else if(fc_daytype is  null or fc_daytype = "" ,NULL,0) end , '
                   +' fn_ot4 = '
                   +' case when fc_daytype in ("H","O") then '
	           +' case when fn_ot3 >= 1 then '
		   +' fn_effectivehour - fn_ot3 - 7 '
	           +' else 0 end '
                   +' when fc_daytype in ("H+") then '
	           +' case when fn_effectivehour - 7 >= 0 then '
		   +' 7 '
	           +' when fn_effectivehour - 7 <= 0 then '
		   +' fn_effectivehour '
	           +' else 0 end '
                   +' else if(fc_daytype is  null or fc_daytype = "" ,NULL,0) end, '
                   +' fn_ot5 = '
                   +' case when fc_daytype in ("H+") then '
	           +' case when fn_effectivehour - 7 >= 1 then '
		   +' 1 '
	           +' when fn_effectivehour - 7 <= 1 then '
		   +' if( fn_effectivehour - 7 > 0 ,fn_effectivehour - 7,0) '
	           +' else 0 end '
                   +' else if(fc_daytype is  null or fc_daytype = "" ,NULL,0) end , '
                   +' fn_ot6 = '
                   +' case when fc_daytype in ("H+") then '
	           +' case when fn_ot5 >=1 then '
		   +' fn_effectivehour - fn_ot5 - 7 '
	           +' else 0 end '
                   +' else if(fc_daytype is  null or fc_daytype = "" ,NULL,0) end '
                   +' where fc_periode = :periode and fd_date = :date and fc_membercode = :membercode;'
                   //update adjustment yg tipe ='-'
                   +' update '+vTableNameAttendance
                   +' set '
                   +' fn_jumlahshift = (abs(fn_jumlahshift) * -1), '
                   +' fn_ot1 = (abs(fn_ot1) * -1), '
                   +' fn_ot2 = (abs(fn_ot2) * -1), '
                   +' fn_ot3 = (abs(fn_ot3) * -1), '
                   +' fn_ot4 = (abs(fn_ot4) * -1), '
                   +' fn_ot5 = (abs(fn_ot5) * -1), '
                   +' fn_ot6 = (abs(fn_ot6) * -1), '
                   +' fn_totalhour = (abs(fn_totalhour) * -1), '
                   +' fn_effectivehour = (abs(fn_effectivehour) * -1) '
                   +' where fc_periode = :periode and fd_date = :date and fc_membercode = :membercode '
                   +' and fc_attendancetype = "ADJUSTMENT" and fc_adjustmenttype = "-" ; ';
      ParamByName('periode').AsString:=qt_attendance.FieldByName('fc_periode').AsString;
      ParamByName('date').AsDate:=qt_attendance.FieldByName('fd_date').AsDateTime;
      ParamByName('membercode').AsString:=qt_attendance.FieldByName('fc_membercode').AsString;
    Execute;
  end;
  BitBtnRefreshAttendanceClick(Self);
end;

procedure Tf_attendance.UpdateTime;
begin
  
  With MyProcessor do begin
    Script.Clear;
    Script.Text:=
                    //update timein / out shift 1,2,3 date out
                    ' update '+ vTableNameAttendance + ' a '
                    +' set '
                    +' a.fc_timeinshift1 = if( a.fc_timeinshift1 = "" or a.fc_timeinshift1 is NULL,a.fc_workinshift1 ,a.fc_timeinshift1 ), '
                    +' a.fc_timeoutshift1 = if( a.fc_timeoutshift1 = "" or a.fc_timeoutshift1 is NULL,a.fc_workoutshift1 ,a.fc_timeoutshift1 ), '
                    +' a.fc_timeinshift2 = if( a.fc_timeinshift2 = "" or a.fc_timeinshift2 is NULL,a.fc_workinshift2 ,a.fc_timeinshift2 ), '
                    +' a.fc_timeoutshift2 = if( a.fc_timeoutshift2 = "" or a.fc_timeoutshift2 is NULL,a.fc_workoutshift2 ,a.fc_timeoutshift2 ), '
                    +' a.fc_timeinshift3 = if( a.fc_timeinshift3 = "" or a.fc_timeinshift3 is NULL,a.fc_workinshift3 ,a.fc_timeinshift3 ), '
                    +' a.fc_timeoutshift3 = if( a.fc_timeoutshift3 = "" or a.fc_timeoutshift3 is NULL,a.fc_workoutshift3 ,a.fc_timeoutshift3 ) '
                    +' where a.fc_periode = :periode and a.fd_date = :date and a.fc_membercode = :membercode;'
                    //Update Totalhour  & effective Hour time
                    +' update '+vTableNameAttendance +' a '
                    +' set '
                    +' a.fn_totaltimeshift1 = TIME_TO_SEC( '
	            +' if( a.fc_timeinshift1 > a.fc_timeoutshift1 , '
		    +' subtime(addtime(a.fc_timeoutshift1,"24:00"), a.fc_timeinshift1 ) , '
		    +' subtime(a.fc_timeoutshift1, a.fc_timeinshift1 ) '
	            +' ) )/60/60, '
                    +' a.fn_totaltimeshift2 = TIME_TO_SEC( '
	            +' if( a.fc_timeinshift2 > a.fc_timeoutshift2 , '
		    +' subtime(addtime(a.fc_timeoutshift2,"24:00"), a.fc_timeinshift2 ) , '
		    +' subtime(a.fc_timeoutshift2, a.fc_timeinshift2 ) '
	            +' ) )/60/60, '
                    +' a.fn_totaltimeshift3 = TIME_TO_SEC( '
	            +' if( a.fc_timeinshift3 > a.fc_timeoutshift3 , '
		    +' subtime(addtime(a.fc_timeoutshift3,"24:00"), a.fc_timeinshift3 ) , '
		    +' subtime(a.fc_timeoutshift3, a.fc_timeinshift3 ) '
	            +' ) )/60/60, '
                    +' a.fn_totalhour = a.fn_totaltimeshift1 + coalesce(a.fn_totaltimeshift2,0 ) + coalesce(a.fn_totaltimeshift3,0), '
                    +' a.fn_effectivehour = a.fn_totalhour - '
                       +'case when ( a.fn_totalhour >= ( (2 * if( a.fn_totalhourshift1 < 8 , 8 , a.fn_totalhourshift1 ) ) + 5 ) ) then 3 '
                       +' when ( a.fn_totalhour >= ( if( a.fn_totalhourshift1 < 8 , 8 , a.fn_totalhourshift1 ) + 5 ) ) then 2 '
                       +' when ( a.fn_totalhour >= 5 ) then if(a.fn_totalhourshift1 = 5, if(a.fn_totalhour > 5,1,0) , 1) else 0 end'
                    +' where a.fc_periode = :periode and a.fd_date = :date and a.fc_membercode = :membercode;'
                    +' update ' + vTableNameAttendance +' a '
                    +' set '
                    +' fn_jumlahshift = '                    // untuk mengatasi hari sabtu 8jamkerja dianggap 8+4 baru menambah 1 shift (excel)  (RECHECK lAGI)
                       +' case when ( a.fn_effectivehour >= ( (2 * if( a.fn_totalhourshift1 < 8 , 7 , a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) ) + 4 ) ) then 3 '
                       +' when ( a.fn_effectivehour >= ( if( a.fn_totalhourshift1 < 8 , 7 , a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) + 4 ) ) then 2 '
                       +' when ( a.fn_effectivehour >= 4   ) then 1 when ( a.fn_effectivehour < 4   ) then 0 else null end'
                    +' where a.fc_periode = :periode and a.fd_date = :date and a.fc_membercode = :membercode;'
                   //update TI1,TI2,TI3
                   +' update ' + vTableNameAttendance
                   +' set '
                   +' fn_effectivehour = '
                   +' case '
	           +' when fc_ti in ("TI1","NR1") then '
		   +' fn_effectivehour + 1 '
	           +' when fc_ti in ("TI2","NR2") then '
		   +' fn_effectivehour + 2 '
	           +' when fc_ti in ("TI3","NR3") then '
		   +' fn_effectivehour + 3 '
	           +' else '
		   +' fn_effectivehour '
                   +' end '
                   +' where fc_periode = :periode and fd_date = :date and fc_membercode = :membercode;'
                    //OT Update
                   +' update '+vTableNameAttendance
                   +' set '
                   +' fn_ot1 = '
                   +' case when fc_daytype = "N" then '
                                                      //untuk 5 jam kerja effective hour 5 jam karena istirahat setelah pulang   (RECHECK lAGI)
	           +' case when fn_effectivehour - ( fn_totalhourshift1 - if(fn_totalhourshift1 > 5,fn_totalbreakshift1,0) ) >= 1 then '
		   +' 1 '
	           +' when fn_effectivehour - (fn_totalhourshift1 - if(fn_totalhourshift1 > 5,fn_totalbreakshift1,0) ) >= 0 then '
		   +' fn_effectivehour - (fn_totalhourshift1 - if(fn_totalhourshift1 > 5,fn_totalbreakshift1,0) ) '
	           +' else 0 end '
                   +' else if(fc_daytype is null or fc_daytype = "" ,NULL,0) end , '
                   +' fn_ot2 = '
                   +' case when fc_daytype = "N" then '
	           +' case when fn_ot1 >= 1 then '
		   +' fn_effectivehour - fn_ot1 - (fn_totalhourshift1 - if(fn_totalhourshift1 > 5,fn_totalbreakshift1,0) ) '
	           +' else 0 end '
                   +' when fc_daytype in ("H","O") then '
	           +' case when fn_effectivehour - 7 >= 0 then '
		   +' 7 '
	           +' when fn_effectivehour - 7 <= 0 then '
		   +' fn_effectivehour '
	           +' else 0 end '
                   +' else if(fc_daytype is  null or fc_daytype = "" ,NULL,0) end , '
                   +' fn_ot3 = '
                   +' case when fc_daytype in ("H","O") then '
	           +' case when fn_effectivehour - 7 >= 1 then '
		   +' 1 '
	           +' when fn_effectivehour - 7 <= 1 then '
		   +' if(fn_effectivehour - 7 > 0, fn_effectivehour - 7 ,0 )  '
	           +' else 0 end '
                   +' else if(fc_daytype is  null or fc_daytype = "" ,NULL,0) end , '
                   +' fn_ot4 = '
                   +' case when fc_daytype in ("H","O") then '
	           +' case when fn_ot3 >= 1 then '
		   +' fn_effectivehour - fn_ot3 - 7 '
	           +' else 0 end '
                   +' when fc_daytype in ("H+") then '
	           +' case when fn_effectivehour - 7 >= 0 then '
		   +' 7 '
	           +' when fn_effectivehour - 7 <= 0 then '
		   +' fn_effectivehour '
	           +' else 0 end '
                   +' else if(fc_daytype is  null or fc_daytype = "" ,NULL,0) end, '
                   +' fn_ot5 = '
                   +' case when fc_daytype in ("H+") then '
	           +' case when fn_effectivehour - 7 >= 1 then '
		   +' 1 '
	           +' when fn_effectivehour - 7 <= 1 then '
		   +' if( fn_effectivehour - 7 > 0 ,fn_effectivehour - 7,0) '
	           +' else 0 end '
                   +' else if(fc_daytype is  null or fc_daytype = "" ,NULL,0) end , '
                   +' fn_ot6 = '
                   +' case when fc_daytype in ("H+") then '
	           +' case when fn_ot5 >=1 then '
		   +' fn_effectivehour - fn_ot5 - 7 '
	           +' else 0 end '
                   +' else if(fc_daytype is  null or fc_daytype = "" ,NULL,0) end '
                   +' where fc_periode = :periode and fd_date = :date and fc_membercode = :membercode;'
                   //update adjustment yg tipe ='-'
                   +' update '+vTableNameAttendance
                   +' set '
                   +' fn_jumlahshift = (abs(fn_jumlahshift) * -1), '
                   +' fn_ot1 = (abs(fn_ot1) * -1), '
                   +' fn_ot2 = (abs(fn_ot2) * -1), '
                   +' fn_ot3 = (abs(fn_ot3) * -1), '
                   +' fn_ot4 = (abs(fn_ot4) * -1), '
                   +' fn_ot5 = (abs(fn_ot5) * -1), '
                   +' fn_ot6 = (abs(fn_ot6) * -1), '
                   +' fn_totalhour = (abs(fn_totalhour) * -1), '
                   +' fn_effectivehour = (abs(fn_effectivehour) * -1) '
                   +' where fc_periode = :periode and fd_date = :date and fc_membercode = :membercode '
                   +' and fc_attendancetype = "ADJUSTMENT" and fc_adjustmenttype = "-" ; ';
      ParamByName('periode').AsString:=qt_attendance.FieldByName('fc_periode').AsString;
      ParamByName('date').AsDate:=qt_attendance.FieldByName('fd_date').AsDateTime;
      ParamByName('membercode').AsString:=qt_attendance.FieldByName('fc_membercode').AsString;
    Execute;
  end;
  BitBtnRefreshAttendanceClick(Self);
end;

procedure Tf_attendance.UpdateOvertime;
begin
  
  With MyProcessor do begin
    Script.Clear;
    Script.Text:=  //Update Totalhour  & effective Hour time
                    ' update '+vTableNameAttendance +' a '
                    +' set '
                    +' a.fn_totaltimeshift1 = TIME_TO_SEC( '
	            +' if( a.fc_timeinshift1 > a.fc_timeoutshift1 , '
		    +' subtime(addtime(a.fc_timeoutshift1,"24:00"), a.fc_timeinshift1 ) , '
		    +' subtime(a.fc_timeoutshift1, a.fc_timeinshift1 ) '
	            +' ) )/60/60, '
                    +' a.fn_totaltimeshift2 = TIME_TO_SEC( '
	            +' if( a.fc_timeinshift2 > a.fc_timeoutshift2 , '
		    +' subtime(addtime(a.fc_timeoutshift2,"24:00"), a.fc_timeinshift2 ) , '
		    +' subtime(a.fc_timeoutshift2, a.fc_timeinshift2 ) '
	            +' ) )/60/60, '
                    +' a.fn_totaltimeshift3 = TIME_TO_SEC( '
	            +' if( a.fc_timeinshift3 > a.fc_timeoutshift3 , '
		    +' subtime(addtime(a.fc_timeoutshift3,"24:00"), a.fc_timeinshift3 ) , '
		    +' subtime(a.fc_timeoutshift3, a.fc_timeinshift3 ) '
	            +' ) )/60/60, '
                    +' a.fn_totalhour = a.fn_totaltimeshift1 + coalesce(a.fn_totaltimeshift2,0 ) + coalesce(a.fn_totaltimeshift3,0), '
                    +' a.fn_effectivehour = a.fn_totalhour - '   //untuk 5 jam kerja sabtu yg 7 jam kerja th = 8
                       +'case when ( a.fn_totalhour >= ( (2 * if( a.fn_totalhourshift1 = 5 , 8 , a.fn_totalhourshift1 ) ) + 5 ) ) then 3 '
                       +' when ( a.fn_totalhour >= ( if( a.fn_totalhourshift1 < 8 , 8 , a.fn_totalhourshift1 ) + 5 ) ) then 2 '
                       +' when ( a.fn_totalhour >= 5 ) then if(a.fn_totalhourshift1 = 5, if(a.fn_totalhour > 5,1,0) , 1) else 0 end'
                    +' where a.fc_periode = :periode and a.fd_date = :date and a.fc_membercode = :membercode;'
                    +' update ' + vTableNameAttendance +' a '
                    +' set '
                    +' fn_jumlahshift = '                    // untuk mengatasi hari sabtu 7 jamkerja dianggap 8+4 baru menambah 1 shift (excel)  (RECHECK lAGI)
                       +' case when ( a.fn_effectivehour >= ( (2 * if( a.fn_totalhourshift1 = 5 , 7 , a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) ) + 4 ) ) then 3 '
                       +' when ( a.fn_effectivehour >= ( if( a.fn_totalhourshift1 = 5 , 7 , a.fn_totalhourshift1 - a.fn_totalbreakshift1 ) + 4 ) ) then 2 '
                       +' when ( a.fn_effectivehour >= 4   ) then 1 when ( a.fn_effectivehour < 4   ) then 0 else null end'
                    +' where a.fc_periode = :periode and a.fd_date = :date and a.fc_membercode = :membercode;'
                   //update TI1,TI2,TI3
                   +' update ' + vTableNameAttendance
                   +' set '
                   +' fn_effectivehour = '
                   +' case '
	           +' when fc_ti in ("TI1","NR1") then '
		   +' fn_effectivehour + 1 '
	           +' when fc_ti in ("TI2","NR2") then '
		   +' fn_effectivehour + 2 '
	           +' when fc_ti in ("TI3","NR3") then '
		   +' fn_effectivehour + 3 '
	           +' else '
		   +' fn_effectivehour '
                   +' end '
                   +' where fc_periode = :periode and fd_date = :date and fc_membercode = :membercode;'
                    //OT Update
                   +' update '+vTableNameAttendance
                   +' set '
                   +' fn_ot1 = '
                   +' case when fc_daytype = "N" then '
                                                      //untuk 5 jam kerja effective hour 5 jam karena istirahat setelah pulang   (RECHECK lAGI)
	           +' case when fn_effectivehour - ( fn_totalhourshift1 - if(fn_totalhourshift1 > 5,fn_totalbreakshift1,0) ) >= 1 then '
		   +' 1 '
	           +' when fn_effectivehour - (fn_totalhourshift1 - if(fn_totalhourshift1 > 5,fn_totalbreakshift1,0) ) >= 0 then '
		   +' fn_effectivehour - (fn_totalhourshift1 - if(fn_totalhourshift1 > 5,fn_totalbreakshift1,0) ) '
	           +' else 0 end '
                   +' else if(fc_daytype is null or fc_daytype = "" ,NULL,0) end , '
                   +' fn_ot2 = '
                   +' case when fc_daytype = "N" then '
	           +' case when fn_ot1 >= 1 then '
		   +' fn_effectivehour - fn_ot1 - (fn_totalhourshift1 - if(fn_totalhourshift1 > 5,fn_totalbreakshift1,0) ) '
	           +' else 0 end '
                   +' when fc_daytype in ("H","O") then '
	           +' case when fn_effectivehour - 7 >= 0 then '
		   +' 7 '
	           +' when fn_effectivehour - 7 <= 0 then '
		   +' fn_effectivehour '
	           +' else 0 end '
                   +' else if(fc_daytype is  null or fc_daytype = "" ,NULL,0) end , '
                   +' fn_ot3 = '
                   +' case when fc_daytype in ("H","O") then '
	           +' case when fn_effectivehour - 7 >= 1 then '
		   +' 1 '
	           +' when fn_effectivehour - 7 <= 1 then '
		   +' if(fn_effectivehour - 7 > 0, fn_effectivehour - 7 ,0 )  '
	           +' else 0 end '
                   +' else if(fc_daytype is  null or fc_daytype = "" ,NULL,0) end , '
                   +' fn_ot4 = '
                   +' case when fc_daytype in ("H","O") then '
	           +' case when fn_ot3 >= 1 then '
		   +' fn_effectivehour - fn_ot3 - 7 '
	           +' else 0 end '
                   +' when fc_daytype in ("H+") then '
	           +' case when fn_effectivehour - 7 >= 0 then '
		   +' 7 '
	           +' when fn_effectivehour - 7 <= 0 then '
		   +' fn_effectivehour '
	           +' else 0 end '
                   +' else if(fc_daytype is  null or fc_daytype = "" ,NULL,0) end, '
                   +' fn_ot5 = '
                   +' case when fc_daytype in ("H+") then '
	           +' case when fn_effectivehour - 7 >= 1 then '
		   +' 1 '
	           +' when fn_effectivehour - 7 <= 1 then '
		   +' if( fn_effectivehour - 7 > 0 ,fn_effectivehour - 7,0) '
	           +' else 0 end '
                   +' else if(fc_daytype is  null or fc_daytype = "" ,NULL,0) end , '
                   +' fn_ot6 = '
                   +' case when fc_daytype in ("H+") then '
	           +' case when fn_ot5 >=1 then '
		   +' fn_effectivehour - fn_ot5 - 7 '
	           +' else 0 end '
                   +' else if(fc_daytype is  null or fc_daytype = "" ,NULL,0) end '
                   +' where fc_periode = :periode and fd_date = :date and fc_membercode = :membercode;'
                   //update adjustment yg tipe ='-'
                   +' update '+vTableNameAttendance
                   +' set '
                   +' fn_jumlahshift = (abs(fn_jumlahshift) * -1), '
                   +' fn_ot1 = (abs(fn_ot1) * -1), '
                   +' fn_ot2 = (abs(fn_ot2) * -1), '
                   +' fn_ot3 = (abs(fn_ot3) * -1), '
                   +' fn_ot4 = (abs(fn_ot4) * -1), '
                   +' fn_ot5 = (abs(fn_ot5) * -1), '
                   +' fn_ot6 = (abs(fn_ot6) * -1), '
                   +' fn_totalhour = (abs(fn_totalhour) * -1), '
                   +' fn_effectivehour = (abs(fn_effectivehour) * -1) '
                   +' where fc_periode = :periode and fd_date = :date and fc_membercode = :membercode '
                   +' and fc_attendancetype = "ADJUSTMENT" and fc_adjustmenttype = "-" ; ';
      ParamByName('periode').AsString:=qt_attendance.FieldByName('fc_periode').AsString;
      ParamByName('date').AsDate:=qt_attendance.FieldByName('fd_date').AsDateTime;
      ParamByName('membercode').AsString:=qt_attendance.FieldByName('fc_membercode').AsString;
    Execute;
  end;
  BitBtnRefreshAttendanceClick(Self);
end;

procedure Tf_attendance.UpdateAdjType;
begin
  With MyProcessor do begin
    Script.Clear;
    Script.Text:=  //update adjustment yg tipe ='-'
                   ' update '+vTableNameAttendance
                   +' set '
                   +' fn_jumlahshift = (abs(fn_jumlahshift) * -1), '
                   +' fn_ot1 = (abs(fn_ot1) * -1), '
                   +' fn_ot2 = (abs(fn_ot2) * -1), '
                   +' fn_ot3 = (abs(fn_ot3) * -1), '
                   +' fn_ot4 = (abs(fn_ot4) * -1), '
                   +' fn_ot5 = (abs(fn_ot5) * -1), '
                   +' fn_ot6 = (abs(fn_ot6) * -1), '
                   +' fn_totalhour = (abs(fn_totalhour) * -1), '
                   +' fn_effectivehour = (abs(fn_effectivehour) * -1) '
                   +' where fc_periode = :periode and fd_date = :date and fc_membercode = :membercode '
                   +' and fc_attendancetype = "ADJUSTMENT" and fc_adjustmenttype = "-" ; ';
      ParamByName('periode').AsString:=qt_attendance.FieldByName('fc_periode').AsString;
      ParamByName('date').AsDate:=qt_attendance.FieldByName('fd_date').AsDateTime;
      ParamByName('membercode').AsString:=qt_attendance.FieldByName('fc_membercode').AsString;
    Execute;
  end;
  BitBtnRefreshAttendanceClick(Self);
end;


procedure Tf_attendance.BitBtnRefreshAttendanceClick(Sender: TObject);
begin
  DM.RefreshDBGgridWithoutLosingCurRow(DBGridAttendance);
end;

procedure Tf_attendance.BitBtn1Click(Sender: TObject);
begin
  Panel2.Width:=0;
end;

procedure Tf_attendance.BitBtnAddAdjustmentClick(Sender: TObject);
begin
  if PanelAdjustment.Visible = True then begin
     PanelAdjustment.Visible:= False;
  end else if PanelAdjustment.Visible = False then begin
     PanelAdjustment.Visible:=True;
     MemoDescriptionInput.Clear;
     MemoDescriptionInput.SetFocus;
     MemoDescriptionInput.Text:='Adjustment ';
  end;
end;

procedure Tf_attendance.BitBtnFilter1Click(Sender: TObject);
begin
  with qt_attendance do begin
     Close;
     SQL.Clear;
     SQL.Text:= vq_qt_attendance_adjustment ;
     ParamByName('date').AsDate:=DateTimePickerAdjustmentPeriode.Date;
     ParamByName('member').AsString:='%'+EditSearchAttendance1.Text+'%';
     ParamByName('position').AsString:='%'+EditSearchPosition1.Text+'%';
     //if CheckBoxAttendance.Checked then begin
     // ParamByName('active').AsString:='F';
     //end else if CheckBoxAttendance.Checked = False then begin
     // ParamByName('active').AsString:='T'
     //end;
     Open;
  end;
end;

procedure Tf_attendance.BitBtnFilter2Click(Sender: TObject);
begin
  with qt_attendance do begin
     Close;
     SQL.Clear;
     SQL.Text:= vq_qt_attendance_remind ;
     ParamByName('datestart').AsDate:=DateTimePickerStartAttendance.Date;
     ParamByName('datefinish').AsDate:=DateTimePickerFinishAttendance.Date;
     ParamByName('member').AsString:='%'+EditSearchAttendance.Text+'%';
     ParamByName('position').AsString:='%'+EditSearchPosition.Text+'%';
     ParamByName('division').AsString:='%'+EditSearchDivision.Text+'%';
     Open;
  end;
end;

procedure Tf_attendance.BitBtnFilterClick(Sender: TObject);
begin
  with qt_attendance do begin
     Close;
     SQL.Clear;
     SQL.Text:= vq_qt_attendance ;
     ParamByName('datestart').AsDate:=DateTimePickerStartAttendance.Date;
     ParamByName('datefinish').AsDate:=DateTimePickerFinishAttendance.Date;
     ParamByName('member').AsString:='%'+EditSearchAttendance.Text+'%';
     ParamByName('position').AsString:='%'+EditSearchPosition.Text+'%';
     ParamByName('division').AsString:='%'+EditSearchDivision.Text+'%';
     Open;
  end;
end;

procedure Tf_attendance.BitBtnPrintClick(Sender: TObject);
begin
  frReport.LoadFromFile('AttendanceReport.lrf');
  frReport.FindObject('MemoPeriode').Memo.Text:= ': '
  + DM.NamaBulan(DateTimePickerStartAttendance.Date)
  + ' - '
  + dM.NamaBulan(DateTimePickerFinishAttendance.Date) ;
  frReport.ShowReport;
end;

procedure Tf_attendance.BitBtnSaveAdjustmentClick(Sender: TObject);
begin
  with DM.MyBatch do begin
    Script.Clear;
    Script.Text:=' insert into t_attendance '
                +' ( fc_branch,fd_dategenerate,fc_periode, '
                +' fc_membercode,fd_date,fc_daytype,'
                +' fc_shift1,fc_shift2,fc_shift3, '
                +' fc_timeinshift1,fc_timeoutshift1, '
                +' fc_timeinshift2,fc_timeoutshift2, '
                +' fc_timeinshift3,fc_timeoutshift3, '
                +' fc_ti,fv_description,fc_attendancetype )'
                +'values ( :fc_branch,:fd_dategenerate,:fc_periode, '
                +' :fc_membercode,:fd_date,:fc_daytype,'
                +' :fc_shift1,:fc_shift2,:fc_shift3, '
                +' :fc_timeinshift1,:fc_timeoutshift1, '
                +' :fc_timeinshift2,:fc_timeoutshift2, '
                +' :fc_timeinshift3,:fc_timeoutshift3, '
                +' :fc_ti,:fv_description,:fc_attendancetype )';
    ParamByName('fc_branch').AsString:='001';
    ParamByName('fd_dategenerate').AsDateTime:=DM.NowX;
    ParamByName('fc_periode').AsString:=FormatDateTime('YYYYMM',DateTimePickerAdjustmentInput.Date);
    ParamByName('fc_membercode').AsString:=qt_membersearchfc_membercode.AsString;
    ParamByName('fd_date').AsDateTime:=DateTimePickerDate.Date;
    ParamByName('fc_daytype').AsString:=EdtDaytype.Text;
    ParamByName('fc_shift1').AsString:=EdtSHift1.Text;
    ParamByName('fc_shift2').AsString:=EdtSHift2.Text;
    ParamByName('fc_shift3').AsString:=EdtSHift3.Text;
    ParamByName('fc_timeinshift1').AsString:=EdtTimeInShift1.Text;
    ParamByName('fc_timeoutshift1').AsString:=EdtTimeOutShift1.Text;
    ParamByName('fc_timeinshift2').AsString:=EdtTimeInShift2.Text;
    ParamByName('fc_timeoutshift2').AsString:=EdtTimeOutShift2.Text;
    ParamByName('fc_timeinshift3').AsString:=EdtTimeInShift3.Text;
    ParamByName('fc_timeoutshift3').AsString:=EdtTimeOutShift3.Text;
    ParamByName('fc_ti').AsString:=EdtNoRest.Text;
    ParamByName('fv_description').AsString:=MemoDescriptionInput.Text;
    ParamByName('fc_attendancetype').AsString:='ADJUSTMENT';
    Execute;
  end;
  UpdateAdjustmentAttendance(qt_membersearchfc_membercode.AsString,FormatDateTime('YYYYMM',DateTimePickerAdjustmentInput.Date),DateTimePickerDate.Date);   //sudah ada refresh query
  PanelAdjustment.Visible:=False;
  DateTimePickerAdjustmentPeriode.Date:=DateTimePickerAdjustmentInput.Date;
  EditSearchAttendance1.Text:=qt_membersearchfc_membername1.AsString;
  BitBtnFilter1Click(Sender);
  qt_attendance.Locate('fc_membercode;fd_date;fc_periode',
              VarArrayOf([qt_membersearchfc_membercode.AsString,DateTimePickerDate.Date,FormatDateTime('YYYYMM',DateTimePickerAdjustmentInput.Date)])
                ,[]);
end;

procedure Tf_attendance.Button1Click(Sender: TObject);
begin
  PanelAdjustment.Visible:=False;
end;

procedure Tf_attendance.Button2Click(Sender: TObject);
begin
  f_counshiftperday.Show;
end;

procedure Tf_attendance.Button3Click(Sender: TObject);
begin
  f_overtime.Show;
end;

procedure Tf_attendance.Button4Click(Sender: TObject);
begin
  f_leave.Show;
end;

procedure Tf_attendance.Button5Click(Sender: TObject);
begin
  SaveDialog.FileName:='';
  SaveDialog.FileName:='SAVE_ATTENDANCE'+FormatDateTime('yyyymmdd',DM.NowX);
  if SaveDialog.Execute then begin
     if FileExists(SaveDialog.FileName) then DeleteFile(SaveDialog.FileName);

     with DM.MyBatch do begin
       Script.Clear;
       Script.Text:=vq_qt_attendance_exportcsv + ' into outfile '+QuotedStr(AnsiReplaceStr(SaveDialog.FileName,'\','\\'))
                  +' FIELDS TERMINATED BY '+ QuotedStr(';');
     ParamByName('datestart').AsDate:=DateTimePickerStartAttendance.Date;
     ParamByName('datefinish').AsDate:=DateTimePickerFinishAttendance.Date;
     ParamByName('member').AsString:='%'+EditSearchAttendance.Text+'%';
     ParamByName('position').AsString:='%'+EditSearchPosition.Text+'%';
     ParamByName('division').AsString:='%'+EditSearchDivision.Text+'%';
     Execute;
     end;

     MessageDlg('INFORMATION','Berhasil Disimpan',mtInformation,[mbOK],0);
  end;
end;

procedure Tf_attendance.Button6Click(Sender: TObject);
begin
  SaveDialog.FileName:='';
  SaveDialog.FileName:='SAVE_ADJ_ATTENDANCE'+FormatDateTime('yyyymmdd',DM.NowX);
  if SaveDialog.Execute then begin
     if FileExists(SaveDialog.FileName) then DeleteFile(SaveDialog.FileName);

     with DM.MyBatch do begin
       Script.Clear;
       Script.Text:=vq_qt_attendance_adjustment_exportcsv + ' into outfile '+QuotedStr(AnsiReplaceStr(SaveDialog.FileName,'\','\\'))
                  +' FIELDS TERMINATED BY '+ QuotedStr(';');
     ParamByName('date').AsDate:=DateTimePickerAdjustmentPeriode.Date;
     ParamByName('member').AsString:='%'+EditSearchAttendance1.Text+'%';
     ParamByName('position').AsString:='%'+EditSearchPosition1.Text+'%';
     //ParamByName('division').AsString:='%'+EditSearchDivision.Text+'%';
     Execute;
     end;

     MessageDlg('INFORMATION','Berhasil Disimpan',mtInformation,[mbOK],0);
  end;
end;

procedure Tf_attendance.Button7Click(Sender: TObject);
begin
  f_checkattendancesave.Show;
end;

procedure Tf_attendance.Button8Click(Sender: TObject);
begin
  OpenDialog1.FileName:='';
  if OpenDialog1.Execute then begin
    with DM.MyBatch do begin
      try
        Script.Text:=' load data local infile '+QuotedStr(AnsiReplaceStr(OpenDialog1.FileName,'\','\\'))
                  +' REPLACE ' //delete jika sudah ada lalu insert
                  +' into table t_attendance '
                  +' FIELDS TERMINATED BY '+ QuotedStr(';')
                  +' ENCLOSED BY '+QuotedStr('"')
                  +' lines terminated by '+ QuotedStr('\n')
                  +' ;';
        Execute;
        MessageDlg('INFORMATION','Attendance CSV Berhasil Di Import',mtInformation,[mbOK],0);
      except
         on E:Exception do begin
           ShowMessage(E.Message);
         end;
      end;
    end;
  end;
end;

procedure Tf_attendance.CheckBox1Change(Sender: TObject);
begin
  if CheckBox1.Checked then qt_attendancefc_periode.Visible:=True else qt_attendancefc_periode.Visible:=False;
end;

procedure Tf_attendance.CheckBox2Change(Sender: TObject);
begin
  if CheckBox2.Checked then qt_attendancefd_dategenerate.Visible:=True else qt_attendancefd_dategenerate.Visible:=False;
end;

procedure Tf_attendance.ComboBox1Change(Sender: TObject);
begin
  if ComboBox1.ItemIndex = 0 then begin
     PageAttendance.Show;
     Panel1.Caption:='Rapid Attendance';
     qt_attendancefc_adjustmenttype.Visible:=False;
     BitBtnFilterClick(Sender);
  end else if ComboBox1.ItemIndex = 1 then begin
     PageAdjustment.Show;
     Panel1.Caption:='Adjustment Attendance';
     qt_attendancefc_adjustmenttype.Visible:=True;
     BitBtnFilter1Click(Sender);
  end else ShowMessage('Invalid menu');
end;

procedure Tf_attendance.DateTimePicker1Change(Sender: TObject);
begin

  if FormatDateTime('MM',DateTimePicker1.Date) = '01' then begin
     DateTimePickerStartAttendance.Date:=StrToDate('16'+DateSeparator+'12'+DateSeparator+IntToStr( StrToInt( FormatDateTime('YYYY',DateTimePicker1.Date) ) - 1 ) );
     DateTimePickerFinishAttendance.Date:=StrToDate('15'+DateSeparator+'01'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
  end else if FormatDateTime('MM',DateTimePicker1.Date) = '02' then begin
     DateTimePickerStartAttendance.Date:=StrToDate('16'+DateSeparator+'01'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
     DateTimePickerFinishAttendance.Date:=StrToDate('15'+DateSeparator+'02'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
  end else if FormatDateTime('MM',DateTimePicker1.Date) = '03' then begin
     DateTimePickerStartAttendance.Date:=StrToDate('16'+DateSeparator+'02'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
     DateTimePickerFinishAttendance.Date:=StrToDate('15'+DateSeparator+'03'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
  end else if FormatDateTime('MM',DateTimePicker1.Date) = '04' then begin
     DateTimePickerStartAttendance.Date:=StrToDate('16'+DateSeparator+'03'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
     DateTimePickerFinishAttendance.Date:=StrToDate('15'+DateSeparator+'04'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
  end else if FormatDateTime('MM',DateTimePicker1.Date) = '05' then begin
     DateTimePickerStartAttendance.Date:=StrToDate('16'+DateSeparator+'04'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
     DateTimePickerFinishAttendance.Date:=StrToDate('15'+DateSeparator+'05'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
  end else if FormatDateTime('MM',DateTimePicker1.Date) = '06' then begin
     DateTimePickerStartAttendance.Date:=StrToDate('16'+DateSeparator+'05'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
     DateTimePickerFinishAttendance.Date:=StrToDate('15'+DateSeparator+'06'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
  end else if FormatDateTime('MM',DateTimePicker1.Date) = '07' then begin
     DateTimePickerStartAttendance.Date:=StrToDate('16'+DateSeparator+'06'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
     DateTimePickerFinishAttendance.Date:=StrToDate('15'+DateSeparator+'07'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
  end else if FormatDateTime('MM',DateTimePicker1.Date) = '08' then begin
     DateTimePickerStartAttendance.Date:=StrToDate('16'+DateSeparator+'07'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
     DateTimePickerFinishAttendance.Date:=StrToDate('15'+DateSeparator+'08'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
  end else if FormatDateTime('MM',DateTimePicker1.Date) = '09' then begin
     DateTimePickerStartAttendance.Date:=StrToDate('16'+DateSeparator+'08'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
     DateTimePickerFinishAttendance.Date:=StrToDate('15'+DateSeparator+'09'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
  end else if FormatDateTime('MM',DateTimePicker1.Date) = '10' then begin
     DateTimePickerStartAttendance.Date:=StrToDate('16'+DateSeparator+'09'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
     DateTimePickerFinishAttendance.Date:=StrToDate('15'+DateSeparator+'10'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
  end else if FormatDateTime('MM',DateTimePicker1.Date) = '11' then begin
     DateTimePickerStartAttendance.Date:=StrToDate('16'+DateSeparator+'10'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
     DateTimePickerFinishAttendance.Date:=StrToDate('15'+DateSeparator+'11'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
  end else if FormatDateTime('MM',DateTimePicker1.Date) = '12' then begin
     DateTimePickerStartAttendance.Date:=StrToDate('16'+DateSeparator+'11'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
     DateTimePickerFinishAttendance.Date:=StrToDate('15'+DateSeparator+'12'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
  end;

end;

procedure Tf_attendance.DateTimePickerDateChange(Sender: TObject);
var
   q:TZQuery;
begin
  q := TZQuery.Create(Self);
 with q do begin
     Connection:=DM.MyData;
     SQL.Text:='SELECT * FROM t_attendance where fc_membercode = :fc_membercode and fd_date = :fd_date';
     ParamByName('fc_membercode').AsString:=qt_membersearchfc_membercode.AsString;
     ParamByName('fd_date').AsDate:=DateTimePickerDate.Date;
     Open;
     MemoDescriptionInput.Text:=FieldByName('fv_description').AsString;
     EdtSHift1.Text:=Trim(FieldByName('fc_shift1').AsString);
     EdtTimeInShift1.Text:=Trim(FieldByName('fc_timeinshift1').AsString);
     EdtTimeOutShift1.Text:=Trim(FieldByName('fc_timeoutshift1').AsString);
     EdtSHift2.Text:=Trim(FieldByName('fc_shift2').AsString);
     EdtTimeInShift2.Text:=Trim(FieldByName('fc_timeinshift2').AsString);
     EdtTimeOutShift2.Text:=Trim(FieldByName('fc_timeoutshift2').AsString);
     EdtSHift3.Text:=Trim(FieldByName('fc_shift3').AsString);
     EdtTimeInShift3.Text:=Trim(FieldByName('fc_timeinshift3').AsString);
     EdtTimeOutShift3.Text:=Trim(FieldByName('fc_timeoutshift3').AsString);
     EdtDaytype.Text:=Trim(FieldByName('fc_daytype').AsString);
     EdtNoRest.Text:=Trim(FieldByName('fc_ti').AsString);
     Free;
 end;
end;

procedure Tf_attendance.DBGridAttendance1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if qt_attendancefl_isremindagain.AsBoolean then
  DBGridAttendance1.Canvas.Brush.Color:=clYellow;
  DBGridAttendance1.Canvas.Font.Color:=clBlack;
  DBGridAttendance1.Canvas.FillRect(Rect);
  DBGridAttendance1.DefaultDrawColumnCell(Rect, DataCol, Column, State);
end;

procedure Tf_attendance.DBGridAttendanceColEnter(Sender: TObject);
begin
  curRecNo:= DBGridAttendance.DataSource.DataSet.RecNo;
  curField := DBGridAttendance.SelectedField;
end;

procedure Tf_attendance.DBGridAttendanceColExit(Sender: TObject);
begin
   qt_attendance.ApplyUpdates;
   PanelAdjustment.Visible:=false;
end;

procedure Tf_attendance.DBGridAttendanceDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if qt_attendancefl_isremindagain.AsBoolean then
  DBGridAttendance.Canvas.Brush.Color:=clYellow;
  DBGridAttendance.Canvas.Font.Color:=clBlack;
  DBGridAttendance.Canvas.FillRect(Rect);
  DBGridAttendance.DefaultDrawColumnCell(Rect, DataCol, Column, State);
end;

procedure Tf_attendance.ds_membersearchDataChange(Sender: TObject; Field: TField
  );
begin
  DateTimePickerDateChange(Sender);
end;

procedure Tf_attendance.EditFixColumn1Change(Sender: TObject);
begin
  if StrToInt(EditFixColumn1.Text) > DBGridAttendance1.Columns.Count + 1  then begin
     EditFixColumn1.Text := IntToStr(DBGridAttendance1.Columns.Count + 1) ;
  end else if StrToInt(EditFixColumn.Text) < 1 then begin
     EditFixColumn1.Text := '1';
  end;
  DBGridAttendance.FixedCols:= StrToInt(EditFixColumn.Text);
end;

procedure Tf_attendance.EditFixColumnChange(Sender: TObject);
begin
  if StrToInt(EditFixColumn.Text) > DBGridAttendance.Columns.Count + 1  then begin
     EditFixColumn.Text := IntToStr(DBGridAttendance.Columns.Count + 1) ;
  end else if StrToInt(EditFixColumn.Text) < 1 then begin
     EditFixColumn.Text := '1';
  end;
  DBGridAttendance.FixedCols:= StrToInt(EditFixColumn.Text);
end;

procedure Tf_attendance.EditSeacrhEmpKeyPress(Sender: TObject; var Key: char);
begin
  if Key = #13 then begin
     with qt_membersearch do begin
       Close;
       ParamByName('search').AsString:='%'+EditSeacrhEmp.Text+'%';
       Open;
     end;
  end;
end;

procedure Tf_attendance.EditSearchAttendance1KeyPress(Sender: TObject;
  var Key: char);
begin
  if Key= chr(13) then BitBtnFilter1Click(Sender);
end;

procedure Tf_attendance.EditSearchAttendanceKeyPress(Sender: TObject;
  var Key: char);
begin
  if Key= chr(13) then BitBtnFilterClick(Sender);
end;

procedure Tf_attendance.EditSearchPosition1KeyPress(Sender: TObject;
  var Key: char);
begin
  if Key= chr(13) then BitBtnFilter1Click(Sender);
end;

procedure Tf_attendance.EditSearchPositionKeyPress(Sender: TObject;
  var Key: char);
begin
  if Key= chr(13) then BitBtnFilterClick(Sender);
end;

procedure Tf_attendance.EdtSHift1Change(Sender: TObject);
var
   q: TZQuery;
begin
 q := TZQuery.Create(Self);
 with q do begin
     Connection:=DM.MyData;
     SQL.Text:='SELECT * FROM t_dailytimerange where fc_timerangecode = :shift';
     ParamByName('shift').AsString:=EdtSHift1.Text;
     Open;
     EdtTimeInShift1.Text:=FieldByName('fc_workin').AsString;
     EdtTimeOutShift1.Text:=FieldByName('fc_workout').AsString;
     Free;
 end;
 if EdtSHift1.Text='' then begin
   EdtSHift2.Enabled:=False;EdtTimeInShift2.Enabled:=False;EdtTimeOutShift2.Enabled:=False;
   EdtSHift2.Text:='';EdtTimeInShift2.Text:='';EdtTimeOutShift2.Text:='';
 end else begin
   EdtSHift2.Enabled:=True;EdtTimeInShift2.Enabled:=True;EdtTimeOutShift2.Enabled:=True;
 end;
end;

procedure Tf_attendance.EdtShift2Change(Sender: TObject);
var
   q: TZQuery;
begin
 q := TZQuery.Create(Self);
 with q do begin
     Connection:=DM.MyData;
     SQL.Text:='SELECT * FROM t_dailytimerange where fc_timerangecode = :shift';
     ParamByName('shift').AsString:=EdtSHift2.Text;
     Open;
     EdtTimeInShift2.Text:=FieldByName('fc_workin').AsString;
     EdtTimeOutShift2.Text:=FieldByName('fc_workout').AsString;
     Free;
 end;

 if EdtSHift2.Text='' then begin
   EdtSHift3.Enabled:=False;EdtTimeInShift3.Enabled:=False;EdtTimeOutShift3.Enabled:=False;
   EdtSHift3.Text:='';EdtTimeInShift3.Text:='';EdtTimeOutShift3.Text:='';
 end else begin
   EdtSHift3.Enabled:=True;EdtTimeInShift3.Enabled:=True;EdtTimeOutShift3.Enabled:=True;
 end;

end;

procedure Tf_attendance.EdtShift3Change(Sender: TObject);
var
   q: TZQuery;
begin
 q := TZQuery.Create(Self);
 with q do begin
     Connection:=DM.MyData;
     SQL.Text:='SELECT * FROM t_dailytimerange where fc_timerangecode = :shift';
     ParamByName('shift').AsString:=EdtSHift3.Text;
     Open;
     EdtTimeInShift3.Text:=FieldByName('fc_workin').AsString;
     EdtTimeOutShift3.Text:=FieldByName('fc_workout').AsString;
     Free;
 end;

end;

procedure Tf_attendance.FormCreate(Sender: TObject);
begin
  vTableNameAttendance := 't_attendance';
  SetUpGridPickList('fc_daytype','select fc_trxcode from t_trxtype where fc_trxid = "DAY_TYPE" order by fn_trxnumber',DBGridAttendance);
  SetUpGridPickList('fc_ti','select fc_trxcode from t_trxtype where fc_trxid = "NOREST_TYPE" order by fn_trxnumber',DBGridAttendance);
  SetUpGridPickList('fc_daytype','select fc_trxcode from t_trxtype where fc_trxid = "DAY_TYPE" order by fn_trxnumber',DBGridAttendance1);
  SetUpGridPickList('fc_ti','select fc_trxcode from t_trxtype where fc_trxid = "NOREST_TYPE" order by fn_trxnumber',DBGridAttendance1);
  DateTimePicker1.Date:=DM.NowX;
  DateTimePickerAdjustmentPeriode.Date:=DM.NowX;
  DateTimePickerStartAttendance.Date:=DM.NowX;
  DateTimePickerFinishAttendance.Date:=DM.NowX;
  EditFixColumn.Text:=inttostr(DBGridAttendance.FixedCols);

  CheckBox1Change(Sender);
  CheckBox2Change(Sender);
end;

procedure Tf_attendance.frReportEnterRect(MemoDescription: TStringList; View: TfrView);
begin
  if frReport.FindObject('Memodaytype').Memo.Text = 'N' then begin
      if view.Name='MemoDate' then
      view.FillColor:=clRed;
  end;
end;

procedure Tf_attendance.qt_attendancefc_adjustmenttypeChange(Sender: TField);
begin
  qt_attendance.ApplyUpdates;
  UpdateAdjustmentAttendance(
   qt_attendancefc_membercode.AsString,
   qt_attendancefc_periode.AsString,
   qt_attendancefd_date.AsDateTime
  );
  BitBtnRefreshAttendanceClick(Self);
end;

end.

