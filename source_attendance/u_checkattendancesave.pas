unit u_checkattendancesave;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, StdCtrls,
  DBGrids, Buttons, ComCtrls, DBCtrls, ZDataset, DB, DateTimePicker,StrUtils;

const
  vq_qt_attendance = ' select '+
                     ' a.fc_branch,a.fc_periode ,a.fd_dategenerate , ' +
                     ' a.fc_membercode,b.fc_membername1, c.fv_trxdescription fc_memberposition, '+
                     ' a.fd_date ,a.fc_daytype , a.fc_shift1 ,a.fc_shift2,a.fc_shift3, '+
                     ' a.fc_timeinshift1,a.fc_timeoutshift1 , '+
                     ' a.fc_timeinshift2,a.fc_timeoutshift2 , '+
                     ' a.fc_timeinshift3,a.fc_timeoutshift3 , '+
                     ' a.fn_jumlahshift , '+
                     ' a.fd_realdatein ,a.fd_realdateout, '+
                     ' a.fn_totalhour ,a.fn_effectivehour, '+
                     ' a.fn_ot1 ,a.fn_ot2 ,a.fn_ot3 ,a.fn_ot4 ,a.fn_ot5 ,a.fn_ot6, '+
                     ' a.fc_ti, a.fv_description, a.fl_ischecked1 , a.fl_ischeked2, a.fl_isremindagain,a.fc_adjustmenttype '+
                     ' from t_attendance_save a '+
                     ' left join t_member b on a.fc_membercode = b.fc_membercode '+
                     ' left join t_trxtype c on b.fc_memberposition = c.fc_trxcode and c.fc_trxid = "EMP_POSITION" '+
                     ' left join t_division d on b.fc_divisioncode = d.fc_divisioncode '+
                     ' where '+
                     ' a.fc_membercode like :member and c.fv_trxdescription like :position and d.fc_divisionname like :division and a.fd_date >= :datestart and a.fd_date <= :datefinish '+
                     ' or '+
                     ' b.fc_membername1 like :member and c.fv_trxdescription like :position and d.fc_divisionname like :division and a.fd_date >= :datestart and a.fd_date <= :datefinish '+
                     ' order by fc_memberposition,a.fc_membercode ,c.fv_trxdescription, a.fc_periode ,a.fd_date ';
  vq_qt_attendance_adjustment = ' select '+
                     ' a.fc_branch,a.fc_periode ,a.fd_dategenerate , ' +
                     ' a.fc_membercode,b.fc_membername1, c.fv_trxdescription fc_memberposition, '+
                     ' a.fd_date ,a.fc_daytype , a.fc_shift1 ,a.fc_shift2,a.fc_shift3, '+
                     ' a.fc_timeinshift1,a.fc_timeoutshift1 , '+
                     ' a.fc_timeinshift2,a.fc_timeoutshift2 , '+
                     ' a.fc_timeinshift3,a.fc_timeoutshift3 , '+
                     ' a.fn_jumlahshift , '+
                     ' a.fd_realdatein ,a.fd_realdateout, '+
                     ' a.fn_totalhour ,a.fn_effectivehour, '+
                     ' a.fn_ot1 ,a.fn_ot2 ,a.fn_ot3 ,a.fn_ot4 ,a.fn_ot5 ,a.fn_ot6, '+
                     ' a.fc_ti, a.fv_description, a.fl_ischecked1 , a.fl_ischeked2, a.fl_isremindagain,a.fc_adjustmenttype '+
                     ' from t_attendance_save a '+
                     ' left join t_member b on a.fc_membercode = b.fc_membercode '+
                     ' left join t_trxtype c on b.fc_memberposition = c.fc_trxcode and c.fc_trxid = "EMP_POSITION" '+
                     ' where '+
                     ' a.fc_membercode like :member and c.fv_trxdescription like :position and a.fc_periode = date_format(:date,"%Y%m") and a.fc_attendancetype = "ADJUSTMENT" '+
                     ' or '+
                     ' b.fc_membername1 like :member and c.fv_trxdescription like :position and a.fc_periode = date_format(:date,"%Y%m") and a.fc_attendancetype = "ADJUSTMENT" '+
                     ' order by fc_memberposition,a.fc_membercode ,c.fv_trxdescription, a.fc_periode ,a.fd_date ';

type

  { Tf_checkattendancesave }

  Tf_checkattendancesave = class(TForm)
    BitBtnFilter: TBitBtn;
    BitBtnFilter1: TBitBtn;
    BitBtnPrint: TBitBtn;
    BitBtnRefreshAttendance: TBitBtn;
    BitBtnRefreshAttendance1: TBitBtn;
    Button1: TButton;
    Button5: TButton;
    Button7: TButton;
    CheckBoxAttendanceFilter1: TCheckBox;
    ComboBox1: TComboBox;
    ds_attendance_save: TDataSource;
    DateTimePicker1: TDateTimePicker;
    DateTimePickerAdjustmentPeriode: TDateTimePicker;
    DateTimePickerFinishAttendance: TDateTimePicker;
    DateTimePickerStartAttendance: TDateTimePicker;
    DBGridAttendance: TDBGrid;
    DBGridAttendance1: TDBGrid;
    EditSearchAttendance: TEdit;
    EditSearchAttendance1: TEdit;
    EditSearchDivision: TEdit;
    EditSearchPosition: TEdit;
    EditSearchPosition1: TEdit;
    Label11: TLabel;
    Label12: TLabel;
    Label15: TLabel;
    Label26: TLabel;
    Label36: TLabel;
    Label37: TLabel;
    Label38: TLabel;
    Label5: TLabel;
    Notebook1: TNotebook;
    OpenDialog1: TOpenDialog;
    PageAdjustment: TPage;
    PageAttendance: TPage;
    Panel1: TPanel;
    Panel10: TPanel;
    Panel14: TPanel;
    Panel15: TPanel;
    Panel9: TPanel;
    qt_attendance_save: TZQuery;
    qt_attendance_savefc_adjustmenttype: TStringField;
    qt_attendance_savefc_branch: TStringField;
    qt_attendance_savefc_daytype: TStringField;
    qt_attendance_savefc_membercode: TStringField;
    qt_attendance_savefc_membername1: TStringField;
    qt_attendance_savefc_memberposition: TStringField;
    qt_attendance_savefc_periode: TStringField;
    qt_attendance_savefc_shift1: TStringField;
    qt_attendance_savefc_shift2: TStringField;
    qt_attendance_savefc_shift3: TStringField;
    qt_attendance_savefc_ti: TStringField;
    qt_attendance_savefc_timeinshift1: TStringField;
    qt_attendance_savefc_timeinshift2: TStringField;
    qt_attendance_savefc_timeinshift3: TStringField;
    qt_attendance_savefc_timeoutshift1: TStringField;
    qt_attendance_savefc_timeoutshift2: TStringField;
    qt_attendance_savefc_timeoutshift3: TStringField;
    qt_attendance_savefd_date: TDateField;
    qt_attendance_savefd_dategenerate: TDateTimeField;
    qt_attendance_savefd_realdatein: TDateTimeField;
    qt_attendance_savefd_realdateout: TDateTimeField;
    qt_attendance_savefl_ischecked1: TSmallintField;
    qt_attendance_savefl_ischeked2: TSmallintField;
    qt_attendance_savefl_isremindagain: TSmallintField;
    qt_attendance_savefn_effectivehour: TFloatField;
    qt_attendance_savefn_jumlahshift: TLongintField;
    qt_attendance_savefn_ot1: TFloatField;
    qt_attendance_savefn_ot2: TFloatField;
    qt_attendance_savefn_ot3: TFloatField;
    qt_attendance_savefn_ot4: TFloatField;
    qt_attendance_savefn_ot5: TFloatField;
    qt_attendance_savefn_ot6: TFloatField;
    qt_attendance_savefn_totalhour: TFloatField;
    qt_attendance_savefv_description: TStringField;
    procedure BitBtnFilter1Click(Sender: TObject);
    procedure BitBtnFilterClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure DateTimePicker1Change(Sender: TObject);
    procedure EditSearchAttendance1KeyPress(Sender: TObject; var Key: char);
    procedure EditSearchAttendanceKeyPress(Sender: TObject; var Key: char);
    procedure EditSearchDivisionKeyPress(Sender: TObject; var Key: char);
    procedure EditSearchPosition1KeyPress(Sender: TObject; var Key: char);
    procedure EditSearchPositionKeyPress(Sender: TObject; var Key: char);
    procedure FormCreate(Sender: TObject);
  private

  public

  end;

var
  f_checkattendancesave: Tf_checkattendancesave;

implementation
uses
  u_datamodule;

{$R *.lfm}

{ Tf_checkattendancesave }

procedure Tf_checkattendancesave.Button5Click(Sender: TObject);
begin
  OpenDialog1.FileName:='';
  if OpenDialog1.Execute then begin
    with DM.MyBatch do begin
      try
        Script.Text:=' load data local infile '+QuotedStr(AnsiReplaceStr(OpenDialog1.FileName,'\','\\'))
                  +' REPLACE ' //delete jika sudah ada lalu insert
                  +' into table t_attendance_save '
                  +' FIELDS TERMINATED BY '+ QuotedStr(';')
                  +' ENCLOSED BY '+QuotedStr('"')
                  +' lines terminated by '+ QuotedStr('\n')
                  +' ;';
        Execute;
        MessageDlg('INFORMATION','Rapid Attendance Berhasil Di Import',mtInformation,[mbOK],0);
      except
         on E:Exception do begin
           ShowMessage(E.Message);
         end;
      end;
    end;
  end;
end;

procedure Tf_checkattendancesave.Button7Click(Sender: TObject);
begin
  OpenDialog1.FileName:='';
  if OpenDialog1.Execute then begin
    with DM.MyBatch do begin
      try
        Script.Text:=' load data local infile '+QuotedStr(AnsiReplaceStr(OpenDialog1.FileName,'\','\\'))
                  +' REPLACE ' //delete jika sudah ada lalu insert
                  +' into table t_attendance_save '
                  +' FIELDS TERMINATED BY '+ QuotedStr(';')
                  +' ENCLOSED BY '+QuotedStr('"')
                  +' lines terminated by '+ QuotedStr('\n')
                  +' ;';
        Execute;
        MessageDlg('INFORMATION','Adjustment Attendance Berhasil Di Import',mtInformation,[mbOK],0);
      except
         on E:Exception do begin
           ShowMessage(E.Message);
         end;
      end;
    end;
  end;
end;

procedure Tf_checkattendancesave.ComboBox1Change(Sender: TObject);
begin
  if ComboBox1.ItemIndex = 0 then begin
     PageAttendance.Show;
     Panel1.Caption:='Check Saved Rapid Attendance';
     qt_attendance_savefc_adjustmenttype.Visible:=False;
     BitBtnFilterClick(Sender);
  end else if ComboBox1.ItemIndex = 1 then begin
     PageAdjustment.Show;
     Panel1.Caption:='Check Saved Adjustment Attendance';
     qt_attendance_savefc_adjustmenttype.Visible:=True;
     BitBtnFilter1Click(Sender);
  end else ShowMessage('Invalid menu');
end;

procedure Tf_checkattendancesave.DateTimePicker1Change(Sender: TObject);
begin
  
  if FormatDateTime('MM',DateTimePicker1.Date) = '01' then begin
     DateTimePickerStartAttendance.Date:=StrToDate('16'+DateSeparator+'12'+DateSeparator+IntToStr( StrToInt( FormatDateTime('YYYY',DateTimePicker1.Date) ) - 1 ) );
     DateTimePickerFinishAttendance.Date:=StrToDate('15'+DateSeparator+'01'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
  end else if FormatDateTime('MM',DateTimePicker1.Date) = '02' then begin
     DateTimePickerStartAttendance.Date:=StrToDate('16'+DateSeparator+'01'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
     DateTimePickerFinishAttendance.Date:=StrToDate('15'+DateSeparator+'02'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
  end else if FormatDateTime('MM',DateTimePicker1.Date) = '03' then begin
     DateTimePickerStartAttendance.Date:=StrToDate('16'+DateSeparator+'02'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
     DateTimePickerFinishAttendance.Date:=StrToDate('15'+DateSeparator+'03'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
  end else if FormatDateTime('MM',DateTimePicker1.Date) = '04' then begin
     DateTimePickerStartAttendance.Date:=StrToDate('16'+DateSeparator+'03'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
     DateTimePickerFinishAttendance.Date:=StrToDate('15'+DateSeparator+'04'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
  end else if FormatDateTime('MM',DateTimePicker1.Date) = '05' then begin
     DateTimePickerStartAttendance.Date:=StrToDate('16'+DateSeparator+'04'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
     DateTimePickerFinishAttendance.Date:=StrToDate('15'+DateSeparator+'05'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
  end else if FormatDateTime('MM',DateTimePicker1.Date) = '06' then begin
     DateTimePickerStartAttendance.Date:=StrToDate('16'+DateSeparator+'05'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
     DateTimePickerFinishAttendance.Date:=StrToDate('15'+DateSeparator+'06'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
  end else if FormatDateTime('MM',DateTimePicker1.Date) = '07' then begin
     DateTimePickerStartAttendance.Date:=StrToDate('16'+DateSeparator+'06'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
     DateTimePickerFinishAttendance.Date:=StrToDate('15'+DateSeparator+'07'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
  end else if FormatDateTime('MM',DateTimePicker1.Date) = '08' then begin
     DateTimePickerStartAttendance.Date:=StrToDate('16'+DateSeparator+'07'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
     DateTimePickerFinishAttendance.Date:=StrToDate('15'+DateSeparator+'08'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
  end else if FormatDateTime('MM',DateTimePicker1.Date) = '09' then begin
     DateTimePickerStartAttendance.Date:=StrToDate('16'+DateSeparator+'08'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
     DateTimePickerFinishAttendance.Date:=StrToDate('15'+DateSeparator+'09'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
  end else if FormatDateTime('MM',DateTimePicker1.Date) = '10' then begin
     DateTimePickerStartAttendance.Date:=StrToDate('16'+DateSeparator+'09'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
     DateTimePickerFinishAttendance.Date:=StrToDate('15'+DateSeparator+'10'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
  end else if FormatDateTime('MM',DateTimePicker1.Date) = '11' then begin
     DateTimePickerStartAttendance.Date:=StrToDate('16'+DateSeparator+'10'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
     DateTimePickerFinishAttendance.Date:=StrToDate('15'+DateSeparator+'11'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
  end else if FormatDateTime('MM',DateTimePicker1.Date) = '12' then begin
     DateTimePickerStartAttendance.Date:=StrToDate('16'+DateSeparator+'11'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
     DateTimePickerFinishAttendance.Date:=StrToDate('15'+DateSeparator+'12'+DateSeparator+FormatDateTime('YYYY',DateTimePicker1.Date) );
  end;
end;

procedure Tf_checkattendancesave.EditSearchAttendance1KeyPress(Sender: TObject;
  var Key: char);
begin
  if Key= chr(13) then BitBtnFilter1Click(Sender);
end;

procedure Tf_checkattendancesave.EditSearchAttendanceKeyPress(Sender: TObject;
  var Key: char);
begin
  if Key= chr(13) then BitBtnFilterClick(Sender);
end;

procedure Tf_checkattendancesave.EditSearchDivisionKeyPress(Sender: TObject;
  var Key: char);
begin
  if Key= chr(13) then BitBtnFilterClick(Sender);
end;

procedure Tf_checkattendancesave.EditSearchPosition1KeyPress(Sender: TObject;
  var Key: char);
begin
  if Key= chr(13) then BitBtnFilter1Click(Sender);
end;

procedure Tf_checkattendancesave.EditSearchPositionKeyPress(Sender: TObject;
  var Key: char);
begin
  if Key= chr(13) then BitBtnFilterClick(Sender);
end;

procedure Tf_checkattendancesave.FormCreate(Sender: TObject);
begin
  DateTimePicker1.Date:=DM.NowX;
  DateTimePickerAdjustmentPeriode.Date:=DM.NowX;
  DateTimePickerStartAttendance.Date:=DM.NowX;
  DateTimePickerFinishAttendance.Date:=DM.NowX;
end;

procedure Tf_checkattendancesave.BitBtnFilterClick(Sender: TObject);
begin
  with qt_attendance_save do begin
     Close;
     SQL.Clear;
     SQL.Text:= vq_qt_attendance ;
     ParamByName('datestart').AsDate:=DateTimePickerStartAttendance.Date;
     ParamByName('datefinish').AsDate:=DateTimePickerFinishAttendance.Date;
     ParamByName('member').AsString:='%'+EditSearchAttendance.Text+'%';
     ParamByName('position').AsString:='%'+EditSearchPosition.Text+'%';
     ParamByName('division').AsString:='%'+EditSearchDivision.Text+'%';
     Open;
  end;
end;

procedure Tf_checkattendancesave.Button1Click(Sender: TObject);
begin
  if MessageDlg('CONFIRMATION','Yakin Untuk Mmbersihkan Table Temporary',mtConfirmation,mbYesNo,0) = mrYes then begin
    with DM.MyBatch do begin
      Clear;
      Script.Text :=' delete from t_attendance_save ';
      Execute;
      MessageDlg('INFORMATION','Temporary Table Berhasil Di bersihkan',mtInformation,[mbYes],0);
    end;
  end;
end;

procedure Tf_checkattendancesave.BitBtnFilter1Click(Sender: TObject);
begin
  with qt_attendance_save do begin
     Close;
     SQL.Clear;
     SQL.Text:= vq_qt_attendance_adjustment ;
     ParamByName('date').AsDate:=DateTimePickerAdjustmentPeriode.Date;
     ParamByName('member').AsString:='%'+EditSearchAttendance1.Text+'%';
     ParamByName('position').AsString:='%'+EditSearchPosition1.Text+'%';
     Open;
  end;
end;

end.

